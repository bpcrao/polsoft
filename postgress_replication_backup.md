#Backup strategies

##Strategy SQL DUMP
PG_DUMP command every day midnight as windows scheduler job 
db_backup.bat file as Admin and C:\MOUNT as target directory

##File System Level Backup
tar -cf backup.tar /usr/local/pgsql/data
For this we need to stop the DB the solution is to use replication, stop the slave DB and copy its dump

##WAL this needs to be enabled in configuration ( Write Ahead Logs )
C:\Program Files\PostgreSQL\9.6\data\pg_xlog
this is inbuilt and we just need to enable it by 

* Enable archiving (in postgresql.conf file)
* Archive_command: just copy to a safe mounted location
# - Archiving -

```
wal_level = hot_standby	 

archive_mode = on		# enables archiving; off, on, or always
				# (change requires restart)
archive_command = 'copy %p  \\\DESKTOP-CVVG27U\\mount\\%f'		# command to use to archive a logfile segment
				# placeholders: %p = path of file to archive
				#               %f = file name only
				# e.g. 'test ! -f /mnt/server/archivedir/%f && cp %p /mnt/server/archivedir/%f'
#archive_timeout = 0		# force a logfile segment switch after this
				# number of seconds; 0 disables
```


# Replication Strategy 

Though we do not have a requirement for hot_standby, this is also useful for keeping a standby for the db
https://hub.docker.com/r/bitnami/postgresql/

