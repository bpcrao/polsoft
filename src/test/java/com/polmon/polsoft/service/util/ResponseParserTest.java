/**
 * 
 */
package com.polmon.polsoft.service.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

/**
 * @author admin
 *
 */
public class ResponseParserTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testParser() throws JSONException {
		String stringData[] = {"{",
"\"Start Address is\":\"~011035\",",
"\"Date Is \":\"26/01/17\",",
"\"Time Is \":\"09:18:14\",",
"\"Data Is \":[+002.565 -001.456 -000.0 +0.67 null",
"]",
"}"};
		
		List strData = Arrays.asList(stringData);
		ResponseParser rp = new ResponseParser();
		rp.parseData(strData);
		
}

	
}
