//package com.polmon.polsoft.web.rest;
//
//import com.polmon.polsoft.PolsoftApp;
//import com.polmon.polsoft.domain.Dchannel;
//import com.polmon.polsoft.repository.DchannelRepository;
//import com.polmon.polsoft.service.DchannelService;
//import com.polmon.polsoft.repository.search.DchannelSearchRepository;
//import com.polmon.polsoft.service.dto.DchannelDTO;
//import com.polmon.polsoft.service.mapper.DchannelMapper;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import static org.hamcrest.Matchers.hasItem;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.PostConstruct;
//import javax.inject.Inject;
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Test class for the DchannelResource REST controller.
// *
// * @see DchannelResource
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = PolsoftApp.class)
//public class DchannelResourceIntTest {
//    private static final String DEFAULT_NAME = "AAAAA";
//    private static final String UPDATED_NAME = "BBBBB";
//    private static final String DEFAULT_UNIT = "AAAAA";
//    private static final String UPDATED_UNIT = "BBBBB";
//    private static final String DEFAULT_CHANNELDATA = "AAAAA";
//    private static final String UPDATED_CHANNELDATA = "BBBBB";
//
//    @Inject
//    private DchannelRepository dchannelRepository;
//
//    @Inject
//    private DchannelMapper dchannelMapper;
//
//    @Inject
//    private DchannelService dchannelService;
//
//    @Inject
//    private DchannelSearchRepository dchannelSearchRepository;
//
//    @Inject
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Inject
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Inject
//    private EntityManager em;
//
//    private MockMvc restDchannelMockMvc;
//
//    private Dchannel dchannel;
//
//    @PostConstruct
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        DchannelResource dchannelResource = new DchannelResource();
//        ReflectionTestUtils.setField(dchannelResource, "dchannelService", dchannelService);
//        this.restDchannelMockMvc = MockMvcBuilders.standaloneSetup(dchannelResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setMessageConverters(jacksonMessageConverter).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Dchannel createEntity(EntityManager em) {
//        Dchannel dchannel = new Dchannel();
//        dchannel = new Dchannel()
//                .name(DEFAULT_NAME)
//                .unit(DEFAULT_UNIT)
//                .channeldata(DEFAULT_CHANNELDATA);
//        return dchannel;
//    }
//
//    @Before
//    public void initTest() {
//        dchannelSearchRepository.deleteAll();
//        dchannel = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createDchannel() throws Exception {
//        int databaseSizeBeforeCreate = dchannelRepository.findAll().size();
//
//        // Create the Dchannel
//        DchannelDTO dchannelDTO = dchannelMapper.dchannelToDchannelDTO(dchannel);
//
//        restDchannelMockMvc.perform(post("/api/dchannels")
//                .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(dchannelDTO)))
//                .andExpect(status().isCreated());
//
//        // Validate the Dchannel in the database
//        List<Dchannel> dchannels = dchannelRepository.findAll();
//        assertThat(dchannels).hasSize(databaseSizeBeforeCreate + 1);
//        Dchannel testDchannel = dchannels.get(dchannels.size() - 1);
//        assertThat(testDchannel.getName()).isEqualTo(DEFAULT_NAME);
//        assertThat(testDchannel.getUnit()).isEqualTo(DEFAULT_UNIT);
//        assertThat(testDchannel.getChanneldata()).isEqualTo(DEFAULT_CHANNELDATA);
//
//        // Validate the Dchannel in ElasticSearch
//        Dchannel dchannelEs = dchannelSearchRepository.findOne(testDchannel.getId());
//        assertThat(dchannelEs).isEqualToComparingFieldByField(testDchannel);
//    }
//
//    @Test
//    @Transactional
//    public void getAllDchannels() throws Exception {
//        // Initialize the database
//        dchannelRepository.saveAndFlush(dchannel);
//
//        // Get all the dchannels
//        restDchannelMockMvc.perform(get("/api/dchannels?sort=id,desc"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//                .andExpect(jsonPath("$.[*].id").value(hasItem(dchannel.getId().intValue())))
//                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
//                .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT.toString())))
//                .andExpect(jsonPath("$.[*].channeldata").value(hasItem(DEFAULT_CHANNELDATA.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getDchannel() throws Exception {
//        // Initialize the database
//        dchannelRepository.saveAndFlush(dchannel);
//
//        // Get the dchannel
//        restDchannelMockMvc.perform(get("/api/dchannels/{id}", dchannel.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(dchannel.getId().intValue()))
//            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
//            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT.toString()))
//            .andExpect(jsonPath("$.channeldata").value(DEFAULT_CHANNELDATA.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingDchannel() throws Exception {
//        // Get the dchannel
//        restDchannelMockMvc.perform(get("/api/dchannels/{id}", Long.MAX_VALUE))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateDchannel() throws Exception {
//        // Initialize the database
//        dchannelRepository.saveAndFlush(dchannel);
//        dchannelSearchRepository.save(dchannel);
//        int databaseSizeBeforeUpdate = dchannelRepository.findAll().size();
//
//        // Update the dchannel
//        Dchannel updatedDchannel = dchannelRepository.findOne(dchannel.getId());
//        updatedDchannel
//                .name(UPDATED_NAME)
//                .unit(UPDATED_UNIT)
//                .channeldata(UPDATED_CHANNELDATA);
//        DchannelDTO dchannelDTO = dchannelMapper.dchannelToDchannelDTO(updatedDchannel);
//
//        restDchannelMockMvc.perform(put("/api/dchannels")
//                .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(dchannelDTO)))
//                .andExpect(status().isOk());
//
//        // Validate the Dchannel in the database
//        List<Dchannel> dchannels = dchannelRepository.findAll();
//        assertThat(dchannels).hasSize(databaseSizeBeforeUpdate);
//        Dchannel testDchannel = dchannels.get(dchannels.size() - 1);
//        assertThat(testDchannel.getName()).isEqualTo(UPDATED_NAME);
//        assertThat(testDchannel.getUnit()).isEqualTo(UPDATED_UNIT);
//        assertThat(testDchannel.getChanneldata()).isEqualTo(UPDATED_CHANNELDATA);
//
//        // Validate the Dchannel in ElasticSearch
//        Dchannel dchannelEs = dchannelSearchRepository.findOne(testDchannel.getId());
//        assertThat(dchannelEs).isEqualToComparingFieldByField(testDchannel);
//    }
//
//    @Test
//    @Transactional
//    public void deleteDchannel() throws Exception {
//        // Initialize the database
//        dchannelRepository.saveAndFlush(dchannel);
//        dchannelSearchRepository.save(dchannel);
//        int databaseSizeBeforeDelete = dchannelRepository.findAll().size();
//
//        // Get the dchannel
//        restDchannelMockMvc.perform(delete("/api/dchannels/{id}", dchannel.getId())
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk());
//
//        // Validate ElasticSearch is empty
//        boolean dchannelExistsInEs = dchannelSearchRepository.exists(dchannel.getId());
//        assertThat(dchannelExistsInEs).isFalse();
//
//        // Validate the database is empty
//        List<Dchannel> dchannels = dchannelRepository.findAll();
//        assertThat(dchannels).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void searchDchannel() throws Exception {
//        // Initialize the database
//        dchannelRepository.saveAndFlush(dchannel);
//        dchannelSearchRepository.save(dchannel);
//
//        // Search the dchannel
//        restDchannelMockMvc.perform(get("/api/_search/dchannels?query=id:" + dchannel.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(dchannel.getId().intValue())))
//            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
//            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT.toString())))
//            .andExpect(jsonPath("$.[*].channeldata").value(hasItem(DEFAULT_CHANNELDATA.toString())));
//    }
//}
