//package com.polmon.polsoft.web.rest;
//
//import com.polmon.polsoft.PolsoftApp;
//import com.polmon.polsoft.domain.Alarmtemplate;
//import com.polmon.polsoft.repository.AlarmtemplateRepository;
//import com.polmon.polsoft.service.AlarmtemplateService;
//import com.polmon.polsoft.repository.search.AlarmtemplateSearchRepository;
//import com.polmon.polsoft.service.dto.AlarmtemplateDTO;
//import com.polmon.polsoft.service.mapper.AlarmtemplateMapper;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import static org.hamcrest.Matchers.hasItem;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.PostConstruct;
//import javax.inject.Inject;
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Test class for the AlarmtemplateResource REST controller.
// *
// * @see AlarmtemplateResource
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = PolsoftApp.class)
//public class AlarmtemplateResourceIntTest {
//
//    private static final Boolean DEFAULT_ACKREQUIRED = false;
//    private static final Boolean UPDATED_ACKREQUIRED = true;
//
//    private static final Boolean DEFAULT_COMMENTSREQUIRED = false;
//    private static final Boolean UPDATED_COMMENTSREQUIRED = true;
//    private static final String DEFAULT_LOGICALMEASUREPOINT = "AAAAA";
//    private static final String UPDATED_LOGICALMEASUREPOINT = "BBBBB";
//    private static final String DEFAULT_LOGICALDEFAULTVALUE = "AAAAA";
//    private static final String UPDATED_LOGICALDEFAULTVALUE = "BBBBB";
//
//    private static final Boolean DEFAULT_ACTIVETIMECHECKED = false;
//    private static final Boolean UPDATED_ACTIVETIMECHECKED = true;
//
//    @Inject
//    private AlarmtemplateRepository alarmtemplateRepository;
//
//    @Inject
//    private AlarmtemplateMapper alarmtemplateMapper;
//
//    @Inject
//    private AlarmtemplateService alarmtemplateService;
//
//    @Inject
//    private AlarmtemplateSearchRepository alarmtemplateSearchRepository;
//
//    @Inject
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Inject
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Inject
//    private EntityManager em;
//
//    private MockMvc restAlarmtemplateMockMvc;
//
//    private Alarmtemplate alarmtemplate;
//
//    @PostConstruct
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        AlarmtemplateResource alarmtemplateResource = new AlarmtemplateResource();
//        ReflectionTestUtils.setField(alarmtemplateResource, "alarmtemplateService", alarmtemplateService);
//        this.restAlarmtemplateMockMvc = MockMvcBuilders.standaloneSetup(alarmtemplateResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setMessageConverters(jacksonMessageConverter).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Alarmtemplate createEntity(EntityManager em) {
//        Alarmtemplate alarmtemplate = new Alarmtemplate();
//        alarmtemplate = new Alarmtemplate()
//                .ackrequired(DEFAULT_ACKREQUIRED)
//                .commentsrequired(DEFAULT_COMMENTSREQUIRED)
//                .logicalmeasurepoint(DEFAULT_LOGICALMEASUREPOINT)
//                .logicaldefaultvalue(DEFAULT_LOGICALDEFAULTVALUE)
//                .activetimechecked(DEFAULT_ACTIVETIMECHECKED);
//        return alarmtemplate;
//    }
//
//    @Before
//    public void initTest() {
//        alarmtemplateSearchRepository.deleteAll();
//        alarmtemplate = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createAlarmtemplate() throws Exception {
//        int databaseSizeBeforeCreate = alarmtemplateRepository.findAll().size();
//
//        // Create the Alarmtemplate
//        AlarmtemplateDTO alarmtemplateDTO = alarmtemplateMapper.alarmtemplateToAlarmtemplateDTO(alarmtemplate);
//
//        restAlarmtemplateMockMvc.perform(post("/api/alarmtemplates")
//                .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(alarmtemplateDTO)))
//                .andExpect(status().isCreated());
//
//        // Validate the Alarmtemplate in the database
//        List<Alarmtemplate> alarmtemplates = alarmtemplateRepository.findAll();
//        assertThat(alarmtemplates).hasSize(databaseSizeBeforeCreate + 1);
//        Alarmtemplate testAlarmtemplate = alarmtemplates.get(alarmtemplates.size() - 1);
//        assertThat(testAlarmtemplate.isAckrequired()).isEqualTo(DEFAULT_ACKREQUIRED);
//        assertThat(testAlarmtemplate.isCommentsrequired()).isEqualTo(DEFAULT_COMMENTSREQUIRED);
//        assertThat(testAlarmtemplate.getLogicalmeasurepoint()).isEqualTo(DEFAULT_LOGICALMEASUREPOINT);
//        assertThat(testAlarmtemplate.getLogicaldefaultvalue()).isEqualTo(DEFAULT_LOGICALDEFAULTVALUE);
//        assertThat(testAlarmtemplate.isActivetimechecked()).isEqualTo(DEFAULT_ACTIVETIMECHECKED);
//
//        // Validate the Alarmtemplate in ElasticSearch
//        Alarmtemplate alarmtemplateEs = alarmtemplateSearchRepository.findOne(testAlarmtemplate.getId());
//        assertThat(alarmtemplateEs).isEqualToComparingFieldByField(testAlarmtemplate);
//    }
//
//    @Test
//    @Transactional
//    public void getAllAlarmtemplates() throws Exception {
//        // Initialize the database
//        alarmtemplateRepository.saveAndFlush(alarmtemplate);
//
//        // Get all the alarmtemplates
//        restAlarmtemplateMockMvc.perform(get("/api/alarmtemplates?sort=id,desc"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//                .andExpect(jsonPath("$.[*].id").value(hasItem(alarmtemplate.getId().intValue())))
//                .andExpect(jsonPath("$.[*].ackrequired").value(hasItem(DEFAULT_ACKREQUIRED.booleanValue())))
//                .andExpect(jsonPath("$.[*].commentsrequired").value(hasItem(DEFAULT_COMMENTSREQUIRED.booleanValue())))
//                .andExpect(jsonPath("$.[*].logicalmeasurepoint").value(hasItem(DEFAULT_LOGICALMEASUREPOINT.toString())))
//                .andExpect(jsonPath("$.[*].logicaldefaultvalue").value(hasItem(DEFAULT_LOGICALDEFAULTVALUE.toString())))
//                .andExpect(jsonPath("$.[*].activetimechecked").value(hasItem(DEFAULT_ACTIVETIMECHECKED.booleanValue())));
//    }
//
//    @Test
//    @Transactional
//    public void getAlarmtemplate() throws Exception {
//        // Initialize the database
//        alarmtemplateRepository.saveAndFlush(alarmtemplate);
//
//        // Get the alarmtemplate
//        restAlarmtemplateMockMvc.perform(get("/api/alarmtemplates/{id}", alarmtemplate.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(alarmtemplate.getId().intValue()))
//            .andExpect(jsonPath("$.ackrequired").value(DEFAULT_ACKREQUIRED.booleanValue()))
//            .andExpect(jsonPath("$.commentsrequired").value(DEFAULT_COMMENTSREQUIRED.booleanValue()))
//            .andExpect(jsonPath("$.logicalmeasurepoint").value(DEFAULT_LOGICALMEASUREPOINT.toString()))
//            .andExpect(jsonPath("$.logicaldefaultvalue").value(DEFAULT_LOGICALDEFAULTVALUE.toString()))
//            .andExpect(jsonPath("$.activetimechecked").value(DEFAULT_ACTIVETIMECHECKED.booleanValue()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingAlarmtemplate() throws Exception {
//        // Get the alarmtemplate
//        restAlarmtemplateMockMvc.perform(get("/api/alarmtemplates/{id}", Long.MAX_VALUE))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateAlarmtemplate() throws Exception {
//        // Initialize the database
//        alarmtemplateRepository.saveAndFlush(alarmtemplate);
//        alarmtemplateSearchRepository.save(alarmtemplate);
//        int databaseSizeBeforeUpdate = alarmtemplateRepository.findAll().size();
//
//        // Update the alarmtemplate
//        Alarmtemplate updatedAlarmtemplate = alarmtemplateRepository.findOne(alarmtemplate.getId());
//        updatedAlarmtemplate
//                .ackrequired(UPDATED_ACKREQUIRED)
//                .commentsrequired(UPDATED_COMMENTSREQUIRED)
//                .logicalmeasurepoint(UPDATED_LOGICALMEASUREPOINT)
//                .logicaldefaultvalue(UPDATED_LOGICALDEFAULTVALUE)
//                .activetimechecked(UPDATED_ACTIVETIMECHECKED);
//        AlarmtemplateDTO alarmtemplateDTO = alarmtemplateMapper.alarmtemplateToAlarmtemplateDTO(updatedAlarmtemplate);
//
//        restAlarmtemplateMockMvc.perform(put("/api/alarmtemplates")
//                .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(alarmtemplateDTO)))
//                .andExpect(status().isOk());
//
//        // Validate the Alarmtemplate in the database
//        List<Alarmtemplate> alarmtemplates = alarmtemplateRepository.findAll();
//        assertThat(alarmtemplates).hasSize(databaseSizeBeforeUpdate);
//        Alarmtemplate testAlarmtemplate = alarmtemplates.get(alarmtemplates.size() - 1);
//        assertThat(testAlarmtemplate.isAckrequired()).isEqualTo(UPDATED_ACKREQUIRED);
//        assertThat(testAlarmtemplate.isCommentsrequired()).isEqualTo(UPDATED_COMMENTSREQUIRED);
//        assertThat(testAlarmtemplate.getLogicalmeasurepoint()).isEqualTo(UPDATED_LOGICALMEASUREPOINT);
//        assertThat(testAlarmtemplate.getLogicaldefaultvalue()).isEqualTo(UPDATED_LOGICALDEFAULTVALUE);
//        assertThat(testAlarmtemplate.isActivetimechecked()).isEqualTo(UPDATED_ACTIVETIMECHECKED);
//
//        // Validate the Alarmtemplate in ElasticSearch
//        Alarmtemplate alarmtemplateEs = alarmtemplateSearchRepository.findOne(testAlarmtemplate.getId());
//        assertThat(alarmtemplateEs).isEqualToComparingFieldByField(testAlarmtemplate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteAlarmtemplate() throws Exception {
//        // Initialize the database
//        alarmtemplateRepository.saveAndFlush(alarmtemplate);
//        alarmtemplateSearchRepository.save(alarmtemplate);
//        int databaseSizeBeforeDelete = alarmtemplateRepository.findAll().size();
//
//        // Get the alarmtemplate
//        restAlarmtemplateMockMvc.perform(delete("/api/alarmtemplates/{id}", alarmtemplate.getId())
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk());
//
//        // Validate ElasticSearch is empty
//        boolean alarmtemplateExistsInEs = alarmtemplateSearchRepository.exists(alarmtemplate.getId());
//        assertThat(alarmtemplateExistsInEs).isFalse();
//
//        // Validate the database is empty
//        List<Alarmtemplate> alarmtemplates = alarmtemplateRepository.findAll();
//        assertThat(alarmtemplates).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void searchAlarmtemplate() throws Exception {
//        // Initialize the database
//        alarmtemplateRepository.saveAndFlush(alarmtemplate);
//        alarmtemplateSearchRepository.save(alarmtemplate);
//
//        // Search the alarmtemplate
//        restAlarmtemplateMockMvc.perform(get("/api/_search/alarmtemplates?query=id:" + alarmtemplate.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(alarmtemplate.getId().intValue())))
//            .andExpect(jsonPath("$.[*].ackrequired").value(hasItem(DEFAULT_ACKREQUIRED.booleanValue())))
//            .andExpect(jsonPath("$.[*].commentsrequired").value(hasItem(DEFAULT_COMMENTSREQUIRED.booleanValue())))
//            .andExpect(jsonPath("$.[*].logicalmeasurepoint").value(hasItem(DEFAULT_LOGICALMEASUREPOINT.toString())))
//            .andExpect(jsonPath("$.[*].logicaldefaultvalue").value(hasItem(DEFAULT_LOGICALDEFAULTVALUE.toString())))
//            .andExpect(jsonPath("$.[*].activetimechecked").value(hasItem(DEFAULT_ACTIVETIMECHECKED.booleanValue())));
//    }
//}
