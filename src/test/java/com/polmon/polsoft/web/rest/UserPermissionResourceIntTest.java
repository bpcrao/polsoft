package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.UserPermission;
import com.polmon.polsoft.repository.UserPermissionRepository;
import com.polmon.polsoft.service.UserPermissionService;
import com.polmon.polsoft.repository.search.UserPermissionSearchRepository;
import com.polmon.polsoft.service.dto.UserPermissionDTO;
import com.polmon.polsoft.service.mapper.UserPermissionMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserPermissionResource REST controller.
 *
 * @see UserPermissionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class UserPermissionResourceIntTest {

    private static final Integer DEFAULT_PERMISSION = 1;
    private static final Integer UPDATED_PERMISSION = 2;
    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private UserPermissionRepository userPermissionRepository;

    @Inject
    private UserPermissionMapper userPermissionMapper;

    @Inject
    private UserPermissionService userPermissionService;

    @Inject
    private UserPermissionSearchRepository userPermissionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restUserPermissionMockMvc;

    private UserPermission userPermission;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserPermissionResource userPermissionResource = new UserPermissionResource();
        ReflectionTestUtils.setField(userPermissionResource, "userPermissionService", userPermissionService);
        this.restUserPermissionMockMvc = MockMvcBuilders.standaloneSetup(userPermissionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserPermission createEntity(EntityManager em) {
        UserPermission userPermission = new UserPermission();
        userPermission = new UserPermission()
                .permission(DEFAULT_PERMISSION)
                .name(DEFAULT_NAME);
        return userPermission;
    }

    @Before
    public void initTest() {
        userPermissionSearchRepository.deleteAll();
        userPermission = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserPermission() throws Exception {
        int databaseSizeBeforeCreate = userPermissionRepository.findAll().size();

        // Create the UserPermission
        UserPermissionDTO userPermissionDTO = userPermissionMapper.userPermissionToUserPermissionDTO(userPermission);

        restUserPermissionMockMvc.perform(post("/api/user-permissions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userPermissionDTO)))
                .andExpect(status().isCreated());

        // Validate the UserPermission in the database
        List<UserPermission> userPermissions = userPermissionRepository.findAll();
        assertThat(userPermissions).hasSize(databaseSizeBeforeCreate + 1);
        UserPermission testUserPermission = userPermissions.get(userPermissions.size() - 1);
        assertThat(testUserPermission.getPermission()).isEqualTo(DEFAULT_PERMISSION);
        assertThat(testUserPermission.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the UserPermission in ElasticSearch
        UserPermission userPermissionEs = userPermissionSearchRepository.findOne(testUserPermission.getId());
        assertThat(userPermissionEs).isEqualToComparingFieldByField(testUserPermission);
    }

    @Test
    @Transactional
    public void getAllUserPermissions() throws Exception {
        // Initialize the database
        userPermissionRepository.saveAndFlush(userPermission);

        // Get all the userPermissions
        restUserPermissionMockMvc.perform(get("/api/user-permissions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userPermission.getId().intValue())))
                .andExpect(jsonPath("$.[*].permission").value(hasItem(DEFAULT_PERMISSION)))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getUserPermission() throws Exception {
        // Initialize the database
        userPermissionRepository.saveAndFlush(userPermission);

        // Get the userPermission
        restUserPermissionMockMvc.perform(get("/api/user-permissions/{id}", userPermission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userPermission.getId().intValue()))
            .andExpect(jsonPath("$.permission").value(DEFAULT_PERMISSION))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserPermission() throws Exception {
        // Get the userPermission
        restUserPermissionMockMvc.perform(get("/api/user-permissions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserPermission() throws Exception {
        // Initialize the database
        userPermissionRepository.saveAndFlush(userPermission);
        userPermissionSearchRepository.save(userPermission);
        int databaseSizeBeforeUpdate = userPermissionRepository.findAll().size();

        // Update the userPermission
        UserPermission updatedUserPermission = userPermissionRepository.findOne(userPermission.getId());
        updatedUserPermission
                .permission(UPDATED_PERMISSION)
                .name(UPDATED_NAME);
        UserPermissionDTO userPermissionDTO = userPermissionMapper.userPermissionToUserPermissionDTO(updatedUserPermission);

        restUserPermissionMockMvc.perform(put("/api/user-permissions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userPermissionDTO)))
                .andExpect(status().isOk());

        // Validate the UserPermission in the database
        List<UserPermission> userPermissions = userPermissionRepository.findAll();
        assertThat(userPermissions).hasSize(databaseSizeBeforeUpdate);
        UserPermission testUserPermission = userPermissions.get(userPermissions.size() - 1);
        assertThat(testUserPermission.getPermission()).isEqualTo(UPDATED_PERMISSION);
        assertThat(testUserPermission.getName()).isEqualTo(UPDATED_NAME);

        // Validate the UserPermission in ElasticSearch
        UserPermission userPermissionEs = userPermissionSearchRepository.findOne(testUserPermission.getId());
        assertThat(userPermissionEs).isEqualToComparingFieldByField(testUserPermission);
    }

    @Test
    @Transactional
    public void deleteUserPermission() throws Exception {
        // Initialize the database
        userPermissionRepository.saveAndFlush(userPermission);
        userPermissionSearchRepository.save(userPermission);
        int databaseSizeBeforeDelete = userPermissionRepository.findAll().size();

        // Get the userPermission
        restUserPermissionMockMvc.perform(delete("/api/user-permissions/{id}", userPermission.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean userPermissionExistsInEs = userPermissionSearchRepository.exists(userPermission.getId());
        assertThat(userPermissionExistsInEs).isFalse();

        // Validate the database is empty
        List<UserPermission> userPermissions = userPermissionRepository.findAll();
        assertThat(userPermissions).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUserPermission() throws Exception {
        // Initialize the database
        userPermissionRepository.saveAndFlush(userPermission);
        userPermissionSearchRepository.save(userPermission);

        // Search the userPermission
        restUserPermissionMockMvc.perform(get("/api/_search/user-permissions?query=id:" + userPermission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userPermission.getId().intValue())))
            .andExpect(jsonPath("$.[*].permission").value(hasItem(DEFAULT_PERMISSION)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
}
