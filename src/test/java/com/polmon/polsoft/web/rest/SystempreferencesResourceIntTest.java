package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Systempreferences;
import com.polmon.polsoft.repository.SystempreferencesRepository;
import com.polmon.polsoft.service.SystempreferencesService;
import com.polmon.polsoft.repository.search.SystempreferencesSearchRepository;
import com.polmon.polsoft.service.dto.SystempreferencesDTO;
import com.polmon.polsoft.service.mapper.SystempreferencesMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SystempreferencesResource REST controller.
 *
 * @see SystempreferencesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class SystempreferencesResourceIntTest {
    private static final String DEFAULT_DATA = "AAAAA";
    private static final String UPDATED_DATA = "BBBBB";
    private static final String DEFAULT_KEYPROPERTY = "AAAAA";
    private static final String UPDATED_KEYPROPERTY = "BBBBB";

    @Inject
    private SystempreferencesRepository systempreferencesRepository;

    @Inject
    private SystempreferencesMapper systempreferencesMapper;

    @Inject
    private SystempreferencesService systempreferencesService;

    @Inject
    private SystempreferencesSearchRepository systempreferencesSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restSystempreferencesMockMvc;

    private Systempreferences systempreferences;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SystempreferencesResource systempreferencesResource = new SystempreferencesResource();
        ReflectionTestUtils.setField(systempreferencesResource, "systempreferencesService", systempreferencesService);
        this.restSystempreferencesMockMvc = MockMvcBuilders.standaloneSetup(systempreferencesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Systempreferences createEntity(EntityManager em) {
        Systempreferences systempreferences = new Systempreferences();
        systempreferences = new Systempreferences()
                .data(DEFAULT_DATA)
                .keyproperty(DEFAULT_KEYPROPERTY);
        return systempreferences;
    }

    @Before
    public void initTest() {
        systempreferencesSearchRepository.deleteAll();
        systempreferences = createEntity(em);
    }

    @Test
    @Transactional
    public void createSystempreferences() throws Exception {
        int databaseSizeBeforeCreate = systempreferencesRepository.findAll().size();

        // Create the Systempreferences
        SystempreferencesDTO systempreferencesDTO = systempreferencesMapper.systempreferencesToSystempreferencesDTO(systempreferences);

        restSystempreferencesMockMvc.perform(post("/api/systempreferences")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(systempreferencesDTO)))
                .andExpect(status().isCreated());

        // Validate the Systempreferences in the database
        List<Systempreferences> systempreferences = systempreferencesRepository.findAll();
        assertThat(systempreferences).hasSize(databaseSizeBeforeCreate + 1);
        Systempreferences testSystempreferences = systempreferences.get(systempreferences.size() - 1);
        assertThat(testSystempreferences.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testSystempreferences.getKeyproperty()).isEqualTo(DEFAULT_KEYPROPERTY);

        // Validate the Systempreferences in ElasticSearch
        Systempreferences systempreferencesEs = systempreferencesSearchRepository.findOne(testSystempreferences.getId());
        assertThat(systempreferencesEs).isEqualToComparingFieldByField(testSystempreferences);
    }

    @Test
    @Transactional
    public void getAllSystempreferences() throws Exception {
        // Initialize the database
        systempreferencesRepository.saveAndFlush(systempreferences);

        // Get all the systempreferences
        restSystempreferencesMockMvc.perform(get("/api/systempreferences?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(systempreferences.getId().intValue())))
                .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
                .andExpect(jsonPath("$.[*].keyproperty").value(hasItem(DEFAULT_KEYPROPERTY.toString())));
    }

    @Test
    @Transactional
    public void getSystempreferences() throws Exception {
        // Initialize the database
        systempreferencesRepository.saveAndFlush(systempreferences);

        // Get the systempreferences
        restSystempreferencesMockMvc.perform(get("/api/systempreferences/{id}", systempreferences.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(systempreferences.getId().intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()))
            .andExpect(jsonPath("$.keyproperty").value(DEFAULT_KEYPROPERTY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSystempreferences() throws Exception {
        // Get the systempreferences
        restSystempreferencesMockMvc.perform(get("/api/systempreferences/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSystempreferences() throws Exception {
        // Initialize the database
        systempreferencesRepository.saveAndFlush(systempreferences);
        systempreferencesSearchRepository.save(systempreferences);
        int databaseSizeBeforeUpdate = systempreferencesRepository.findAll().size();

        // Update the systempreferences
        Systempreferences updatedSystempreferences = systempreferencesRepository.findOne(systempreferences.getId());
        updatedSystempreferences
                .data(UPDATED_DATA)
                .keyproperty(UPDATED_KEYPROPERTY);
        SystempreferencesDTO systempreferencesDTO = systempreferencesMapper.systempreferencesToSystempreferencesDTO(updatedSystempreferences);

        restSystempreferencesMockMvc.perform(put("/api/systempreferences")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(systempreferencesDTO)))
                .andExpect(status().isOk());

        // Validate the Systempreferences in the database
        List<Systempreferences> systempreferences = systempreferencesRepository.findAll();
        assertThat(systempreferences).hasSize(databaseSizeBeforeUpdate);
        Systempreferences testSystempreferences = systempreferences.get(systempreferences.size() - 1);
        assertThat(testSystempreferences.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testSystempreferences.getKeyproperty()).isEqualTo(UPDATED_KEYPROPERTY);

        // Validate the Systempreferences in ElasticSearch
        Systempreferences systempreferencesEs = systempreferencesSearchRepository.findOne(testSystempreferences.getId());
        assertThat(systempreferencesEs).isEqualToComparingFieldByField(testSystempreferences);
    }

    @Test
    @Transactional
    public void deleteSystempreferences() throws Exception {
        // Initialize the database
        systempreferencesRepository.saveAndFlush(systempreferences);
        systempreferencesSearchRepository.save(systempreferences);
        int databaseSizeBeforeDelete = systempreferencesRepository.findAll().size();

        // Get the systempreferences
        restSystempreferencesMockMvc.perform(delete("/api/systempreferences/{id}", systempreferences.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean systempreferencesExistsInEs = systempreferencesSearchRepository.exists(systempreferences.getId());
        assertThat(systempreferencesExistsInEs).isFalse();

        // Validate the database is empty
        List<Systempreferences> systempreferences = systempreferencesRepository.findAll();
        assertThat(systempreferences).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSystempreferences() throws Exception {
        // Initialize the database
        systempreferencesRepository.saveAndFlush(systempreferences);
        systempreferencesSearchRepository.save(systempreferences);

        // Search the systempreferences
        restSystempreferencesMockMvc.perform(get("/api/_search/systempreferences?query=id:" + systempreferences.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(systempreferences.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
            .andExpect(jsonPath("$.[*].keyproperty").value(hasItem(DEFAULT_KEYPROPERTY.toString())));
    }
}
