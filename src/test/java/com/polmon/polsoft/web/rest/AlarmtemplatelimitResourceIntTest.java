package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Alarmtemplatelimit;
import com.polmon.polsoft.repository.AlarmtemplatelimitRepository;
import com.polmon.polsoft.service.AlarmtemplatelimitService;
import com.polmon.polsoft.repository.search.AlarmtemplatelimitSearchRepository;
import com.polmon.polsoft.service.dto.AlarmtemplatelimitDTO;
import com.polmon.polsoft.service.mapper.AlarmtemplatelimitMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AlarmtemplatelimitResource REST controller.
 *
 * @see AlarmtemplatelimitResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class AlarmtemplatelimitResourceIntTest {

    private static final Double DEFAULT_HIGHLIMIT = 1D;
    private static final Double UPDATED_HIGHLIMIT = 2D;

    private static final Double DEFAULT_LOWLIMIT = 1D;
    private static final Double UPDATED_LOWLIMIT = 2D;

    private static final Double DEFAULT_ROC = 1D;
    private static final Double UPDATED_ROC = 2D;
    private static final String DEFAULT_SCHEDULAR = "AAAAA";
    private static final String UPDATED_SCHEDULAR = "BBBBB";

    private static final Long DEFAULT_DELAY = 1L;
    private static final Long UPDATED_DELAY = 2L;
    private static final String DEFAULT_ACTION = "AAAAA";
    private static final String UPDATED_ACTION = "BBBBB";

    private static final LocalDate DEFAULT_N = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_N = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private AlarmtemplatelimitRepository alarmtemplatelimitRepository;

    @Inject
    private AlarmtemplatelimitMapper alarmtemplatelimitMapper;

    @Inject
    private AlarmtemplatelimitService alarmtemplatelimitService;

    @Inject
    private AlarmtemplatelimitSearchRepository alarmtemplatelimitSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restAlarmtemplatelimitMockMvc;

    private Alarmtemplatelimit alarmtemplatelimit;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AlarmtemplatelimitResource alarmtemplatelimitResource = new AlarmtemplatelimitResource();
        ReflectionTestUtils.setField(alarmtemplatelimitResource, "alarmtemplatelimitService", alarmtemplatelimitService);
        this.restAlarmtemplatelimitMockMvc = MockMvcBuilders.standaloneSetup(alarmtemplatelimitResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Alarmtemplatelimit createEntity(EntityManager em) {
        Alarmtemplatelimit alarmtemplatelimit = new Alarmtemplatelimit();
        alarmtemplatelimit = new Alarmtemplatelimit()
                .highlimit(DEFAULT_HIGHLIMIT)
                .lowlimit(DEFAULT_LOWLIMIT)
                .roc(DEFAULT_ROC)
                .schedular(DEFAULT_SCHEDULAR)
                .delay(DEFAULT_DELAY)
                .action(DEFAULT_ACTION);
        return alarmtemplatelimit;
    }

    @Before
    public void initTest() {
        alarmtemplatelimitSearchRepository.deleteAll();
        alarmtemplatelimit = createEntity(em);
    }

    @Test
    @Transactional
    public void createAlarmtemplatelimit() throws Exception {
        int databaseSizeBeforeCreate = alarmtemplatelimitRepository.findAll().size();

        // Create the Alarmtemplatelimit
        AlarmtemplatelimitDTO alarmtemplatelimitDTO = alarmtemplatelimitMapper.alarmtemplatelimitToAlarmtemplatelimitDTO(alarmtemplatelimit);

        restAlarmtemplatelimitMockMvc.perform(post("/api/alarmtemplatelimits")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(alarmtemplatelimitDTO)))
                .andExpect(status().isCreated());

        // Validate the Alarmtemplatelimit in the database
        List<Alarmtemplatelimit> alarmtemplatelimits = alarmtemplatelimitRepository.findAll();
        assertThat(alarmtemplatelimits).hasSize(databaseSizeBeforeCreate + 1);
        Alarmtemplatelimit testAlarmtemplatelimit = alarmtemplatelimits.get(alarmtemplatelimits.size() - 1);
        assertThat(testAlarmtemplatelimit.getHighlimit()).isEqualTo(DEFAULT_HIGHLIMIT);
        assertThat(testAlarmtemplatelimit.getLowlimit()).isEqualTo(DEFAULT_LOWLIMIT);
        assertThat(testAlarmtemplatelimit.getRoc()).isEqualTo(DEFAULT_ROC);
        assertThat(testAlarmtemplatelimit.getSchedular()).isEqualTo(DEFAULT_SCHEDULAR);
        assertThat(testAlarmtemplatelimit.getDelay()).isEqualTo(DEFAULT_DELAY);
        assertThat(testAlarmtemplatelimit.getAction()).isEqualTo(DEFAULT_ACTION);

        // Validate the Alarmtemplatelimit in ElasticSearch
        Alarmtemplatelimit alarmtemplatelimitEs = alarmtemplatelimitSearchRepository.findOne(testAlarmtemplatelimit.getId());
        assertThat(alarmtemplatelimitEs).isEqualToComparingFieldByField(testAlarmtemplatelimit);
    }

    @Test
    @Transactional
    public void getAllAlarmtemplatelimits() throws Exception {
        // Initialize the database
        alarmtemplatelimitRepository.saveAndFlush(alarmtemplatelimit);

        // Get all the alarmtemplatelimits
        restAlarmtemplatelimitMockMvc.perform(get("/api/alarmtemplatelimits?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(alarmtemplatelimit.getId().intValue())))
                .andExpect(jsonPath("$.[*].highlimit").value(hasItem(DEFAULT_HIGHLIMIT.doubleValue())))
                .andExpect(jsonPath("$.[*].lowlimit").value(hasItem(DEFAULT_LOWLIMIT.doubleValue())))
                .andExpect(jsonPath("$.[*].roc").value(hasItem(DEFAULT_ROC.doubleValue())))
                .andExpect(jsonPath("$.[*].schedular").value(hasItem(DEFAULT_SCHEDULAR.toString())))
                .andExpect(jsonPath("$.[*].delay").value(hasItem(DEFAULT_DELAY.intValue())))
                .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
                .andExpect(jsonPath("$.[*].n").value(hasItem(DEFAULT_N.toString())));
    }

    @Test
    @Transactional
    public void getAlarmtemplatelimit() throws Exception {
        // Initialize the database
        alarmtemplatelimitRepository.saveAndFlush(alarmtemplatelimit);

        // Get the alarmtemplatelimit
        restAlarmtemplatelimitMockMvc.perform(get("/api/alarmtemplatelimits/{id}", alarmtemplatelimit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(alarmtemplatelimit.getId().intValue()))
            .andExpect(jsonPath("$.highlimit").value(DEFAULT_HIGHLIMIT.doubleValue()))
            .andExpect(jsonPath("$.lowlimit").value(DEFAULT_LOWLIMIT.doubleValue()))
            .andExpect(jsonPath("$.roc").value(DEFAULT_ROC.doubleValue()))
            .andExpect(jsonPath("$.schedular").value(DEFAULT_SCHEDULAR.toString()))
            .andExpect(jsonPath("$.delay").value(DEFAULT_DELAY.intValue()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.n").value(DEFAULT_N.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAlarmtemplatelimit() throws Exception {
        // Get the alarmtemplatelimit
        restAlarmtemplatelimitMockMvc.perform(get("/api/alarmtemplatelimits/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAlarmtemplatelimit() throws Exception {
        // Initialize the database
        alarmtemplatelimitRepository.saveAndFlush(alarmtemplatelimit);
        alarmtemplatelimitSearchRepository.save(alarmtemplatelimit);
        int databaseSizeBeforeUpdate = alarmtemplatelimitRepository.findAll().size();

        // Update the alarmtemplatelimit
        Alarmtemplatelimit updatedAlarmtemplatelimit = alarmtemplatelimitRepository.findOne(alarmtemplatelimit.getId());
        updatedAlarmtemplatelimit
                .highlimit(UPDATED_HIGHLIMIT)
                .lowlimit(UPDATED_LOWLIMIT)
                .roc(UPDATED_ROC)
                .schedular(UPDATED_SCHEDULAR)
                .delay(UPDATED_DELAY)
                .action(UPDATED_ACTION);
        AlarmtemplatelimitDTO alarmtemplatelimitDTO = alarmtemplatelimitMapper.alarmtemplatelimitToAlarmtemplatelimitDTO(updatedAlarmtemplatelimit);

        restAlarmtemplatelimitMockMvc.perform(put("/api/alarmtemplatelimits")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(alarmtemplatelimitDTO)))
                .andExpect(status().isOk());

        // Validate the Alarmtemplatelimit in the database
        List<Alarmtemplatelimit> alarmtemplatelimits = alarmtemplatelimitRepository.findAll();
        assertThat(alarmtemplatelimits).hasSize(databaseSizeBeforeUpdate);
        Alarmtemplatelimit testAlarmtemplatelimit = alarmtemplatelimits.get(alarmtemplatelimits.size() - 1);
        assertThat(testAlarmtemplatelimit.getHighlimit()).isEqualTo(UPDATED_HIGHLIMIT);
        assertThat(testAlarmtemplatelimit.getLowlimit()).isEqualTo(UPDATED_LOWLIMIT);
        assertThat(testAlarmtemplatelimit.getRoc()).isEqualTo(UPDATED_ROC);
        assertThat(testAlarmtemplatelimit.getSchedular()).isEqualTo(UPDATED_SCHEDULAR);
        assertThat(testAlarmtemplatelimit.getDelay()).isEqualTo(UPDATED_DELAY);
        assertThat(testAlarmtemplatelimit.getAction()).isEqualTo(UPDATED_ACTION);

        // Validate the Alarmtemplatelimit in ElasticSearch
        Alarmtemplatelimit alarmtemplatelimitEs = alarmtemplatelimitSearchRepository.findOne(testAlarmtemplatelimit.getId());
        assertThat(alarmtemplatelimitEs).isEqualToComparingFieldByField(testAlarmtemplatelimit);
    }

    @Test
    @Transactional
    public void deleteAlarmtemplatelimit() throws Exception {
        // Initialize the database
        alarmtemplatelimitRepository.saveAndFlush(alarmtemplatelimit);
        alarmtemplatelimitSearchRepository.save(alarmtemplatelimit);
        int databaseSizeBeforeDelete = alarmtemplatelimitRepository.findAll().size();

        // Get the alarmtemplatelimit
        restAlarmtemplatelimitMockMvc.perform(delete("/api/alarmtemplatelimits/{id}", alarmtemplatelimit.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean alarmtemplatelimitExistsInEs = alarmtemplatelimitSearchRepository.exists(alarmtemplatelimit.getId());
        assertThat(alarmtemplatelimitExistsInEs).isFalse();

        // Validate the database is empty
        List<Alarmtemplatelimit> alarmtemplatelimits = alarmtemplatelimitRepository.findAll();
        assertThat(alarmtemplatelimits).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAlarmtemplatelimit() throws Exception {
        // Initialize the database
        alarmtemplatelimitRepository.saveAndFlush(alarmtemplatelimit);
        alarmtemplatelimitSearchRepository.save(alarmtemplatelimit);

        // Search the alarmtemplatelimit
        restAlarmtemplatelimitMockMvc.perform(get("/api/_search/alarmtemplatelimits?query=id:" + alarmtemplatelimit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(alarmtemplatelimit.getId().intValue())))
            .andExpect(jsonPath("$.[*].highlimit").value(hasItem(DEFAULT_HIGHLIMIT.doubleValue())))
            .andExpect(jsonPath("$.[*].lowlimit").value(hasItem(DEFAULT_LOWLIMIT.doubleValue())))
            .andExpect(jsonPath("$.[*].roc").value(hasItem(DEFAULT_ROC.doubleValue())))
            .andExpect(jsonPath("$.[*].schedular").value(hasItem(DEFAULT_SCHEDULAR.toString())))
            .andExpect(jsonPath("$.[*].delay").value(hasItem(DEFAULT_DELAY.intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].n").value(hasItem(DEFAULT_N.toString())));
    }
}
