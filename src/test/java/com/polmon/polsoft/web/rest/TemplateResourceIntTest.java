package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Template;
import com.polmon.polsoft.repository.TemplateRepository;
import com.polmon.polsoft.service.TemplateService;
import com.polmon.polsoft.repository.search.TemplateSearchRepository;
import com.polmon.polsoft.service.dto.TemplateDTO;
import com.polmon.polsoft.service.mapper.TemplateMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TemplateResource REST controller.
 *
 * @see TemplateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class TemplateResourceIntTest {
    private static final String DEFAULT_TEMPLATE_NAME = "AAAAA";
    private static final String UPDATED_TEMPLATE_NAME = "BBBBB";
    private static final String DEFAULT_THRESHOLDS = "AAAAA";
    private static final String UPDATED_THRESHOLDS = "BBBBB";
    private static final String DEFAULT_SMS = "AAAAA";
    private static final String UPDATED_SMS = "BBBBB";
    private static final String DEFAULT_EMAILS = "AAAAA";
    private static final String UPDATED_EMAILS = "BBBBB";

    private static final Boolean DEFAULT_ACKNOWLEDGE_WITH_COMMENT = false;
    private static final Boolean UPDATED_ACKNOWLEDGE_WITH_COMMENT = true;

    @Inject
    private TemplateRepository templateRepository;

    @Inject
    private TemplateMapper templateMapper;

    @Inject
    private TemplateService templateService;

    @Inject
    private TemplateSearchRepository templateSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restTemplateMockMvc;

    private Template template;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TemplateResource templateResource = new TemplateResource();
        ReflectionTestUtils.setField(templateResource, "templateService", templateService);
        this.restTemplateMockMvc = MockMvcBuilders.standaloneSetup(templateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Template createEntity(EntityManager em) {
        Template template = new Template();
        template = new Template()
                .templateName(DEFAULT_TEMPLATE_NAME)
                .thresholds(DEFAULT_THRESHOLDS)
                .sms(DEFAULT_SMS)
                .emails(DEFAULT_EMAILS)
                .acknowledgeWithComment(DEFAULT_ACKNOWLEDGE_WITH_COMMENT);
        return template;
    }

    @Before
    public void initTest() {
        templateSearchRepository.deleteAll();
        template = createEntity(em);
    }

    @Test
    @Transactional
    public void createTemplate() throws Exception {
        int databaseSizeBeforeCreate = templateRepository.findAll().size();

        // Create the Template
        TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(template);

        restTemplateMockMvc.perform(post("/api/templates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(templateDTO)))
                .andExpect(status().isCreated());

        // Validate the Template in the database
        List<Template> templates = templateRepository.findAll();
        assertThat(templates).hasSize(databaseSizeBeforeCreate + 1);
        Template testTemplate = templates.get(templates.size() - 1);
        assertThat(testTemplate.getTemplateName()).isEqualTo(DEFAULT_TEMPLATE_NAME);
        assertThat(testTemplate.getThresholds()).isEqualTo(DEFAULT_THRESHOLDS);
        assertThat(testTemplate.getSms()).isEqualTo(DEFAULT_SMS);
        assertThat(testTemplate.getEmails()).isEqualTo(DEFAULT_EMAILS);
        assertThat(testTemplate.isAcknowledgeWithComment()).isEqualTo(DEFAULT_ACKNOWLEDGE_WITH_COMMENT);

        // Validate the Template in ElasticSearch
        Template templateEs = templateSearchRepository.findOne(testTemplate.getId());
        assertThat(templateEs).isEqualToComparingFieldByField(testTemplate);
    }

    @Test
    @Transactional
    public void checkTemplateNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateRepository.findAll().size();
        // set the field null
        template.setTemplateName(null);

        // Create the Template, which fails.
        TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(template);

        restTemplateMockMvc.perform(post("/api/templates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(templateDTO)))
                .andExpect(status().isBadRequest());

        List<Template> templates = templateRepository.findAll();
        assertThat(templates).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAcknowledgeWithCommentIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateRepository.findAll().size();
        // set the field null
        template.setAcknowledgeWithComment(null);

        // Create the Template, which fails.
        TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(template);

        restTemplateMockMvc.perform(post("/api/templates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(templateDTO)))
                .andExpect(status().isBadRequest());

        List<Template> templates = templateRepository.findAll();
        assertThat(templates).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTemplates() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        // Get all the templates
        restTemplateMockMvc.perform(get("/api/templates?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(template.getId().intValue())))
                .andExpect(jsonPath("$.[*].templateName").value(hasItem(DEFAULT_TEMPLATE_NAME.toString())))
                .andExpect(jsonPath("$.[*].thresholds").value(hasItem(DEFAULT_THRESHOLDS.toString())))
                .andExpect(jsonPath("$.[*].sms").value(hasItem(DEFAULT_SMS.toString())))
                .andExpect(jsonPath("$.[*].emails").value(hasItem(DEFAULT_EMAILS.toString())))
                .andExpect(jsonPath("$.[*].acknowledgeWithComment").value(hasItem(DEFAULT_ACKNOWLEDGE_WITH_COMMENT.booleanValue())));
    }

    @Test
    @Transactional
    public void getTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        // Get the template
        restTemplateMockMvc.perform(get("/api/templates/{id}", template.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(template.getId().intValue()))
            .andExpect(jsonPath("$.templateName").value(DEFAULT_TEMPLATE_NAME.toString()))
            .andExpect(jsonPath("$.thresholds").value(DEFAULT_THRESHOLDS.toString()))
            .andExpect(jsonPath("$.sms").value(DEFAULT_SMS.toString()))
            .andExpect(jsonPath("$.emails").value(DEFAULT_EMAILS.toString()))
            .andExpect(jsonPath("$.acknowledgeWithComment").value(DEFAULT_ACKNOWLEDGE_WITH_COMMENT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTemplate() throws Exception {
        // Get the template
        restTemplateMockMvc.perform(get("/api/templates/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);
        templateSearchRepository.save(template);
        int databaseSizeBeforeUpdate = templateRepository.findAll().size();

        // Update the template
        Template updatedTemplate = templateRepository.findOne(template.getId());
        updatedTemplate
                .templateName(UPDATED_TEMPLATE_NAME)
                .thresholds(UPDATED_THRESHOLDS)
                .sms(UPDATED_SMS)
                .emails(UPDATED_EMAILS)
                .acknowledgeWithComment(UPDATED_ACKNOWLEDGE_WITH_COMMENT);
        TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(updatedTemplate);

        restTemplateMockMvc.perform(put("/api/templates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(templateDTO)))
                .andExpect(status().isOk());

        // Validate the Template in the database
        List<Template> templates = templateRepository.findAll();
        assertThat(templates).hasSize(databaseSizeBeforeUpdate);
        Template testTemplate = templates.get(templates.size() - 1);
        assertThat(testTemplate.getTemplateName()).isEqualTo(UPDATED_TEMPLATE_NAME);
        assertThat(testTemplate.getThresholds()).isEqualTo(UPDATED_THRESHOLDS);
        assertThat(testTemplate.getSms()).isEqualTo(UPDATED_SMS);
        assertThat(testTemplate.getEmails()).isEqualTo(UPDATED_EMAILS);
        assertThat(testTemplate.isAcknowledgeWithComment()).isEqualTo(UPDATED_ACKNOWLEDGE_WITH_COMMENT);

        // Validate the Template in ElasticSearch
        Template templateEs = templateSearchRepository.findOne(testTemplate.getId());
        assertThat(templateEs).isEqualToComparingFieldByField(testTemplate);
    }

    @Test
    @Transactional
    public void deleteTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);
        templateSearchRepository.save(template);
        int databaseSizeBeforeDelete = templateRepository.findAll().size();

        // Get the template
        restTemplateMockMvc.perform(delete("/api/templates/{id}", template.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean templateExistsInEs = templateSearchRepository.exists(template.getId());
        assertThat(templateExistsInEs).isFalse();

        // Validate the database is empty
        List<Template> templates = templateRepository.findAll();
        assertThat(templates).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);
        templateSearchRepository.save(template);

        // Search the template
        restTemplateMockMvc.perform(get("/api/_search/templates?query=id:" + template.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(template.getId().intValue())))
            .andExpect(jsonPath("$.[*].templateName").value(hasItem(DEFAULT_TEMPLATE_NAME.toString())))
            .andExpect(jsonPath("$.[*].thresholds").value(hasItem(DEFAULT_THRESHOLDS.toString())))
            .andExpect(jsonPath("$.[*].sms").value(hasItem(DEFAULT_SMS.toString())))
            .andExpect(jsonPath("$.[*].emails").value(hasItem(DEFAULT_EMAILS.toString())))
            .andExpect(jsonPath("$.[*].acknowledgeWithComment").value(hasItem(DEFAULT_ACKNOWLEDGE_WITH_COMMENT.booleanValue())));
    }
}
