package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Alarmtemplateschedulars;
import com.polmon.polsoft.repository.AlarmtemplateschedularsRepository;
import com.polmon.polsoft.service.AlarmtemplateschedularsService;
import com.polmon.polsoft.repository.search.AlarmtemplateschedularsSearchRepository;
import com.polmon.polsoft.service.dto.AlarmtemplateschedularsDTO;
import com.polmon.polsoft.service.mapper.AlarmtemplateschedularsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AlarmtemplateschedularsResource REST controller.
 *
 * @see AlarmtemplateschedularsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class AlarmtemplateschedularsResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));
    private static final String DEFAULT_DAY = "AAAAA";
    private static final String UPDATED_DAY = "BBBBB";

    private static final String DEFAULT_STARTTIME = "1:0";
    private static final String UPDATED_STARTTIME = "2:0";
    private static final String DEFAULT_STARTTIME_STR = "1";

    private static final String DEFAULT_ENDTIME = "1:0";
    private static final String UPDATED_ENDTIME = "2:0";
    private static final String DEFAULT_ENDTIME_STR = "1";

    @Inject
    private AlarmtemplateschedularsRepository alarmtemplateschedularsRepository;

    @Inject
    private AlarmtemplateschedularsMapper alarmtemplateschedularsMapper;

    @Inject
    private AlarmtemplateschedularsService alarmtemplateschedularsService;

    @Inject
    private AlarmtemplateschedularsSearchRepository alarmtemplateschedularsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restAlarmtemplateschedularsMockMvc;

    private Alarmtemplateschedulars alarmtemplateschedulars;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AlarmtemplateschedularsResource alarmtemplateschedularsResource = new AlarmtemplateschedularsResource();
        ReflectionTestUtils.setField(alarmtemplateschedularsResource, "alarmtemplateschedularsService", alarmtemplateschedularsService);
        this.restAlarmtemplateschedularsMockMvc = MockMvcBuilders.standaloneSetup(alarmtemplateschedularsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Alarmtemplateschedulars createEntity(EntityManager em) {
        Alarmtemplateschedulars alarmtemplateschedulars = new Alarmtemplateschedulars();
        alarmtemplateschedulars = new Alarmtemplateschedulars()
                .day(DEFAULT_DAY)
                .starttime(DEFAULT_STARTTIME)
                .endtime(DEFAULT_ENDTIME);
        return alarmtemplateschedulars;
    }

    @Before
    public void initTest() {
        alarmtemplateschedularsSearchRepository.deleteAll();
        alarmtemplateschedulars = createEntity(em);
    }

    @Test
    @Transactional
    public void createAlarmtemplateschedulars() throws Exception {
        int databaseSizeBeforeCreate = alarmtemplateschedularsRepository.findAll().size();

        // Create the Alarmtemplateschedulars
        AlarmtemplateschedularsDTO alarmtemplateschedularsDTO = alarmtemplateschedularsMapper.alarmtemplateschedularsToAlarmtemplateschedularsDTO(alarmtemplateschedulars);

        restAlarmtemplateschedularsMockMvc.perform(post("/api/alarmtemplateschedulars")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(alarmtemplateschedularsDTO)))
                .andExpect(status().isCreated());

        // Validate the Alarmtemplateschedulars in the database
        List<Alarmtemplateschedulars> alarmtemplateschedulars = alarmtemplateschedularsRepository.findAll();
        assertThat(alarmtemplateschedulars).hasSize(databaseSizeBeforeCreate + 1);
        Alarmtemplateschedulars testAlarmtemplateschedulars = alarmtemplateschedulars.get(alarmtemplateschedulars.size() - 1);
        assertThat(testAlarmtemplateschedulars.getDay()).isEqualTo(DEFAULT_DAY);
        assertThat(testAlarmtemplateschedulars.getStarttime()).isEqualTo(DEFAULT_STARTTIME);
        assertThat(testAlarmtemplateschedulars.getEndtime()).isEqualTo(DEFAULT_ENDTIME);

        // Validate the Alarmtemplateschedulars in ElasticSearch
        Alarmtemplateschedulars alarmtemplateschedularsEs = alarmtemplateschedularsSearchRepository.findOne(testAlarmtemplateschedulars.getId());
        assertThat(alarmtemplateschedularsEs).isEqualToComparingFieldByField(testAlarmtemplateschedulars);
    }

    @Test
    @Transactional
    public void getAllAlarmtemplateschedulars() throws Exception {
        // Initialize the database
        alarmtemplateschedularsRepository.saveAndFlush(alarmtemplateschedulars);

        // Get all the alarmtemplateschedulars
        restAlarmtemplateschedularsMockMvc.perform(get("/api/alarmtemplateschedulars?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(alarmtemplateschedulars.getId().intValue())))
                .andExpect(jsonPath("$.[*].day").value(hasItem(DEFAULT_DAY.toString())))
                .andExpect(jsonPath("$.[*].starttime").value(hasItem(DEFAULT_STARTTIME_STR)))
                .andExpect(jsonPath("$.[*].endtime").value(hasItem(DEFAULT_ENDTIME_STR)));
    }

    @Test
    @Transactional
    public void getAlarmtemplateschedulars() throws Exception {
        // Initialize the database
        alarmtemplateschedularsRepository.saveAndFlush(alarmtemplateschedulars);

        // Get the alarmtemplateschedulars
        restAlarmtemplateschedularsMockMvc.perform(get("/api/alarmtemplateschedulars/{id}", alarmtemplateschedulars.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(alarmtemplateschedulars.getId().intValue()))
            .andExpect(jsonPath("$.day").value(DEFAULT_DAY.toString()))
            .andExpect(jsonPath("$.starttime").value(DEFAULT_STARTTIME_STR))
            .andExpect(jsonPath("$.endtime").value(DEFAULT_ENDTIME_STR));
    }

    @Test
    @Transactional
    public void getNonExistingAlarmtemplateschedulars() throws Exception {
        // Get the alarmtemplateschedulars
        restAlarmtemplateschedularsMockMvc.perform(get("/api/alarmtemplateschedulars/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAlarmtemplateschedulars() throws Exception {
        // Initialize the database
        alarmtemplateschedularsRepository.saveAndFlush(alarmtemplateschedulars);
        alarmtemplateschedularsSearchRepository.save(alarmtemplateschedulars);
        int databaseSizeBeforeUpdate = alarmtemplateschedularsRepository.findAll().size();

        // Update the alarmtemplateschedulars
        Alarmtemplateschedulars updatedAlarmtemplateschedulars = alarmtemplateschedularsRepository.findOne(alarmtemplateschedulars.getId());
        updatedAlarmtemplateschedulars
                .day(UPDATED_DAY)
                .starttime(UPDATED_STARTTIME)
                .endtime(UPDATED_ENDTIME);
        AlarmtemplateschedularsDTO alarmtemplateschedularsDTO = alarmtemplateschedularsMapper.alarmtemplateschedularsToAlarmtemplateschedularsDTO(updatedAlarmtemplateschedulars);

        restAlarmtemplateschedularsMockMvc.perform(put("/api/alarmtemplateschedulars")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(alarmtemplateschedularsDTO)))
                .andExpect(status().isOk());

        // Validate the Alarmtemplateschedulars in the database
        List<Alarmtemplateschedulars> alarmtemplateschedulars = alarmtemplateschedularsRepository.findAll();
        assertThat(alarmtemplateschedulars).hasSize(databaseSizeBeforeUpdate);
        Alarmtemplateschedulars testAlarmtemplateschedulars = alarmtemplateschedulars.get(alarmtemplateschedulars.size() - 1);
        assertThat(testAlarmtemplateschedulars.getDay()).isEqualTo(UPDATED_DAY);
        assertThat(testAlarmtemplateschedulars.getStarttime()).isEqualTo(UPDATED_STARTTIME);
        assertThat(testAlarmtemplateschedulars.getEndtime()).isEqualTo(UPDATED_ENDTIME);

        // Validate the Alarmtemplateschedulars in ElasticSearch
        Alarmtemplateschedulars alarmtemplateschedularsEs = alarmtemplateschedularsSearchRepository.findOne(testAlarmtemplateschedulars.getId());
        assertThat(alarmtemplateschedularsEs).isEqualToComparingFieldByField(testAlarmtemplateschedulars);
    }

    @Test
    @Transactional
    public void deleteAlarmtemplateschedulars() throws Exception {
        // Initialize the database
        alarmtemplateschedularsRepository.saveAndFlush(alarmtemplateschedulars);
        alarmtemplateschedularsSearchRepository.save(alarmtemplateschedulars);
        int databaseSizeBeforeDelete = alarmtemplateschedularsRepository.findAll().size();

        // Get the alarmtemplateschedulars
        restAlarmtemplateschedularsMockMvc.perform(delete("/api/alarmtemplateschedulars/{id}", alarmtemplateschedulars.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean alarmtemplateschedularsExistsInEs = alarmtemplateschedularsSearchRepository.exists(alarmtemplateschedulars.getId());
        assertThat(alarmtemplateschedularsExistsInEs).isFalse();

        // Validate the database is empty
        List<Alarmtemplateschedulars> alarmtemplateschedulars = alarmtemplateschedularsRepository.findAll();
        assertThat(alarmtemplateschedulars).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAlarmtemplateschedulars() throws Exception {
        // Initialize the database
        alarmtemplateschedularsRepository.saveAndFlush(alarmtemplateschedulars);
        alarmtemplateschedularsSearchRepository.save(alarmtemplateschedulars);

        // Search the alarmtemplateschedulars
        restAlarmtemplateschedularsMockMvc.perform(get("/api/_search/alarmtemplateschedulars?query=id:" + alarmtemplateschedulars.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(alarmtemplateschedulars.getId().intValue())))
            .andExpect(jsonPath("$.[*].day").value(hasItem(DEFAULT_DAY.toString())))
            .andExpect(jsonPath("$.[*].starttime").value(hasItem(DEFAULT_STARTTIME_STR)))
            .andExpect(jsonPath("$.[*].endtime").value(hasItem(DEFAULT_ENDTIME_STR)));
    }
}
