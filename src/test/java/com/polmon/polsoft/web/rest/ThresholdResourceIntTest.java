package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Threshold;
import com.polmon.polsoft.repository.ThresholdRepository;
import com.polmon.polsoft.service.ThresholdService;
import com.polmon.polsoft.repository.search.ThresholdSearchRepository;
import com.polmon.polsoft.service.dto.ThresholdDTO;
import com.polmon.polsoft.service.mapper.ThresholdMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ThresholdResource REST controller.
 *
 * @see ThresholdResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class ThresholdResourceIntTest {
    private static final String DEFAULT_THRESHOLD_NAME = "AAAAA";
    private static final String UPDATED_THRESHOLD_NAME = "BBBBB";
    private static final String DEFAULT_PARAMETER = "AAAAA";
    private static final String UPDATED_PARAMETER = "BBBBB";

    private static final Double DEFAULT_VALUE = 1D;
    private static final Double UPDATED_VALUE = 2D;
    private static final String DEFAULT_SEVERITY = "AAAAA";
    private static final String UPDATED_SEVERITY = "BBBBB";

    private static final Float DEFAULT_LOWER_LIMIT_ALARM = 1F;
    private static final Float UPDATED_LOWER_LIMIT_ALARM = 2F;

    private static final Float DEFAULT_UPPER_LIMIT_ALARM = 1F;
    private static final Float UPDATED_UPPER_LIMIT_ALARM = 2F;

    private static final Float DEFAULT_LOWER_LIMIT_WARNING = 1F;
    private static final Float UPDATED_LOWER_LIMIT_WARNING = 2F;

    private static final Float DEFAULT_UPPER_LIMIT_WARNING = 1F;
    private static final Float UPDATED_UPPER_LIMIT_WARNING = 2F;

    private static final Boolean DEFAULT_WARNING_ENABLED = false;
    private static final Boolean UPDATED_WARNING_ENABLED = true;

    private static final Boolean DEFAULT_ALARM_ENABLED = false;
    private static final Boolean UPDATED_ALARM_ENABLED = true;

    private static final Float DEFAULT_WARNING_HYSTERESIS = 1F;
    private static final Float UPDATED_WARNING_HYSTERESIS = 2F;

    private static final Float DEFAULT_ALARM_HYSTERESIS = 1F;
    private static final Float UPDATED_ALARM_HYSTERESIS = 2F;

    private static final Float DEFAULT_ALARM_DELAY = 1F;
    private static final Float UPDATED_ALARM_DELAY = 2F;

    private static final Float DEFAULT_WARNING_DELAY = 1F;
    private static final Float UPDATED_WARNING_DELAY = 2F;

    @Inject
    private ThresholdRepository thresholdRepository;

    @Inject
    private ThresholdMapper thresholdMapper;

    @Inject
    private ThresholdService thresholdService;

    @Inject
    private ThresholdSearchRepository thresholdSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restThresholdMockMvc;

    private Threshold threshold;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ThresholdResource thresholdResource = new ThresholdResource();
        ReflectionTestUtils.setField(thresholdResource, "thresholdService", thresholdService);
        this.restThresholdMockMvc = MockMvcBuilders.standaloneSetup(thresholdResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Threshold createEntity(EntityManager em) {
        Threshold threshold = new Threshold();
        threshold = new Threshold()
                .thresholdName(DEFAULT_THRESHOLD_NAME)
                .parameter(DEFAULT_PARAMETER)
                .value(DEFAULT_VALUE)
                .severity(DEFAULT_SEVERITY)
                .lowerLimitAlarm(DEFAULT_LOWER_LIMIT_ALARM)
                .upperLimitAlarm(DEFAULT_UPPER_LIMIT_ALARM)
                .lowerLimitWarning(DEFAULT_LOWER_LIMIT_WARNING)
                .upperLimitWarning(DEFAULT_UPPER_LIMIT_WARNING)
                .warningEnabled(DEFAULT_WARNING_ENABLED)
                .alarmEnabled(DEFAULT_ALARM_ENABLED)
                .warningHysteresis(DEFAULT_WARNING_HYSTERESIS)
                .alarmHysteresis(DEFAULT_ALARM_HYSTERESIS)
                .alarmDelay(DEFAULT_ALARM_DELAY)
                .warningDelay(DEFAULT_WARNING_DELAY);
        return threshold;
    }

    @Before
    public void initTest() {
        thresholdSearchRepository.deleteAll();
        threshold = createEntity(em);
    }

    @Test
    @Transactional
    public void createThreshold() throws Exception {
        int databaseSizeBeforeCreate = thresholdRepository.findAll().size();

        // Create the Threshold
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isCreated());

        // Validate the Threshold in the database
        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeCreate + 1);
        Threshold testThreshold = thresholds.get(thresholds.size() - 1);
        assertThat(testThreshold.getThresholdName()).isEqualTo(DEFAULT_THRESHOLD_NAME);
        assertThat(testThreshold.getParameter()).isEqualTo(DEFAULT_PARAMETER);
        assertThat(testThreshold.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testThreshold.getSeverity()).isEqualTo(DEFAULT_SEVERITY);
        assertThat(testThreshold.getLowerLimitAlarm()).isEqualTo(DEFAULT_LOWER_LIMIT_ALARM);
        assertThat(testThreshold.getUpperLimitAlarm()).isEqualTo(DEFAULT_UPPER_LIMIT_ALARM);
        assertThat(testThreshold.getLowerLimitWarning()).isEqualTo(DEFAULT_LOWER_LIMIT_WARNING);
        assertThat(testThreshold.getUpperLimitWarning()).isEqualTo(DEFAULT_UPPER_LIMIT_WARNING);
        assertThat(testThreshold.isWarningEnabled()).isEqualTo(DEFAULT_WARNING_ENABLED);
        assertThat(testThreshold.isAlarmEnabled()).isEqualTo(DEFAULT_ALARM_ENABLED);
        assertThat(testThreshold.getWarningHysteresis()).isEqualTo(DEFAULT_WARNING_HYSTERESIS);
        assertThat(testThreshold.getAlarmHysteresis()).isEqualTo(DEFAULT_ALARM_HYSTERESIS);
        assertThat(testThreshold.getAlarmDelay()).isEqualTo(DEFAULT_ALARM_DELAY);
        assertThat(testThreshold.getWarningDelay()).isEqualTo(DEFAULT_WARNING_DELAY);

        // Validate the Threshold in ElasticSearch
        Threshold thresholdEs = thresholdSearchRepository.findOne(testThreshold.getId());
        assertThat(thresholdEs).isEqualToComparingFieldByField(testThreshold);
    }

    @Test
    @Transactional
    public void checkThresholdNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setThresholdName(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkParameterIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setParameter(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setValue(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSeverityIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setSeverity(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLowerLimitAlarmIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setLowerLimitAlarm(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpperLimitAlarmIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setUpperLimitAlarm(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLowerLimitWarningIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setLowerLimitWarning(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpperLimitWarningIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setUpperLimitWarning(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkWarningEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setWarningEnabled(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAlarmEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setAlarmEnabled(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkWarningHysteresisIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setWarningHysteresis(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAlarmHysteresisIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setAlarmHysteresis(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAlarmDelayIsRequired() throws Exception {
        int databaseSizeBeforeTest = thresholdRepository.findAll().size();
        // set the field null
        threshold.setAlarmDelay(null);

        // Create the Threshold, which fails.
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);

        restThresholdMockMvc.perform(post("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isBadRequest());

        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllThresholds() throws Exception {
        // Initialize the database
        thresholdRepository.saveAndFlush(threshold);

        // Get all the thresholds
        restThresholdMockMvc.perform(get("/api/thresholds?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(threshold.getId().intValue())))
                .andExpect(jsonPath("$.[*].thresholdName").value(hasItem(DEFAULT_THRESHOLD_NAME.toString())))
                .andExpect(jsonPath("$.[*].parameter").value(hasItem(DEFAULT_PARAMETER.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.doubleValue())))
                .andExpect(jsonPath("$.[*].severity").value(hasItem(DEFAULT_SEVERITY.toString())))
                .andExpect(jsonPath("$.[*].lowerLimitAlarm").value(hasItem(DEFAULT_LOWER_LIMIT_ALARM.doubleValue())))
                .andExpect(jsonPath("$.[*].upperLimitAlarm").value(hasItem(DEFAULT_UPPER_LIMIT_ALARM.doubleValue())))
                .andExpect(jsonPath("$.[*].lowerLimitWarning").value(hasItem(DEFAULT_LOWER_LIMIT_WARNING.doubleValue())))
                .andExpect(jsonPath("$.[*].upperLimitWarning").value(hasItem(DEFAULT_UPPER_LIMIT_WARNING.doubleValue())))
                .andExpect(jsonPath("$.[*].warningEnabled").value(hasItem(DEFAULT_WARNING_ENABLED.booleanValue())))
                .andExpect(jsonPath("$.[*].alarmEnabled").value(hasItem(DEFAULT_ALARM_ENABLED.booleanValue())))
                .andExpect(jsonPath("$.[*].warningHysteresis").value(hasItem(DEFAULT_WARNING_HYSTERESIS.doubleValue())))
                .andExpect(jsonPath("$.[*].alarmHysteresis").value(hasItem(DEFAULT_ALARM_HYSTERESIS.doubleValue())))
                .andExpect(jsonPath("$.[*].alarmDelay").value(hasItem(DEFAULT_ALARM_DELAY.doubleValue())))
                .andExpect(jsonPath("$.[*].warningDelay").value(hasItem(DEFAULT_WARNING_DELAY.doubleValue())));
    }

    @Test
    @Transactional
    public void getThreshold() throws Exception {
        // Initialize the database
        thresholdRepository.saveAndFlush(threshold);

        // Get the threshold
        restThresholdMockMvc.perform(get("/api/thresholds/{id}", threshold.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(threshold.getId().intValue()))
            .andExpect(jsonPath("$.thresholdName").value(DEFAULT_THRESHOLD_NAME.toString()))
            .andExpect(jsonPath("$.parameter").value(DEFAULT_PARAMETER.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.doubleValue()))
            .andExpect(jsonPath("$.severity").value(DEFAULT_SEVERITY.toString()))
            .andExpect(jsonPath("$.lowerLimitAlarm").value(DEFAULT_LOWER_LIMIT_ALARM.doubleValue()))
            .andExpect(jsonPath("$.upperLimitAlarm").value(DEFAULT_UPPER_LIMIT_ALARM.doubleValue()))
            .andExpect(jsonPath("$.lowerLimitWarning").value(DEFAULT_LOWER_LIMIT_WARNING.doubleValue()))
            .andExpect(jsonPath("$.upperLimitWarning").value(DEFAULT_UPPER_LIMIT_WARNING.doubleValue()))
            .andExpect(jsonPath("$.warningEnabled").value(DEFAULT_WARNING_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.alarmEnabled").value(DEFAULT_ALARM_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.warningHysteresis").value(DEFAULT_WARNING_HYSTERESIS.doubleValue()))
            .andExpect(jsonPath("$.alarmHysteresis").value(DEFAULT_ALARM_HYSTERESIS.doubleValue()))
            .andExpect(jsonPath("$.alarmDelay").value(DEFAULT_ALARM_DELAY.doubleValue()))
            .andExpect(jsonPath("$.warningDelay").value(DEFAULT_WARNING_DELAY.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingThreshold() throws Exception {
        // Get the threshold
        restThresholdMockMvc.perform(get("/api/thresholds/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateThreshold() throws Exception {
        // Initialize the database
        thresholdRepository.saveAndFlush(threshold);
        thresholdSearchRepository.save(threshold);
        int databaseSizeBeforeUpdate = thresholdRepository.findAll().size();

        // Update the threshold
        Threshold updatedThreshold = thresholdRepository.findOne(threshold.getId());
        updatedThreshold
                .thresholdName(UPDATED_THRESHOLD_NAME)
                .parameter(UPDATED_PARAMETER)
                .value(UPDATED_VALUE)
                .severity(UPDATED_SEVERITY)
                .lowerLimitAlarm(UPDATED_LOWER_LIMIT_ALARM)
                .upperLimitAlarm(UPDATED_UPPER_LIMIT_ALARM)
                .lowerLimitWarning(UPDATED_LOWER_LIMIT_WARNING)
                .upperLimitWarning(UPDATED_UPPER_LIMIT_WARNING)
                .warningEnabled(UPDATED_WARNING_ENABLED)
                .alarmEnabled(UPDATED_ALARM_ENABLED)
                .warningHysteresis(UPDATED_WARNING_HYSTERESIS)
                .alarmHysteresis(UPDATED_ALARM_HYSTERESIS)
                .alarmDelay(UPDATED_ALARM_DELAY)
                .warningDelay(UPDATED_WARNING_DELAY);
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(updatedThreshold);

        restThresholdMockMvc.perform(put("/api/thresholds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(thresholdDTO)))
                .andExpect(status().isOk());

        // Validate the Threshold in the database
        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeUpdate);
        Threshold testThreshold = thresholds.get(thresholds.size() - 1);
        assertThat(testThreshold.getThresholdName()).isEqualTo(UPDATED_THRESHOLD_NAME);
        assertThat(testThreshold.getParameter()).isEqualTo(UPDATED_PARAMETER);
        assertThat(testThreshold.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testThreshold.getSeverity()).isEqualTo(UPDATED_SEVERITY);
        assertThat(testThreshold.getLowerLimitAlarm()).isEqualTo(UPDATED_LOWER_LIMIT_ALARM);
        assertThat(testThreshold.getUpperLimitAlarm()).isEqualTo(UPDATED_UPPER_LIMIT_ALARM);
        assertThat(testThreshold.getLowerLimitWarning()).isEqualTo(UPDATED_LOWER_LIMIT_WARNING);
        assertThat(testThreshold.getUpperLimitWarning()).isEqualTo(UPDATED_UPPER_LIMIT_WARNING);
        assertThat(testThreshold.isWarningEnabled()).isEqualTo(UPDATED_WARNING_ENABLED);
        assertThat(testThreshold.isAlarmEnabled()).isEqualTo(UPDATED_ALARM_ENABLED);
        assertThat(testThreshold.getWarningHysteresis()).isEqualTo(UPDATED_WARNING_HYSTERESIS);
        assertThat(testThreshold.getAlarmHysteresis()).isEqualTo(UPDATED_ALARM_HYSTERESIS);
        assertThat(testThreshold.getAlarmDelay()).isEqualTo(UPDATED_ALARM_DELAY);
        assertThat(testThreshold.getWarningDelay()).isEqualTo(UPDATED_WARNING_DELAY);

        // Validate the Threshold in ElasticSearch
        Threshold thresholdEs = thresholdSearchRepository.findOne(testThreshold.getId());
        assertThat(thresholdEs).isEqualToComparingFieldByField(testThreshold);
    }

    @Test
    @Transactional
    public void deleteThreshold() throws Exception {
        // Initialize the database
        thresholdRepository.saveAndFlush(threshold);
        thresholdSearchRepository.save(threshold);
        int databaseSizeBeforeDelete = thresholdRepository.findAll().size();

        // Get the threshold
        restThresholdMockMvc.perform(delete("/api/thresholds/{id}", threshold.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean thresholdExistsInEs = thresholdSearchRepository.exists(threshold.getId());
        assertThat(thresholdExistsInEs).isFalse();

        // Validate the database is empty
        List<Threshold> thresholds = thresholdRepository.findAll();
        assertThat(thresholds).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchThreshold() throws Exception {
        // Initialize the database
        thresholdRepository.saveAndFlush(threshold);
        thresholdSearchRepository.save(threshold);

        // Search the threshold
        restThresholdMockMvc.perform(get("/api/_search/thresholds?query=id:" + threshold.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(threshold.getId().intValue())))
            .andExpect(jsonPath("$.[*].thresholdName").value(hasItem(DEFAULT_THRESHOLD_NAME.toString())))
            .andExpect(jsonPath("$.[*].parameter").value(hasItem(DEFAULT_PARAMETER.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].severity").value(hasItem(DEFAULT_SEVERITY.toString())))
            .andExpect(jsonPath("$.[*].lowerLimitAlarm").value(hasItem(DEFAULT_LOWER_LIMIT_ALARM.doubleValue())))
            .andExpect(jsonPath("$.[*].upperLimitAlarm").value(hasItem(DEFAULT_UPPER_LIMIT_ALARM.doubleValue())))
            .andExpect(jsonPath("$.[*].lowerLimitWarning").value(hasItem(DEFAULT_LOWER_LIMIT_WARNING.doubleValue())))
            .andExpect(jsonPath("$.[*].upperLimitWarning").value(hasItem(DEFAULT_UPPER_LIMIT_WARNING.doubleValue())))
            .andExpect(jsonPath("$.[*].warningEnabled").value(hasItem(DEFAULT_WARNING_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].alarmEnabled").value(hasItem(DEFAULT_ALARM_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].warningHysteresis").value(hasItem(DEFAULT_WARNING_HYSTERESIS.doubleValue())))
            .andExpect(jsonPath("$.[*].alarmHysteresis").value(hasItem(DEFAULT_ALARM_HYSTERESIS.doubleValue())))
            .andExpect(jsonPath("$.[*].alarmDelay").value(hasItem(DEFAULT_ALARM_DELAY.doubleValue())))
            .andExpect(jsonPath("$.[*].warningDelay").value(hasItem(DEFAULT_WARNING_DELAY.doubleValue())));
    }
}
