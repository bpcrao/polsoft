package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Logindata;
import com.polmon.polsoft.repository.LogindataRepository;
import com.polmon.polsoft.service.LogindataService;
import com.polmon.polsoft.repository.search.LogindataSearchRepository;
import com.polmon.polsoft.service.dto.LogindataDTO;
import com.polmon.polsoft.service.mapper.LogindataMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LogindataResource REST controller.
 *
 * @see LogindataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class LogindataResourceIntTest {

    private static final Integer DEFAULT_FAILED_ATTEMPTS = 1;
    private static final Integer UPDATED_FAILED_ATTEMPTS = 2;

    private static final LocalDate DEFAULT_LAST_ACTIVE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_ACTIVE_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private LogindataRepository logindataRepository;

    @Inject
    private LogindataMapper logindataMapper;

    @Inject
    private LogindataService logindataService;

    @Inject
    private LogindataSearchRepository logindataSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restLogindataMockMvc;

    private Logindata logindata;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        LogindataResource logindataResource = new LogindataResource();
        ReflectionTestUtils.setField(logindataResource, "logindataService", logindataService);
        this.restLogindataMockMvc = MockMvcBuilders.standaloneSetup(logindataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Logindata createEntity(EntityManager em) {
        Logindata logindata = new Logindata();
        logindata = new Logindata()
                .failedAttempts(DEFAULT_FAILED_ATTEMPTS)
                .lastActiveDate(DEFAULT_LAST_ACTIVE_DATE);
        return logindata;
    }

    @Before
    public void initTest() {
        logindataSearchRepository.deleteAll();
        logindata = createEntity(em);
    }

    @Test
    @Transactional
    public void createLogindata() throws Exception {
        int databaseSizeBeforeCreate = logindataRepository.findAll().size();

        // Create the Logindata
        LogindataDTO logindataDTO = logindataMapper.logindataToLogindataDTO(logindata);

        restLogindataMockMvc.perform(post("/api/logindata")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(logindataDTO)))
                .andExpect(status().isCreated());

        // Validate the Logindata in the database
        List<Logindata> logindata = logindataRepository.findAll();
        assertThat(logindata).hasSize(databaseSizeBeforeCreate + 1);
        Logindata testLogindata = logindata.get(logindata.size() - 1);
        assertThat(testLogindata.getFailedAttempts()).isEqualTo(DEFAULT_FAILED_ATTEMPTS);
        assertThat(testLogindata.getLastActiveDate()).isEqualTo(DEFAULT_LAST_ACTIVE_DATE);

        // Validate the Logindata in ElasticSearch
        Logindata logindataEs = logindataSearchRepository.findOne(testLogindata.getId());
        assertThat(logindataEs).isEqualToComparingFieldByField(testLogindata);
    }

    @Test
    @Transactional
    public void getAllLogindata() throws Exception {
        // Initialize the database
        logindataRepository.saveAndFlush(logindata);

        // Get all the logindata
        restLogindataMockMvc.perform(get("/api/logindata?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(logindata.getId().intValue())))
                .andExpect(jsonPath("$.[*].failedAttempts").value(hasItem(DEFAULT_FAILED_ATTEMPTS)))
                .andExpect(jsonPath("$.[*].lastActiveDate").value(hasItem(DEFAULT_LAST_ACTIVE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getLogindata() throws Exception {
        // Initialize the database
        logindataRepository.saveAndFlush(logindata);

        // Get the logindata
        restLogindataMockMvc.perform(get("/api/logindata/{id}", logindata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(logindata.getId().intValue()))
            .andExpect(jsonPath("$.failedAttempts").value(DEFAULT_FAILED_ATTEMPTS))
            .andExpect(jsonPath("$.lastActiveDate").value(DEFAULT_LAST_ACTIVE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLogindata() throws Exception {
        // Get the logindata
        restLogindataMockMvc.perform(get("/api/logindata/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLogindata() throws Exception {
        // Initialize the database
        logindataRepository.saveAndFlush(logindata);
        logindataSearchRepository.save(logindata);
        int databaseSizeBeforeUpdate = logindataRepository.findAll().size();

        // Update the logindata
        Logindata updatedLogindata = logindataRepository.findOne(logindata.getId());
        updatedLogindata
                .failedAttempts(UPDATED_FAILED_ATTEMPTS)
                .lastActiveDate(UPDATED_LAST_ACTIVE_DATE);
        LogindataDTO logindataDTO = logindataMapper.logindataToLogindataDTO(updatedLogindata);

        restLogindataMockMvc.perform(put("/api/logindata")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(logindataDTO)))
                .andExpect(status().isOk());

        // Validate the Logindata in the database
        List<Logindata> logindata = logindataRepository.findAll();
        assertThat(logindata).hasSize(databaseSizeBeforeUpdate);
        Logindata testLogindata = logindata.get(logindata.size() - 1);
        assertThat(testLogindata.getFailedAttempts()).isEqualTo(UPDATED_FAILED_ATTEMPTS);
        assertThat(testLogindata.getLastActiveDate()).isEqualTo(UPDATED_LAST_ACTIVE_DATE);

        // Validate the Logindata in ElasticSearch
        Logindata logindataEs = logindataSearchRepository.findOne(testLogindata.getId());
        assertThat(logindataEs).isEqualToComparingFieldByField(testLogindata);
    }

    @Test
    @Transactional
    public void deleteLogindata() throws Exception {
        // Initialize the database
        logindataRepository.saveAndFlush(logindata);
        logindataSearchRepository.save(logindata);
        int databaseSizeBeforeDelete = logindataRepository.findAll().size();

        // Get the logindata
        restLogindataMockMvc.perform(delete("/api/logindata/{id}", logindata.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean logindataExistsInEs = logindataSearchRepository.exists(logindata.getId());
        assertThat(logindataExistsInEs).isFalse();

        // Validate the database is empty
        List<Logindata> logindata = logindataRepository.findAll();
        assertThat(logindata).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLogindata() throws Exception {
        // Initialize the database
        logindataRepository.saveAndFlush(logindata);
        logindataSearchRepository.save(logindata);

        // Search the logindata
        restLogindataMockMvc.perform(get("/api/_search/logindata?query=id:" + logindata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(logindata.getId().intValue())))
            .andExpect(jsonPath("$.[*].failedAttempts").value(hasItem(DEFAULT_FAILED_ATTEMPTS)))
            .andExpect(jsonPath("$.[*].lastActiveDate").value(hasItem(DEFAULT_LAST_ACTIVE_DATE.toString())));
    }
}
