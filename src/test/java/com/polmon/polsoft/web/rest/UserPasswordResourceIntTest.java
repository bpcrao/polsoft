package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.UserPassword;
import com.polmon.polsoft.repository.UserPasswordRepository;
import com.polmon.polsoft.service.UserPasswordService;
import com.polmon.polsoft.repository.search.UserPasswordSearchRepository;
import com.polmon.polsoft.service.dto.UserPasswordDTO;
import com.polmon.polsoft.service.mapper.UserPasswordMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserPasswordResource REST controller.
 *
 * @see UserPasswordResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class UserPasswordResourceIntTest {
    private static final String DEFAULT_PASSWORD = "AAAAA";
    private static final String UPDATED_PASSWORD = "BBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private UserPasswordRepository userPasswordRepository;

    @Inject
    private UserPasswordMapper userPasswordMapper;

    @Inject
    private UserPasswordService userPasswordService;

    @Inject
    private UserPasswordSearchRepository userPasswordSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restUserPasswordMockMvc;

    private UserPassword userPassword;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserPasswordResource userPasswordResource = new UserPasswordResource();
        ReflectionTestUtils.setField(userPasswordResource, "userPasswordService", userPasswordService);
        this.restUserPasswordMockMvc = MockMvcBuilders.standaloneSetup(userPasswordResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserPassword createEntity(EntityManager em) {
        UserPassword userPassword = new UserPassword();
        userPassword = new UserPassword()
                .password(DEFAULT_PASSWORD)
                .createdDate(DEFAULT_CREATED_DATE);
        return userPassword;
    }

    @Before
    public void initTest() {
        userPasswordSearchRepository.deleteAll();
        userPassword = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserPassword() throws Exception {
        int databaseSizeBeforeCreate = userPasswordRepository.findAll().size();

        // Create the UserPassword
        UserPasswordDTO userPasswordDTO = userPasswordMapper.userPasswordToUserPasswordDTO(userPassword);

        restUserPasswordMockMvc.perform(post("/api/user-passwords")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userPasswordDTO)))
                .andExpect(status().isCreated());

        // Validate the UserPassword in the database
        List<UserPassword> userPasswords = userPasswordRepository.findAll();
        assertThat(userPasswords).hasSize(databaseSizeBeforeCreate + 1);
        UserPassword testUserPassword = userPasswords.get(userPasswords.size() - 1);
        assertThat(testUserPassword.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testUserPassword.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);

        // Validate the UserPassword in ElasticSearch
        UserPassword userPasswordEs = userPasswordSearchRepository.findOne(testUserPassword.getId());
        assertThat(userPasswordEs).isEqualToComparingFieldByField(testUserPassword);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = userPasswordRepository.findAll().size();
        // set the field null
        userPassword.setPassword(null);

        // Create the UserPassword, which fails.
        UserPasswordDTO userPasswordDTO = userPasswordMapper.userPasswordToUserPasswordDTO(userPassword);

        restUserPasswordMockMvc.perform(post("/api/user-passwords")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userPasswordDTO)))
                .andExpect(status().isBadRequest());

        List<UserPassword> userPasswords = userPasswordRepository.findAll();
        assertThat(userPasswords).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserPasswords() throws Exception {
        // Initialize the database
        userPasswordRepository.saveAndFlush(userPassword);

        // Get all the userPasswords
        restUserPasswordMockMvc.perform(get("/api/user-passwords?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userPassword.getId().intValue())))
                .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
                .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getUserPassword() throws Exception {
        // Initialize the database
        userPasswordRepository.saveAndFlush(userPassword);

        // Get the userPassword
        restUserPasswordMockMvc.perform(get("/api/user-passwords/{id}", userPassword.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userPassword.getId().intValue()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserPassword() throws Exception {
        // Get the userPassword
        restUserPasswordMockMvc.perform(get("/api/user-passwords/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserPassword() throws Exception {
        // Initialize the database
        userPasswordRepository.saveAndFlush(userPassword);
        userPasswordSearchRepository.save(userPassword);
        int databaseSizeBeforeUpdate = userPasswordRepository.findAll().size();

        // Update the userPassword
        UserPassword updatedUserPassword = userPasswordRepository.findOne(userPassword.getId());
        updatedUserPassword
                .password(UPDATED_PASSWORD)
                .createdDate(UPDATED_CREATED_DATE);
        UserPasswordDTO userPasswordDTO = userPasswordMapper.userPasswordToUserPasswordDTO(updatedUserPassword);

        restUserPasswordMockMvc.perform(put("/api/user-passwords")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userPasswordDTO)))
                .andExpect(status().isOk());

        // Validate the UserPassword in the database
        List<UserPassword> userPasswords = userPasswordRepository.findAll();
        assertThat(userPasswords).hasSize(databaseSizeBeforeUpdate);
        UserPassword testUserPassword = userPasswords.get(userPasswords.size() - 1);
        assertThat(testUserPassword.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testUserPassword.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);

        // Validate the UserPassword in ElasticSearch
        UserPassword userPasswordEs = userPasswordSearchRepository.findOne(testUserPassword.getId());
        assertThat(userPasswordEs).isEqualToComparingFieldByField(testUserPassword);
    }

    @Test
    @Transactional
    public void deleteUserPassword() throws Exception {
        // Initialize the database
        userPasswordRepository.saveAndFlush(userPassword);
        userPasswordSearchRepository.save(userPassword);
        int databaseSizeBeforeDelete = userPasswordRepository.findAll().size();

        // Get the userPassword
        restUserPasswordMockMvc.perform(delete("/api/user-passwords/{id}", userPassword.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean userPasswordExistsInEs = userPasswordSearchRepository.exists(userPassword.getId());
        assertThat(userPasswordExistsInEs).isFalse();

        // Validate the database is empty
        List<UserPassword> userPasswords = userPasswordRepository.findAll();
        assertThat(userPasswords).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUserPassword() throws Exception {
        // Initialize the database
        userPasswordRepository.saveAndFlush(userPassword);
        userPasswordSearchRepository.save(userPassword);

        // Search the userPassword
        restUserPasswordMockMvc.perform(get("/api/_search/user-passwords?query=id:" + userPassword.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userPassword.getId().intValue())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())));
    }
}
