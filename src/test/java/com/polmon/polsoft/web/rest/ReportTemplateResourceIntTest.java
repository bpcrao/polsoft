package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.ReportTemplate;
import com.polmon.polsoft.repository.ReportTemplateRepository;
import com.polmon.polsoft.service.ReportTemplateService;
import com.polmon.polsoft.repository.search.ReportTemplateSearchRepository;
import com.polmon.polsoft.service.dto.ReportTemplateDTO;
import com.polmon.polsoft.service.mapper.ReportTemplateMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReportTemplateResource REST controller.
 *
 * @see ReportTemplateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class ReportTemplateResourceIntTest {
    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_SETTINGS = "AAAAA";
    private static final String UPDATED_SETTINGS = "BBBBB";

    @Inject
    private ReportTemplateRepository reportTemplateRepository;

    @Inject
    private ReportTemplateMapper reportTemplateMapper;

    @Inject
    private ReportTemplateService reportTemplateService;

    @Inject
    private ReportTemplateSearchRepository reportTemplateSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restReportTemplateMockMvc;

    private ReportTemplate reportTemplate;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReportTemplateResource reportTemplateResource = new ReportTemplateResource();
        ReflectionTestUtils.setField(reportTemplateResource, "reportTemplateService", reportTemplateService);
        this.restReportTemplateMockMvc = MockMvcBuilders.standaloneSetup(reportTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReportTemplate createEntity(EntityManager em) {
        ReportTemplate reportTemplate = new ReportTemplate();
        reportTemplate = new ReportTemplate()
                .name(DEFAULT_NAME)
                .settings(DEFAULT_SETTINGS);
        return reportTemplate;
    }

    @Before
    public void initTest() {
        reportTemplateSearchRepository.deleteAll();
        reportTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createReportTemplate() throws Exception {
        int databaseSizeBeforeCreate = reportTemplateRepository.findAll().size();

        // Create the ReportTemplate
        ReportTemplateDTO reportTemplateDTO = reportTemplateMapper.reportTemplateToReportTemplateDTO(reportTemplate);

        restReportTemplateMockMvc.perform(post("/api/report-templates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(reportTemplateDTO)))
                .andExpect(status().isCreated());

        // Validate the ReportTemplate in the database
        List<ReportTemplate> reportTemplates = reportTemplateRepository.findAll();
        assertThat(reportTemplates).hasSize(databaseSizeBeforeCreate + 1);
        ReportTemplate testReportTemplate = reportTemplates.get(reportTemplates.size() - 1);
        assertThat(testReportTemplate.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testReportTemplate.getSettings()).isEqualTo(DEFAULT_SETTINGS);

        // Validate the ReportTemplate in ElasticSearch
        ReportTemplate reportTemplateEs = reportTemplateSearchRepository.findOne(testReportTemplate.getId());
        assertThat(reportTemplateEs).isEqualToComparingFieldByField(testReportTemplate);
    }

    @Test
    @Transactional
    public void getAllReportTemplates() throws Exception {
        // Initialize the database
        reportTemplateRepository.saveAndFlush(reportTemplate);

        // Get all the reportTemplates
        restReportTemplateMockMvc.perform(get("/api/report-templates?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(reportTemplate.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].settings").value(hasItem(DEFAULT_SETTINGS.toString())));
    }

    @Test
    @Transactional
    public void getReportTemplate() throws Exception {
        // Initialize the database
        reportTemplateRepository.saveAndFlush(reportTemplate);

        // Get the reportTemplate
        restReportTemplateMockMvc.perform(get("/api/report-templates/{id}", reportTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(reportTemplate.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.settings").value(DEFAULT_SETTINGS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingReportTemplate() throws Exception {
        // Get the reportTemplate
        restReportTemplateMockMvc.perform(get("/api/report-templates/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReportTemplate() throws Exception {
        // Initialize the database
        reportTemplateRepository.saveAndFlush(reportTemplate);
        reportTemplateSearchRepository.save(reportTemplate);
        int databaseSizeBeforeUpdate = reportTemplateRepository.findAll().size();

        // Update the reportTemplate
        ReportTemplate updatedReportTemplate = reportTemplateRepository.findOne(reportTemplate.getId());
        updatedReportTemplate
                .name(UPDATED_NAME)
                .settings(UPDATED_SETTINGS);
        ReportTemplateDTO reportTemplateDTO = reportTemplateMapper.reportTemplateToReportTemplateDTO(updatedReportTemplate);

        restReportTemplateMockMvc.perform(put("/api/report-templates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(reportTemplateDTO)))
                .andExpect(status().isOk());

        // Validate the ReportTemplate in the database
        List<ReportTemplate> reportTemplates = reportTemplateRepository.findAll();
        assertThat(reportTemplates).hasSize(databaseSizeBeforeUpdate);
        ReportTemplate testReportTemplate = reportTemplates.get(reportTemplates.size() - 1);
        assertThat(testReportTemplate.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testReportTemplate.getSettings()).isEqualTo(UPDATED_SETTINGS);

        // Validate the ReportTemplate in ElasticSearch
        ReportTemplate reportTemplateEs = reportTemplateSearchRepository.findOne(testReportTemplate.getId());
        assertThat(reportTemplateEs).isEqualToComparingFieldByField(testReportTemplate);
    }

    @Test
    @Transactional
    public void deleteReportTemplate() throws Exception {
        // Initialize the database
        reportTemplateRepository.saveAndFlush(reportTemplate);
        reportTemplateSearchRepository.save(reportTemplate);
        int databaseSizeBeforeDelete = reportTemplateRepository.findAll().size();

        // Get the reportTemplate
        restReportTemplateMockMvc.perform(delete("/api/report-templates/{id}", reportTemplate.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean reportTemplateExistsInEs = reportTemplateSearchRepository.exists(reportTemplate.getId());
        assertThat(reportTemplateExistsInEs).isFalse();

        // Validate the database is empty
        List<ReportTemplate> reportTemplates = reportTemplateRepository.findAll();
        assertThat(reportTemplates).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchReportTemplate() throws Exception {
        // Initialize the database
        reportTemplateRepository.saveAndFlush(reportTemplate);
        reportTemplateSearchRepository.save(reportTemplate);

        // Search the reportTemplate
        restReportTemplateMockMvc.perform(get("/api/_search/report-templates?query=id:" + reportTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reportTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].settings").value(hasItem(DEFAULT_SETTINGS.toString())));
    }
}
