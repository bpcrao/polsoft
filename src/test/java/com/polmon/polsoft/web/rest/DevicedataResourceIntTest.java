package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Devicedata;
import com.polmon.polsoft.repository.DevicedataRepository;
import com.polmon.polsoft.service.DevicedataService;
import com.polmon.polsoft.repository.search.DevicedataSearchRepository;
import com.polmon.polsoft.service.dto.DevicedataDTO;
import com.polmon.polsoft.service.mapper.DevicedataMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DevicedataResource REST controller.
 *
 * @see DevicedataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class DevicedataResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final Long DEFAULT_DEVICEID = 1L;
    private static final Long UPDATED_DEVICEID = 2L;
    private static final String DEFAULT_CHANNEL_DATA = "AAAAA";
    private static final String UPDATED_CHANNEL_DATA = "BBBBB";

    private static final ZonedDateTime DEFAULT_TIME_COLLECTED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_TIME_COLLECTED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TIME_COLLECTED_STR = dateTimeFormatter.format(DEFAULT_TIME_COLLECTED);

    @Inject
    private DevicedataRepository devicedataRepository;

    @Inject
    private DevicedataMapper devicedataMapper;

    @Inject
    private DevicedataService devicedataService;

    @Inject
    private DevicedataSearchRepository devicedataSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDevicedataMockMvc;

    private Devicedata devicedata;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DevicedataResource devicedataResource = new DevicedataResource();
        ReflectionTestUtils.setField(devicedataResource, "devicedataService", devicedataService);
        this.restDevicedataMockMvc = MockMvcBuilders.standaloneSetup(devicedataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Devicedata createEntity(EntityManager em) {
        Devicedata devicedata = new Devicedata();
        devicedata = new Devicedata()
                .deviceid(DEFAULT_DEVICEID)
                .channelData(DEFAULT_CHANNEL_DATA)
                .timeCollected(DEFAULT_TIME_COLLECTED);
        return devicedata;
    }

    @Before
    public void initTest() {
        devicedataSearchRepository.deleteAll();
        devicedata = createEntity(em);
    }

    @Test
    @Transactional
    public void createDevicedata() throws Exception {
        int databaseSizeBeforeCreate = devicedataRepository.findAll().size();

        // Create the Devicedata
        DevicedataDTO devicedataDTO = devicedataMapper.devicedataToDevicedataDTO(devicedata);

        restDevicedataMockMvc.perform(post("/api/devicedata")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(devicedataDTO)))
                .andExpect(status().isCreated());

        // Validate the Devicedata in the database
        List<Devicedata> devicedata = devicedataRepository.findAll();
        assertThat(devicedata).hasSize(databaseSizeBeforeCreate + 1);
        Devicedata testDevicedata = devicedata.get(devicedata.size() - 1);
        assertThat(testDevicedata.getDeviceid()).isEqualTo(DEFAULT_DEVICEID);
        assertThat(testDevicedata.getChannelData()).isEqualTo(DEFAULT_CHANNEL_DATA);
        assertThat(testDevicedata.getTimeCollected()).isEqualTo(DEFAULT_TIME_COLLECTED);

        // Validate the Devicedata in ElasticSearch
        Devicedata devicedataEs = devicedataSearchRepository.findOne(testDevicedata.getId());
        assertThat(devicedataEs).isEqualToComparingFieldByField(testDevicedata);
    }

    @Test
    @Transactional
    public void getAllDevicedata() throws Exception {
        // Initialize the database
        devicedataRepository.saveAndFlush(devicedata);

        // Get all the devicedata
        restDevicedataMockMvc.perform(get("/api/devicedata?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(devicedata.getId().intValue())))
                .andExpect(jsonPath("$.[*].deviceid").value(hasItem(DEFAULT_DEVICEID.intValue())))
                .andExpect(jsonPath("$.[*].channelData").value(hasItem(DEFAULT_CHANNEL_DATA.toString())))
                .andExpect(jsonPath("$.[*].timeCollected").value(hasItem(DEFAULT_TIME_COLLECTED_STR)));
    }

    @Test
    @Transactional
    public void getDevicedata() throws Exception {
        // Initialize the database
        devicedataRepository.saveAndFlush(devicedata);

        // Get the devicedata
        restDevicedataMockMvc.perform(get("/api/devicedata/{id}", devicedata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(devicedata.getId().intValue()))
            .andExpect(jsonPath("$.deviceid").value(DEFAULT_DEVICEID.intValue()))
            .andExpect(jsonPath("$.channelData").value(DEFAULT_CHANNEL_DATA.toString()))
            .andExpect(jsonPath("$.timeCollected").value(DEFAULT_TIME_COLLECTED_STR));
    }

    @Test
    @Transactional
    public void getNonExistingDevicedata() throws Exception {
        // Get the devicedata
        restDevicedataMockMvc.perform(get("/api/devicedata/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDevicedata() throws Exception {
        // Initialize the database
        devicedataRepository.saveAndFlush(devicedata);
        devicedataSearchRepository.save(devicedata);
        int databaseSizeBeforeUpdate = devicedataRepository.findAll().size();

        // Update the devicedata
        Devicedata updatedDevicedata = devicedataRepository.findOne(devicedata.getId());
        updatedDevicedata
                .deviceid(UPDATED_DEVICEID)
                .channelData(UPDATED_CHANNEL_DATA)
                .timeCollected(UPDATED_TIME_COLLECTED);
        DevicedataDTO devicedataDTO = devicedataMapper.devicedataToDevicedataDTO(updatedDevicedata);

        restDevicedataMockMvc.perform(put("/api/devicedata")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(devicedataDTO)))
                .andExpect(status().isOk());

        // Validate the Devicedata in the database
        List<Devicedata> devicedata = devicedataRepository.findAll();
        assertThat(devicedata).hasSize(databaseSizeBeforeUpdate);
        Devicedata testDevicedata = devicedata.get(devicedata.size() - 1);
        assertThat(testDevicedata.getDeviceid()).isEqualTo(UPDATED_DEVICEID);
        assertThat(testDevicedata.getChannelData()).isEqualTo(UPDATED_CHANNEL_DATA);
        assertThat(testDevicedata.getTimeCollected()).isEqualTo(UPDATED_TIME_COLLECTED);

        // Validate the Devicedata in ElasticSearch
        Devicedata devicedataEs = devicedataSearchRepository.findOne(testDevicedata.getId());
        assertThat(devicedataEs).isEqualToComparingFieldByField(testDevicedata);
    }

    @Test
    @Transactional
    public void deleteDevicedata() throws Exception {
        // Initialize the database
        devicedataRepository.saveAndFlush(devicedata);
        devicedataSearchRepository.save(devicedata);
        int databaseSizeBeforeDelete = devicedataRepository.findAll().size();

        // Get the devicedata
        restDevicedataMockMvc.perform(delete("/api/devicedata/{id}", devicedata.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean devicedataExistsInEs = devicedataSearchRepository.exists(devicedata.getId());
        assertThat(devicedataExistsInEs).isFalse();

        // Validate the database is empty
        List<Devicedata> devicedata = devicedataRepository.findAll();
        assertThat(devicedata).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDevicedata() throws Exception {
        // Initialize the database
        devicedataRepository.saveAndFlush(devicedata);
        devicedataSearchRepository.save(devicedata);

        // Search the devicedata
        restDevicedataMockMvc.perform(get("/api/_search/devicedata?query=id:" + devicedata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(devicedata.getId().intValue())))
            .andExpect(jsonPath("$.[*].deviceid").value(hasItem(DEFAULT_DEVICEID.intValue())))
            .andExpect(jsonPath("$.[*].channelData").value(hasItem(DEFAULT_CHANNEL_DATA.toString())))
            .andExpect(jsonPath("$.[*].timeCollected").value(hasItem(DEFAULT_TIME_COLLECTED_STR)));
    }
}
