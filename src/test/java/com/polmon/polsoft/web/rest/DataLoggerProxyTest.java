package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.repository.UserRepository;
import com.polmon.polsoft.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserResource
 */

public class DataLoggerProxyTest {



    @Before
    public void setup() {
    }
//
//    @Test
//    public void testGetExistingUser() throws Exception {
//    	DataLoggerProxy dataLoggerProxy = new DataLoggerProxy();
//    	dataLoggerProxy.parseData("DL122M-X-YYMM-XXXX,CH1= 9999.99,CH2= 9999.99,CH3=ERR1,CH4=ERR2,CH5= 9999.99,CH6= 9999.99,CH7= 9999.99,CH8= 9999.99,CH9= 9999.99,CH10= 9999.99,CH11= 9999.99,CH12= 9999.99,CH13= 9999.99,CH14= 9999.99,CH15= 9999.99,CH16= 9999.99,CH17= 9999.99,CH18= 9999.99,CH19= 9999.99,CH20= 9999.99,CH21= 9999.99,CH22= 9999.99,CH23= 9999.99,CH24= 9999.99,CH25= 9999.99,CH26= 9999.99,CH27= 9999.99,CH28= 9999.99,CH29= 9999.99,CH30= 9999.99,CH31= 9999.99,CH32= 9999.99,CH33= 9999.99,CH34= 9999.99,CH35= 9999.99,CH36= 9999.99,CH37= 9999.99,CH38= 9999.99,CH39= 9999.99,CH40= 9999.99,CH41= 9999.99,CH42= 9999.99,CH43= 9999.99,CH44= 9999.99,CH45= 9999.99,CH46= 9999.99,CH47= 9999.99,CH48= 9999.99,CH49= 9999.99,CH50= 9999.99,CH51= 9999.99,CH52= 9999.99,CH53= 9999.99,CH54= 9999.99,CH55= 9999.99,CH56= 9999.99,CH57= 9999.99,CH58= 9999.99,CH59= 9999.99,CH60= 9999.99,CH61= 9999.99,CH62= 9999.99,CH63= 9999.99,CH64= 9999.99,CH65= 9999.99,CH66= 9999.99,CH67= 9999.99,CH68= 9999.99,CH69= 9999.99,CH70= 9999.99,CH71= 9999.99,CH72= 9999.99,CH73= 9999.99,CH74= 9999.99,CH75= 9999.99,CH76= 9999.99,CH77= 9999.99,CH78= 9999.99,CH79= 9999.99,CH80= 9999.99,CH81= 9999.99,CH82= 9999.99,CH83= 9999.99,CH84= 9999.99,CH85= 9999.99,CH86= 9999.99,CH87= 9999.99,CH88= 9999.99,CH89= 9999.99,CH90= 9999.99,CH91= 9999.99,CH92= 9999.99,CH93= 9999.99,CH94= 9999.99,CH95= 9999.99,CH96= 9999.99,CH97= 9999.99,CH98= 9999.99,CH99= 9999.99,CH100= 9999.99,CH101= 9999.99,CH102= 9999.99,CH103= 9999.99,CH104= 9999.99,CH105= 9999.99,CH106= 9999.99,CH107= 9999.99,CH108= 9999.99,CH109= 9999.99,CH110= 9999.99,CH111= 9999.99,CH112= 9999.99,CH113= 9999.99,CH114= 9999.99,CH115= 9999.99,CH116= 9999.99,CH117= 9999.99,CH118= 9999.99,CH119= 9999.99,CH120= 9999.99,CH121= 9999.99,CH122= 9999.99,CH123= 9999.99,CH124= 9999.99,CH125= 9999.99,CH126= 9999.99,CH127= 9999.99,CH128= 9999.99,CH129= 9999.99,CH130= 9999.99,CH131= 9999.99,CH132= 9999.99,CH133= 9999.99,CH134= 9999.99,CH135= 9999.99,CH136= 9999.99,CH137= 9999.99,CH138= 9999.99,CH139= 9999.99,CH140= 9999.99,CH141= 9999.99,CH142= 9999.99,CH143= 9999.99,CH144= 9999.99,MSG1=254,MSG2=254,MSG3=254,MSG4=254,MSG5=254,MSG6=254,MSG7=254,MSG8=254,MSG9=254");
//    }

    

}
