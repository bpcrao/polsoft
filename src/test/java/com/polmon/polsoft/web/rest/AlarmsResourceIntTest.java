package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.Alarms;
import com.polmon.polsoft.repository.AlarmsRepository;
import com.polmon.polsoft.service.AlarmsService;
import com.polmon.polsoft.repository.search.AlarmsSearchRepository;
import com.polmon.polsoft.service.dto.AlarmsDTO;
import com.polmon.polsoft.service.mapper.AlarmsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AlarmsResource REST controller.
 *
 * @see AlarmsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class AlarmsResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));
    private static final String DEFAULT_TYPE = "AAAAA";
    private static final String UPDATED_TYPE = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";
    private static final String DEFAULT_DURATION = "AAAAA";
    private static final String UPDATED_DURATION = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);
    private static final String DEFAULT_SOURCE = "AAAAA";
    private static final String UPDATED_SOURCE = "BBBBB";
    private static final String DEFAULT_ACTION = "AAAAA";
    private static final String UPDATED_ACTION = "BBBBB";
    private static final String DEFAULT_COMMENTS = "AAAAA";
    private static final String UPDATED_COMMENTS = "BBBBB";
    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_USERNAME = "BBBBB";
    private static final String DEFAULT_STATUS = "AAAAA";
    private static final String UPDATED_STATUS = "BBBBB";

    @Inject
    private AlarmsRepository alarmsRepository;

    @Inject
    private AlarmsMapper alarmsMapper;

    @Inject
    private AlarmsService alarmsService;

    @Inject
    private AlarmsSearchRepository alarmsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restAlarmsMockMvc;

    private Alarms alarms;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AlarmsResource alarmsResource = new AlarmsResource();
        ReflectionTestUtils.setField(alarmsResource, "alarmsService", alarmsService);
        this.restAlarmsMockMvc = MockMvcBuilders.standaloneSetup(alarmsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Alarms createEntity(EntityManager em) {
        Alarms alarms = new Alarms();
        alarms = new Alarms()
                .type(DEFAULT_TYPE)
                .description(DEFAULT_DESCRIPTION)
                .duration(DEFAULT_DURATION)
                .created(DEFAULT_CREATED)
                .source(DEFAULT_SOURCE)
                .action(DEFAULT_ACTION)
                .comments(DEFAULT_COMMENTS)
                .username(DEFAULT_USERNAME)
                .status(DEFAULT_STATUS);
        return alarms;
    }

    @Before
    public void initTest() {
        alarmsSearchRepository.deleteAll();
        alarms = createEntity(em);
    }

    @Test
    @Transactional
    public void createAlarms() throws Exception {
        int databaseSizeBeforeCreate = alarmsRepository.findAll().size();

        // Create the Alarms
        AlarmsDTO alarmsDTO = alarmsMapper.alarmsToAlarmsDTO(alarms);

        restAlarmsMockMvc.perform(post("/api/alarms")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(alarmsDTO)))
                .andExpect(status().isCreated());

        // Validate the Alarms in the database
        List<Alarms> alarms = alarmsRepository.findAll();
        assertThat(alarms).hasSize(databaseSizeBeforeCreate + 1);
        Alarms testAlarms = alarms.get(alarms.size() - 1);
        assertThat(testAlarms.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testAlarms.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAlarms.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testAlarms.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testAlarms.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testAlarms.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testAlarms.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAlarms.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testAlarms.getStatus()).isEqualTo(DEFAULT_STATUS);

        // Validate the Alarms in ElasticSearch
        Alarms alarmsEs = alarmsSearchRepository.findOne(testAlarms.getId());
        assertThat(alarmsEs).isEqualToComparingFieldByField(testAlarms);
    }

    @Test
    @Transactional
    public void getAllAlarms() throws Exception {
        // Initialize the database
        alarmsRepository.saveAndFlush(alarms);

        // Get all the alarms
        restAlarmsMockMvc.perform(get("/api/alarms?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(alarms.getId().intValue())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.toString())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
                .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
                .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
                .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getAlarms() throws Exception {
        // Initialize the database
        alarmsRepository.saveAndFlush(alarms);

        // Get the alarms
        restAlarmsMockMvc.perform(get("/api/alarms/{id}", alarms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(alarms.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.toString()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAlarms() throws Exception {
        // Get the alarms
        restAlarmsMockMvc.perform(get("/api/alarms/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAlarms() throws Exception {
        // Initialize the database
        alarmsRepository.saveAndFlush(alarms);
        alarmsSearchRepository.save(alarms);
        int databaseSizeBeforeUpdate = alarmsRepository.findAll().size();

        // Update the alarms
        Alarms updatedAlarms = alarmsRepository.findOne(alarms.getId());
        updatedAlarms
                .type(UPDATED_TYPE)
                .description(UPDATED_DESCRIPTION)
                .duration(UPDATED_DURATION)
                .created(UPDATED_CREATED)
                .source(UPDATED_SOURCE)
                .action(UPDATED_ACTION)
                .comments(UPDATED_COMMENTS)
                .username(UPDATED_USERNAME)
                .status(UPDATED_STATUS);
        AlarmsDTO alarmsDTO = alarmsMapper.alarmsToAlarmsDTO(updatedAlarms);

        restAlarmsMockMvc.perform(put("/api/alarms")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(alarmsDTO)))
                .andExpect(status().isOk());

        // Validate the Alarms in the database
        List<Alarms> alarms = alarmsRepository.findAll();
        assertThat(alarms).hasSize(databaseSizeBeforeUpdate);
        Alarms testAlarms = alarms.get(alarms.size() - 1);
        assertThat(testAlarms.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testAlarms.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAlarms.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testAlarms.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testAlarms.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testAlarms.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testAlarms.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAlarms.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testAlarms.getStatus()).isEqualTo(UPDATED_STATUS);

        // Validate the Alarms in ElasticSearch
        Alarms alarmsEs = alarmsSearchRepository.findOne(testAlarms.getId());
        assertThat(alarmsEs).isEqualToComparingFieldByField(testAlarms);
    }

    @Test
    @Transactional
    public void deleteAlarms() throws Exception {
        // Initialize the database
        alarmsRepository.saveAndFlush(alarms);
        alarmsSearchRepository.save(alarms);
        int databaseSizeBeforeDelete = alarmsRepository.findAll().size();

        // Get the alarms
        restAlarmsMockMvc.perform(delete("/api/alarms/{id}", alarms.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean alarmsExistsInEs = alarmsSearchRepository.exists(alarms.getId());
        assertThat(alarmsExistsInEs).isFalse();

        // Validate the database is empty
        List<Alarms> alarms = alarmsRepository.findAll();
        assertThat(alarms).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAlarms() throws Exception {
        // Initialize the database
        alarmsRepository.saveAndFlush(alarms);
        alarmsSearchRepository.save(alarms);

        // Search the alarms
        restAlarmsMockMvc.perform(get("/api/_search/alarms?query=id:" + alarms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(alarms.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.toString())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
}
