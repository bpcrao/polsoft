package com.polmon.polsoft.web.rest;

import com.polmon.polsoft.PolsoftApp;
import com.polmon.polsoft.domain.ChannelAlarm;
import com.polmon.polsoft.repository.ChannelAlarmRepository;
import com.polmon.polsoft.service.ChannelAlarmService;
import com.polmon.polsoft.repository.search.ChannelAlarmSearchRepository;
import com.polmon.polsoft.service.dto.ChannelAlarmDTO;
import com.polmon.polsoft.service.mapper.ChannelAlarmMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChannelAlarmResource REST controller.
 *
 * @see ChannelAlarmResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PolsoftApp.class)
public class ChannelAlarmResourceIntTest {

    private static final BigDecimal DEFAULT_LOW = new BigDecimal(1);
    private static final BigDecimal UPDATED_LOW = new BigDecimal(2);

    private static final BigDecimal DEFAULT_HIGH = new BigDecimal(1);
    private static final BigDecimal UPDATED_HIGH = new BigDecimal(2);

    private static final BigDecimal DEFAULT_LOW_LOW = new BigDecimal(1);
    private static final BigDecimal UPDATED_LOW_LOW = new BigDecimal(2);

    private static final BigDecimal DEFAULT_HIGH_HIGH = new BigDecimal(1);
    private static final BigDecimal UPDATED_HIGH_HIGH = new BigDecimal(2);

    @Inject
    private ChannelAlarmRepository channelAlarmRepository;

    @Inject
    private ChannelAlarmMapper channelAlarmMapper;

    @Inject
    private ChannelAlarmService channelAlarmService;

    @Inject
    private ChannelAlarmSearchRepository channelAlarmSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restChannelAlarmMockMvc;

    private ChannelAlarm channelAlarm;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ChannelAlarmResource channelAlarmResource = new ChannelAlarmResource();
        ReflectionTestUtils.setField(channelAlarmResource, "channelAlarmService", channelAlarmService);
        this.restChannelAlarmMockMvc = MockMvcBuilders.standaloneSetup(channelAlarmResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChannelAlarm createEntity(EntityManager em) {
        ChannelAlarm channelAlarm = new ChannelAlarm();
        channelAlarm = new ChannelAlarm()
                .low(DEFAULT_LOW)
                .high(DEFAULT_HIGH)
                .lowLow(DEFAULT_LOW_LOW)
                .highHigh(DEFAULT_HIGH_HIGH);
        return channelAlarm;
    }

    @Before
    public void initTest() {
        channelAlarmSearchRepository.deleteAll();
        channelAlarm = createEntity(em);
    }

    @Test
    @Transactional
    public void createChannelAlarm() throws Exception {
        int databaseSizeBeforeCreate = channelAlarmRepository.findAll().size();

        // Create the ChannelAlarm
        ChannelAlarmDTO channelAlarmDTO = channelAlarmMapper.channelAlarmToChannelAlarmDTO(channelAlarm);

        restChannelAlarmMockMvc.perform(post("/api/channel-alarms")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(channelAlarmDTO)))
                .andExpect(status().isCreated());

        // Validate the ChannelAlarm in the database
        List<ChannelAlarm> channelAlarms = channelAlarmRepository.findAll();
        assertThat(channelAlarms).hasSize(databaseSizeBeforeCreate + 1);
        ChannelAlarm testChannelAlarm = channelAlarms.get(channelAlarms.size() - 1);
        assertThat(testChannelAlarm.getLow()).isEqualTo(DEFAULT_LOW);
        assertThat(testChannelAlarm.getHigh()).isEqualTo(DEFAULT_HIGH);
        assertThat(testChannelAlarm.getLowLow()).isEqualTo(DEFAULT_LOW_LOW);
        assertThat(testChannelAlarm.getHighHigh()).isEqualTo(DEFAULT_HIGH_HIGH);

        // Validate the ChannelAlarm in ElasticSearch
        ChannelAlarm channelAlarmEs = channelAlarmSearchRepository.findOne(testChannelAlarm.getId());
        assertThat(channelAlarmEs).isEqualToComparingFieldByField(testChannelAlarm);
    }

    @Test
    @Transactional
    public void getAllChannelAlarms() throws Exception {
        // Initialize the database
        channelAlarmRepository.saveAndFlush(channelAlarm);

        // Get all the channelAlarms
        restChannelAlarmMockMvc.perform(get("/api/channel-alarms?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(channelAlarm.getId().intValue())))
                .andExpect(jsonPath("$.[*].low").value(hasItem(DEFAULT_LOW.intValue())))
                .andExpect(jsonPath("$.[*].high").value(hasItem(DEFAULT_HIGH.intValue())))
                .andExpect(jsonPath("$.[*].lowLow").value(hasItem(DEFAULT_LOW_LOW.intValue())))
                .andExpect(jsonPath("$.[*].highHigh").value(hasItem(DEFAULT_HIGH_HIGH.intValue())));
    }

    @Test
    @Transactional
    public void getChannelAlarm() throws Exception {
        // Initialize the database
        channelAlarmRepository.saveAndFlush(channelAlarm);

        // Get the channelAlarm
        restChannelAlarmMockMvc.perform(get("/api/channel-alarms/{id}", channelAlarm.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(channelAlarm.getId().intValue()))
            .andExpect(jsonPath("$.low").value(DEFAULT_LOW.intValue()))
            .andExpect(jsonPath("$.high").value(DEFAULT_HIGH.intValue()))
            .andExpect(jsonPath("$.lowLow").value(DEFAULT_LOW_LOW.intValue()))
            .andExpect(jsonPath("$.highHigh").value(DEFAULT_HIGH_HIGH.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingChannelAlarm() throws Exception {
        // Get the channelAlarm
        restChannelAlarmMockMvc.perform(get("/api/channel-alarms/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChannelAlarm() throws Exception {
        // Initialize the database
        channelAlarmRepository.saveAndFlush(channelAlarm);
        channelAlarmSearchRepository.save(channelAlarm);
        int databaseSizeBeforeUpdate = channelAlarmRepository.findAll().size();

        // Update the channelAlarm
        ChannelAlarm updatedChannelAlarm = channelAlarmRepository.findOne(channelAlarm.getId());
        updatedChannelAlarm
                .low(UPDATED_LOW)
                .high(UPDATED_HIGH)
                .lowLow(UPDATED_LOW_LOW)
                .highHigh(UPDATED_HIGH_HIGH);
        ChannelAlarmDTO channelAlarmDTO = channelAlarmMapper.channelAlarmToChannelAlarmDTO(updatedChannelAlarm);

        restChannelAlarmMockMvc.perform(put("/api/channel-alarms")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(channelAlarmDTO)))
                .andExpect(status().isOk());

        // Validate the ChannelAlarm in the database
        List<ChannelAlarm> channelAlarms = channelAlarmRepository.findAll();
        assertThat(channelAlarms).hasSize(databaseSizeBeforeUpdate);
        ChannelAlarm testChannelAlarm = channelAlarms.get(channelAlarms.size() - 1);
        assertThat(testChannelAlarm.getLow()).isEqualTo(UPDATED_LOW);
        assertThat(testChannelAlarm.getHigh()).isEqualTo(UPDATED_HIGH);
        assertThat(testChannelAlarm.getLowLow()).isEqualTo(UPDATED_LOW_LOW);
        assertThat(testChannelAlarm.getHighHigh()).isEqualTo(UPDATED_HIGH_HIGH);

        // Validate the ChannelAlarm in ElasticSearch
        ChannelAlarm channelAlarmEs = channelAlarmSearchRepository.findOne(testChannelAlarm.getId());
        assertThat(channelAlarmEs).isEqualToComparingFieldByField(testChannelAlarm);
    }

    @Test
    @Transactional
    public void deleteChannelAlarm() throws Exception {
        // Initialize the database
        channelAlarmRepository.saveAndFlush(channelAlarm);
        channelAlarmSearchRepository.save(channelAlarm);
        int databaseSizeBeforeDelete = channelAlarmRepository.findAll().size();

        // Get the channelAlarm
        restChannelAlarmMockMvc.perform(delete("/api/channel-alarms/{id}", channelAlarm.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean channelAlarmExistsInEs = channelAlarmSearchRepository.exists(channelAlarm.getId());
        assertThat(channelAlarmExistsInEs).isFalse();

        // Validate the database is empty
        List<ChannelAlarm> channelAlarms = channelAlarmRepository.findAll();
        assertThat(channelAlarms).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchChannelAlarm() throws Exception {
        // Initialize the database
        channelAlarmRepository.saveAndFlush(channelAlarm);
        channelAlarmSearchRepository.save(channelAlarm);

        // Search the channelAlarm
        restChannelAlarmMockMvc.perform(get("/api/_search/channel-alarms?query=id:" + channelAlarm.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(channelAlarm.getId().intValue())))
            .andExpect(jsonPath("$.[*].low").value(hasItem(DEFAULT_LOW.intValue())))
            .andExpect(jsonPath("$.[*].high").value(hasItem(DEFAULT_HIGH.intValue())))
            .andExpect(jsonPath("$.[*].lowLow").value(hasItem(DEFAULT_LOW_LOW.intValue())))
            .andExpect(jsonPath("$.[*].highHigh").value(hasItem(DEFAULT_HIGH_HIGH.intValue())));
    }
}
