'use strict';

describe('Controller Tests', function() {

    describe('Alarmtemplateschedulars Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAlarmtemplateschedulars, MockAlarmtemplate;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAlarmtemplateschedulars = jasmine.createSpy('MockAlarmtemplateschedulars');
            MockAlarmtemplate = jasmine.createSpy('MockAlarmtemplate');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Alarmtemplateschedulars': MockAlarmtemplateschedulars,
                'Alarmtemplate': MockAlarmtemplate
            };
            createController = function() {
                $injector.get('$controller')("AlarmtemplateschedularsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'polsoftApp:alarmtemplateschedularsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
