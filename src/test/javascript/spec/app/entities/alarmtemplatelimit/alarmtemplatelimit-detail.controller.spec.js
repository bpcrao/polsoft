'use strict';

describe('Controller Tests', function() {

    describe('Alarmtemplatelimit Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAlarmtemplatelimit, MockAlarmtemplate;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAlarmtemplatelimit = jasmine.createSpy('MockAlarmtemplatelimit');
            MockAlarmtemplate = jasmine.createSpy('MockAlarmtemplate');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Alarmtemplatelimit': MockAlarmtemplatelimit,
                'Alarmtemplate': MockAlarmtemplate
            };
            createController = function() {
                $injector.get('$controller')("AlarmtemplatelimitDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'polsoftApp:alarmtemplatelimitUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
