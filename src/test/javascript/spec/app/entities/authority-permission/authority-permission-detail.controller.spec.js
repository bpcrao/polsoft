'use strict';

describe('Controller Tests', function() {

    describe('Authority_permission Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAuthority_permission, MockPermission, MockJhi_authority;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAuthority_permission = jasmine.createSpy('MockAuthority_permission');
            MockPermission = jasmine.createSpy('MockPermission');
            MockJhi_authority = jasmine.createSpy('MockJhi_authority');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Authority_permission': MockAuthority_permission,
                'Permission': MockPermission,
                'Jhi_authority': MockJhi_authority
            };
            createController = function() {
                $injector.get('$controller')("Authority_permissionDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'polsoftApp:authority_permissionUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
