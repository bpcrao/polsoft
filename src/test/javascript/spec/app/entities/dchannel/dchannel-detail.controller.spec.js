'use strict';

describe('Controller Tests', function() {

    describe('Dchannel Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockDchannel, MockDevice, MockChannelAlarm, MockLocation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockDchannel = jasmine.createSpy('MockDchannel');
            MockDevice = jasmine.createSpy('MockDevice');
            MockChannelAlarm = jasmine.createSpy('MockChannelAlarm');
            MockLocation = jasmine.createSpy('MockLocation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Dchannel': MockDchannel,
                'Device': MockDevice,
                'ChannelAlarm': MockChannelAlarm,
                'Location': MockLocation
            };
            createController = function() {
                $injector.get('$controller')("DchannelDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'polsoftApp:dchannelUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
