'use strict';

describe('Controller Tests', function() {

    describe('Alarmtemplate Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAlarmtemplate, MockAlarmtemplateschedulars, MockAlarmtemplatelimit;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAlarmtemplate = jasmine.createSpy('MockAlarmtemplate');
            MockAlarmtemplateschedulars = jasmine.createSpy('MockAlarmtemplateschedulars');
            MockAlarmtemplatelimit = jasmine.createSpy('MockAlarmtemplatelimit');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Alarmtemplate': MockAlarmtemplate,
                'Alarmtemplateschedulars': MockAlarmtemplateschedulars,
                'Alarmtemplatelimit': MockAlarmtemplatelimit
            };
            createController = function() {
                $injector.get('$controller')("AlarmtemplateDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'polsoftApp:alarmtemplateUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
