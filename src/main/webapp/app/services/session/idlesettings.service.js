(function() {
	'use strict';

	angular
    .module('polsoftApp')
    .factory('IdleSettingsService', IdleSettingsService);
	
	function IdleSettingsService() {

		var service = {
			getSettings : getSettings
		};

		return service;

		function getSettings() {
			return {
				timeOut : 5,
				idleTime : 5
			};
		}
	}
})();
