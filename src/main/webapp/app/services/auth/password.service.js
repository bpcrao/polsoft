(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('Password', Password);

    Password.$inject = ['$resource'];

    function Password($resource) {
        var service = $resource('api/account/change_password', {}, {});
        return service;
    }
})();
