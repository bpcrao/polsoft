(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('EncryptService', Encrypt);

    Encrypt.$inject = ['$resource'];

    function Encrypt($resource,login,password) {
        return $resource('api/encryption-parameters', {}, {
            'getPublicKey': { method:'GET'}
        });
    }
})();
