(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('AuthServerProvider', AuthServerProvider);

    AuthServerProvider.$inject = ['$http', '$localStorage' , 'JhiTrackerService','$cookies', 'EncryptService'];

    function AuthServerProvider ($http, $localStorage , JhiTrackerService,$cookies, EncryptService) {
        var service = {
            getToken: getToken,
            hasValidToken: hasValidToken,
            login: login,
            logout: logout
        };

        return service;

        function getToken () {
            var token = $localStorage.authenticationToken;
            return token;
        }

        function hasValidToken () {
            var token = this.getToken();
            return !!token;
        }

        function login (credentials) {
            var key = EncryptService.getPublicKey();
            return key.$promise.then(function (result) {
            	 var encrypt = new JSEncrypt();                 
                 encrypt.setPublicKey(result.publicKey);                 
                 var data = 'j_username=' + encodeURIComponent(credentials.username) +
                 '&j_password=' + encodeURIComponent(encrypt.encrypt(credentials.password)) +
                 '&submit=Login';
                 return $http.post('api/authentication', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).success(function (response) {
                    return response;
                });	
            });
        }

        function logout () {
            //return;
            console.log('$cookies',$cookies.getAll());
            
            JhiTrackerService.disconnect();
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", "api/logout", false);//the false is for making the call synchronous
            xmlhttp.setRequestHeader("Content-type", "application/json");    
            xmlhttp.setRequestHeader('X-CSRF-Token', $cookies.get($http.defaults.xsrfCookieName));
            xmlhttp.send();
            
            if(xmlhttp.status == 200){          
            	console.log("logged out");
            } else{
            	console.log("failed : logged out");
            }            
            
        }
    }
})();
