(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('ChangePassword', ChangePassword);

    ChangePassword.$inject = ['$resource'];

    function ChangePassword($resource,login,password) {
        return $resource('api/account/changepassword', {}, {
            'update': { method:'POST'}
        });
    }
})();
