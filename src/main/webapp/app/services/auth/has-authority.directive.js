(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .directive('hasAuthority', hasAuthority);

    hasAuthority.$inject = ['Principal'];

    function hasAuthority(Principal) {
        var directive = {
            restrict: 'A',
            link: linkFunc
        };

        return directive;

        function linkFunc(scope, element, attrs) {
        	var authority = attrs.hasAuthority.replace(/\s+/g, '');
        	var permission = attrs.hasPermission ? attrs.hasPermission.replace(/\s+/g, ''):'{}';
        	permission = JSON.parse(permission);
            var setVisible = function () {
                    element.removeClass('hidden');
                },
                setHidden = function () {
                    element.addClass('hidden');
                },
                defineVisibility = function (reset) {

                    if (reset) {
                        setVisible();
                    }
                    Principal.hasAuthority(authority, permission)                    
                        .then(function (result) {
                        	if (result) {
                                setVisible();
                            } else {
                                setHidden();
                            }
                        });
                };
            if (authority.length > 0 || permission) {
                defineVisibility(true);

                scope.$watch(function() {
                    return Principal.isAuthenticated();
                }, function() {
                    defineVisibility(true);
                });
            }
        }
    }
})();
