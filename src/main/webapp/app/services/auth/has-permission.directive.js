
(function () {
    'use strict';

    angular.module('polsoftApp').directive('hasPermission', hasPermission);

    hasPermission.$inject = ['Principal'];

    function hasPermission(Principal) {
        var directive = {
            restrict: 'EA',
            link: linkFunc
        };

        return directive;

        function linkFunc(scope, element, attrs) {
            var permission = attrs.hasPermission ? attrs.hasPermission.replace(
                /\s+/g, '') : '{}';
            var disableForPermission = permission.disableForPermission;
            permission = JSON.parse(permission);
            var authority = permission.authority;

            var setVisible = function () {
                    element.removeClass('hidden');
                    element.removeAttr('disabled');
                },
                setHidden = function (disableForPermission) {
                    if (disableForPermission) {
                        element.attr('disabled', '');
                        element.attr('title','You are not authorized to access');
                    } else {
                        element.addClass('hidden');
                    }
                },

                defineVisibility = function (reset, disableForPermission) {
                    if (reset) {
                        setVisible();
                    }
                    Principal.hasAuthority(authority, permission).then(
                        function (result) {
                            if (result) {
                                setVisible();
                            } else {
                                setHidden(disableForPermission);

                            }
                        });
                };
            if (authority.length > 0 || permission) {
                defineVisibility(true, permission.disableForPermission);
                scope.$watch(function () {
                    return Principal.isAuthenticated();
                }, function () {
                    defineVisibility(true, permission.disableForPermission);
                });
            }
        }
    }
})();