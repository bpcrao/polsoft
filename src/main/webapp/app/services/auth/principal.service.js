(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('Principal', Principal);

    Principal.$inject = ['$q', 'Account', 'JhiTrackerService','DataSocketService','LocationTrackerService','AuditTrackerService','AlarmsTrackerService','DeviceTrackerService' ];

    function Principal($q, Account, JhiTrackerService, DataSocketService, LocationTrackerService, AuditTrackerService, AlarmsTrackerService, DeviceTrackerService) {
        var _identity,
            _authenticated = false,
            _isReset = null;
        var levels = {
            'NONE': 0,
            'VIEW': 1,            
            'CREATE': 2,
            'EDIT': 3,
            'DELETE': 4
        };

        var service = {
            authenticate: authenticate,
            hasAnyAuthority: hasAnyAuthority,
            hasAuthority: hasAuthority,
            identity: identity,
            isAuthenticated: isAuthenticated,
            getReset: getReset,
            setReset: setReset,
            isIdentityResolved: isIdentityResolved
        };

        return service;

        function authenticate(identity) {
            _identity = identity;
            _authenticated = identity !== null;
        }

        function hasAnyAuthority(authorities, permission) {
            if (!_authenticated || !_identity || !_identity.authorities) {
                return false;
            }
            if (authorities) {
                for (var index = 0; index < authorities.length; index++) {
                    if (_identity.authorities.indexOf(authorities[index]) !== -1) {
                        return true;
                    }
                }
            }
            if (permission && _identity && _identity.userPermissions) {
                return checkPermission(_identity, permission);
            }
            return false;
        }

        function checkPermission(_id, permission) {
            var permissionData = _id.userPermissions.find(function (upermission) {
                return upermission.name == permission.resource;
            });
            var permissionLevel = 0;
            if (permissionData && permissionData.permission) {
                permissionLevel = permissionData.permission;
            }
            if (permissionLevel >= levels[permission.level ? permission.level : 'NONE']) {
                return true;
            }

        }

        function hasAuthority(authority, permission) {
            if (!_authenticated) {
                return $q.when(false);
            }
            return this.identity().then(function (_id) {
                return checkPermission(_id, permission, authority) || (_id.authorities && _id.authorities.indexOf(authority) !== -1);
            }, function () {
                return false;
            });
        }

        function identity(force, resetKey) {
            var deferred = $q.defer();

            if (force === true) {
                _identity = undefined;
                _isReset = resetKey;
            }


            // check and see if we have retrieved the identity data from the server.
            // if we have, reuse it by immediately resolving
            if (angular.isDefined(_identity)) {
                deferred.resolve(_identity);
                return deferred.promise;
            }

            // retrieve the identity data from the server, update the identity object, and then resolve.
            Account.get().$promise
                .then(getAccountThen)
                .catch(getAccountCatch);

            return deferred.promise;

            function getAccountThen(account) {
                _identity = account.data;
                _authenticated = true;
                if (account.data.expiryDate) {
                    var expiryDate = new Date(account.data.expiryDate)
                   // if (expiryDate.getTime() < (new Date()).getTime() && account.resetKey) {
                        _isReset = account.headers().resetkey;
                    //}
                }
                deferred.resolve(_identity);
                JhiTrackerService.connect();
                DeviceTrackerService.connect();
                DataSocketService.connect();
                LocationTrackerService.connect();
                AuditTrackerService.connect();
                AlarmsTrackerService.connect();
            }

            function getAccountCatch() {
                _identity = null;
                _authenticated = false;
                deferred.resolve(_identity);
            }
        }

        function isAuthenticated() {
            return _authenticated;
        }

        function getReset() {
            return _isReset;
        }

        function setReset() {
            _isReset = '';
        }

        function isIdentityResolved() {
            return angular.isDefined(_identity);
        }
    }
})();
