(function() {
  'use strict';

  angular.module('polsoftApp').factory('ChannelService', ChannelService);

  ChannelService.$inject = ['$http', '$q'];

  function ChannelService($http, $q) {

    var dataPromise;

    var service = {
      data: [],
      getDataFromServer: getChannelData,
      isInitialised: 0
    };

    var currentChannel;

    // Implementation
    function getChannelData(channelList, startDateTime, endDateTime,
      selectedInterval) {

      var def = $q.defer();

      var startT = 'Fri Jan 01 2016 11:00:00 GMT+0530 (IST)';

      var endT = 'Sun Jan 03 2016 11:30:00 GMT+0530 (IST)';

      $http
        .get("api/channeldata", {
          params: {
            "channelList": channelList,
            "startDateTime": startDateTime.toString(),
            "endDateTime": endDateTime.toString(),
            "selectedInterval": selectedInterval.name
          }
        })
        .success(
          function(data) {

            service.data = data;
            
            if (!service.isInitialised) {

              service.isInitialised = 1;

              var defaultNoOfChannels = 6;

              var noOfChannels = 8;

              service.channelList = [];

              service.noOfChannels = noOfChannels;

              var id = 0;

              while (noOfChannels) {

                var channnelString = 'ch0';

                service.channelList
                  .push({
                    id: id,
                    name: channnelString + (id + 1),
                    toShow: (id == 0 || id == 1 || id == 4 || id == 5) ? true : false
                  });

                noOfChannels--;
                id++;
              }
            }

            def.resolve(service);
          }).error(function() {
          def.reject("Failed to get data");
        });

      return def.promise;
    }

    return service;
  }
})();