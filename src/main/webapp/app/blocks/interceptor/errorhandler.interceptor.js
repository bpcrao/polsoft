(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('errorHandlerInterceptor', errorHandlerInterceptor);

    errorHandlerInterceptor.$inject = ['$q', '$rootScope','$location'];

    function errorHandlerInterceptor ($q, $rootScope,$location) {
        var service = {
            responseError: responseError
        };

        return service;

        function responseError (response) {
        	if(response.status==-1)
            {
        		window.alert('server disconnected');
            }
            if (!(response.status === 401 && (response.data === '' || (response.data.path && response.data.path.indexOf('/api/account') === 0 )))) {
                $rootScope.$emit('polsoftApp.httpError', response);
            } 
            return $q.reject(response);
        }
    }
})();
