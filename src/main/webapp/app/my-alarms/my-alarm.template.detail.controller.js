(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('myAlarmTemplateDetailController', myAlarmTemplateDetailController);

    myAlarmTemplateDetailController.$inject = ['$timeout','$rootScope', 'previousState', '$scope', '$stateParams', '$uibModalInstance'];

    function myAlarmTemplateDetailController($timeout,$rootScope, previousState, $scope, $stateParams, $uibModalInstance) {
        var vm = this;

        vm.clear = clear;
        vm.save = save;

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {

            vm.isSaving = true;
            onSaveSuccess(vm.comments);
            // send({}, onSaveSuccess, onSaveError);
        }

        function onSaveSuccess(result) {
            $scope.$emit('polsoftApp:eventsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
            console.log(result);
        }

        function onSaveError() {
            vm.isSaving = false;
        }

    }
})();