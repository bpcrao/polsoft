(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('options', {
                parent: 'app',
                url: '/options',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: "app/my-alarms/views/sideoptions.html",
                        controller: 'options'
                    }
                },
                resolve: {
                    mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('options');
                        return $translate.refresh();
                    }]
                }
            })
            .state('options.channels', {
                parent: 'options',
                url: "/channels",
                templateUrl: "app/my-alarms/views/channels.html",
                controller: 'channels',
                data: {
                    authorities: ['Administrator'],
                    permission: {
                        'resource': 'DEVICE',
                        'level': 'VIEW'
                    }
                }
            })
            .state('options.channels.channel', {
                parent: 'options.channels',
                url: "/channel/:id",
                templateUrl: "app/my-alarms/views/channel.html",
                controller: 'channelMyAlarm',
                data: {
                    authorities: ['Administrator'],
                    permission: {
                        'resource': 'DEVICE',
                        'level': 'VIEW'
                    }
                }
            })
            .state('options.channels.channel.detail', {
                parent: 'options.channels.channel',
                url: "/channel-detail/:id",
                templateUrl: "app/my-alarms/views/channeldetail.html",
                controller: 'channeldetail',
                data: {
                    authorities: ['Operator']
                }
            });
        // .state('options.channels.channel.detail.more', {
        //     url: "/more/:id",
        //     templateUrl: "views/channeldetail-more.html",
        //     controller: 'channeldetail-more'
        // });
    }
})();
