(function() {
	'use strict';

	angular
		.module('polsoftApp')
		.controller('myAlarmController', myAlarmController);

	myAlarmController.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$timeout', 'Template', '$q', 'Threshold', 'TemplateSearch'];

	function myAlarmController($scope, Principal, LoginService, $state, $timeout, Template, $q, Threshold,TemplateSearch) {

		var vm = this;

		vm.templates = [];
		vm.loadAll = loadAll;
		vm.thresholds = [];

		loadAll();

		function loadAll() {
			Template.query(function(result) {

				vm.templates = result;

				console.log(result);

				for (var i = 0; i < vm.templates.length; i++) {

					var currentTemplate = vm.templates[i];

					currentTemplate.emails = JSON.parse(currentTemplate.emails);

					currentTemplate.sms = JSON.parse(currentTemplate.sms);

					currentTemplate.thresholds = JSON.parse(currentTemplate.thresholds);
				}

			});
		}

		function getThresholds() {
			// There will always be a promise so always declare it.
			var deferred = $q.defer();

			Threshold.query(function(result) {

				vm.thresholds = result;
				vm.options = JSON.parse(angular.toJson(result));

				deferred.resolve(vm.options);
			});

			return deferred.promise;
		}

		vm.getThresholds = getThresholds;


		$scope.sideOptions = [{
			id: 1,
			name: "Devices"
		}, {
			id: 2,
			name: "Measuring Points"
		}, {
			id: 3,
			name: "Actions"
		}, {
			id: 4,
			name: "Notifications"
		}];

		$scope.selectedChannel = '';

		var tree, treedata_avm, treedata_geography;

		$scope.my_tree_handler = function(branch) {
			var _ref;
			$scope.output = getProp(branch);
		};

		function getProp(obj) {

			$scope.selectedNodes = [];
			$scope.selectedChannel = '';

			function toArray(obj) {
				var result = [];
				for (var prop in obj) {
					var value = obj[prop];
					if (typeof value === 'object') {
						result.push(toArray(value));
					} else {
						if (prop === 'label') {
							result.push(value);
							$scope.selectedNodes.push(value);
						}
					}
				}
				return result;
			}

			toArray(obj);
		}

		$scope.moreDetails = function() {

			var selectedTemplate = $scope.selectedScheme;

			// $state.transitionTo('my-alarm.detail', {  
			//          })

			console.log(selectedTemplate);
		}

		var treedata_avm = [{
			label: 'Plant',
			children: [{
				label: 'Block A',
				children: [{
					label: 'Channel 1',
					customData: "Hello"
				}, {
					label: 'Channel 2',
					customData: "Hello"
				}, {
					label: 'Channel 3',
					customData: "Hello"
				}]
			}, {
				label: 'Block B',
				children: ['Channel 4', 'Channel 5', 'Channel 6']
			}, {
				label: 'Block C',
				children: ['Channel 7', 'Channel 8', 'Channel 9']
			}]
		}];

		$scope.my_data = treedata_avm;

		$scope.setSideOptions = function(option) {
			$scope.selectedSideOption = option;
			if (option.name == 'Measuring Points') {
				$scope.showChannels = true;
			} else {
				$scope.showChannels = false;
				$scope.selectedChannel = '';
			}
		}

		$scope.setChannels = function(channel) {

			$scope.selectedChannel = channel;

			$scope.changeTemplate(channel);
		}

		$scope.changeTemplate = function(channel) {
			console.log(channel);
		}

		$scope.isSelected = function(option) {
			$scope.selectedSideOption === option;
		}
	}

})();