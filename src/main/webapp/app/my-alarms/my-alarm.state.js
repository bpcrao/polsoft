(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('my-alarm', {
                parent: 'app',
                url: '/my-alarm',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'app/my-alarms/my-alarm.html',
                        controller: 'myAlarmController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('my-alarm');
                        return $translate.refresh();
                    }]
                }
            })
            .state('my-alarm.detail', {
            parent: 'my-alarm',
            url: '/my-alarm/edit',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/my-alarms/alarmoff.html',
                    controller: 'myAlarmTemplateDetailController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Template', function(Template) {
                            return Template.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }
})();