// myApp.service('myService', function() {
// 	this.result = {};
// });


(function() {
	'use strict';
	angular
		.module('polsoftApp')
		.factory('myService', myService);

	myService.$inject = ['$resource'];

	function myService() {
		this.result = {};

		return this.result;
	}
})();