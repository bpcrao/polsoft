(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('channels', channels);

    channels.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$filter', '$stateParams', 'myService', 'Template', 'TemplateSearch','$q'];

    function channels($scope, Principal, LoginService, $state, $filter, $stateParams, myService, Template, TemplateSearch,$q) {

        var vm = this;

        vm.templates = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Template.query(function(result) {
                vm.templates = result;

                for (var i = 0; i < vm.templates.length; i++) {

                    var currentTemplate = vm.templates[i];

                    currentTemplate.emails = JSON.parse(currentTemplate.emails);

                    currentTemplate.sms = JSON.parse(currentTemplate.sms);

                    currentTemplate.thresholds = JSON.parse(currentTemplate.thresholds);
                }

            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            TemplateSearch.query({
                query: vm.searchQuery
            }, function(result) {
                vm.templates = result;
            });
        }

        $scope.myService = myService;

        var tree, treedata_avm;

        $scope.output = '';

        $scope.my_tree_handler = function(branch) {
            var _ref;
            $scope.output = getProp(branch);
        };

        function getProp(obj) {

            $scope.selectedNodes = [];
            $scope.selectedChannel = '';

            function toArray(obj) {
                var result = [];
                for (var prop in obj) {
                    var value = obj[prop];
                    if (typeof value === 'object') {
                        result.push(toArray(value));
                    } else {
                        if (prop === 'label') {
                            if (obj['isChannel'] == true) {

                                $scope.selectedNodes.push({
                                    "id": obj["id"],
                                    "value": value
                                });
                            }
                            result.push(value);
                        }
                    }
                }

                return result;
            }

            toArray(obj);

            $scope.channels = [];

            for (var i = 0; i < $scope.selectedNodes.length; i++) {

                $scope.channels.push($scope.selectedNodes[i]);
            }

            myService.result = {
                'selectedNodes': $scope.channels
            };

            myService.result.channels = $scope.channels;
        }

        // var isLoadedPlants = false;
        // var isLoadedBlocks = false;
        // var isLoadedDevices = false;
        // var isLoadedChannels = false;

        // var blockData = {
        //     label: 'Block',
        //     isChannel: false,
        //     id: 1,
        //     noLeaf: true,
        //     onSelect: function(branch) {
        //         alert(branch);
        //     }
        // };

        // var plantData = {
        //     label: 'Plant',
        //     isChannel: false,
        //     id: 0,
        //     noLeaf: true,
        //     onSelect: function(branch) {

        //         if (!isLoadedPlants) {

        //             branch.children = branch.children || [];
        //             branch.children.push(blockData);

        //             isLoadedPlants = true;
        //         }
        //     }
        // };



        // $scope.my_data = [];

        // $scope.my_data.push(plantData);

////////////////////////////////////////////////////////////////////////////

        var treedata_avm = [{
            label: 'Plant',
            isChannel: false,
            id: 0,
            noLeaf: true
        }];

        var treedata_avm = [{
            label: 'Plant',
            isChannel: false,
            id: 0,
            children: [{
                label: 'Block A',
                isChannel: false,
                id: 1,
                children: [{
                    label: 'Channel 1',
                    isChannel: true,
                    id: 2
                }, {
                    label: 'Channel 2',
                    isChannel: true,
                    id: 3
                }, {
                    label: 'Channel 3',
                    isChannel: true,
                    id: 4
                }]
            }, {
                label: 'Block B',
                isChannel: false,
                id: 5,
                children: [{
                    label: 'Channel 4',
                    isChannel: true,
                    id: 6
                }, {
                    label: 'Channel 5',
                    isChannel: true,
                    id: 7
                }, {
                    label: 'Channel 6',
                    isChannel: true,
                    id: 8
                }]
            }, {
                label: 'Block C',
                isChannel: false,
                id: 9,
                children: [{
                    label: 'Channel 7',
                    isChannel: true,
                    id: 10
                }, {
                    label: 'Channel 8',
                    isChannel: true,
                    id: 11
                }, {
                    label: 'Channel 9',
                    isChannel: true,
                    id: 12
                }]
            }]
        }];

        $scope.my_data = treedata_avm;

    }
})();

