(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('channeldetail', channeldetail);

    channeldetail.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$filter', '$stateParams', 'myService'];

    function channeldetail($scope, Principal, LoginService, $state, $filter, $stateParams, myService) {

        $scope.myService = myService;
        $scope.id = $stateParams.id;
        $scope.channels = myService.result.channels || [];

        $scope.channel = $filter('filter')($scope.channels, $stateParams.id)[0];

    }
})();