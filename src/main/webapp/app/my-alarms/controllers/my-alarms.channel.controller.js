(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('channelMyAlarm', channelMyAlarm);

    channelMyAlarm.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$filter', '$stateParams', 'myService', '$uibModal', '$log', '$document', 'Threshold', 'ThresholdSearch'];

    function channelMyAlarm($scope, Principal, LoginService, $state, $filter, $stateParams, myService, $uibModal, $log, $document, Threshold, ThresholdSearch) {

        var vm = this;

        vm.thresholds = [];

        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Threshold.query(function(result) {
                $scope.thresholds = result;
            });
        }

        $scope.myService = myService;
        $scope.id = $stateParams.id;
        $scope.channels = myService.result.channels || [];

        $scope.selectedScheme = '';

        $scope.channel = $filter('filter')($scope.channels, $stateParams.id)[0];

        // $scope.alarmRadio = 'Alarm scheme';

        $scope.newValue = function(value) {
            console.log(value);
            console.log($scope.thresholds)
        }

        $scope.onChoice = function() {
            alert("changed.");
        };


        $scope.myService = myService;
        $scope.id = $stateParams.id;
        $scope.channels = myService.result.channels || [];

        $scope.channel = $filter('filter')($scope.channels, $stateParams.id)[0];

        $scope.saveTheAlarm = function(){

            console.log($scope.alarmRadio);

            if ($scope.alarmRadio == 'alarmScheme') {
                console.log($scope.selectedScheme);
            }else if ($scope.alarmRadio == 'measuringPoint') {


            }else if ($scope.alarmRadio == 'off') {


            }
        };

        $scope.items = $scope.channel;

        console.log($scope.items);

        $scope.animationsEnabled = true;

        $scope.open = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function() {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };


    }
})();


(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ModalInstanceCtrl', ModalInstanceCtrl);

    ModalInstanceCtrl.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$filter', '$stateParams', 'myService', '$uibModalInstance','items'];

    function ModalInstanceCtrl($scope, Principal, LoginService, $state, $filter, $stateParams, myService, $uibModalInstance, items) {

        $scope.items = items;
        // console.log(items);

        $scope.animationsEnabled = true;

        $scope.open = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function() {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        $scope.selected = {
            item: $scope.items[0]||''
        };

        $scope.ok = function() {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();
