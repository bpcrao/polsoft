(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ResetFinishController', ResetFinishController);

    ResetFinishController.$inject = ['$timeout', 'Auth', 'LoginService','Systempreferences','$state','$stateParams','$location','$q'];

    function ResetFinishController ($timeout, Auth, LoginService,Systempreferences,$state,$stateParams,$location,$q) {
        var vm = this;

        vm.keyMissing = angular.isUndefined($stateParams.key);
        vm.confirmPassword = null;
        vm.doNotMatch = null;
        vm.error = null;
        vm.finishReset = finishReset;
        vm.gotoLogin=gotoLogin;
        vm.login=login;
        vm.resetAccount = {};
        vm.success = null;
        vm.passwordStrength = 0;  
        vm.passwordPattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;
        $timeout(function (){angular.element('#password').focus();});

        loadAll();
        function loadAll() {
            Systempreferences.query(function(result) {
                vm.systempreferences = result;
               vm.Passwordlength = parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "Password Minlength"})[0].data)
             //vm.passwordMinLength=parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "password length"})[0].data);
            });
        }

        function gotoLogin()
        {
            $location.path('/loginView');
        }
        function login()
        {
        	LoginService.open();
        }
        function finishReset() {
            vm.doNotMatch = null;
            vm.oldpassword=null;
            vm.error = null;
            if (vm.resetAccount.password !== vm.confirmPassword) {
                vm.doNotMatch = 'ERROR';
            } else {
                Auth.resetPasswordFinish({key: $stateParams.key, newPassword: vm.resetAccount.password}).then(function () {
                    vm.success = 'OK';
                    // vm.gotoLogin();
                }).catch(function (error) {
                	vm.success = null;
                	  if(error.data==="Password Exists"){
                		  vm.oldpassword = error.data;
                      } else{
                    	  vm.error = 'ERROR';
                      }
                });
            }
        }
        
    }
})();
