(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('PasswordController', PasswordController);
    	
    PasswordController.$inject = ['Auth', 'Principal', 'Systempreferences'];

    function PasswordController (Auth, Principal, Systempreferences) {
        var vm = this;
        vm.passwordStrength=0;
        vm.passwordPattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;
        vm.changePassword = changePassword;
        vm.doNotMatch = null;
        vm.error = null;
        vm.success = null;
        vm.oldpassword = null;
        
        Principal.identity().then(function(account) {
            vm.account = account;
        });

        loadAll();
        function loadAll() {
            Systempreferences.query(function(result) {
                vm.systempreferences = result;
               vm.Passwordlength = parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "Password Minlength"})[0].data)
             //vm.passwordMinLength=parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "password length"})[0].data);
            });
        }
        
        function changePassword () {
        	vm.oldpassword = null;
            if (vm.password !== vm.confirmPassword) {
                vm.error = null;
                vm.success = null;
                vm.doNotMatch = 'ERROR';
            } else {
                vm.doNotMatch = null;               
                
                	Auth.changePassword((vm.password)).then(function () {
                        vm.error = null;
                        vm.oldpassword = null;
                        vm.success = 'OK';
                    }).catch(function (error) {
                        vm.success = null;
                        if(error.data==="Password Exists"){
                        	vm.oldpassword = error.data;
                        } else {
                        vm.error = 'ERROR';
                        }
                    });
            }
        }        
    }
})();
