(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('DeviceDataService', DeviceDataService);

    DeviceDataService.$inject = ['$resource'];

    function DeviceDataService ($resource) {
        var resourceUrl =  'api/chartdevicedata';

        return $resource(resourceUrl, {}, {
            'query': { method:'GET'}
        });       
        
    }
})();
