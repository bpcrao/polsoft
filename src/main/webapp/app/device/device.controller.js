(function() {
	'use strict';

	angular.module('polsoftApp').controller('DeviceDataController',
			ChannelController);

	ChannelController.$inject = [ '$scope', 'Principal', 'LoginService',
			'$state', 'DeviceDataService', 'DataSocketService' ];

	function ChannelController($scope, Principal, LoginService, $state,
			DeviceDataService, DataSocketService) {
		var vm = this;
		var ch1=null;
		var ch2=null;
		

		DataSocketService.receive().then(null, null, function(activity) {
			if(ch1)
			ch1.push(activity.ch1);
			if(ch2)
			ch2.push(activity.ch2);
        });
        
        
		$scope.multipleChartConfig = {
			chart : {
				zoomType : 'xy'
			},

			title : {
				text : 'Channel Time Series'
			},
			subtitle : {
				text : 'another subtitle'
			},
			credits : {
				enabled : false
			},
			exporting : {
				enabled : false
			},
			xAxis : [ {
				categories : [],
				crosshair : true
			} ],
			yAxis : [ { // Primary yAxis
				labels : {
					format : '{value} Pa',
					style : {
						color : 'red'
					}
				},
				title : {
					text : 'Pressure',
					style : {
						color : 'red'
					}
				},
				opposite : true

			}
			// , { // Secondary yAxis
			// gridLineWidth: 0,
			// title: {
			// text: 'Temperature',
			// style: {
			// color: Highcharts.getOptions().colors[0]
			// }
			// },
			// labels: {
			// format: '{value} °C',
			// style: {
			// color: Highcharts.getOptions().colors[0]
			// }
			// }
			// }
			],
			tooltip : {
				shared : true
			},
			legend : {
				layout : 'vertical',
				align : 'left',
				x : 80,
				verticalAlign : 'top',
				y : 55,
				floating : true,
				backgroundColor : (Highcharts.theme && Highcharts.theme.legendBackgroundColor)
						|| '#FFFFFF'
			},
			series : [
					{
						data : [ 29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6,
								148.5, 216.4, 194.1, 95.6, 54.4 ]
					},
					{
						datad : [ 2.9, 7.5, 136.4, 149.2, 144.0, 16.0, 15.6,
								145.5, 16.4, 94.1, 5.6, 54.4 ]
					} ]
		};

		DeviceDataService.query(function(resources) {
			ch1=resources.ch0.slice(1300);
			ch2=resources.ch1.slice(1300);
			$scope.multipleChartConfig.series = [ {
				"data" : ch1
			}, {
				"data" : ch2
			} ];
		});
	}
})();
