(function() {
	'use strict';

	angular.module('polsoftApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('devicegraph', {
			parent : 'app',
			url : '/devicegraph',
			data : {
				authorities : []
			},
			views : {
				'content@' : {
					templateUrl : 'app/device/device.html',
					controller : 'DeviceDataController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				mainTranslatePartialLoader : [ '$translate',
						'$translatePartialLoader',
						function($translate, $translatePartialLoader) {
							$translatePartialLoader.addPart('channel');
							return $translate.refresh();
						} ]
			},
			onEnter : [ 'DataSocketService', function(DataSocketService) {
				DataSocketService.subscribe();
			} ],
			onExit : [ 'DataSocketService', function(DataSocketService) {
				DataSocketService.unsubscribe();
			} ]
		});
	}
})();
