(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('HomeService', HomeService);

        HomeService.$inject = [];

    function HomeService() {
        var service = {
            getInfo: getInfo,
            setInfo: setInfo
        },info;

        // .................

        function getInfo() {
            return info;
        }

        function setInfo(value) {
            info = value;
        }

        return service;

       
    }
})();
