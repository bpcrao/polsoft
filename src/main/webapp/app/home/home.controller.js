(function () {
    'use strict';

    angular.module('polsoftApp').controller('HomeController', HomeController);

    HomeController.$inject = ['$scope','$window','Auth', 'Principal', 'LoginService', '$state', 'Locationtree', '$timeout', 'Reports', 'HomeService'];

    function HomeController($scope, $window,Auth,Principal, LoginService, $state, Locationtree, $timeout, Reports, HomeService) {
        var vm = this;
    
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.isLiveView = false;
        $scope.live_Turn_Off_By_User = false;
        var tree;
        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });


        $scope.onExit = function() {
           // debugger;
            //return ('bye bye');
            console.log("calling logout");
            Auth.logout();
            // $http.post('api/logout').success(function (response) {
                
            //     delete $localStorage.authenticationToken;
            //     // to get a new csrf token call the api
            //     $http.get('api/account');
            //     return response;
            // });

            
          };
      
         $window.onbeforeunload =  $scope.onExit;

        getAccount();

        function getAccount() {
            console.log('Get Account called');
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        function register() {
            $state.go('register');
        }

        $scope.my_tree_handler = function (branch, isDeselect, isOverView) {
            if (!isDeselect) {
                //$timeout.cancel($scope.timer);
                $scope.selectedLocation = branch;
                // HomeService.setInfo({branch:branch,treeData:$scope.my_data});
                $scope.getLocationData(isOverView);
                if (!$scope.live_Turn_Off_By_User) {
                    $scope.isLiveView = true;
                }

            }

        };

        // $scope.$on('$viewContentLoaded', function() {
        //     // Code you want executed every time view is opened
        //     console.log('Page Refresh! ',HomeService.getInfo());
        //     if(HomeService.getInfo())
        //         {
        //             $scope.selectedLocation = HomeService.getInfo().branch;
        //             $scope.my_data = HomeService.getInfo().treeData;
        //             $scope.getLocationData();
        //         }

        //  });

        $scope.getFirstObjectOfAlarm=function()
        {
           // console.log(Object.keys($scope.alarmData)[0])
            return Object.keys($scope.alarmData)[0];
        }
        
		$scope.getFirstObject=function()
		{
           // console.log(Object.keys($scope.tbData)[0]);
           if($scope.tbData)
		    return Object.keys($scope.tbData)[0];
		}
        $scope.changeToTable = function (showTable,isOverView) {
            $scope.isFromOpen = false;
            $scope.isToOpen = false;
            $scope.showTable = showTable;
            $scope.isOverView=isOverView;
            //if (showTable) {
                $scope.getLocationData(isOverView);
            //}
        }

        $scope.isFromOpen = false;
        $scope.isToOpen = false;

        $scope.fromAndToDates = {};



        $scope.maxDate = new Date();



        $scope.dateOpened = false;
        $scope.hourStep = 1;
        $scope.format = "dd-MMM-yyyy";
        $scope.minuteStep = 1;
        $scope.showMeridian = true;


        $scope.datepickers = {
            dtFrom: false,
            dtTo: false
        }


        $scope.toggleOpenDatePicker = function ($event, which) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepickers[which] = true;
        };

        $scope.changeData = function (fromDate, Todate, whichOneChanged, isOverView) {
            $scope.getLocationData(isOverView);
        }



        /*Random Data Generator */
        $scope.getLocationData = function (isOverView) {
            $('#mydiv').show();
            var queryData = Reports.getMultipleSeletedLocation($scope.my_data,isOverView);


            $scope.arrayOfSelectedLocations = queryData.arrayOfSelectedLocations;
            $scope.arrayOfSelectedLocationNames = queryData.arrayOfSelectedLocationNames;
            console.log('Query for Multiple Location is ', queryData.query);

            if ($scope.arrayOfSelectedLocations.length > 0) {
            	if($scope.isOverView == false || $scope.isOverView == undefined){
            	//	$scope.fromAndToDates.toDate.setSeconds(60);
            	//	$scope.fromAndToDates.fromDate.setSeconds(-60)
                Reports.getDataBasedonLocation($scope.selectedLocation.id, $scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate, queryData.query, $scope.showTable)
                    .then(function (response) {
                        setTimeout(function () {
                            $('#mydiv').hide();
                        }, 10);

                        if (response.data && response.data.results.length > 0 && response.data.results[0].series != undefined && response.data.results[0].series.length > 0) {
                            $scope.reportsData = response.data.results[0].series[0];
                            if (!$scope.isLiveView) {
                                d3.selectAll("svg > *").remove();
                            }
                            sinAndCos($scope.showTable?response.data.results[0].series[0].values:response.data.results[0].series);
                            $scope.noDataAvailble = false;
                        } else {
                            d3.selectAll("svg > *").remove();

                            $scope.reportsData = {};
                            sinAndCos(response.data.results[0]);
                            $scope.noDataAvailble = true;
                        }

                    });
                //Data is represented as an array of {x,y} pairs.
            	}
            	else {
            		Reports.getLastDataBasedonLocation($scope.selectedLocation.id, queryData.query, $scope.showTable, $scope.arrayOfSelectedLocations.length)
                    .then(function (response) {
                        setTimeout(function () {
                            $('#mydiv').hide();
                        }, 10);
                        if (response.data && response.data.results.length > 0 && response.data.results[0].series != undefined && response.data.results[0].series.length > 0) {
                            $scope.reportsData = response.data.results[0].series[0];
                            if (!$scope.isLiveView) {
                                d3.selectAll("svg > *").remove();

                            }
                          
          
                            	sinAndCos($scope.showTable?response.data.results[0].series[0].values:response.data.results[0].series);
                            
                            $scope.noDataAvailble = false;
                        } else {
                            d3.selectAll("svg > *").remove();

                            $scope.reportsData = {};
                            sinAndCos(response.data.results[0]);
                            $scope.noDataAvailble = true;
                        }

                    });
            		}
            } else {
                setTimeout(function () {
                    $('#mydiv').hide();
                }, 10);
                d3.selectAll("svg > *").remove();
                $scope.arrayOfSelectedLocations = [];
                $scope.arrayOfSelectedLocationNames = [];
                $scope.data = sinAndCos([]);
            }
        };

        $scope.timer;

        $scope.$watchCollection('fromAndToDates', function (oldValues, newValues) {
            //console.log("Dates are changed");
            $scope.changeData($scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate,false,$scope.isOverView);

        });

        $scope.getFormatedValue = function (value) {
            return angular.isNumber(value) ? (Math.round(value * 10) / 10).toFixed(1) : value;
            //  return angular.isNumber(value)?value.toFixed(1):value;
        }

        $scope.enableLiveView = function () {
            $scope.isLiveView = !$scope.isLiveView;
            if ($scope.isLiveView) {
                $scope.setdatesForLiveView();
                $("#datesDiv").css('pointer-events', 'none');
            } else {
                $scope.live_Turn_Off_By_User = true;
                $("#datesDiv").css('pointer-events', 'auto');
            }
        }

        $scope.setdatesForLiveView = function () {
            var today = new Date();
            $scope.fromAndToDates.fromDate = new Date();

            $scope.fromAndToDates.fromDate.setHours($scope.fromAndToDates.fromDate.getHours() - 1);

            $scope.fromAndToDates.toDate = today;

        }

        $scope.setdatesForLiveView();

        $scope.updateGraph = function () {
            if ($scope.isLiveView) {
                $scope.timer = $timeout(function () {
                    $scope.setdatesForLiveView();
                    // console.log('%%%%%%%%%%  Live View is Enabled %%%%%%%%%%%%%%%%%%%', $scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate);
                }, 30000)
            }
        }


        $scope.livedTime = Date();
        $scope.updateTime = function () {
            $timeout(function () {
                $scope.trigger = Date();
                $scope.updateTime();
            }, 1000);

        }
        $scope.updateTime();
        $scope.$watch(function ()
        { return Date(); },
            function (newVal) {
                $scope.livedTime = newVal;
                //console.log('Data is changes live view status is ',$scope.isLiveView);
                if (new Date($scope.livedTime).getSeconds() == 5 && $scope.isLiveView) {
                    $scope.setdatesForLiveView();
                    console.log('%%%%%%%%%%  Live View is Enabled %%%%%%%%%%%%%%%%%%%', $scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate);
                }
                {

                }
            }
        );
        var coloursArray = ['#000000', '#FF0000', '#808000', '#FFFF00', '#008000', '#00FF00', '#008080', '#00FFFF', '#808080', '#000080', '#0000FF', '#800080', '#FF00FF', '#7d4537', '#4c5468', '#00FF00'];

        $scope.getGraphData = function (cos) {
            var arrayOfReturnArray = [];
            var n = 1;/*Math.floor(Math.random() * 16) + 1;*/
            angular.forEach(cos, function (value, key) {

                arrayOfReturnArray.push({
                    values: value,
                    key: $scope.arrayOfSelectedLocationNames[key],
                    color: coloursArray[n]
                })
                n++;

            })

            return arrayOfReturnArray;
        }

        function sinAndCos(resrData) {
 
            var graphandTableData = Reports.getTableData(resrData, $scope.arrayOfSelectedLocationNames, $scope.showTable,$scope.isOverView);
            if ($scope.showTable) {
                $scope.tbData = graphandTableData.tbData;
                $scope.alarmData = graphandTableData.alarmData;
                $scope.tabledata = graphandTableData.tabledata;
                $scope.tableColums = graphandTableData.tableColums;
                if ($scope.tableColums)
                    delete $scope.tableColums["0"];
            } else {
                $scope.unitBasedGraphData = graphandTableData.cos;

                //Data is represented as an array of {x,y} pairs.

                $scope.GraphDataHum = $scope.getGraphData($scope.unitBasedGraphData['Humidity %RH']);
                $scope.GraphDataTempF = $scope.getGraphData($scope.unitBasedGraphData['Temperature DegF']);
                $scope.GraphDataTempC = $scope.getGraphData($scope.unitBasedGraphData['Temperature DegC']);
                console.log('$scope.GraphDataHum ', $scope.GraphDataHum, $scope.GraphDataTempF, $scope.GraphDataTempC);
            }
        };

        var treedata_avm = [{
            label: 'Plant',
            isChannel: false,
            id: 0,
            noLeaf: true
        }];

        $scope.my_tree = tree = {};


        Locationtree.query(function (result) {
            console.log("Getting Location $$$$ ", result);
            $scope.my_data = [result];
            $timeout(function () { $scope.my_tree.expand_all() }, 10);
        });

        $scope.my_data = treedata_avm;

        $scope.options = {
            chart: {
            	yDomain:([-50,200]),
                type: 'lineChart',
                height: 450,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 80,
                    left: 90
                },
                // x: function (d) {
                //     if (d && d.x) {

                //         return d.x;
                //     }
                // },
                y: function (d) {
                    if (d && d.y) {
                        //console.log('Valus is ',d.y);
                        return d.y === -8999 ? '0' : d.y === -9000 ? '0' : d.y === -9003 ? '0' : d.y;
                    }
                },
                useInteractiveGuideline: true,
                tooltips: true,
                dispatch: {
                    stateChange: function (e) { console.log("stateChange"); },
                    changeState: function (e) { console.log("changeState"); },
                    tooltipShow: function (e) { console.log("tooltipShow"); },
                    tooltipHide: function (e) { console.log("tooltipHide"); }
                },
                //  xScale: d3.time.scale(),
                //xDomain:[$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate],
                //  xScale:d3.time.scale().clamp(true),
                // duration: 60,
                xAxis: {
                    // scale: d3.time.scale().domain([$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate]),
                    domain: [$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate],
                    rotateLabels: -15,
                    axisLabel: 'Date',
                    tickFormat: function (d) {
                        var newD = angular.copy(d);
                        //return d3.time.format('%X')(new Date(newD))
                        if ($scope.fromAndToDates.fromDate <= new Date(newD) && new Date(newD) <= $scope.fromAndToDates.toDate) {
                            return d3.time.format('%d-%m-%y %H:%M:%S')(new Date(newD));
                        }
                    },

                },
                yAxis: {
                    axisLabel: 'Temparature (°C)',
                    tickFormat: function (d) {
                        return d3.format('.01f')(d === -8999 ? '0' : d === -9000 ? '0' : d === -9003 ? '0' : d);
                    },
                    axisLabelDistance: -10,
                    ticks: 20
                },
                callback: function (chart) {
                    console.log("!!! lineChart callback !!!");
                }
            },
            title: {
                enable: true,
                text: ''
            },
            subtitle: {
                enable: false,
                text: 'Graph',
                css: {
                    'text-align': 'center',
                    'margin': '10px 13px 0px 7px'
                }
            },
            caption: {
                enable: false,
                html: '',
                css: {
                    'text-align': 'justify',
                    'margin': '10px 13px 0px 7px'
                }
            }
        };

        $scope.options2 = {
            chart: {
            	yDomain:([10,100]),
                type: 'lineChart',
                height: 450,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 80,
                    left: 90
                },
                // x: function (d) {
                //     if (d && d.x) {

                //         return d.x;
                //     }
                // },
                y: function (d) {
                    if (d && d.y) {
                        //console.log('Valus is ',d.y);
                        return d.y === -8999 ? '0' : d.y === -9000 ? '0' : d.y === -9003 ? '0' : d.y;
                    }
                },
                useInteractiveGuideline: true,
                tooltips: true,
                dispatch: {
                    stateChange: function (e) { console.log("stateChange"); },
                    changeState: function (e) { console.log("changeState"); },
                    tooltipShow: function (e) { console.log("tooltipShow"); },
                    tooltipHide: function (e) { console.log("tooltipHide"); }
                },
                //  xScale: d3.time.scale(),
                //xDomain:[$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate],
                //  xScale:d3.time.scale().clamp(true),
                // duration: 60,
                xAxis: {
                    // scale: d3.time.scale().domain([$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate]),
                    domain: [$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate],
                    rotateLabels: -15,
                    axisLabel: 'Date',
                    tickFormat: function (d) {
                        var newD = angular.copy(d);
                        //return d3.time.format('%X')(new Date(newD))
                        if ($scope.fromAndToDates.fromDate <= new Date(newD) && new Date(newD) <= $scope.fromAndToDates.toDate) {
                            return d3.time.format('%d-%m-%y %H:%M:%S')(new Date(newD));
                        }
                    },

                },
                yAxis: {
                    axisLabel: 'Temparature (°F)',
                    tickFormat: function (d) {
                        return d3.format('.01f')(d === -8999 ? '0' : d === -9000 ? '0' : d === -9003 ? '0' : d);
                    },
                    axisLabelDistance: -10,
                    ticks: 25
                },
                callback: function (chart) {
                    console.log("!!! lineChart callback !!!");
                }
            },
            title: {
                enable: true,
                text: ''
            },
            subtitle: {
                enable: false,
                text: 'Graph',
                css: {
                    'text-align': 'center',
                    'margin': '10px 13px 0px 7px'
                }
            },
            caption: {
                enable: false,
                html: '',
                css: {
                    'text-align': 'justify',
                    'margin': '10px 13px 0px 7px'
                }
            }
        };

        $scope.options3 = {
            chart: {
            	yDomain: ([10,100]),
                type: 'lineChart',
                height: 450,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 80,
                    left: 90
                },
                // x: function (d) {
                //     if (d && d.x) {

                //         return d.x;
                //     }
                // },
                y: function (d) {
                    if (d && d.y) {
                        //console.log('Valus is ',d.y);
                        return d.y === -8999 ? '0' : d.y === -9000 ? '0' : d.y === -9003 ? '0' : d.y;
                    }
                },
                useInteractiveGuideline: true,
                tooltips: true,
                dispatch: {
                    stateChange: function (e) { console.log("stateChange"); },
                    changeState: function (e) { console.log("changeState"); },
                    tooltipShow: function (e) { console.log("tooltipShow"); },
                    tooltipHide: function (e) { console.log("tooltipHide"); }
                },
                //  xScale: d3.time.scale(),
                //xDomain:[$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate],
                //  xScale:d3.time.scale().clamp(true),
                // duration: 60,
                xAxis: {
                    // scale: d3.time.scale().domain([$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate]),
                    domain: [$scope.fromAndToDates.fromDate, $scope.fromAndToDates.toDate],
                    rotateLabels: -15,
                    axisLabel: 'Date',
                    tickFormat: function (d) {
                        var newD = angular.copy(d);
                        //return d3.time.format('%X')(new Date(newD))
                        if ($scope.fromAndToDates.fromDate <= new Date(newD) && new Date(newD) <= $scope.fromAndToDates.toDate) {
                            return d3.time.format('%d-%m-%y %H:%M:%S')(new Date(newD));
                        }
                    },

                },
                yAxis: {
                    axisLabel: 'Humidity-RH(%)',
                    tickFormat: function (d) {
                        return d3.format('.01f')(d === -8999 ? '0' : d === -9000 ? '0' : d === -9003 ? '0' : d);
                    },
                    axisLabelDistance: -10,
                    ticks: 20
                },
                callback: function (chart) {
                    console.log("!!! lineChart callback !!!");
                }
            },
            title: {
                enable: true,
                text: ''
            },
            subtitle: {
                enable: false,
                text: 'Graph',
                css: {
                    'text-align': 'center',
                    'margin': '10px 13px 0px 7px'
                }
            },
            caption: {
                enable: false,
                html: '',
                css: {
                    'text-align': 'justify',
                    'margin': '10px 13px 0px 7px'
                }
            }
        };
    }
})();
