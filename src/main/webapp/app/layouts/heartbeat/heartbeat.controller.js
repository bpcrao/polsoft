(function() {
	'use strict';

	var heartBeat = angular.module('polsoftApp').controller(
			'HeartbeatController', HeartbeatController);

	HeartbeatController.$inject = [ '$scope', 'Keepalive', '$uibModal', 'Idle',
			'IdleSettingsService', 'Principal', 'Auth', '$state',
			'LoginService' ];

	function HeartbeatController($scope, Keepalive, $uibModal, Idle,
			IdleSettingsService, Principal, Auth, $state, LoginService) {
		var vm = this;
		vm.idleTime = 900;
		vm.timeOut = 900;

		$scope.$on('authenticationSuccess', function() {
			Idle.watch();
			Idle.setIdle(vm.idleTime); // in seconds
			Idle.setTimeout(vm.timeOut); // in seconds 
			getAccount();
		});

		getAccount();

		function getAccount() {
			Principal.identity().then(function(account) {
				vm.account = account;
				vm.isAuthenticated = Principal.isAuthenticated;
			});
		}

		function closeModals() {
			if ($scope.warning) {
				$scope.warning.close();
				$scope.warning = null;
			}

			if ($scope.timedout) {
				$scope.timedout.close();
				$scope.timedout = null;
			}
		}

		$scope.$on('IdleStart', function() {
			if (vm.isAuthenticated) {
				closeModals();

				$scope.warning = $uibModal.open({
					templateUrl : 'warning-dialog.html',
					windowClass : 'modal-danger'
				});
			}
		});

		$scope.$on('IdleEnd', function() {
			if (vm.isAuthenticated) {
				closeModals();
			}
		});

		$scope.$on('IdleTimeout', function() {
			if (vm.isAuthenticated) {
				closeModals();
				Auth.logout();
				$state.go('loginView');
				//LoginService.open();
			}
		});

	}
	;

})();
