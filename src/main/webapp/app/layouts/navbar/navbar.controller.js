(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$scope', '$state', 'Auth', 'Principal', 'ProfileService', 'LoginService','$rootScope','Alarms','AlarmsTrackerService','JhiConfigurationService','BackupService'];

    function NavbarController($scope, $state, Auth, Principal, ProfileService, LoginService, $rootScope,Alarms,AlarmsTrackerService,JhiConfigurationService,BackupService) {
        var vm = this;
        vm.isNavbarCollapsed = true;
        vm.showExpiryMessage = false;
        vm.showPasswordExpiredMessage = false;
        vm.isAuthenticated = Principal.isAuthenticated;
        $rootScope.alarms = 0;
        $rootScope.warnings = 0
        
        vm.isRestore = [];
        
        
        ProfileService.getProfileInfo().then(function (response) {
            vm.inProduction = response.inProduction;
            vm.isRestore= response.activeProfiles.filter(function(profile) {
            	return "restore" == profile
            	});
            vm.swaggerEnabled = response.swaggerEnabled;
        });
        AlarmsTrackerService.subscribe();
        
        vm.login = login;
        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.$state = $state;
        vm.getAlarmsCount = getAlarmsCount;
        AlarmsTrackerService.receive().then(null, null, function (data) {
        	vm.getAlarmsCount();
        });
        vm.verifyFileName ="";
        BackupService.verifyFolderName().then(function(data){
        	 vm.verifyFileName = data.data; 
         });
        vm.getAlarmsCount();
        function getAlarmsCount()
        {
        	Alarms.count().then(function (response) {
            	if(response && response.data && response.data[0]){
            		if(response.data[0].severity == 'warning'){
    	        		$rootScope.warnings = response.data[0].alarms_count;
    	        		$rootScope.alarms = response.data[1].alarms_count;
            		}else{
            			$rootScope.warnings = response.data[1].alarms_count;
    	        		$rootScope.alarms = response.data[0].alarms_count;
                	}
            	}
            });
        }
        function login() {
            collapseNavbar();
            //LoginService.open();
            $state.go('loginView');
        }

        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        getAccount();
        checkExpiryMessage();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                checkExpiryMessage();
            });
        }

        function checkExpiryMessage() {
            if (vm.account && vm.account.expiryDate) {
                var days = moment.duration(moment(vm.account.expiryDate).diff(moment(new Date()))).asDays();
                if (days >= 0 && days <= 15) {
                    vm.showExpiryMessage = true;
                } else if (days < 0) {
                    vm.showPasswordExpiredMessage = true;
                } else if (days > 15) {
                    vm.showExpiryMessage = false;
                    vm.showPasswordExpiredMessage = false;
                }
            }
        }

        function logout() {
            collapseNavbar();
            Auth.logout();
            vm.account.login = null;
            $state.go('loginView');
            AlarmsTrackerService.unsubscribe();
        }

        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }
    }
})();
