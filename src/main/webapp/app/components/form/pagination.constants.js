(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
