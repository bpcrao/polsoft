(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LoginViewController', LoginController);

    LoginController.$inject = ['$rootScope', '$state', '$timeout', 'Auth'];

    function LoginController($rootScope, $state, $timeout, Auth, $uibModalInstance) {
        var vm = this;
        vm.authenticationError = false;
        vm.cancel = cancel;
        vm.credentials = {};
        vm.login = login;
        vm.password = null;
        vm.register = register;
        vm.rememberMe = false;
        vm.requestResetPassword = requestResetPassword;
        vm.username = null;
        vm.additionaMessage = '';
        Auth.logout();
        $timeout(function () {
            angular.element('#username').focus();
        });

        function cancel() {
            vm.credentials = {
                username: null,
                password: null,
                rememberMe: true
            };
            vm.authenticationError = false;
            // $uibModalInstance.dismiss('cancel');
        }

        function login(event, form) {
            
            if (form.$valid) {
                vm.userDeactivated = false;                
                event.preventDefault();
                if(!vm.isLoginInProgress){
                	vm.isLoginInProgress = true;
                Auth.login({
                    username: vm.username,
                    password: vm.password,
                    rememberMe: vm.rememberMe
                }).then(function (data) {
                	vm.isLoginInProgress = false;
                    vm.authenticationError = false;
                    if ($state.current.name === 'register' || $state.current.name === 'activate' ||
                        $state.current.name === 'finishReset' || $state.current.name === 'requestReset' || $state.current.name === 'loginView') {
                        $state.go('home');
                    }

                    $rootScope.$broadcast('authenticationSuccess');

                    // previousState was set in the authExpiredInterceptor
					// before being redirected to login modal.
                    // since login is succesful, go to stored previousState and
					// clear previousState
                    if (data.headers('resetKey')) {
                        $state.go('finishReset', {
                            'key': data.headers('resetKey')
                        });
                    } else if (Auth.getPreviousState() && Auth.getPreviousState().name != "loginView") {
                        var previousState = Auth.getPreviousState();
                        Auth.resetPreviousState();
                        $state.go(previousState.name, previousState.params);
                    }
                }).catch(function (error) {
                	vm.isLoginInProgress = false;
                	if (error.data.message === "SessionExists") {
                		vm.userSessionExists = true;
                        vm.authenticationError = false;
                        vm.userSessionExceeded = false;
                        vm.UserloggedinIPNotAllowed = false;
                        vm.userDeactivated = false;
                	}
                	else if (error.data.message === "userSessionExceeded") {
                		vm.userSessionExceeded = true;
                        vm.authenticationError = false;
                        vm.userSessionExists = false;
                        vm.UserloggedinIPNotAllowed = false;
                        vm.userDeactivated = false;
                	}
                	else if (error.data.message === "UserloggedinIPNotAllowed") {
                		vm.UserloggedinIPNotAllowed = true;
                        vm.authenticationError = false;
                        vm.userSessionExists = false;
                        vm.userSessionExceeded = false;
                        vm.userDeactivated = false;
                	}
                	else if (error.data.message === "Deactivated") {
                        vm.userDeactivated = true;
                        vm.authenticationError = false;
                        vm.userSessionExists = false;
                        vm.userSessionExceeded = false;
                        vm.UserloggedinIPNotAllowed = false;
                    } else {
                        vm.authenticationError = true;
                        vm.userDeactivated = false;
                        vm.userSessionExists = false;
                        vm.userSessionExceeded = false;
                        vm.UserloggedinIPNotAllowed = false;
                        if (error.data.message === 'we do not know such user' || error.data.message.indexOf('more attempts left.') != -1) {
                            vm.authenticationError = false;
                            vm.userDeactivated = false;
                            vm.userSessionExists = false;
                            vm.userSessionExceeded = false;
                            vm.UserloggedinIPNotAllowed = false;
                            vm.additionaMessage = error.data.message || '';
                        }

                    }
                });
                }
            }
        }

        function register() {
            // $uibModalInstance.dismiss('cancel');
            $state.go('register');
        }

        function requestResetPassword() {
            // $uibModalInstance.dismiss('cancel');
            $state.go('requestReset');
        }
    }
})();
