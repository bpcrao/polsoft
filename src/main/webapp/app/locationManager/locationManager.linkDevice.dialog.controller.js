(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LocationManagementLinkDeviceDialogController', LocationManagementLinkDeviceDialogController);

    LocationManagementLinkDeviceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserPermission', 'User', 'LocationManagerService','Principal','Dchannel'];

    function LocationManagementLinkDeviceDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, UserPermission, User, LocationManagerService,Principal,Dchannel) {
        var vm = this;

        vm.addUser = { "permissions": [{ "permission": 32, "granting": false }, { "permission": 64, "granting": false }, { "permission": 128, "granting": false }] };
        vm.clear = clear;
        vm.linkDevice = linkDevice;
        vm.devices =  Dchannel.query();
        console.log('$stateParams ', $stateParams);

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });


        function clear() {
            $uibModalInstance.dismiss('cancel');
        }


        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                console.log('Checking UserData', vm.account,Principal.isAuthenticated());
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        function linkDevice(linkDeviceId) {
             vm.isSaving = true;
             var channelLinkObj =$stateParams.location

            channelLinkObj.dchannelId=linkDeviceId;

            LocationManagerService.linkChannel(channelLinkObj)
                .success(onSaveSuccess)
                .error(onSaveError);
        }

        function onSaveSuccess(result) {
            $scope.$emit('polsoftApp:LocationManagerLinkDeviceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError(result) {
                $scope.$emit('polsoftApp:LocationManagerLinkDeviceUpdate', result);

                 $uibModalInstance.close(result);
            vm.isSaving = false;
        }


    }
})();
