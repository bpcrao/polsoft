(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LocationManagerController', LocationManagerController);

    LocationManagerController.$inject = ['$scope', 'Locationtree', 'Principal', 'LoginService', '$state', 'LocationManagerService', '$rootScope', 'Device', '$timeout', 'AlarmtemplateService1', 'naturalService'];

    function LocationManagerController($scope, Locationtree, Principal, LoginService, $state, LocationManagerService, $rootScope, Device, $timeout, AlarmtemplateService1, naturalService) {
        var vm = this;
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        $scope.hourStep = 1;
        $scope.minuteStep = 1;
        vm.unit = "";
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        $rootScope.$on('polsoftApp:LocationManagerAdduserUpdate', function(event, result) {
            $scope.getUsers($scope.selectedLocation);
        })

        $rootScope.$on('polsoftApp:LocationManagerLinkDeviceUpdate', function(event, result) {
            $scope.getChannels($scope.selectedLocation);
        })

        $rootScope.natural = function(field) {
            return function(item) {
                return naturalService.naturalValue(item[field]);
            }
        };

        $scope.alaramValues = {
            id: null,
            dchannelId: null
        };

        $scope.saveAlaram = function(saveAlaramForm, alaramObj) {
            if (saveAlaramForm.$valid) {
                LocationManagerService.updateAlaram(alaramObj)
                    .success(function(res) {
                        console.log('Save Alaram Response ', res);
                    })
                    .error(function(error) {
                        console.log('Save Alaram error  ', error);
                    });
            }

        }
        $scope.getUnits = function() {
            LocationManagerService.getUnits()
                .success(function(response) {
                    console.log('getUnits response ', response);
                    $scope.units = response;
                })
        }
        $scope.getUnits();
        $scope.oneAtATime = true;
        $scope.groups = [];
        $scope.status = {
            isCustomHeaderOpen: false,
            isFirstOpen: true,
            isFirstDisabled: false
        };

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        $scope.my_tree_handler = function(branch) {
            console.log('clicked row ', branch);
            $scope.selectedLocation = branch;
            vm.unit = $scope.selectedLocation.unit;
            $scope.alaramValues.id = branch.id;
            $scope.alaramValues.dchannelId = branch.dchannelId;
            vm.alarmtemplate = {};
            $scope.branch = branch;
            $scope.dchannelId = branch.dchannelId;
            $scope.getUsers(branch)
            $scope.getChannels(branch);
        };

        $scope.getUsers = function(branch) {
            LocationManagerService.getUsers(branch.id)
                .success(function(response) {
                    console.log('getUsers response ', response);
                    $scope.groups = response.permissions;
                })
        }

        $scope.getChannels = function(branch) {
            LocationManagerService.getChannels(branch.id)
                .success(function(response) {
                    console.log('getChannels response ', response);
                    $scope.linkedChanels = response;
                    $scope.templateName = "";
                    if ($scope.alarmtemplates) {
                        $scope.alarmtemplates = [];
                        $scope.alarmtemplates.length = 0;
                    }
                    vm.unit = response.unit;
                    if ($scope.linkedChanels.dchannelId != null) {
                        $scope.getAlaram($scope.linkedChanels.dchannelId);
                    }

                    $scope.allDevices = Device.query();

                })
        }

        $scope.linkUnit = function() {
            var channelLinkObj = $scope.selectedLocation;
            channelLinkObj.unit = vm.unit;
            LocationManagerService.saveUnit(channelLinkObj)
                .success(function(res) {
                    console.log('Save Unit Response ', res);
                })
                .error(function(error) {
                    console.log('Save Unit error  ', error);
                });
        }

        $scope.linkDevice = function(linkDeviceId, deviceId, DeviceName) {
            var channelLinkObj = $scope.selectedLocation;
            channelLinkObj.dchannelId = linkDeviceId;
            channelLinkObj.dchannelId = linkDeviceId;
            channelLinkObj.device_Id = deviceId;
            channelLinkObj.device_Name = DeviceName;
            LocationManagerService.linkChannel(channelLinkObj)
                .success(function(result) {
                    $scope.$emit('polsoftApp:LocationManagerLinkDeviceUpdate', result);
                })
                .error(function(result) {});
        }


        $scope.unlinkDevice = function(linkDeviceId) {
            var channelLinkObj = $scope.selectedLocation;
            channelLinkObj.dchannelId = null;
            LocationManagerService.linkChannel(channelLinkObj)
                .success(function(result) {
                    $scope.$emit('polsoftApp:LocationManagerLinkDeviceUpdate', result);

                })
                .error(function(result) {});
        }



        $scope.updatePermitions = function(permitions_Object, userId) {
            console.log('permitionsObject Update', permitions_Object);
            var permitionsObject = angular.copy(permitions_Object);
            permitionsObject.userId = userId;

            delete permitionsObject.permissions[0]['user'];
            delete permitionsObject.permissions[1]['user'];
            delete permitionsObject.permissions[2]['user'];

            permitionsObject.permissions[0].granting = permitionsObject.permissions[0].granted;
            delete permitionsObject.permissions[0].granted;

            permitionsObject.permissions[1].granting = permitionsObject.permissions[1].granted;
            delete permitionsObject.permissions[1].granted;

            permitionsObject.permissions[2].granting = permitionsObject.permissions[2].granted;
            delete permitionsObject.permissions[2].granted;


            LocationManagerService.createUser(permitionsObject, $scope.selectedLocation)
                .success(function(result) {
                    $scope.$emit('polsoftApp:LocationManagerAdduserUpdate', result);
                })
                .error(function(result) {});
        }

        $scope.removeUser = function(permitions_Object, userId) {

            var permitionsObject = angular.copy(permitions_Object);
            permitionsObject.userId = userId;
            permitionsObject.permissions.forEach(function(permission) {
                delete permission['user'];
                permission.granting = false;
                delete permission.granted;
            });
            permitionsObject.permissions.push({
                granting: false,
                permission: 16
            });

            LocationManagerService.removeUser(permitionsObject, $scope.selectedLocation)
                .success(function(result) {
                    $scope.$emit('polsoftApp:LocationManagerAdduserUpdate', result);
                })
                .error(function(result) {});
        }

        $scope.getAlaram = function(dchannelId) {
            AlarmtemplateService1.getAlaramTemplatesWithChannel(dchannelId)
                .then(function(response) {
                    console.log('getAlaram response ', response);
                    $scope.alarmtemplates = response.data;
                    if ($scope.alarmtemplates && $scope.alarmtemplates.length > 0) {
                        AlarmtemplateService1.getAlaramTempateUsingId($scope.alarmtemplates[0]).then(function(response) {
                            vm.alarmtemplate = response;
                        });
                        vm.alarmtemplate.dchannelId = $scope.dchannelId;
                        $scope.showSelect = false;
                    } else {
                        $scope.getAlaramTemplatesWithoutChannel();
                        $scope.showSelect = true;
                    }
                })
        }

        $scope.getChannelsForDevice = function(deviceId) {
            LocationManagerService.getAllChannelsBasedOnDeviceId(deviceId)
                .success(function(response) {
                    console.log('getChannelsForDevice response ', response);
                    $scope.deviceChannels = response;
                })
        }

        $scope.selectedTab = 'devices';

        $scope.changeTab = function(tabName) {
            $scope.selectedTab = tabName;
        }
        var treedata_avm = [{
            label: 'Plant',
            isChannel: false,
            id: 0,
            noLeaf: true
        }];
        $scope.my_tree = {};

        Locationtree.query(function(result) {
            console.log('result ', result);
            $scope.my_data = [result];
            $timeout(function() {
                $scope.my_tree.expand_all()
            }, 10);
        });

        $scope.my_data = treedata_avm;

        // Alarm template code starts
        vm.save = save;
        $scope.defaultValues = [{
            id: 1,
            name: 'closed'
        }, {
            id: 2,
            name: 'opened'
        }];
        $scope.measurePoints = [{
            id: 1,
            name: 'closed'
        }, {
            id: 2,
            name: 'opened'
        }];

        $scope.getAlaramTemplates = function() {
            AlarmtemplateService1.getAlaramTemplates()
                .then(function(response) {
                    console.log('getAlaramTemplates response ', response);
                    $scope.alarmtemplates = response;
                })
        }

        $scope.getAlaramTemplatesWithoutChannel = function() {
            AlarmtemplateService1.getAlaramTemplatesWithoutChannel()
                .then(function(response) {
                    console.log('getAlaramTemplatesWithoutChannel response ', response);
                    $scope.alarmtemplates = response.data;
                })
        }

        $scope.deleteAlarmTemplate = function() {
            AlarmtemplateService1.deleteAlaramTempates(vm.alarmtemplate).then(function(data) {
                vm.alarmtemplate = data;
                $scope.showSelect = true;
            }).then(function(error) {

            });
        }
        $scope.getAlarmTemplate = function(template) {
            if (template)
                AlarmtemplateService1.getAlaramTempateUsingId(template).then(function(response) {
                    vm.alarmtemplate = response;
                });
        }

        function save() {
            $scope.started = true;

            vm.alarmtemplate.dchannelId = $scope.branch.dchannelId | $scope.dchannelId;
            if ($scope.showSelect) {
                vm.alarmtemplate.newObject = true;
            }
            if (vm.alarmtemplate.alarmtemplateschedulars && vm.alarmtemplate.alarmtemplateschedulars.length > 0) {
                angular.forEach(vm.alarmtemplate.alarmtemplateschedulars, function(value) {
                    //THIS MUST FOLLOW THE PATTERN HH:MM 24 hour format
                    value.starttime = value.starttime.toString();
                    value.endtime = value.endtime.toString();
                });
            }
            if (vm.alarmtemplate.alarmtemplateschedulars) {

                AlarmtemplateService1.saveAlarmTemplate(vm.alarmtemplate).then(function(response) {
                    console.log('Refreshing the alarms');
                    $scope.getAlaram($scope.linkedChanels.dchannelId);

                    if (!response.valid) {
                        $scope.serverError = response.data;
                    } else {
                        $scope.getAlaramTemplates();
                    }
                });
            }
        }
    }
})();