(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LocationManagementAddUserDialogController', LocationManagementAddUserDialogController);

    LocationManagementAddUserDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserPermission', 'User', 'LocationManagerService','Principal'];

    function LocationManagementAddUserDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, UserPermission, User, LocationManagerService,Principal) {
        var vm = this;

        vm.addUser = { "permissions": [{ "permission": 32, "granting": false }, { "permission": 64, "granting": false }, { "permission": 128, "granting": false }] };
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        console.log('$stateParams ', $stateParams);

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });


        function clear() {
            $uibModalInstance.dismiss('cancel');
        }


        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                console.log('Checking UserData', vm.account,Principal.isAuthenticated());
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        function save(permitionsObject) {
            vm.isSaving = true;
            console.log('Creating new User', permitionsObject);
            LocationManagerService.createUser(permitionsObject,$stateParams.location)
                .success(onSaveSuccess)
                .error(onSaveError);
        }

        function onSaveSuccess(result) {
            $scope.$emit('polsoftApp:LocationManagerAdduserUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError(result) {
                $scope.$emit('polsoftApp:LocationManagerAdduserUpdate', result);

                 $uibModalInstance.close(result);
            vm.isSaving = false;
        }


    }
})();
