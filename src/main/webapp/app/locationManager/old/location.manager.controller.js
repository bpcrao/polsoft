(function() {
	'use strict';

	angular.module('polsoftApp').controller('LocationManagerController1',
			LocationManagerController);

	LocationManagerController.$inject = [ '$scope', 'Principal', 'LoginService',
			'$state', 'DeviceDataService', 'DataSocketService' ];

	function LocationManagerController($scope, Principal, LoginService, $state,
			DeviceDataService, DataSocketService) {
		var vm = this;
		
		$scope.remove = function(scope) {
			scope.remove();
		};

		$scope.toggle = function(scope) {
			scope.toggle();
		};

		$scope.moveLastToTheBeginning = function() {
			var a = $scope.data.pop();
			$scope.data.splice(0, 0, a);
		};

		$scope.newSubItem = function(scope) {
			var nodeData = scope.$modelValue;
			nodeData.nodes.push({
				id: nodeData.id * 10 + nodeData.nodes.length,
				title: nodeData.title + '.' + (nodeData.nodes.length + 1),
				nodes: []
			});
		};

		$scope.count = 0;

		$scope.newItemParentNotSet = function() {

			var newNode = {
				'id': $scope.count++,
				'title': 'NewText',
				'parentSetByUser': false,
				'nodes': []
			};

			$scope.deviceHierarchy.push(newNode);
		};

		$scope.saveHierarchy = function() {
			var obj = $scope.deviceHierarchy;
			var property = 'parentSetByUser';

			function setParentSetByUser(obj, propToChange, value) {
				Object.keys(obj).forEach(function(key) {
					if (typeof obj[key] === 'object') {
						return setParentSetByUser(obj[key], propToChange, value);
					}
					if (key == propToChange) {
						obj[key] = value;
					}
				});
			}
			setParentSetByUser(obj, 'parentSetByUser', true);
		};

		$scope.collapseAll = function() {
			$scope.$broadcast('angular-ui-tree:collapse-all');
		};

		$scope.expandAll = function() {
			$scope.$broadcast('angular-ui-tree:expand-all');
		};

		$scope.deviceHierarchy = [{
			'id': 1,
			'title': 'node1',
			'parentSetByUser': true,
			'nodes': [{
				'id': 11,
				'title': 'node1.1',
				'parentSetByUser': true,
				'nodes': []
			}]
		}];        
	}
})();
