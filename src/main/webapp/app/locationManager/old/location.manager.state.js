(function() {
	'use strict';

	angular.module('polsoftApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('locationmanager_old', {
			parent : 'app',
			url : '/locationmanager_old',
			data : {
				authorities : []
			},
			views : {
				'content@' : {
					templateUrl : 'app/locationManager/old/location.manager.html',
					controller : 'LocationManagerController1',
					controllerAs : 'vm'
				}
			},
			resolve : {
				mainTranslatePartialLoader : [ '$translate',
						'$translatePartialLoader',
						function($translate, $translatePartialLoader) {
							$translatePartialLoader.addPart('channel');
							return $translate.refresh();
						} ]
			},
			onEnter : [ 'DataSocketService', function(DataSocketService) {
				DataSocketService.subscribe();
			} ],
			onExit : [ 'DataSocketService', function(DataSocketService) {
				DataSocketService.unsubscribe();
			} ]
		});
	}
})();
