(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('LocationManagerService', LocationManagerService)
        .factory("naturalService", ["$locale", function($locale) {
            // the cache prevents re-creating the values every time, at the expense of
            // storing the results forever. Not recommended for highly changing data
            // on long-term applications.
        var natCache = {},
            // amount of extra zeros to padd for sorting
            padding = function(value) {
                return '00000000000000000000'.slice(value.length);
            };
             function natValue (value) {
                if(natCache[value]) {
                    return natCache[value];
                }
                var newValue = fixNumbers(fixDates(value));
                return natCache[value] = newValue;
            }
            
            // Calculate the default out-of-order date format (d/m/yyyy vs m/d/yyyy)
           var natDateMonthFirst = $locale.DATETIME_FORMATS.shortDate.charAt(0) == 'm';
            // Replaces all suspected dates with a standardized yyyy-m-d, which is fixed below
            function fixDates (value) {
                // first look for dd?-dd?-dddd, where "-" can be one of "-", "/", or "."
                return value.replace(/(\d\d?)[-\/\.](\d\d?)[-\/\.](\d{4})/, function($0, $m, $d, $y) {
                    // temporary holder for swapping below
                    var t = $d;
                    // if the month is not first, we'll swap month and day...
                    if(!natDateMonthFirst) {
                        // ...but only if the day value is under 13.
                        if(Number($d) < 13) {
                            $d = $m;
                            $m = t;
                        }
                    } else if(Number($m) > 12) {
                        // Otherwise, we might still swap the values if the month value is currently over 12.
                        $d = $m;
                        $m = t;
                    }
                    // return a standardized format.
                    return $y+'-'+$m+'-'+$d;
                });
            }
            
            // Fix numbers to be correctly padded
            function fixNumbers (value) {
                 // First, look for anything in the form of d.d or d.d.d...
                return value.replace(/(\d+)((\.\d+)+)?/g, function ($0, integer, decimal, $3) {
                    // If there's more than 2 sets of numbers...
                    if (decimal !== $3) {
                        // treat as a series of integers, like versioning,
                        // rather than a decimal
                        return $0.replace(/(\d+)/g, function ($d) {
                            return padding($d) + $d
                        });
                    } else {
                        // add a decimal if necessary to ensure decimal sorting
                        decimal = decimal || ".0";
                        return padding(integer) + integer + decimal + padding(decimal);
                    }
                });
            }
    
            // Finally, this function puts it all together.
           
    var service={
        naturalValue: natValue,
        naturalSort: function(a, b) {

            a = natVale(a);
            b = natValue(b);
            return (a < b) ? -1 : ((a > b) ? 1 : 0)
        }
    }
        // The actual object used by this service
        return service;
    }]);

    LocationManagerService.$inject = ['$http', '$localStorage', 'JhiTrackerService', '$resource'];

    function LocationManagerService($http, $localStorage, JhiTrackerService) {
        var service = {
            getUsers: getUsers,
            createUser: createUser,
            removeUser: removeUser,
            saveAlaram: saveAlaram,
            getChannels :getChannels,
            getAlaram:getAlaram,
            updateAlaram:updateAlaram,
            linkChannel:linkChannel,
            getAllChannelsBasedOnDeviceId:getAllChannelsBasedOnDeviceId,
            getUnits:getUnits,
            saveUnit:saveUnit
        };

        return service;

        function getUsers(resource_Id) {
            var resourceUrl = '/api/permissions?resource=location&resourceId=' + resource_Id;
            return $http.get(resourceUrl)
                .success(function (response) {
                    return response;
                });
        }

         function getAllChannelsBasedOnDeviceId(deviceId) {
            var resourceUrl = '/api/dchannels/device/' + deviceId;
            return $http.get(resourceUrl)
                .success(function (response) {
                    return response;
                });
        }

        function getAlaram(dchannel_Id) {
            var resourceUrl = '/api/dchannels/' + dchannel_Id;
            return $http.get(resourceUrl)
                .success(function (response) {
                    return response;
                });
        }

        function getChannels(resource_Id) {
            var resourceUrl = '/api/locations/' + resource_Id;
            return $http.get(resourceUrl)
                .success(function (response) {
                    return response;
                });
        }
        
        function getUnits() {
            var resourceUrl = '/api/locations/units';
            return $http.get(resourceUrl)
                .success(function (response) {
                    return response;
                });
        }
        function removeUser(postObject, locationData) {
            var resourceUrl = '/api/permissions?resource=Location&resourceId=' + locationData.id + '&principal=' + postObject.userId;
            var requestObj=angular.copy(postObject);
            delete requestObj.userId;
            return $http.delete(resourceUrl, requestObj, {
                headers: {
                    'Content-Type': 'application/JSON'
                }
            }) .success(function (response) {
                    return response;
                }).error(function(error){
                    return error;
                });
        }

        function createUser(postObject, locationData) {
            var resourceUrl = '/api/permissions?resource=Location&resourceId=' + locationData.id + '&principal=' + postObject.userId;
            var requestObj=angular.copy(postObject);
            
            delete requestObj.userId;

            return $http.post(resourceUrl, requestObj, {
                headers: {
                    'Content-Type': 'application/JSON'
                }
            }) .success(function (response) {
                    return response;
                }).error(function(error){
                    return error;
                });
        }

        function linkChannel(postObject, locationData) {
            var resourceUrl = 'api/locations';
            return $http.put(resourceUrl, postObject, {
                headers: {
                    'Content-Type': 'application/JSON'
                }
            }) .success(function (response) {
                    return response;
                }).error(function(error){
                    return error;
                });
        }

         function updateAlaram(postObject, locationData) {
            var resourceUrl = 'api/channel-alarms';
            return $http.put(resourceUrl, postObject, {
                headers: {
                    'Content-Type': 'application/JSON'
                }
            }) .success(function (response) {
                    return response;
                }).error(function(error){
                    return error;
                });
        }


        function saveAlaram(postObj) {
            $http.post('api/save', postObj)
                .success(function (response) {
                    return response;
                });
        }
        
        function saveUnit(channelLinkObj) {
           return  $http.put('/api/locations/'+channelLinkObj.id+'/unit', channelLinkObj.unit)
            	.success(function (response) {
                    return response;
                });
        }
    }
})();
