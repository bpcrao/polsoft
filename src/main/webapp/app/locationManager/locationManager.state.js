(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('locationmanager', {
            parent: 'app',
            url: '/locationmanager',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/locationManager/locationManager.html',
                    controller: 'LocationManagerController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('home');
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('alarmtemplate');
                    $translatePartialLoader.addPart('alarmtemplateschedulars');
                    $translatePartialLoader.addPart('alarmtemplatelimit');
                    return $translate.refresh();
                }]
            }
        })
            .state('locationmanager.new', {
                parent: 'locationmanager',
                url: '/addUser',
                data: {
                    authorities: ['Administrator','Supervisor']
                },
                params: {
                    location: null
                },
                resolve: {
                    mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('location-manager');
                        $translatePartialLoader.addPart('global');

                        return $translate.refresh();
                    }]
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/locationManager/locationManager.addUser.dialog.html',
                        controller: 'LocationManagementAddUserDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    id: null, login: null, firstName: null, lastName: null, email: null,
                                    activated: true, langKey: null, createdBy: null, createdDate: null,
                                    lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                                    resetKey: null, authorities: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('locationmanager');
                    }, function () {
                        $state.go('locationmanager');
                    });
                }]
            })
            .state('locationmanager.linkDevice', {
                parent: 'locationmanager',
                url: '/linkDevice',
                data: {
                    authorities: ['Administrator','Supervisor']
                },
                params: {
                    location: null
                },
                resolve: {
                    mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('location-manager');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('device');
                        return $translate.refresh();
                    }]
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/locationManager/locationManager.linkDevice.dialog.html',
                        controller: 'LocationManagementLinkDeviceDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    id: null, login: null, firstName: null, lastName: null, email: null,
                                    activated: true, langKey: null, createdBy: null, createdDate: null,
                                    lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                                    resetKey: null, authorities: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('locationmanager');
                    }, function () {
                        $state.go('locationmanager');
                    });
                }]
            });
    }
})();
