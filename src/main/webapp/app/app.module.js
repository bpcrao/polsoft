(function() {
    'use strict';

    var app = angular.module('polsoftApp', ['ngStorage', 'tmh.dynamicLocale',
        'pascalprecht.translate', 'ngResource', 'ngCookies', 'ngAria',
        'ngCacheBuster', 'ngFileUpload', 'ui.bootstrap',
        'ui.bootstrap.datetimepicker', 'ui.router', 'infinite-scroll',
        "highcharts-ng", 'btorfs.multiselect', 'ngTagsInput', 'jsTag',
        'ngIdle', 'angularBootstrapNavTree', 'ui.bootstrap',
        'ds.objectDiff',
        'angular-loading-bar', 'angularMoment', 'ui.bootstrap',
        'ui.toggle', 'ngNumberPicker', 'ui.tree', 'nvd3',
        'angular-cron-jobs'
    ]);

    app.config(['KeepaliveProvider', function(KeepaliveProvider) {
        KeepaliveProvider.interval(60);
        //KeepaliveProvider.http('/api/heartbeat'); // URL that makes sure session is alive
    }]);

    app.run(run);

    run.$inject = ['stateHandler', 'translationHandler', 'Idle', 'Principal','Auth','$window'];

    function run(stateHandler, translationHandler, Idle, Principal,$window,Auth) {

        $window.onbeforeunload = function (evt) {
            console.log("Logout called");
            
            Auth.logout();

           

      return "Did you save your stuff?"
    
         }
        console.log('$$$$$$ Applicationb Started $$$$$$', Principal
            .isAuthenticated());
        Idle.watch();
        stateHandler.initialize();
        translationHandler.initialize();
    }
})();

