(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('alarms', {
            parent: 'entity',
            url: '/alarms?page&sort&search',
            data: {
                authorities: [],
                    permission: {
                        'resource': 'ALARMS',
                        'level': 'VIEW'
                    },
                    pageTitle: 'polsoftApp.alarm.home.title'
                    
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/alarms/alarms.html',
                    controller: 'AlarmsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'generated,desc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('alarms');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            },
            onEnter: ['AlarmsTrackerService', function(AlarmsTrackerService) {
            	AlarmsTrackerService.subscribe();
            }],
            onExit: ['AlarmsTrackerService', function(AlarmsTrackerService) {
//            	AlarmsTrackerService.unsubscribe();
            }]
        })
        .state('alarms-detail', {
            parent: 'entity',
            url: '/alarms/{id}',
            data: {
               authorities: [],
                    permission: {
                        'resource': 'ALARMS',
                        'level': 'VIEW'
                    },
                pageTitle: 'polsoftApp.alarms.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/alarms/alarms-detail.html',
                    controller: 'AlarmsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('alarms');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Alarms', function($stateParams, Alarms) {
                    return Alarms.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'alarms',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('alarms-detail.edit', {
            parent: 'alarms-detail',
            url: '/detail/edit',
            data: {
                authorities: ['Administrator','Supervisor'],
                    permission: {
                        'resource': 'ALARMS',
                        'level': 'EDIT'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/alarms/alarms-dialog.html',
                    controller: 'AlarmsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Alarms', function(Alarms) {
                            return Alarms.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('alarms.new', {
            parent: 'alarms',
            url: '/new',
            data: {
                authorities: ['Administrator','Supervisor'],
                    permission: {
                        'resource': 'ALARMS',
                        'level': 'CREATE'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/alarms/alarms-dialog.html',
                    controller: 'AlarmsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                type: null,
                                description: null,
                                duration: null,
                                created: null,
                                source: null,
                                action: null,
                                comments: null,
                                username: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('alarms', null, { reload: 'alarms' });
                }, function() {
                    $state.go('alarms');
                });
            }]
        })
        .state('alarms.edit', {
            parent: 'alarms',
            url: '/{id}/edit',
            data: {
                authorities: ['Administrator','Supervisor'],
                    permission: {
                        'resource': 'ALARMS',
                        'level': 'EDIT'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/alarms/alarms-dialog.html',
                    controller: 'AlarmsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Alarms', function(Alarms) {
                            return Alarms.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('alarms', null, { reload: 'alarms' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('alarms.delete', {
            parent: 'alarms',
            url: '/{id}/delete',
            data: {
                authorities: ['Administrator'],
                    permission: {
                        'resource': 'ALARMS',
                        'level': 'DELETE'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/alarms/alarms-delete-dialog.html',
                    controller: 'AlarmsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Alarms', function(Alarms) {
                            return Alarms.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('alarms', null, { reload: 'alarms' });
                }, function() {
                    $state.go('^');
                });
            }]
        }).state('alarms.template', {
            parent: 'alarms',
            url: '/template',
            data: {
                authorities: ['Administrator'],
                    permission: {
                        'resource': 'ALARMTEMPLATE',
                        'level': 'CREATE'
                    }
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/alarms/alarmtemplates.html',
                    controller: 'AlarmtemplateControllerNew',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('alarmtemplate');
                    $translatePartialLoader.addPart('alarmtemplateschedulars');
                    $translatePartialLoader.addPart('alarmtemplatelimit');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        });
    }

})();
