(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('AlarmsController', AlarmsController);

    AlarmsController.$inject = ['$scope', '$filter', '$state', 'Alarms', 'AlarmsSearch', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Principal', '$location', 'AlarmsTrackerService'];

    function AlarmsController($scope, $filter, $state, Alarms, AlarmsSearch, ParseLinks, AlertService, pagingParams, paginationConstants, Principal, $location, AlarmsTrackerService) {
        var vm = this;
       // vm.fromDate = null;
        vm.previousMonth = previousMonth;
        vm.onChangeDate = onChangeDate;
        //vm.toDate = null;
        vm.today = today;
        vm.dateClicked = false;
        vm.today();
       // vm.previousMonth();
       // vm.onChangeDate();
        vm.print = print;
        vm.sort = sort;
        vm.selectedAlarmData = null;

        $scope.isFromOpen = false;
        $scope.isToOpen = false;
        //var today = new Date();

        $scope.datepickers = {
            dtFrom: false,
            dtTo: false
        }
        $scope.toggleOpenDatePicker = function ($event, which) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepickers[which] = true;
        };
        function today() {
            // Today + 1 day - needed if the current day must be included
            var today = new Date();
            vm.toDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
            vm.previousMonth();
        }
        function previousMonth() {
            var fromDate = new Date();
            if (fromDate.getMonth() === 0) {
                fromDate = new Date(fromDate.getFullYear() - 1, 11, fromDate.getDate());
            } else {
                fromDate = new Date(fromDate.getFullYear(), fromDate.getMonth() - 1, fromDate.getDate());
            }

            vm.fromDate = fromDate;
            loadAll();
        }
        $scope.dateOpened = false;
        $scope.format = "dd-MMM-yyyy";
        vm.pauseAlarm = pauseAlarm;
        vm.acknolwdge = acknolwdge;
        Principal.identity().then(function (account) {
            vm.account = account;
        });
        function onChangeDate() {
            loadAll();
        }

        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        $scope.dateClickedOnce = function () 
        {
        	alert("hi");
        	vm.dateClicked = true;
        }
        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
            });
        }

        function print() {
            var dateFormat = 'yyyy-MM-dd';
            var fromDate = $filter('date')(vm.fromDate, dateFormat);
            var toDate = $filter('date')(vm.toDate, dateFormat);
            Alarms.findAll(fromDate, toDate, sort()).then(function (response) {
                vm.alarms = response.data;
            }).then(function () {
                $('#alrmsTable').hide();
                $('#printName').show();
                $('.hideWhenPrint').hide();
                $('#alrmsTablePrint').show();
                setTimeout(function () {
                    //return;
                    window.print();
                    $('#printName').hide();
                    $('#alrmsTable').show();
                    $('#alrmsTablePrint').hide();
                    $('.hideWhenPrint').show();

                }, 1000);

            });
        }
        function pauseAlarm(alarms, pause) {
            if (pause)
                alarms["ackStatus"] = "paused";
            else
                alarms["ackStatus"] = "un paused";
            updateAlarm(alarms);
        }

        function acknolwdge(alarms) {
            alarms["ackStatus"] = "acked";
            updateAlarm(alarms);
        }

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;
        vm.searchQuery = pagingParams.search;
        vm.currentSearch = pagingParams.search;
        $scope.showAlramData = false;
        $scope.showAlramDetails = function (alarmData) {
           // if (alarmData.ackrequired) {
                $scope.showAlramData = true;
                vm.selectedAlarmData = alarmData;
                $("#exampleModal").css("display", "block !important");
                $("#exampleModal").css("display", "block");
           // }
        }
       // loadAll();
        AlarmsTrackerService.receive().then(null, null, function (data) {
        	vm.predicate = 'id';
        	vm.reverse = '';
            vm.loadAll();
        });
        function loadAll() {
            var dateFormat = 'yyyy-MM-dd';
            var fromDate = $filter('date')(vm.fromDate, dateFormat);
            var toDate = $filter('date')(vm.toDate, dateFormat);
            if (pagingParams.search) {
                AlarmsSearch.query({
                    query: pagingParams.search,
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {

                Alarms.query({
                    fromDate: fromDate,
                    toDate: toDate,
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
            	if(!vm.predicate)
            	{
            		vm.predicate = 'generated';
            	}
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.alarms = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function search(searchQuery) {
            if (!searchQuery) {
                return vm.clear();
            }
            vm.links = null;
            vm.page = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.transition();
        }

        function clear() {
            vm.links = null;
            vm.page = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.currentSearch = null;
            vm.transition();
        }
        function sort() {
        	if(!vm.predicate)
        		{
        			vm.predicate = 'generated';
        		}
            var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            if (vm.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }

        function updateAlarm(alarms) {
            Alarms.update(alarms, function () {
                //vm.alarms.id.expanded1 = false;
                //vm.alarms.id.expanded = false;
                alert("updated record");
                loadAll();
            });
        }
    }
})();
