(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('AlarmtemplateControllerNew', ['$scope', '$state', 'AlarmtemplateService1', '$window', AlarmtemplateControllerNew]);

    AlarmtemplateControllerNew.$inject = ['$scope', '$state', 'AlarmtemplateService1', '$window'];

    function AlarmtemplateControllerNew($scope, $state, AlarmtemplateService1, $window) {
        var vm = this;

        // Alarm template code starts
        vm.save = save;

        $scope.defaultValues = [{ id: 1, name: 'closed' }, { id: 2, name: 'opened' }];
        $scope.measurePoints = [{ id: 1, name: 'closed' }, { id: 2, name: 'opened' }];

        $scope.date = new Date();
        $scope.dateTo = new Date();

        $scope.hourStep = 1;
        $scope.minuteStep = 1;


        $scope.resetValues = function () {
            vm.alarmtemplate = {};

            vm.alarmtemplate.alarmtemplateschedulars = [
                { day: 'Monday', checked: true, starttime: new Date("October 13, 2017 09:00:00  GMT+0530 (India Standard Time)"), endtime: new Date("October 13, 2017 18:00:00  GMT+0530 (India Standard Time)") },

                { day: 'Tuesday', checked: true, starttime: new Date("October 13, 2017 09:00:00"), endtime: new Date("October 13, 2017 18:00:00") },

                { day: 'Wednesday', checked: true, starttime: new Date("October 13, 2017 09:00:00"), endtime: new Date("October 13, 2017 18:00:00") },

                { day: 'Thursday', checked: true, starttime: new Date("October 13, 2017 09:00:00"), endtime: new Date("October 13, 2017 18:00:00") },

                { day: 'Friday', checked: true, starttime: new Date("October 13, 2017 09:00:00"), endtime: new Date("October 13, 2017 18:00:00") },

                { day: 'Saturday', checked: true, starttime: new Date("October 13, 2017 09:00:00"), endtime: new Date("October 13, 2017 18:00:00") },

                { day: 'Sunday', checked: true, starttime: new Date("October 13, 2017 09:00:00"), endtime: new Date("October 13, 2017 18:00:00") }];
        }

        $scope.resetValues();

        $scope.createNewTemplate = function () {
            $scope.resetValues();
            vm.alarmtemplate.newObject = true;


        }
        $scope.getAlaramTemplates = function () {
            vm.alarmtemplate.newObject = false;
            vm.alarmtemplate.id = null;
            AlarmtemplateService1.getAlaramTemplatesWithoutChannel().then(function (response) {
                console.log('getAlaramTemplates response ', response);
                $scope.alarmtemplates = response.data;
            });
        }
        $scope.getAlaramTemplates();
        $scope.deleteAlarmTemplate = function () {
            var deleteUser = $window.confirm('Are you sure you want to delete?');
            if (deleteUser) {
                if (vm.alarmtemplate.id == null) {
                    alert('Select the template to delete');
                } else
                    AlarmtemplateService1.deleteAlaramTempates(vm.alarmtemplate).then(function (response) {
                        alert(" Deleted Successfully ");
                        $scope.getAlaramTemplates();
                    });
            }
        }
        $scope.getAlarmTemplate = function (template) {
            console.log(JSON.stringify(template.alarmtemplateschedulars));
            if (template != null)
                AlarmtemplateService1.getAlaramTempateUsingId(template).then(function (response) {
                    vm.alarmtemplate = response;
                    console.log(JSON.stringify(response.alarmtemplateschedulars));
                });
        }

        function save(form) {
             angular.forEach(vm.alarmtemplate.alarmtemplateschedulars,function(value){
                 //THIS MUST FOLLOW THE PATTERN HH:MM 24 hour format
                 value.starttime = value.starttime.toString();
                 value.endtime =value.endtime.toString();
             });

            if (form.$valid) {
                vm.alarmtemplate.newObject = false;
                AlarmtemplateService1.saveAlarmTemplate(vm.alarmtemplate).then(function (response) {
                    if (!response.valid) {
                        $scope.serverError = response.data;
                    } else {
                        $scope.getAlaramTemplates();
                    }
                });
            }
        }
    }
})();
