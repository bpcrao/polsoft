(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('AlarmsSearch', AlarmsSearch);

    AlarmsSearch.$inject = ['$resource'];

    function AlarmsSearch($resource) {
        var resourceUrl =  'api/_search/alarms/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
