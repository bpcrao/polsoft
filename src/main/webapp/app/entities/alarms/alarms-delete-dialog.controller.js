(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('AlarmsDeleteController',AlarmsDeleteController);

    AlarmsDeleteController.$inject = ['$uibModalInstance', 'entity', 'Alarms'];

    function AlarmsDeleteController($uibModalInstance, entity, Alarms) {
        var vm = this;

        vm.alarms = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Alarms.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
