(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('Alarms', Alarms);

    Alarms.$inject = ['$resource', 'DateUtils','$q','$http'];

    function Alarms ($resource, DateUtils,$q,$http) {
        var resourceUrl =  'api/alarms/:id';

        var service =  $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
        service.findAll = function(fromDate,toDate,sort) {
        	return $http.get('api/alarms/all', {
                params: {
                	fromDate: fromDate,
                	toDate: toDate,
                	sort: sort
                }
            })
        },
        service.count = function() {
        	var deferred = $q.defer();
            $http.get('api/alarms/count')
            .then(function (response) {
                deferred.resolve(response);
            },
            function (response) {
            	console.log("failure");
            });
        return deferred.promise;
        }
        return service;
    }
})();
