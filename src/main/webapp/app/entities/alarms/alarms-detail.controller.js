(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('AlarmsDetailController', AlarmsDetailController);

    AlarmsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Alarms'];

    function AlarmsDetailController($scope, $rootScope, $stateParams, previousState, entity, Alarms) {
        var vm = this;

        vm.alarms = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:alarmsUpdate', function(event, result) {
            vm.alarms = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
