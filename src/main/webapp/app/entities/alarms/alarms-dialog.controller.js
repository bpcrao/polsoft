(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('AlarmsDialogController', AlarmsDialogController);

    AlarmsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Alarms'];

    function AlarmsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Alarms) {
        var vm = this;

        vm.alarms = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.alarms.id !== null) {
                Alarms.update(vm.alarms, onSaveSuccess, onSaveError);
            } else {
                Alarms.save(vm.alarms, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:alarmsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
