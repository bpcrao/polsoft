(function() {
    'use strict';

    angular
    .module('polsoftApp')
    .directive('minutesAgo', minutesAgo);

	minutesAgo.$inject = ['$interval','$filter'];

    function minutesAgo($interval,$filter) {
    	var MINUTE = 60000;
    	  function updateMinutes(scope) {
    		var dateFormat = 'yyyy-MM-dd H:mm:ss';
            var fromDate = $filter('date')(scope.startTime, dateFormat);
            var dt = new Date(fromDate);
            var parsedServerOutTime = moment(new Date(), "yyyy-MM-dd H:mm:ss a");
            var parsedServerInTime = moment(dt, "yyyy-MM-dd H:mm:ss a");
            var elapsedTime;
            if(parsedServerOutTime == parsedServerInTime){
            	elapsedTime = 1;
            }
            else{
            elapsedTime = moment.duration(parsedServerOutTime.diff(parsedServerInTime));
            }
            elapsedTime = Math.floor(elapsedTime / 1000);
            if(elapsedTime < 0)
            	elapsedTime = 0;
            console.log("elapsedTime = "+elapsedTime);
    		var seconds = elapsedTime % 60;
    		var minutes = Math.floor((elapsedTime % 3600)/60);
    		if(minutes < 0) minutes = 0;
    		var hours = Math.floor(elapsedTime / 3600);
    		if(hours < 0) hours = 0;
    		var seconds_output = (elapsedTime% 3600)%60;
    		if(seconds_output < 0) seconds_output = 0;
    		//debugger;
    		var time = hours + "hr " + minutes + "min " + seconds_output + "s";
    		//console.log(" hyma -- "+ time);
    		scope.pluralizedMinutes = time;
    	  }

    	  return {
    	    restrict: "AE",
    	    scope: {
    	      startTime: "="
    	    },
    	    template: "<span>{{pluralizedMinutes}} </span>",
    	    link: function(scope, element) {
    	      updateMinutes(scope);
    	      $interval(function() {
    	        updateMinutes(scope);
    	      }, MINUTE);
    	    }
    	  };
    }
})();
