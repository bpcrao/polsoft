(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('AlarmtemplateService1', AlarmtemplateService1);

    AlarmtemplateService1.$inject = ['$resource','$http','$q'];

    function AlarmtemplateService1 ($resource,$http,$q) {
        
        var service = {
                getAlaramTemplates:getAlaramTemplates,
                saveAlaramTempates:saveAlaramTempates,
                deleteAlaramTempates:deleteAlaramTempates,
                updateAlaramTempates:updateAlaramTempates,
                getAlaramTemplatesWithoutChannel:getAlaramTemplatesWithoutChannel,
                getAlaramTempateUsingId:getAlaramTempateUsingId,
                saveAlarmTemplate:saveAlarmTemplate,
                getAlaramTemplatesWithChannel:getAlaramTemplatesWithChannel
            };

            return service;
            
         // Alarm templates
            
            function getAlaramTemplates() {
            	var deferred = $q.defer();
                var resourceUrl = '/api/alarmtemplates/';
                return $http.get(resourceUrl)
                    .then(function (response) {
                       // return response;
                        deferred.resolve(response);
                    },
                    function (response) {
                    	alert("failure");
                    });
                return deferred.promise;
            }
            
            function saveAlaramTempates(postObj) {
            	var deferred = $q.defer();
                $http.post('api/alarmtemplates', postObj)
                    .then(function (response) {
                    	deferred.resolve(response);
                 },
                 function (response) {
                 	alert("failure");
                 });
                return deferred.promise;
            }
            
            function updateAlaramTempates(postObj) {
            	var deferred = $q.defer();
                $http.put('api/alarmtemplates', postObj)
                    .then(function (response) {
                    	deferred.resolve(response);
                    },
                    function (response) {
                    	alert("failure");
                    });
                return deferred.promise;
            }
            
            function deleteAlaramTempates(postObj) {
            	var deferred = $q.defer();
                $http.delete('api/alarmtemplates/'+postObj.id)
                    .then(function (response) {
                    	deferred.resolve(response);
                    },
                    function (response) {
                    	alert("failure");
                    });
                return deferred.promise;
            }
            function getAlaramTemplatesWithoutChannel() {
            	var deferred = $q.defer();
                var resourceUrl = '/api/alarmtemplates/noChannel';
                $http.get(resourceUrl)
                    .then(function (response) {
                    	deferred.resolve(response);
                    },
                    function (response) {
                    	alert("failure");
                    });
                return deferred.promise;
            }
            function getAlaramTemplatesWithChannel(dchannelId) {
            	var deferred = $q.defer();
                var resourceUrl = '/api/alarmtemplates/withChannel/'+dchannelId;
                $http.get(resourceUrl)
                    .then(function (response) {
                    	deferred.resolve(response);
                    },
                    function (response) {
                    	alert("failure");
                    });
                return deferred.promise;
            }
            function saveAlarmTemplate(alarmtemplate){
            	var serverError = [];
            	var count = 0;
            	var deferred = $q.defer();
            	var vm = {};
            	vm.alarmtemplate = alarmtemplate;
            	var schedularsArray = [];
            	var limitsArray = [];
            	if(vm.alarmtemplate.newObject){
            		vm.alarmtemplate.id = "";
            	}
            	for(var k=0; k<vm.alarmtemplate.alarmtemplateschedulars.length; k++){
            		var obj = vm.alarmtemplate.alarmtemplateschedulars[k];
            		if(vm.alarmtemplate.newObject){
                		obj.id = "";
                	}
            		// if(obj.starttime){
            		// 	obj.starttime = obj.starttime+"";
            		// 	if(obj.starttime.indexOf(":") != -1){
                	// 		obj.starttime = obj.starttime.replace(":",".");
                	// 	}
            		// }
            		// if(obj.endtime){
            		// 	obj.endtime = obj.endtime+"";

                	// 	if(obj.endtime.indexOf(":") !=-1 ){
                	// 		obj.endtime = obj.endtime.replace(":",".");
                	// 	}
            		// }
            		if(obj.starttime || obj.endtime){
                		if(!obj.starttime){
                			serverError[count] = "";
            				count = count+1;
            				alert('Enter start time for '+obj.day);
                		}if(!obj.endtime){
                			serverError[count] = "";
            				count = count+1;
            				alert('Enter end time for '+obj.day);
                		}
//                		if(obj.starttime > obj.endtime){
//                			serverError[count] =  "End time should be greater than start time for "+obj.day;
//                			count = count+1;
//                		}
                	}
				 }
				var loopCount=0;
            	if(vm.alarmtemplate["trigger1Alarm"] && vm.alarmtemplate["trigger1Alarm"].clicked){
            		if(vm.alarmtemplate.newObject){
                		vm.alarmtemplate.trigger1Alarm.id = "";
                	}
            		vm.alarmtemplate["trigger1Alarm"]["type"] = "alarm";
            		vm.alarmtemplate["trigger1Alarm"]["triggerset"] = 1;
					limitsArray[loopCount] = vm.alarmtemplate["trigger1Alarm"];
					loopCount++;
            		if(Number(vm.alarmtemplate["trigger1Alarm"].highlimit) <= Number(vm.alarmtemplate["trigger1Alarm"].lowlimit)){
            			serverError[count] =  "";
            			count = count+1;
            			alert('Trigger1 Alarm high limit should be greater than Alarm low limit');
            		}
            	}
            	if(vm.alarmtemplate["trigger1warning"] && vm.alarmtemplate["trigger1warning"].clicked){
            		if(vm.alarmtemplate.newObject){
                		vm.alarmtemplate.trigger1warning.id = "";
                	}
            		vm.alarmtemplate["trigger1warning"]["type"] = "warning";
            		vm.alarmtemplate["trigger1warning"]["triggerset"] = 1;
					limitsArray[loopCount] = vm.alarmtemplate["trigger1warning"];
					loopCount++;
            		if(vm.alarmtemplate["trigger1warning"] && Number(vm.alarmtemplate["trigger1warning"].highlimit) <= Number(vm.alarmtemplate["trigger1warning"].lowlimit)){
            			serverError[count] =  "";
            			count = count+1;
            			alert('Trigger1 warning high limit should be greater than warning low limit');
            		}
            		if(vm.alarmtemplate["trigger1Alarm"] && vm.alarmtemplate["trigger1warning"] && Number(vm.alarmtemplate["trigger1Alarm"].highlimit) <= Number(vm.alarmtemplate["trigger1warning"].highlimit)){
            			serverError[count] =  "";
            			count = count+1;
            			alert('Trigger1 Alarm high limit should be greater than warning high limit');
            		}
            		if(vm.alarmtemplate["trigger1Alarm"] && vm.alarmtemplate["trigger1warning"] && Number(vm.alarmtemplate["trigger1Alarm"].lowlimit) >= Number(vm.alarmtemplate["trigger1warning"].lowlimit)){
            			serverError[count] =  "";
            			count = count+1;
            			alert('Trigger1 Alarm low limit should be greater than warning low limit');
            		}
            	}
            	if(vm.alarmtemplate["trigger2Alarm"] && vm.alarmtemplate["trigger2Alarm"].clicked){
            		if(vm.alarmtemplate.newObject){
                		vm.alarmtemplate.trigger2Alarm.id = "";
                	}
            		vm.alarmtemplate["trigger2Alarm"]["type"] = "alarm";
            		vm.alarmtemplate["trigger2Alarm"]["triggerset"] = 2;
					limitsArray[loopCount] = vm.alarmtemplate["trigger2Alarm"];
					loopCount++
            		if(Number(vm.alarmtemplate["trigger2Alarm"].highlimit) <= Number(vm.alarmtemplate["trigger2Alarm"].lowlimit)){
            			serverError[count] =  "";
            			count = count+1;
            			alert('Trigger2 Alarm high limit should be greater than Alarm low limit');
            		}
            	}
            	if(vm.alarmtemplate["trigger2warning"] && vm.alarmtemplate["trigger2warning"].clicked){
            		if(vm.alarmtemplate.newObject){
                		vm.alarmtemplate.trigger2warning.id = "";
                	}
            		vm.alarmtemplate["trigger2warning"]["type"] = "warning";
            		vm.alarmtemplate["trigger2warning"]["triggerset"] = 2;
					limitsArray[loopCount] = vm.alarmtemplate["trigger2warning"];
					loopCount++;
            		if(vm.alarmtemplate["trigger2warning"] && vm.alarmtemplate["trigger2warning"] && Number(vm.alarmtemplate["trigger2warning"].highlimit) <= Number(vm.alarmtemplate["trigger2warning"].lowlimit)){
            			serverError[count] =  "";
            			count = count+1;
            			alert('Trigger2 warning high limit should be greater than warning low limit');
            		}
            		if(Number(vm.alarmtemplate["trigger2Alarm"].highlimit) <= Number(vm.alarmtemplate["trigger2warning"].highlimit)){
            			serverError[count] =  "";
            			count = count+1;
            			alert('Trigger2 Alarm high limit should be greater than warning high limit');
            		}
            	}
            	if(!vm.alarmtemplate.activetimechecked)
            		{
                	   delete vm.alarmtemplate.alarmtemplateschedulars;
            		}
            	delete vm.alarmtemplate.alarmtemplatelimits;
            	vm.alarmtemplate["alarmtemplatelimits"] = limitsArray;
            	if(serverError && serverError.length > 0){
            		var response = {valid:false,data:serverError};
            		deferred.resolve(response);
            		return deferred.promise;
            	}
            	if(vm.alarmtemplate.id){
            		this.updateAlaramTempates(vm.alarmtemplate).then(function(response) {
            			alert(" Updated successfully ");
            			var response1 = {valid:true,data:response};
            			deferred.resolve(response1);
                    });
            	}
            	else{
            		this.saveAlaramTempates(vm.alarmtemplate).then(function(response) {
            			alert(" Added successfully ");
            			var response1 = {valid:true,data:response};
            			deferred.resolve(response1);
                    },
                    function (response) {
                    	alert("failure");
                    });
            	}
            	return deferred.promise;
            }
            function getAlaramTempateUsingId(alarmtemplate){
            	var deferred = $q.defer();
            	var vm = {};
            	vm.alarmtemplate = alarmtemplate;
            	var days_order = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
                var alarmtemplateschedulars = [{day: 'Monday'},{day: 'Tuesday'},{day: 'Wednesday'},{day: 'Thursday'},{day: 'Friday'},{day: 'Saturday'},{day: 'Sunday'}];
            	if(vm.alarmtemplate.alarmtemplateschedulars && vm.alarmtemplate.alarmtemplateschedulars.length>0 ){
            		for(var k=0; k<vm.alarmtemplate.alarmtemplateschedulars.length; k++){
            			for(var l=0; l<alarmtemplateschedulars.length; l++){
            				if(alarmtemplateschedulars[l] && alarmtemplateschedulars[l].day == vm.alarmtemplate.alarmtemplateschedulars[k].day){
            					alarmtemplateschedulars[l] = vm.alarmtemplate.alarmtemplateschedulars[k];
								alarmtemplateschedulars[l].starttime=alarmtemplateschedulars[l].starttime;
								alarmtemplateschedulars[l].endtime=alarmtemplateschedulars[l].endtime;
            				}
            			}
            		}
            		vm.alarmtemplate.alarmtemplateschedulars = alarmtemplateschedulars;
        		}
            	
        		for(var j=0; j<vm.alarmtemplate.alarmtemplatelimits.length; j++){
        			if(vm.alarmtemplate.alarmtemplatelimits[j].type == "warning"){
        				if(vm.alarmtemplate.alarmtemplatelimits[j].triggerset == 1){
        					vm.alarmtemplate["trigger1warning"] = vm.alarmtemplate.alarmtemplatelimits[j];
        					vm.alarmtemplate["trigger1warning"].clicked = true;
        				}
        				else{
        					vm.alarmtemplate["trigger2warning"] = vm.alarmtemplate.alarmtemplatelimits[j];
        					vm.alarmtemplate["trigger2warning"].clicked = true;
        				}
        			} else if(vm.alarmtemplate.alarmtemplatelimits[j].type == "alarm"){
        				if(vm.alarmtemplate.alarmtemplatelimits[j].triggerset == 1){
        					vm.alarmtemplate["trigger1Alarm"] = vm.alarmtemplate.alarmtemplatelimits[j];
        					vm.alarmtemplate["trigger1Alarm"].clicked = true;
        				}
        				else{
        					vm.alarmtemplate["trigger2Alarm"] = vm.alarmtemplate.alarmtemplatelimits[j];
        					vm.alarmtemplate["trigger2Alarm"].clicked = true;
        				}
        			}
        		}
        		deferred.resolve(vm.alarmtemplate);

        		return deferred.promise;
            }
    }
})();