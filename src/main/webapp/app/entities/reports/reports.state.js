// (function() {
//     'use strict';

//     angular
//         .module('polsoftApp')
//         .config(stateConfig);

//     stateConfig.$inject = ['$stateProvider'];

//     function stateConfig($stateProvider) {
//         $stateProvider
//         .state('reports', {
//             parent: 'entity',
//             url: '/reports',
//             data: {
//                 pageTitle: 'polsoftApp.reports.home.title'
//             },
//             views: {
//                 'content@': {
//                     templateUrl: 'app/entities/reports/reports.html',
//                     controller: 'ReportsController',
//                     controllerAs: 'vm'
//                 }
//             },
//             resolve: {
//                 translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
//                     $translatePartialLoader.addPart('reports');
//                     $translatePartialLoader.addPart('global');
//                     return $translate.refresh();
//                 }]
//             }
//         })
      
//         .state('reports.new', {
//             parent: 'reports',
//             url: '/new',
//              params: {
//                     reportsData: null
//                 },
        
//             onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
//                 $uibModal.open({
//                     templateUrl: 'app/entities/reports/reports-dialog.html',
//                     controller: 'ReportsDialogController',
//                     controllerAs: 'vm',
//                     backdrop: 'static',
//                     size: 'lg',
//                     resolve: {
//                         entity: function () {
//                             return {
//                                 name: null,
//                                 id: null
//                             };
//                         }
//                     }
//                 }).result.then(function() {
//                     $state.go('reports', null, { reload: 'reports' });
//                 }, function() {
//                     $state.go('reports');
//                 });
//             }]
//         })
//     }

// })();
