(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ReportsController', ReportsController);

    ReportsController.$inject = ['$scope', '$state', 'Reports', 'Locationtree', '$timeout'];

    function ReportsController($scope, $state, Reports, Locationtree, $timeout) {
        var vm = this;
        vm.reportData = {template:"1",timeFrame:"1"};
        
        vm.automaticReportData={};
        $scope.my_tree_handler = function (branch) {

            console.log('clicked row ', branch);
            $scope.selectedLocation = branch;
        };


        vm.reportData.fromDate = new Date();

        vm.reportData.fromDate.setHours(vm.reportData.fromDate.getHours() - 1);

        vm.reportData.toDate = new Date();

        $scope.maxDate = new Date();

        $scope.dateOpened = false;
        $scope.hourStep = 1;
        $scope.format = "dd-MMM-yyyy";
        $scope.minuteStep = 1;
        $scope.showMeridian = true;

        $scope.datepickers = {
            dtFrom: false,
            dtTo: false
        }
        $scope.toggleOpenDatePicker = function ($event, which) {
             $scope.noDataAvailble = false;
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepickers[which] = true;
        };

        $scope.showManually=true;
         $scope.changeTab = function (value) {
            $scope.showManually = value
        }


        $scope.changeData = function (fromDate, Todate) {
             $scope.noDataAvailble = false;
            vm.reportData.fromDate = fromDate;
            vm.reportData.toDate = Todate;
        }
$('#mydiv_loader_div').hide();
        /*Random Data Generator */
        $scope.generatePdf = function (reportsForm) {
             $('#mydiv_loader_div').show();

             var queryData= Reports.getMultipleSeletedLocation($scope.my_data);


             $scope.arrayOfSelectedLocations =queryData.arrayOfSelectedLocations;
             $scope.arrayOfSelectedLocationNames = queryData.arrayOfSelectedLocationNames;
            console.log('Query for Multiple Location is ', queryData.query);

            if ($scope.arrayOfSelectedLocations.length > 0 && reportsForm.$valid) {

                Reports.getDataBasedonLocation($scope.selectedLocation.id, vm.reportData.fromDate, vm.reportData.toDate,queryData.query)
                    .then(function (response) {
                         setTimeout(function(){
                             $('#mydiv_loader_div').hide();
                        },10);
                        if (response.data && response.data.results.length > 0 && response.data.results[0].series != undefined && response.data.results[0].series.length > 0) {
                            console.log('Graph response response', response.data.results[0].series[0].values);
                            $state.go('reports.new', { reportsData: Reports.getTableData(response.data.results[0].series[0].values,$scope.arrayOfSelectedLocationNames)});
                            $scope.noDataAvailble = false;
                        } else {

                            $scope.noDataAvailble = true;
                        }
                    });
                //Data is represented as an array of {x,y} pairs.
            }else
            {
                 $('#mydiv_loader_div').hide();
            }
        };

        var treedata_avm = [{
            label: 'Plant',
            isChannel: false,
            id: 0,
            noLeaf: true
        }];
        $scope.my_tree = {};

        Locationtree.query(function (result) {
            console.log('result ', result);
            $scope.my_data = [result];
            $timeout(function () { $scope.my_tree.expand_all() }, 10);
        });

        $scope.my_data = treedata_avm;
    }
})();
