(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ReportsDialogController', ReportsDialogController);

    ReportsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Reports'];

    function ReportsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Reports) {
        var vm = this;

        vm.reports = entity;
        vm.clear = clear;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.reportsData=$stateParams.reportsData;

          $scope.tabledata = $scope.reportsData.tabledata;
          $scope.tableColums = $scope.reportsData.tableColums;

       if(!$scope.reportsData)
       {
           $timeout(function(){$uibModalInstance.dismiss('cancel');},100);
            
       }

    

    }
})();
