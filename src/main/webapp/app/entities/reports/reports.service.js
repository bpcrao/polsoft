(function () {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('Reports', Reports);

    Reports.$inject = ['$resource', '$http'];

    function Reports($resource, $http) {

        var service = {
            getDataBasedonLocation: getDataBasedonLocation,
            getLastDataBasedonLocation: getLastDataBasedonLocation,
            getMultipleSeletedLocation: getMultipleSeletedLocation,
            getTableData: getTableData,
          //  getOverViewTableData: getOverViewTableData,
            getUnitName: getUnitName,
            GetTimeDifferance: GetTimeDifferance

        };
        function getUnitName(key) {
            console.log(key);
            var unitShortName;
            switch (key) {
                case "Temperature DegC":
                    unitShortName = "(°C)";
                    break;
                case "Temperature DegF":
                    unitShortName = "(°F)";
                    break;
                case "Humidity %RH":
                    unitShortName = "-RH(%)";
                    break;
                case "Temperature":
                    unitShortName = "(°C)";
                    break;
                case "Speed rpm":
                    unitShortName = "(RPM)";
                    break;
                default:
                    unitShortName = "";
                    break;
            }

            return unitShortName;
        }
        function getTableData(resrData, arrayOfSelectedLocationNames, isTable,isOverView) {

            var tabledata = [];
            var tbData = {};
            var tableColums = [];
            var cos = {};
            var alarmData = {};
            var unitBasedGraphData = {};

           
                if (isTable) {
                    for (var i = resrData.length - 1; i >= 0; i--) {
                  
                        if(isOverView)
                            {
                                var locationData = 5;
                                if (!tbData[resrData[i][0]]) {
                                    tbData[resrData[i][0]] = {min:resrData[i][6],
                                        max:resrData[i][7]};
                                    tbData[resrData[i][0]][resrData[i][2]] = resrData[i][locationData] === -8999 ? 'Err1' : (resrData[i][locationData] === -9000 ? 'Err0' : (resrData[i][locationData] === -9003 ? 'Err3' : resrData[i][locationData]))
                                } else {
                                    tbData[resrData[i][0]][resrData[i][2]] = resrData[i][locationData] === -8999 ? 'Err1' : (resrData[i][locationData] === -9000 ? 'Err0' : (resrData[i][locationData] === -9003 ? 'Err3' : resrData[i][locationData]))
        
                                }

                                console.log('Data for Over view tab %%%% ',tbData);

                            }else
                        {
                        if(resrData["0"].length == 8){
                      var locationData = 7;
                    if (!tbData[resrData[i][0]]) {
                        tbData[resrData[i][0]] = {}
                        tbData[resrData[i][0]][resrData[i][3]] = resrData[i][locationData] === -8999 ? 'Err1' : (resrData[i][locationData] === -9000 ? 'Err0' : (resrData[i][locationData] === -9003 ? 'Err3' : resrData[i][locationData]))
                    } else {
                        tbData[resrData[i][0]][resrData[i][3]] = resrData[i][locationData] === -8999 ? 'Err1' : (resrData[i][locationData] === -9000 ? 'Err0' : (resrData[i][locationData] === -9003 ? 'Err3' : resrData[i][locationData]))

                    }
                   }
                    if(resrData["0"].length == 7){
                    	var locationData = 6;
                    	if (!tbData[resrData[i][0]]) {
                            tbData[resrData[i][0]] = {}
                            tbData[resrData[i][0]][resrData[i][3]] = resrData[i][locationData] === -8999 ? 'Err1' : (resrData[i][locationData] === -9000 ? 'Err0' : (resrData[i][locationData] === -9003 ? 'Err3' : resrData[i][locationData]))
                        } else {
                            tbData[resrData[i][0]][resrData[i][3]] = resrData[i][locationData] === -8999 ? 'Err1' : (resrData[i][locationData] === -9000 ? 'Err0' : (resrData[i][locationData] === -9003 ? 'Err3' : resrData[i][locationData]))

                        }
                    }
                }
                    
                    //$scope.updateTableData(resrData[i]);
                    locationData = 1;
                    if (!alarmData[resrData[i][0]]) {
                    	alarmData[resrData[i][0]] = {}
                    	alarmData[resrData[i][0]][resrData[i][3]] = resrData[i][locationData];
                    } else {
                    	alarmData[resrData[i][0]][resrData[i][3]] = resrData[i][locationData];

                    }
                    var dateExists = false;

                    angular.forEach(tabledata, function (value, key) {
                        if (value[0] == resrData[i][0]) {
                            dateExists = true;
                            value[resrData[i][3]] = resrData[i][locationData];
                        }

                    })
                    if (!dateExists) {
                        var dic = { 0: resrData[i][0] };
                        dic[resrData[i][3]] = resrData[i][locationData];
                        tabledata.push(dic);
                    }
                }
                    // if (tableColums.indexOf(arrayOfSelectedLocationNames[resrData[i][3]]+'('+this.getUnitName(resrData[i][5])+')') == -1)
                    //     { 
                    //         tableColums.push(arrayOfSelectedLocationNames[resrData[i][3]]+'('+this.getUnitName(resrData[i][5])+')');
                    //     }

                } else {
                    angular.forEach(resrData, function(value, key){
                        console.log(key + ': ' + value);
                        var location=value.values;
                        if(location && location.length)
                        for (var i = location.length - 1; i >= 0; i--) {
                            if(location[i][5])
                                {
                        var locationData = 6;
   
                        if (unitBasedGraphData[location[i][5]] == undefined) {
                            console.log('resrData[i][5] ',location[i][5]);
                            unitBasedGraphData[location[i][5]] = {}
                        }
    
    
                        if (unitBasedGraphData[location[i][5]][location[i][3]] == undefined) {
                            unitBasedGraphData[location[i][5]][location[i][3]] = [];
                        }
    
                        unitBasedGraphData[location[i][5]][location[i][3]].push({ x: new Date(location[i][0]).getTime(), y: location[i][locationData] })
                    }
                    }
                   });
                   
                    
            
            }
            console.log('Final Data after Filters ', unitBasedGraphData, tabledata, tableColums);

            return { cos: unitBasedGraphData, tabledata: tabledata, tableColums: tableColums, tbData: tbData, alarmData: alarmData }

        }
       
        function getMultipleSeletedLocation(treeData,isOverView) {
            var self = this;

            var arrayOfSelectedLocations = [];
            var arrayOfSelectedLocationNames = {};

            //         for(var k; k<$scope.my_data.length;k++)
            //         {
            // console.log('$scope.my_data[0] ',$scope.my_data[0][k]);
            //         }
            // $scope.selectedLocation=[];

            angular.forEach(treeData, function (value, key) {
                //console.log('first row ', value.name);
                angular.forEach(value.children, function (value1, key1) {//this is nested angular.forEach loop
                    //console.log('second row, ' + key1 + ":" + value1.name);
                    if ((value1.selected && !isOverView) || (isOverView && (value1.channelName)) ) {
                        //console.log('Selected Ids are. '+v1.id);
                        arrayOfSelectedLocations.push("location='" + value1.id + "'");
                        arrayOfSelectedLocationNames[value1.id] = value1.name + self.getUnitName(value1.unit);
                    }
                    if (value1.children) {
                        angular.forEach(value1.children, function (Value2, key2) {//
                            //console.log('Third row, ' + Value2 + ":" + Value2.name);
                            if ((Value2.selected && !isOverView) || (isOverView && (Value2.channelName))) {
                                arrayOfSelectedLocations.push("location='" + Value2.id + "'");
                                arrayOfSelectedLocationNames[Value2.id] = Value2.name + self.getUnitName(Value2.unit);
                            }
                            if (Value2.children) {
                                angular.forEach(Value2.children, function (Value3, key3) {//
                                    //console.log('Fourth row, ' + Value3 + ":" + Value3.name);
                                    if ((Value3.selected && !isOverView) || (isOverView && (Value3.channelName))) {
                                        arrayOfSelectedLocations.push("location='" + Value3.id + "'");
                                        arrayOfSelectedLocationNames[Value3.id] = Value3.name + self.getUnitName(Value3.unit);

                                    }
                                });
                            }
                        });
                    }
                });
            });
            var query;
            if (arrayOfSelectedLocations.length > 1) {
                query = "(" + arrayOfSelectedLocations.join(" or ") + ")";
            } else if (arrayOfSelectedLocations.length == 1) {
                query = arrayOfSelectedLocations[0];
            }

            return { query: query, arrayOfSelectedLocations: arrayOfSelectedLocations, arrayOfSelectedLocationNames: arrayOfSelectedLocationNames }
        }

        function GetTimeDifferance(fromDate, toDate) {
            var timeStart = new Date(fromDate).getTime();
            var timeEnd = new Date(toDate).getTime();
            var hourDiff = timeEnd - timeStart; //in ms
            var secDiff = hourDiff / 1000; //in s
            var minDiff = hourDiff / 60 / 1000; //in minutes
            var hDiff = hourDiff / 3600 / 1000; //in hours
            var humanReadable = {};
            humanReadable.hours = Math.floor(hDiff);
            humanReadable.minutes = minDiff - 60 * humanReadable.hours;
            console.log(humanReadable);
            return humanReadable;
        }

        function getDataBasedonLocation(resource_Id, fromDate, toDate, query, isTableView) {
            var self = this;
            if (query == undefined) {
                query = "location='" + resource_Id + "'";
            }

           var resourceUrl ="/api/influx?queryString=select " + (isTableView ? '*' : 'first(*)') + " from data where " + query + " and time >= '" + fromDate.toISOString() + "' and time < '" + toDate.toISOString()+ "'";
            
            if (!isTableView) {
                var timeDiff = self.GetTimeDifferance(fromDate, toDate);
                var interval = Math.floor((timeDiff.hours * 60 + timeDiff.minutes) / 1000);
                console.log('interval ', interval);
                if (interval == 0) {
                    interval = 1;
                }
                resourceUrl += " group by time(" + interval + "m), location_tag order by time desc";
            }

            console.log('INFLUX DB QUERY ', resourceUrl);

            return $http.get(resourceUrl)
                .success(function (response) {
                    return response;
                });
        }
        

        function getLastDataBasedonLocation(resource_Id, query, isTableView,length) {
            var self = this;
            var numberoflinkedlocations = length;
            if (query == undefined) {
                query = "location='" + resource_Id + "'";
            }
            var resourceUrl ="/api/influx?queryString=select first(*),min(value),max(value) from data where " + query;
     
            resourceUrl += " order by time desc limit ";
            resourceUrl += length.toString(); 
            console.log('INFLUX DB QUERY ', resourceUrl);

            return $http.get(resourceUrl)
                .success(function (response) {
                    return response;
                });
        }
        return service;
    }
})();