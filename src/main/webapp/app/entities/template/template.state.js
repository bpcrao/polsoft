(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('template', {
                parent: 'entity',
                url: '/template',
                data: {
                    authorities: ['Administrator'],
                    permission: {
                        'resource': 'TEMPLATE',
                        'level': 'VIEW'
                    },
                    pageTitle: 'polsoftApp.template.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/template/templates.html',
                        controller: 'TemplateController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('template');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('template-detail', {
                parent: 'entity',
                url: '/template/{id}',
                data: {
                    authorities: ['Administrator'],
                    permission: {
                        'resource': 'TEMPLATE',
                        'level': 'VIEW'
                    },
                    pageTitle: 'polsoftApp.template.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/template/template-detail.html',
                        controller: 'TemplateDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('template');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Template', function ($stateParams, Template) {
                        return Template.get({
                            id: $stateParams.id
                        }).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'template',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('template-detail.edit', {
                parent: 'template-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['Administrator'],
                    permission: {
                        'resource': 'TEMPLATE',
                        'level': 'VIEW'
                    },
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/template/template-dialog.html',
                        controller: 'TemplateDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Template', function (Template) {
                                return Template.get({
                                    id: $stateParams.id
                                }).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {
                            reload: false
                        });
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('template.new', {
                parent: 'template',
                url: '/new',
                data: {
                    authorities: ['Administrator'],
                    permission: {
                        'resource': 'TEMPLATE',
                        'level': 'VIEW'
                    },
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/template/template-dialog.html',
                        controller: 'TemplateDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    templateName: null,
                                    thresholds: null,
                                    sms: null,
                                    emails: null,
                                    acknowledgeWithComment: false,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('template', null, {
                            reload: 'template'
                        });
                    }, function () {
                        $state.go('template');
                    });
                }]
            })
            .state('template.edit', {
                parent: 'template',
                url: '/{id}/edit',
                data: {
                    authorities: ['Administrator'],
                    permission: {
                        'resource': 'TEMPLATE',
                        'level': 'VIEW'
                    },
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/template/template-dialog.html',
                        controller: 'TemplateDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Template', function (Template) {
                                return Template.get({
                                    id: $stateParams.id
                                }).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('template', null, {
                            reload: 'template'
                        });
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('template.delete', {
                parent: 'template',
                url: '/{id}/delete',
                data: {
                    authorities: ['Operator']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/template/template-delete-dialog.html',
                        controller: 'TemplateDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Template', function (Template) {
                                return Template.get({
                                    id: $stateParams.id
                                }).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('template', null, {
                            reload: 'template'
                        });
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
