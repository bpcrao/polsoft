(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('TemplateDetailController', TemplateDetailController);

    TemplateDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Template'];

    function TemplateDetailController($scope, $rootScope, $stateParams, previousState, entity, Template) {
        var vm = this;

        vm.template = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:templateUpdate', function(event, result) {
            vm.template = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
