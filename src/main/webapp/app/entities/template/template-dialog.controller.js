(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('TemplateDialogController', TemplateDialogController);

    TemplateDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Template', 'Threshold', '$q', 'JSTagsCollection'];

    function TemplateDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Template, Threshold, $q, JSTagsCollection) {
        var vm = this;

        vm.template = entity;
        vm.clear = clear;
        vm.save = save;

        vm.thresholds = [];

        vm.tags = new JSTagsCollection(["jsTag", "angularJS"]);

        // Export jsTags options, inlcuding our own tags object
        vm.jsTagOptions = {
            "tags": vm.tags,
            'edit': true,
            'breakCodes': [
                13, // Return
                44 // Comma
            ],
            'splitter': ',',
            'texts': {
                'inputPlaceHolder': "Input text",
                'removeSymbol': String.fromCharCode(215)
            }
        };

        vm.smsTags = new JSTagsCollection([]);

        vm.jsTagOptionsForSMS = {
            "tags": vm.smsTags,
            'edit': true,
            'breakCodes': [
                13, // Return
                44 // Comma
            ],
            'splitter': ',',
            'texts': {
                'inputPlaceHolder': "Input text",
                'removeSymbol': String.fromCharCode(215)
            }
        };

        vm.emailTags = new JSTagsCollection([]);

        vm.jsTagOptionsForEMAIL = {
            "tags": vm.emailTags,
            'edit': true,
            'breakCodes': [
                13, // Return
                44 // Comma
            ],
            'splitter': ',',
            'texts': {
                'inputPlaceHolder': "Input text",
                'removeSymbol': String.fromCharCode(215)
            }
        };

        loadAll();

        function loadAll() {

            Threshold.query(function(result) {
                vm.thresholds = result;
                vm.options = JSON.parse(angular.toJson(result));
            });

            return vm.options;
        }

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {

            vm.template.thresholds = JSON.stringify(vm.template.thresholds);
            vm.template.emails = JSON.stringify(vm.emailTags.tags);
            vm.template.sms = JSON.stringify(vm.smsTags.tags);

            vm.isSaving = true;
            if (vm.template.id !== null) {
                Template.update(vm.template, onSaveSuccess, onSaveError);
            } else {
                Template.save(vm.template, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('polsoftApp:templateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function getThresholds() {
            // There will always be a promise so always declare it.
            var deferred = $q.defer();

            Threshold.query(function(result) {

                vm.thresholds = result;
                vm.options = JSON.parse(angular.toJson(result));

                deferred.resolve(vm.options);
            });

            return deferred.promise;
        }

        vm.getThresholds = getThresholds;

        vm.selection = '';
    }
})();