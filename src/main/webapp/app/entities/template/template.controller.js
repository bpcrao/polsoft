(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('TemplateController', TemplateController);

    TemplateController.$inject = ['$scope', '$state', 'Template', 'TemplateSearch'];

    function TemplateController($scope, $state, Template, TemplateSearch) {
        var vm = this;

        vm.templates = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Template.query(function(result) {
                vm.templates = result;

                for (var i = 0; i < vm.templates.length; i++) {

                    var currentTemplate = vm.templates[i];

                    currentTemplate.emails = JSON.parse(currentTemplate.emails);

                    currentTemplate.sms = JSON.parse(currentTemplate.sms);

                    currentTemplate.thresholds = JSON.parse(currentTemplate.thresholds);
                }

            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            TemplateSearch.query({
                query: vm.searchQuery
            }, function(result) {
                vm.templates = result;
            });
        }
    }
})();