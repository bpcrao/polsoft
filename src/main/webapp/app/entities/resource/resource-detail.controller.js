(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ResourceDetailController', ResourceDetailController);

    ResourceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Resource', 'Authority'];

    function ResourceDetailController($scope, $rootScope, $stateParams, previousState, entity, Resource, Authority) {
        var vm = this;

        vm.resource = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:resourceUpdate', function(event, result) {
            vm.resource = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
