(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('EntityConfigSearch', EntityConfigSearch);

    EntityConfigSearch.$inject = ['$resource'];

    function EntityConfigSearch($resource) {
        var resourceUrl =  'api/_search/entity-configs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
