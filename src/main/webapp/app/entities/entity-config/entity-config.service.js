(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('EntityConfig', EntityConfig);

    EntityConfig.$inject = ['$resource'];

    function EntityConfig ($resource) {
        var resourceUrl =  'api/entity-configs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
