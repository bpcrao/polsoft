(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('SystempreferencesSearch', SystempreferencesSearch);


    SystempreferencesSearch.$inject = ['$resource'];


    function SystempreferencesSearch($resource) {
        var resourceUrl =  'api/_search/systempreferences/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
