(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('SystempreferencesDetailController', SystempreferencesDetailController);

    SystempreferencesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Systempreferences'];

    function SystempreferencesDetailController($scope, $rootScope, $stateParams, previousState, entity, Systempreferences) {
        var vm = this;

        vm.systempreferences = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:systempreferencesUpdate', function(event, result) {
            vm.systempreferences = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
