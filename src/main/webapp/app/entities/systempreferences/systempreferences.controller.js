(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .directive("showPassword", function() { 
            return function linkFn(scope, elem, attrs) {
                scope.$watch(attrs.showPassword, function(newValue) {
                    if (newValue) {
                        elem.attr("type", "text");
                    } else {
                        elem.attr("type", "password");
                        elem.attr("autocomplete", "off");
                        
                    };
                });
            };
        })
        .controller('SystempreferencesController', SystempreferencesController);

    SystempreferencesController.$inject = ['$scope', '$state', 'Systempreferences', 'SystempreferencesSearch'];

    function SystempreferencesController($scope, $state, Systempreferences, SystempreferencesSearch) {
        var vm = this;


        vm.systempreferences = [];
        vm.search = search;
        vm.loadAll = loadAll;
        vm.apply = apply;

        loadAll();


        function loadAll() {
            Systempreferences.query(function (result) {
                vm.systempreferences = result;
            });
        }
        function apply(system_preferences,sys_pref_form) {
            if(sys_pref_form.$valid)
                Systempreferences.update(vm.systempreferences, onSaveSuccess, onSaveError);
        }

         function onSaveSuccess (result) {
          console.log("onSaveSuccess ",result);
        }

        function onSaveError (err) {
            console.log("onSaveerr ",err);
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            SystempreferencesSearch.query({ query: vm.searchQuery }, function (result) {
                vm.systempreferences = result;
            });
        }
    }
})();
