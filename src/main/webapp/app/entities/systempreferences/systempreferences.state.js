(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('systempreferences', {
            parent: 'entity',
            url: '/systempreferences',
            data: {
                authorities: [],
                permission: {
                    'resource': 'PREFERENCES',
                    'level': 'VIEW'
                },
                pageTitle: 'polsoftApp.systempreferences.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/systempreferences/systempreferences.html',
                    controller: 'SystempreferencesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('systempreferences');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('systempreferences-detail', {
            parent: 'entity',
            url: '/systempreferences/{id}',
            data: {
                authorities: [],
                permission: {
                    'resource': 'PREFERENCES',
                    'level': 'VIEW'
                },
                pageTitle: 'polsoftApp.systempreferences.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/systempreferences/systempreferences-detail.html',
                    controller: 'SystempreferencesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('systempreferences');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Systempreferences', function($stateParams, Systempreferences) {
                    return Systempreferences.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'systempreferences',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('systempreferences-detail.edit', {
            parent: 'systempreferences-detail',
            url: '/detail/edit',
            data: {
            	   authorities: ['Administrator'],
                   permission: {
                       'resource': 'PREFERENCES',
                       'level': 'EDIT'
                   }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/systempreferences/systempreferences-dialog.html',
                    controller: 'SystempreferencesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Systempreferences', function(Systempreferences) {
                            return Systempreferences.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('systempreferences.new', {
            parent: 'systempreferences',
            url: '/new',
            data: {
            	   authorities: ['Administrator'],
                   permission: {
                       'resource': 'PREFERENCES',
                       'level': 'CREATE'
                   }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/systempreferences/systempreferences-dialog.html',
                    controller: 'SystempreferencesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                data: null,
                                keyproperty: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('systempreferences', null, { reload: 'systempreferences' });
                }, function() {
                    $state.go('systempreferences');
                });
            }]
        })
        .state('systempreferences.edit', {
            parent: 'systempreferences',
            url: '/{id}/edit',
            data: {
            	   authorities: ['Administrator'],
                   permission: {
                       'resource': 'PREFERENCES',
                       'level': 'EDIT'
                   }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/systempreferences/systempreferences-dialog.html',
                    controller: 'SystempreferencesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Systempreferences', function(Systempreferences) {
                            return Systempreferences.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('systempreferences', null, { reload: 'systempreferences' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('systempreferences.delete', {
            parent: 'systempreferences',
            url: '/{id}/delete',
            data: {
            	   authorities: ['Administrator'],
                   permission: {
                       'resource': 'PREFERENCES',
                       'level': 'VIEW'
                   }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/systempreferences/systempreferences-delete-dialog.html',
                    controller: 'SystempreferencesDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Systempreferences', function(Systempreferences) {
                            return Systempreferences.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('systempreferences', null, { reload: 'systempreferences' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
