(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('SystempreferencesDialogController', SystempreferencesDialogController);

    SystempreferencesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Systempreferences'];

    function SystempreferencesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Systempreferences) {
        var vm = this;

        vm.systempreferences = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
        	//debugger;
            vm.isSaving = true;
            if (vm.systempreferences.id !== null) {
                Systempreferences.update(vm.systempreferences, onSaveSuccess, onSaveError);
            } else {
                Systempreferences.save(vm.systempreferences, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:systempreferencesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
