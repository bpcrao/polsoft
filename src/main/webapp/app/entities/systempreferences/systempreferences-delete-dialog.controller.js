(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('SystempreferencesDeleteController',SystempreferencesDeleteController);

    SystempreferencesDeleteController.$inject = ['$uibModalInstance', 'entity', 'Systempreferences'];

    function SystempreferencesDeleteController($uibModalInstance, entity, Systempreferences) {
        var vm = this;

        vm.systempreferences = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Systempreferences.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
