(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('device', {
            parent: 'entity',
            url: '/device',
            data: {
            	  authorities: [],
                  permission: {
                      'resource': 'DEVICE',
                      'level': 'VIEW'
                  },
                pageTitle: 'polsoftApp.device.home.title'
            },
            
            views: {
                'content@': {
                    templateUrl: 'app/entities/device/devices.html',
                    controller: 'DeviceController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('device');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            },
            onEnter: ['DeviceTrackerService', function(DeviceTrackerService) {
            	DeviceTrackerService.subscribe();
            }],
            onExit: ['DeviceTrackerService', function(DeviceTrackerService) {
            	DeviceTrackerService.unsubscribe();
            }]
        })
        .state('device-detail', {
            parent: 'entity',
            url: '/device/{id}',
            data: {
                  authorities: [],
                    permission: {
                        'resource': 'DEVICE',
                        'level': 'VIEW'
                    },
                pageTitle: 'polsoftApp.device.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/device/device-detail.html',
                    controller: 'DeviceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('device');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Device', function($stateParams, Device) {
                    return Device.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'device',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('device-detail.edit', {
            parent: 'device-detail',
            url: '/detail/edit',
            data: {
                authorities: ['Administrator','Supervisor'],
                    permission: {
                        'resource': 'DEVICE',
                        'level': 'EDIT'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/device/device-dialog.html',
                    controller: 'DeviceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Device', function(Device) {
                            return Device.get({id : $stateParams.id}).$promise;
                        },'Dchannel', function(Dchannel){ return Dchannel.query({deviceId :$stateParams.id, detailedData : true}).$promise; }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('device.new', {
            parent: 'device',
            url: '/new',
            data: {
                 authorities: ['Administrator','Supervisor'],
                    permission: {
                        'resource': 'DEVICE',
                        'level': 'CREATE'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/device/device-create.html',
                    controller: 'DeviceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                            	type : "DataLogger(122M)",
                                name : "DataLogger_" + (new Date()).getUTCDate(),    
                                settings: null,
                                status: true,
                                lastRefresh: new Date(),
                                serial: null,
                                location: "/Plant A/Block A",
                                serialCardParams: null,
                                cardparameters: null,
                                alarmDetails: null,
                                id: null
                            };
                        },
                        channelData: function() { return null }
                    }
                }).result.then(function() {
                    $state.go('device', null, { reload: 'device' });
                }, function() {
                    $state.go('device');
                });
            }]
        })
        .state('device.edit', {
            parent: 'device',
            url: '/{id}/edit',
            data: {
                 authorities: ['Administrator','Supervisor'],
                    permission: {
                        'resource': 'DEVICE',
                        'level': 'DELETE'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/device/device-create.html',
                    controller: 'DeviceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Device', function(Device) {
                            return Device.get({id : $stateParams.id}).$promise;
                        }],
                        channelData: ['Dchannel', function(Dchannel) {
                            return Dchannel.query({deviceId : $stateParams.id, detailedData : true}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('device', null, { reload: 'device' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('device.delete', {
            parent: 'device',
            url: '/{id}/delete',
            data: {
                 authorities: ['Administrator','Supervisor'],
                    permission: {
                        'resource': 'DEVICE',
                        'level': 'DELETE'
                    }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/device/device-delete-dialog.html',
                    controller: 'DeviceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Device', function(Device) {
                            return Device.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('device', null, { reload: 'device' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
