(function() {

	angular.module('polsoftApp').controller('DeviceController',
			DeviceController);

	DeviceController.$inject = [ '$scope', '$rootScope', '$state', 'Device', 'DeviceSearch', 'DeviceTrackerService'];

	function DeviceController($scope, $rootScope, $state, Device, DeviceSearch, DeviceTrackerService) {
		var vm = this;

		vm.devices = [];
		vm.search = search;
		vm.loadAll = loadAll;

		loadAll(); 
		DeviceTrackerService.subscribe();		
		
		DeviceTrackerService.receive().then(null, null, function (data) {
        	var deviceDatra  = JSON.parse(data.body);
            var updatedDevice = vm.devices.filter(function(obj){ return obj.id == deviceDatra.id });
            updatedDevice[0].lastRefresh = "";
            updatedDevice[0].lastRefresh = updatedDevice[0].lastRefresh + deviceDatra.lastRefresh;            
            var seconds = Math.abs(moment().diff(new Date(updatedDevice[0].lastRefresh).getTime(), 'seconds'));
            updatedDevice[0].dataLinked = seconds < 60 ? "active" : "inactive";
            
            var deviceHealth = JSON.parse(updatedDevice[0].deviceHealth?updatedDevice[0].deviceHealth:{});
            updatedDevice.channelsLinked = deviceHealth.channelsLinked? 'channels in sync':'channels out of sync';
            updatedDevice.deviceConfig = deviceHealth.deviceConfig? 'device config in sync':'device config out of sync';
            updatedDevice.alarmsLinked = deviceHealth.alarmsLinked? 'alarms config in sync':'alarms config out of sync';
            
            
        });
		

		setInterval(function() {
			vm.devices.forEach(function(updatedDevice){
	            var seconds = Math.abs(moment().diff(new Date(updatedDevice.lastRefresh).getTime(), 'seconds'));
	            updatedDevice.dataLinked = seconds < 60 ? "active" : "inactive";	 

	            var deviceHealth = JSON.parse(updatedDevice.deviceHealth?updatedDevice.deviceHealth:{});
	            updatedDevice.channelsLinked = deviceHealth.channelsLinked? 'channels in sync':'channels out of sync';
	            updatedDevice.deviceConfig = deviceHealth.deviceConfig? 'device config in sync':'device config out of sync';
	            updatedDevice.alarmsLinked = deviceHealth.alarmsLinked? 'alarms config in sync':'alarms config out of sync';
	            
	            
	            $scope.$digest()
			}); 
		},  300);

		
		$scope.oneAtATime = true;

		$scope.deleteDevice = function(obj) {
			$state.go('device.delete', obj);
		}

		$scope.editDevice = function(obj) {
			$state.go('device.edit', obj);
		}

		$scope.status = {
			isCustomHeaderOpen : false,
			isFirstOpen : true,
			isFirstDisabled : false
		};

		function loadAll() {
			Device.query(function(result) {
				vm.devices = result;
				vm.devices.forEach(function(updatedDevice){
		            var seconds = Math.abs(moment().diff(new Date(updatedDevice.lastRefresh).getTime(), 'seconds'));
		            
		            updatedDevice.dataLinked = seconds < 60 ? "active" : "inactive";
		            
		            var deviceHealth = JSON.parse(updatedDevice.deviceHealth?updatedDevice.deviceHealth:{});
		            updatedDevice.channelsLinked = deviceHealth.channelsLinked? 'channels in sync':'channels out of sync';
		            updatedDevice.deviceConfig = deviceHealth.deviceConfig? 'device config in sync':'device config out of sync';
		            updatedDevice.alarmsLinked = deviceHealth.alarmsLinked? 'alarms config in sync':'alarms config out of sync';
		            
		            console.log(seconds);
		            console.log(updatedDevice.dataLinked);
				}); 
			});
		}

		function search() {
			if (!vm.searchQuery) {
				return vm.loadAll();
			}
			DeviceSearch.query({
				query : vm.searchQuery
			}, function(result) {
				vm.devices = result;
			});
		}

		$scope.remove = function(scope) {
			scope.remove();
		};

		$scope.toggle = function(scope) {
			scope.toggle();
		};

		$scope.moveLastToTheBeginning = function() {
			var a = $scope.data.pop();
			$scope.data.splice(0, 0, a);
		};

		$scope.tab = 'EditDevicesTab';

		function recursiveFind(nodes, nodeName) {
			//  return nodes.find(result => result.text === nodeName);;
		}
		;

		$scope.setTab = function(newTab) {

			$scope.tab = newTab;

			if (newTab == 'ChangeHierarchiesTab') {
				$scope.deviceHierarchyJson = $scope
						.getJsonOfTree('#editableTree');
				$scope.editableTreeJson = $scope.deviceHierarchyJson;
				$scope.dragAndDropTreeJson = $scope.deviceHierarchyJson;
				var hie = '/Plant/Block C';
				var levels = hie.split('/').splice(1);

				var childNodes = $scope.deviceHierarchyJson;

			} else if (newTab == 'EditDevicesTab') {
				$scope.deviceHierarchyJson = $scope
						.getJsonOfTree('#dragAndDragTree');
				$scope.editableTreeJson = $scope.deviceHierarchyJson;
				$scope.dragAndDropTreeJson = $scope.deviceHierarchyJson;
			}

			$scope.initialiseTree('#editableTree', $scope.editableTreeJson,
					treeOptionsEditable);
			$scope.initialiseTree('#dragAndDragTree',
					$scope.dragAndDropTreeJson, treeOptionsDragAndDrop);
		};

		$scope.isSet = function(newTab) {
			return $scope.tab === newTab;
		};

		$scope.deviceHierarchyJson = [ {
			text : 'Plant',
			isChannel : false,
			id : 0,
			children : []
		} ];

		$scope.editableTreeJson = [];
		$scope.dragAndDropTreeJson = [];

		angular.copy($scope.deviceHierarchyJson, $scope.editableTreeJson);
		angular.copy($scope.deviceHierarchyJson, $scope.dragAndDropTreeJson);

		var treeOptionsEditable = {
			animate : true,
			onClick : function(node) {
				$(this).tree('beginEdit', node.target);
			}
		};

		var treeOptionsDragAndDrop = {
			animate : true,
			dnd : true
		};

		$scope.initialiseTree = function(treeId, treeData, options) {
			$(treeId).tree(options);
			$(treeId).tree('loadData', treeData);
			vm.devices.forEach(function(device) {
				var childNodes = $scope.deviceHierarchyJson;
				var levels = device.location.split('/').splice(1);
				for (var i = 0; i < levels.length; i++) {
					var found = recursiveFind(childNodes, levels[i]);
					if (!found) {
						break;
					} else if (found && i == levels.length - 1) {
						if (!recursiveFind(found.children, device.name))
							found.children.push({
								id : device.id,
								text : device.name
							})
					}
					if (found) {
						childNodes = found.children;
					}
				}
			});
		};

		drawTree();

		function drawTree() {

			var $editableTree = $("<ul>", {
				id : "editableTree"
			});

			$("#editableTreeHolder").append($editableTree);

			var $dragAndDragTree = $("<ul>", {
				id : "dragAndDragTree"
			});
			$("#dragAndDropTreeHolder").append($dragAndDragTree);

			$scope.initialiseTree('#editableTree', $scope.editableTreeJson,
					treeOptionsEditable);
			$scope.initialiseTree('#dragAndDragTree',
					$scope.dragAndDropTreeJson, treeOptionsDragAndDrop);
			vm.devices.forEach(function(device) {
				var childNodes = $scope.deviceHierarchyJson;
				var levels = device.location.split('/').splice(1);
				for (var i = 0; i < levels.length; i++) {
					var found = recursiveFind(childNodes, levels[i]);
					if (!found) {
						break;
					} else if (found && i == levels.length - 1) {
						if (!recursiveFind(found.children, device.name))
							found.children.push({
								id : device.id,
								text : device.name
							})
					}
					if (found) {
						childNodes = found.children;
					}
				}
			});
		}

		function removeTree() {

			$('#dragAndDragTree').remove();

			$('#editableTree').remove();
		}

		function convertToNormalJson(obj) {

			var newObj = {};

			Object.keys(obj).forEach(function(key) {

				if (key == 'children') {
					var children = [];
					for (var i = 0; i < obj[key].length; i++) {
						children.push(convertToNormalJson(obj[key][i]));
					}
					newObj[key] = children;

				} else if (key == 'text' || key == 'id' || key == 'isChannel') {

					newObj[key] = obj[key];
				}
			});
			return newObj;
		}

		$scope.initialiseTree('#editableTree', $scope.editableTreeJson,
				treeOptionsEditable);
		
		$scope.initialiseTree('#dragAndDragTree', $scope.dragAndDropTreeJson,
				treeOptionsDragAndDrop);

		$scope.getJsonOfTree = function(treeId) {

			var root = $(treeId).tree('getRoot');
			var data = $(treeId).tree('getData', root.target);
			var processedJson = convertToNormalJson(data);

			return [ processedJson ];
		};

	}

})();
