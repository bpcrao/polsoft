(function() {
	'use strict';

	angular.module('polsoftApp').controller('DeviceDialogController',
			DeviceDialogController);

	DeviceDialogController.$inject = [ '$timeout', '$scope', '$stateParams',
			'$uibModalInstance', 'entity', 'channelData', 'Device',
			'Dchannel' ,'$filter'];

	function DeviceDialogController($timeout, $scope, $stateParams,
			$uibModalInstance, entity, channelData, Device, Dchannel,$filter) {
		var vm = this;

		 var programInput = [ {
			"id" : 1,
			"program" : "RTD"
		}, {
			"id" : 2,
			"program" : "TC"
		}, {
			"id" : 3,
			"program" : "MV"
		}, {
			"id" : 4,
			"program" : "MA"
		} ];

		var subInput = {
			"1" : [ {
				"id" : 1,
				"input" : "Pt-50"
			}, {
				"id" : 2,
				"input" : "Pt-100"
			}, {
				"id" : 3,
				"input" : "Pt-1000"
			} ],
			"2" : [

			],
			"3" : [ {
				"id" : 1,
				"input" : "0-1V"
			} ],
			"4" : [ {
				"id" : 1,
				"input" : "0-20"
			}, {
				"id" : 2,
				"input" : "4-20"
			} ]
		};
		 
		 vm.subInput = subInput;
		 
		 vm.getProgramInput = function (){
			 return programInput;
		 };
		vm.tab = 1;
		vm.setTab = setTab;
		vm.updateSubInput  = updateSubInput;
		vm.isRTD = isRTD;
		vm.isTC = isTC;
		vm.isSet = isSet;
		vm.intervals = [ '1 Min', '5 Mins' ];
		vm.device = entity;
		vm.device.status = true;
		vm.clear = clear;
		vm.datePickerOpenStatus = {};
		vm.openCalendar = openCalendar;
		vm.save = save;
		vm.setSelected = setSelected;
		vm.initialize = initialize;
		$scope.channelMaxLimit = 160;
		$scope.getNumber = function(num) {
			return new Array(num);
		}
		vm.channelsDataOriginal = channelData;
		vm.device.card1Channels = [];
		vm.device.card2Channels = [];
		vm.device.card3Channels = [];
		vm.device.card4Channels = [];
		var count = 0;
		var count1 = 0;
		var count2 = 0;
		var count3 = 0;
		var count4 = 0;
		
		// Splitting the channels of each 48 for cards
		if(vm.channelsDataOriginal){
		  vm.channelsData = [];
		  vm.channelsDataOriginal.forEach(function(channel) {
			  var nameC = 0;
			  nameC = channel.name.substring(2);
			  count = Number(nameC)
			  channel.channeldata = eval("(" + channel.channeldata + ")");
				var obj = channel;
				if (count > 0 && count < 49) {
					obj.index = nameC - 1;
					vm.device.card1Channels.push(obj);
				}
				else if (count > 48 && count < 97) {
					obj.index =  nameC-49;
					vm.device.card2Channels.push(obj);
				}
				else if (count > 96 && count < 145) {
					obj.index =  nameC-97;
					vm.device.card3Channels.push(obj);
				} else {
					obj.index =  nameC-145;
					vm.device.card4Channels.push(obj);
				}
				count++;
				
			});
		  vm.device.card1Channels = $filter('orderBy')(vm.device.card1Channels, 'index', false);
		  vm.device.card2Channels = $filter('orderBy')(vm.device.card2Channels, 'index', false);
		  vm.device.card3Channels = $filter('orderBy')(vm.device.card3Channels, 'index', false);
		  vm.device.card4Channels = $filter('orderBy')(vm.device.card4Channels, 'index', false);
		}

		if (!vm.device.settings) {
			vm.device.settings = {
				basics : {
					interval : vm.intervals[1],
					timeout : 1
				},
				a1params : {
					maxcards : 1,
					password : "",
					scanrate : 1,
					lograte : 1
				},
				serialCardParams : {},
				card : {},
				alarmData : {}
			};

		} else {
			vm.device.settings = eval("(" + vm.device.settings + ")");
		}
		vm.initialize();
		
		function initialize(){
			var count = 1;
			if (!vm.device.id) {
				vm.device.channels = [];
				vm.device.card1Channels = [];
				vm.device.card2Channels = [];
				vm.device.card3Channels = [];
				vm.device.card4Channels = [];
				var count1 = 0;
				var count2 = 0;
				var count3 = 0;
				var count4 = 0;
				
				while (count < $scope.channelMaxLimit+1) {
					var obj = {
						index : count,
						name : "CH" + count,
						channeldata : { prginput: '', prgsubinput: '', "maLow":"","maHigh":"","mvLow":"","mvHigh":""},
						unit : 'Temperature'
					}
					if (count > 0 && count < 49) {
						obj.index = count1++;
						vm.device.card1Channels.push(obj);
					}
					else if (count > 48 && count < 97) {
						obj.index = count2++;
						vm.device.card2Channels.push(obj);
					}
					else if (count > 96 && count < 145) {
						obj.index = count3++;
						vm.device.card3Channels.push(obj);
					} else {
						obj.index = count4++;
						vm.device.card4Channels.push(obj);
					}
					count++;
				}
	
			}
		}
		vm.selected = 1;
		vm.selectedChannel1 = vm.device.card1Channels[0];
		vm.selectedChannel2 = vm.device.card2Channels[0];
		vm.selectedChannel3 = vm.device.card3Channels[0];
		vm.selectedChannel4 = vm.device.card4Channels[0];
		console.log(vm.selectedChannel);
		if(vm.selectedChannel){
 			if(!vm.selectedChannel.channeldata){
				vm.selectedChannel.channeldata = {}; 
 			}
 		}
		

		$timeout(function() {
			angular.element('.form-group:eq(1)>input').focus();
		});

		function clear() {
			$uibModalInstance.dismiss('cancel');
		}

		function isSet(tabNum) {
			return this.tab === tabNum;
		}

		function setTab(newTab) {
			this.tab = newTab;
		};
		
	    function isRTD() {
        	return true;
        };
        
        function isTC() {
        	return false;
        };
        
        function updateSubInput(value){
        	value  = vm.device.card1Channels[vm.selectedChannel1.index].channeldata.prginput;
        	return  subInput[value];
        };

		vm.selectedIndex = 0;
		function save() {
			vm.isSaving = true;
			vm.device.selectedChannel = vm.device.card1Channels[0];
			vm.device.channels = vm.device.card1Channels.concat(vm.device.card2Channels, vm.device.card3Channels, vm.device.card4Channels);
			if (vm.device.settings) {
				vm.device.settings = JSON.stringify(vm.device.settings);
			}
			if (vm.device.id !== null) {
				Device.update(vm.device, onSaveSuccess, onSaveError);
			} else {
				Device.save(vm.device, onSaveSuccess, onSaveError);
			}
		}

		function setSelected(selectedChannel,count){
			if(count == 1)
				vm.selectedChannel1 = selectedChannel;
			else if(count == 2)
				vm.selectedChannel2 = selectedChannel;
			else if(count == 3)
				vm.selectedChannel3 = selectedChannel;
			else
				vm.selectedChannel4 = selectedChannel;
		}
		function onSaveSuccess(result) {
			$scope.$emit('polsoftApp:deviceUpdate', result);
			$uibModalInstance.close(result);
			vm.isSaving = false;
		}

		function onSaveError() {
			vm.isSaving = false;
		}

		vm.datePickerOpenStatus.lastRefresh = false;

		function openCalendar(date) {
			vm.datePickerOpenStatus[date] = true;
		}
		
	}

})();