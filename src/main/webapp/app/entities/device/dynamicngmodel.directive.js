(function() {
    'use strict';

    angular
    .module('polsoftApp')
    .directive('dynamicModel', dynamicModel);

	dynamicModel.$inject = ['$compile', '$parse'];

    function dynamicModel($compile, $parse) {
    	  return {
    		  restrict: 'A',
    	        terminal: true,
    	        priority: 100000,
    	        link: function (scope, elem) {
    	            var name = $parse(elem.attr('dynamic-model'))(scope);
    	            elem.removeAttr('dynamic-model');
    	            elem.attr('ng-model', name);
    	            $compile(elem)(scope);
    	        }
    	  };
    }
})();
