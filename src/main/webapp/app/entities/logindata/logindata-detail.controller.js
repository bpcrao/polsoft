(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LogindataDetailController', LogindataDetailController);

    LogindataDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Logindata', 'User'];

    function LogindataDetailController($scope, $rootScope, $stateParams, previousState, entity, Logindata, User) {
        var vm = this;

        vm.logindata = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:logindataUpdate', function(event, result) {
            vm.logindata = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
