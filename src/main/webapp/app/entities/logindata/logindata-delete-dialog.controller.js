(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LogindataDeleteController',LogindataDeleteController);

    LogindataDeleteController.$inject = ['$uibModalInstance', 'entity', 'Logindata'];

    function LogindataDeleteController($uibModalInstance, entity, Logindata) {
        var vm = this;

        vm.logindata = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Logindata.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
