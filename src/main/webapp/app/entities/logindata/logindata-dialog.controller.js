(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LogindataDialogController', LogindataDialogController);

    LogindataDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Logindata', 'User'];

    function LogindataDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Logindata, User) {
        var vm = this;

        vm.logindata = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.logindata.id !== null) {
                Logindata.update(vm.logindata, onSaveSuccess, onSaveError);
            } else {
                Logindata.save(vm.logindata, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:logindataUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.lastActiveDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
