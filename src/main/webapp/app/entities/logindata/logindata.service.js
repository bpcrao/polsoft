(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('Logindata', Logindata);

    Logindata.$inject = ['$resource', 'DateUtils'];

    function Logindata ($resource, DateUtils) {
        var resourceUrl =  'api/logindata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.lastActiveDate = DateUtils.convertLocalDateFromServer(data.lastActiveDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.lastActiveDate = DateUtils.convertLocalDateToServer(data.lastActiveDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.lastActiveDate = DateUtils.convertLocalDateToServer(data.lastActiveDate);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
