(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LogindataController', LogindataController);

    LogindataController.$inject = ['$scope', '$state', 'Logindata', 'LogindataSearch'];

    function LogindataController ($scope, $state, Logindata, LogindataSearch) {
        var vm = this;
        
        vm.logindata = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Logindata.query(function(result) {
                vm.logindata = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            LogindataSearch.query({query: vm.searchQuery}, function(result) {
                vm.logindata = result;
            });
        }    }
})();
