(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('LogindataSearch', LogindataSearch);

    LogindataSearch.$inject = ['$resource'];

    function LogindataSearch($resource) {
        var resourceUrl =  'api/_search/logindata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
