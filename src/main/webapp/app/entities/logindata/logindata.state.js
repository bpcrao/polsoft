(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('logindata', {
            parent: 'entity',
            url: '/logindata',
            data: {
            	 authorities: ['Administrator'],
                 permission: {
                     'resource': 'LOCATION',
                     'level': 'VIEW'
                 },
                pageTitle: 'polsoftApp.logindata.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/logindata/logindata.html',
                    controller: 'LogindataController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('logindata');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('logindata-detail', {
            parent: 'entity',
            url: '/logindata/{id}',
            data: {
            	 authorities: ['Administrator'],
                 permission: {
                     'resource': 'LOCATION',
                     'level': 'VIEW'
                 },
                pageTitle: 'polsoftApp.logindata.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/logindata/logindata-detail.html',
                    controller: 'LogindataDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('logindata');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Logindata', function($stateParams, Logindata) {
                    return Logindata.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'logindata',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('logindata-detail.edit', {
            parent: 'logindata-detail',
            url: '/detail/edit',
            data: {
            	 authorities: ['Administrator'],
                 permission: {
                     'resource': 'LOCATION',
                     'level': 'VIEW'
                 }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/logindata/logindata-dialog.html',
                    controller: 'LogindataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Logindata', function(Logindata) {
                            return Logindata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('logindata.new', {
            parent: 'logindata',
            url: '/new',
            data: {
            	 authorities: ['Administrator'],
                 permission: {
                     'resource': 'LOCATION',
                     'level': 'VIEW'
                 }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/logindata/logindata-dialog.html',
                    controller: 'LogindataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                failedAttempts: null,
                                lastActiveDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('logindata', null, { reload: 'logindata' });
                }, function() {
                    $state.go('logindata');
                });
            }]
        })
        .state('logindata.edit', {
            parent: 'logindata',
            url: '/{id}/edit',
            data: {
            	 authorities: ['Administrator'],
                 permission: {
                     'resource': 'LOCATION',
                     'level': 'VIEW'
                 }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/logindata/logindata-dialog.html',
                    controller: 'LogindataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Logindata', function(Logindata) {
                            return Logindata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('logindata', null, { reload: 'logindata' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('logindata.delete', {
            parent: 'logindata',
            url: '/{id}/delete',
            data: {
            	 authorities: ['Administrator'],
                 permission: {
                     'resource': 'LOCATION',
                     'level': 'VIEW'
                 }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/logindata/logindata-delete-dialog.html',
                    controller: 'LogindataDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Logindata', function(Logindata) {
                            return Logindata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('logindata', null, { reload: 'logindata' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
