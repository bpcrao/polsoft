(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('User_accountController', User_accountController);

    User_accountController.$inject = ['$scope', '$state', 'User_account', 'User_accountSearch'];

    function User_accountController ($scope, $state, User_account, User_accountSearch) {
        var vm = this;
        
        vm.user_accounts = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            User_account.query(function(result) {
                vm.user_accounts = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            User_accountSearch.query({query: vm.searchQuery}, function(result) {
                vm.user_accounts = result;
            });
        }    }
})();
