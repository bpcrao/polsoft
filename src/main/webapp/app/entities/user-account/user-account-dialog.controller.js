(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('User_accountDialogController', User_accountDialogController);

    User_accountDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'User_account', 'User'];

    function User_accountDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, User_account, User) {
        var vm = this;

        vm.user_account = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.user_account.id !== null) {
                User_account.update(vm.user_account, onSaveSuccess, onSaveError);
            } else {
                User_account.save(vm.user_account, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:user_accountUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
