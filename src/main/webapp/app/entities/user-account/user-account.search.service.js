(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('User_accountSearch', User_accountSearch);

    User_accountSearch.$inject = ['$resource'];

    function User_accountSearch($resource) {
        var resourceUrl =  'api/_search/user-accounts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
