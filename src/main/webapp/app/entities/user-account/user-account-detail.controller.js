(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('User_accountDetailController', User_accountDetailController);

    User_accountDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'User_account', 'User'];

    function User_accountDetailController($scope, $rootScope, $stateParams, previousState, entity, User_account, User) {
        var vm = this;

        vm.user_account = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:user_accountUpdate', function(event, result) {
            vm.user_account = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
