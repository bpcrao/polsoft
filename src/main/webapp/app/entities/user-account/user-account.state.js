(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-account', {
            parent: 'entity',
            url: '/user-account',
            data: {
                authorities: ['Operator'],
                pageTitle: 'polsoftApp.user_account.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-account/user-accounts.html',
                    controller: 'User_accountController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('user_account');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-account-detail', {
            parent: 'entity',
            url: '/user-account/{id}',
            data: {
                authorities: ['Operator'],
                pageTitle: 'polsoftApp.user_account.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-account/user-account-detail.html',
                    controller: 'User_accountDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('user_account');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'User_account', function($stateParams, User_account) {
                    return User_account.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-account',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-account-detail.edit', {
            parent: 'user-account-detail',
            url: '/detail/edit',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-account/user-account-dialog.html',
                    controller: 'User_accountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['User_account', function(User_account) {
                            return User_account.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-account.new', {
            parent: 'user-account',
            url: '/new',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-account/user-account-dialog.html',
                    controller: 'User_accountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                password: null,
                                createdDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-account', null, { reload: 'user-account' });
                }, function() {
                    $state.go('user-account');
                });
            }]
        })
        .state('user-account.edit', {
            parent: 'user-account',
            url: '/{id}/edit',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-account/user-account-dialog.html',
                    controller: 'User_accountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['User_account', function(User_account) {
                            return User_account.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-account', null, { reload: 'user-account' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-account.delete', {
            parent: 'user-account',
            url: '/{id}/delete',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-account/user-account-delete-dialog.html',
                    controller: 'User_accountDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['User_account', function(User_account) {
                            return User_account.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-account', null, { reload: 'user-account' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
