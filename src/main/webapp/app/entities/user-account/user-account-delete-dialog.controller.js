(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('User_accountDeleteController',User_accountDeleteController);

    User_accountDeleteController.$inject = ['$uibModalInstance', 'entity', 'User_account'];

    function User_accountDeleteController($uibModalInstance, entity, User_account) {
        var vm = this;

        vm.user_account = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            User_account.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
