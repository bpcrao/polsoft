(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DchannelDialogController', DchannelDialogController);

    DchannelDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Dchannel', 'Device', 'ChannelAlarm', 'Location'];

    function DchannelDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Dchannel, Device, ChannelAlarm, Location) {
        var vm = this;

        vm.dchannel = entity;
        vm.clear = clear;
        vm.save = save;
        vm.devices = Device.query();
        vm.channelalarms = ChannelAlarm.query();
        vm.locations = Location.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dchannel.id !== null) {
                Dchannel.update(vm.dchannel, onSaveSuccess, onSaveError);
            } else {
                Dchannel.save(vm.dchannel, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:dchannelUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
