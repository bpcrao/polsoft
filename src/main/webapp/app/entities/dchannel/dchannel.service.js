(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('Dchannel', Dchannel);

    Dchannel.$inject = ['$resource'];

    function Dchannel ($resource) {
        var resourceUrl =  'api/dchannels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
