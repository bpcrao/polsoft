(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('DchannelSearch', DchannelSearch);

    DchannelSearch.$inject = ['$resource'];

    function DchannelSearch($resource) {
        var resourceUrl =  'api/_search/dchannels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
