(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DchannelDeleteController',DchannelDeleteController);

    DchannelDeleteController.$inject = ['$uibModalInstance', 'entity', 'Dchannel'];

    function DchannelDeleteController($uibModalInstance, entity, Dchannel) {
        var vm = this;

        vm.dchannel = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Dchannel.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
