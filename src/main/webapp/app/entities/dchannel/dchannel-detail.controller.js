(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DchannelDetailController', DchannelDetailController);

    DchannelDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Dchannel', 'Device', 'ChannelAlarm', 'Location'];

    function DchannelDetailController($scope, $rootScope, $stateParams, previousState, entity, Dchannel, Device, ChannelAlarm, Location) {
        var vm = this;

        vm.dchannel = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:dchannelUpdate', function(event, result) {
            vm.dchannel = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
