(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DchannelController', DchannelController);

    DchannelController.$inject = ['$scope', '$state', 'Dchannel', 'DchannelSearch'];

    function DchannelController ($scope, $state, Dchannel, DchannelSearch) {
        var vm = this;
        
        vm.dchannels = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Dchannel.query(function(result) {
                vm.dchannels = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            DchannelSearch.query({query: vm.searchQuery}, function(result) {
                vm.dchannels = result;
            });
        }    }
})();
