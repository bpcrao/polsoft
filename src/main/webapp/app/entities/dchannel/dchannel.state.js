(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dchannel', {
            parent: 'entity',
            url: '/dchannel',
            data: {
            	 authorities: ['Administrator'],
                 permission: {
                     'resource': 'CHANNEL',
                     'level': 'VIEW'
                 },
                pageTitle: 'polsoftApp.dchannel.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dchannel/dchannels.html',
                    controller: 'DchannelController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dchannel');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('dchannel-detail', {
            parent: 'entity',
            url: '/dchannel/{id}',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                },
                pageTitle: 'polsoftApp.dchannel.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dchannel/dchannel-detail.html',
                    controller: 'DchannelDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dchannel');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Dchannel', function($stateParams, Dchannel) {
                    return Dchannel.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'dchannel',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('dchannel-detail.edit', {
            parent: 'dchannel-detail',
            url: '/detail/edit',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dchannel/dchannel-dialog.html',
                    controller: 'DchannelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Dchannel', function(Dchannel) {
                            return Dchannel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dchannel.new', {
            parent: 'dchannel',
            url: '/new',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dchannel/dchannel-dialog.html',
                    controller: 'DchannelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                unit: null,
                                channeldata: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dchannel', null, { reload: 'dchannel' });
                }, function() {
                    $state.go('dchannel');
                });
            }]
        })
        .state('dchannel.edit', {
            parent: 'dchannel',
            url: '/{id}/edit',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dchannel/dchannel-dialog.html',
                    controller: 'DchannelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Dchannel', function(Dchannel) {
                            return Dchannel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dchannel', null, { reload: 'dchannel' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dchannel.delete', {
            parent: 'dchannel',
            url: '/{id}/delete',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dchannel/dchannel-delete-dialog.html',
                    controller: 'DchannelDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Dchannel', function(Dchannel) {
                            return Dchannel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dchannel', null, { reload: 'dchannel' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
