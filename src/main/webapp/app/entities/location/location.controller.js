(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LocationController', LocationController);

    LocationController.$inject = ['$scope', '$state', 'Location', 'LocationSearch','Locationtree','LocationTrackerService'];

    function LocationController ($scope, $state, Location, LocationSearch, LocationTree,LocationTrackerService) {
        var vm = this;
        
        vm.locations = [];
        vm.search = search;
        vm.loadAll = loadAll;
        $scope.treeData =[];


        loadAll();
        LocationTrackerService.receive().then(null, null,function(data){
        	if(data.headers.operation=="CREATE")
        	{
        		vm.locations.push(data.body);
        	}
        	if(data.headers.operation=="UPDATE")
        	{
        		var responseBody =angular.fromJson(data.body);
        		var location = vm.locations.find(function(location) {
      			  return responseBody.id==location.id;
    			});
        		angular.extend(location,responseBody);
        	}
        	if(data.headers.operation=="DELETE")
        	{
        		vm.locations = vm.locations.filter(function( obj ) {
        		    return obj.id !== data.body;
        		});
        	}
        });
        function loadAll() {
            Location.query(function(result) {
                vm.locations = result;
            });
            LocationTree.query(function(result) {
                $scope.treeData = [result];
            });
        }

        $scope.checkHasData=function(Location)
        {
            if(location.children.length)
            {
                angular.forEach(location.children,function(element) {
                    if(element.hasData)
                        return true;
                }, this);
            }
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            LocationSearch.query({query: vm.searchQuery}, function(result) {
                vm.locations = result;
            });
        }    }
})();