(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('Locationtree', Locationtree);

    Locationtree.$inject = ['$resource'];

    function Locationtree ($resource) {
        var resourceUrl =  'api/loctree';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET'},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }
})();
