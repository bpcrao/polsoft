(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('LocationDialogController', LocationDialogController);

    LocationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Location', 'Dchannel'];

    function LocationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Location, Dchannel) {
        var vm = this;

        vm.location = entity;
        vm.clear = clear;
        vm.save = save;
        vm.locations = Location.query();
        vm.dchannels = Dchannel.query({filter: 'location-is-null'});
        $q.all([vm.location.$promise, vm.dchannels.$promise]).then(function() {
            if (!vm.location.dchannelId) {
                return $q.reject();
            }
            return Dchannel.get({id : vm.location.dchannelId}).$promise;
        }).then(function(dchannel) {
            vm.dchannels.push(dchannel);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.location.id !== null) {
                Location.update(vm.location, onSaveSuccess, onSaveError);
            } else {
                Location.save(vm.location, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
