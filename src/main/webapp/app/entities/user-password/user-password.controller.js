(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserPasswordController', UserPasswordController);

    UserPasswordController.$inject = ['$scope', '$state', 'UserPassword', 'UserPasswordSearch'];

    function UserPasswordController ($scope, $state, UserPassword, UserPasswordSearch) {
        var vm = this;
        
        vm.userPasswords = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            UserPassword.query(function(result) {
                vm.userPasswords = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            UserPasswordSearch.query({query: vm.searchQuery}, function(result) {
                vm.userPasswords = result;
            });
        }    }
})();
