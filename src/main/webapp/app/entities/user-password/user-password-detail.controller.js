(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserPasswordDetailController', UserPasswordDetailController);

    UserPasswordDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserPassword', 'User'];

    function UserPasswordDetailController($scope, $rootScope, $stateParams, previousState, entity, UserPassword, User) {
        var vm = this;

        vm.userPassword = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:userPasswordUpdate', function(event, result) {
            vm.userPassword = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
