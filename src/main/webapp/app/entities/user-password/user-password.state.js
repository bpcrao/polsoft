(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-password', {
            parent: 'entity',
            url: '/user-password',
            data: {
                authorities: ['Operator'],
                pageTitle: 'polsoftApp.userPassword.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-password/user-passwords.html',
                    controller: 'UserPasswordController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userPassword');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-password-detail', {
            parent: 'entity',
            url: '/user-password/{id}',
            data: {
                authorities: ['Operator'],
                pageTitle: 'polsoftApp.userPassword.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-password/user-password-detail.html',
                    controller: 'UserPasswordDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userPassword');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserPassword', function($stateParams, UserPassword) {
                    return UserPassword.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-password',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-password-detail.edit', {
            parent: 'user-password-detail',
            url: '/detail/edit',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-password/user-password-dialog.html',
                    controller: 'UserPasswordDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserPassword', function(UserPassword) {
                            return UserPassword.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-password.new', {
            parent: 'user-password',
            url: '/new',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-password/user-password-dialog.html',
                    controller: 'UserPasswordDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                password: null,
                                createdDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-password', null, { reload: 'user-password' });
                }, function() {
                    $state.go('user-password');
                });
            }]
        })
        .state('user-password.edit', {
            parent: 'user-password',
            url: '/{id}/edit',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-password/user-password-dialog.html',
                    controller: 'UserPasswordDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserPassword', function(UserPassword) {
                            return UserPassword.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-password', null, { reload: 'user-password' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-password.delete', {
            parent: 'user-password',
            url: '/{id}/delete',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-password/user-password-delete-dialog.html',
                    controller: 'UserPasswordDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserPassword', function(UserPassword) {
                            return UserPassword.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-password', null, { reload: 'user-password' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
