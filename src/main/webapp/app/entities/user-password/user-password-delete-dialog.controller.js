(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserPasswordDeleteController',UserPasswordDeleteController);

    UserPasswordDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserPassword'];

    function UserPasswordDeleteController($uibModalInstance, entity, UserPassword) {
        var vm = this;

        vm.userPassword = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserPassword.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
