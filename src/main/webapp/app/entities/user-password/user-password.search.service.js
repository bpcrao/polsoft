(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('UserPasswordSearch', UserPasswordSearch);

    UserPasswordSearch.$inject = ['$resource'];

    function UserPasswordSearch($resource) {
        var resourceUrl =  'api/_search/user-passwords/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
