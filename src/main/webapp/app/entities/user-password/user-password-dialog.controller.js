(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserPasswordDialogController', UserPasswordDialogController);

    UserPasswordDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserPassword', 'User'];

    function UserPasswordDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserPassword, User) {
        var vm = this;

        vm.userPassword = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userPassword.id !== null) {
                UserPassword.update(vm.userPassword, onSaveSuccess, onSaveError);
            } else {
                UserPassword.save(vm.userPassword, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:userPasswordUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
