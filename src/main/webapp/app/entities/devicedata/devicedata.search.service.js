(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('DevicedataSearch', DevicedataSearch);

    DevicedataSearch.$inject = ['$resource'];

    function DevicedataSearch($resource) {
        var resourceUrl =  'api/_search/devicedata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
