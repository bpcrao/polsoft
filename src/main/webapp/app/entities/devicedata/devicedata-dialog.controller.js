(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DevicedataDialogController', DevicedataDialogController);

    DevicedataDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Devicedata', 'Device'];

    function DevicedataDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Devicedata, Device) {
        var vm = this;

        vm.devicedata = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.devices = Device.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.devicedata.id !== null) {
                Devicedata.update(vm.devicedata, onSaveSuccess, onSaveError);
            } else {
                Devicedata.save(vm.devicedata, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:devicedataUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timeCollected = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
