(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('Devicedata', Devicedata);

    Devicedata.$inject = ['$resource', 'DateUtils'];

    function Devicedata ($resource, DateUtils) {
        var resourceUrl =  'api/devicedata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timeCollected = DateUtils.convertDateTimeFromServer(data.timeCollected);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
