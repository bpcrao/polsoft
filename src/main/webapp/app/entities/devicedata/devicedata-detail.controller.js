(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DevicedataDetailController', DevicedataDetailController);

    DevicedataDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Devicedata', 'Device'];

    function DevicedataDetailController($scope, $rootScope, $stateParams, previousState, entity, Devicedata, Device) {
        var vm = this;

        vm.devicedata = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:devicedataUpdate', function(event, result) {
            vm.devicedata = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
