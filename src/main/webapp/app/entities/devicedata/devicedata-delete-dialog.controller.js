(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DevicedataDeleteController',DevicedataDeleteController);

    DevicedataDeleteController.$inject = ['$uibModalInstance', 'entity', 'Devicedata'];

    function DevicedataDeleteController($uibModalInstance, entity, Devicedata) {
        var vm = this;

        vm.devicedata = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Devicedata.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
