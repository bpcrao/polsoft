(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('devicedata', {
            parent: 'entity',
            url: '/devicedata',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'polsoftApp.devicedata.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/devicedata/devicedata.html',
                    controller: 'DevicedataController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('devicedata');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('devicedata-detail', {
            parent: 'entity',
            url: '/devicedata/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'polsoftApp.devicedata.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/devicedata/devicedata-detail.html',
                    controller: 'DevicedataDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('devicedata');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Devicedata', function($stateParams, Devicedata) {
                    return Devicedata.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'devicedata',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('devicedata-detail.edit', {
            parent: 'devicedata-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/devicedata/devicedata-dialog.html',
                    controller: 'DevicedataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Devicedata', function(Devicedata) {
                            return Devicedata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('devicedata.new', {
            parent: 'devicedata',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/devicedata/devicedata-dialog.html',
                    controller: 'DevicedataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                deviceid: null,
                                channelData: null,
                                timeCollected: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('devicedata', null, { reload: 'devicedata' });
                }, function() {
                    $state.go('devicedata');
                });
            }]
        })
        .state('devicedata.edit', {
            parent: 'devicedata',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/devicedata/devicedata-dialog.html',
                    controller: 'DevicedataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Devicedata', function(Devicedata) {
                            return Devicedata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('devicedata', null, { reload: 'devicedata' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('devicedata.delete', {
            parent: 'devicedata',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/devicedata/devicedata-delete-dialog.html',
                    controller: 'DevicedataDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Devicedata', function(Devicedata) {
                            return Devicedata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('devicedata', null, { reload: 'devicedata' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
