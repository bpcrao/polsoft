(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('DevicedataController', DevicedataController);

    DevicedataController.$inject = ['$scope', '$state', 'Devicedata', 'DevicedataSearch'];

    function DevicedataController ($scope, $state, Devicedata, DevicedataSearch) {
        var vm = this;
        
        vm.devicedata = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Devicedata.query(function(result) {
                vm.devicedata = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            DevicedataSearch.query({query: vm.searchQuery}, function(result) {
                vm.devicedata = result;
            });
        }    }
})();
