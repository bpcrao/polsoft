(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserPermissionDetailController', UserPermissionDetailController);

    UserPermissionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserPermission', 'User'];

    function UserPermissionDetailController($scope, $rootScope, $stateParams, previousState, entity, UserPermission, User) {
        var vm = this;

        vm.userPermission = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:userPermissionUpdate', function(event, result) {
            vm.userPermission = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
