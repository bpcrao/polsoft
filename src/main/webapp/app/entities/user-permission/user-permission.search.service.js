(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('UserPermissionSearch', UserPermissionSearch);

    UserPermissionSearch.$inject = ['$resource'];

    function UserPermissionSearch($resource) {
        var resourceUrl =  'api/_search/user-permissions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
