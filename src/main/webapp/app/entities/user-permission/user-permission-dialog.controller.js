(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserPermissionDialogController', UserPermissionDialogController);

    UserPermissionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserPermission', 'User'];

    function UserPermissionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserPermission, User) {
        var vm = this;

        vm.userPermission = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userPermission.id !== null) {
                UserPermission.update(vm.userPermission, onSaveSuccess, onSaveError);
            } else {
                UserPermission.save(vm.userPermission, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:userPermissionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
