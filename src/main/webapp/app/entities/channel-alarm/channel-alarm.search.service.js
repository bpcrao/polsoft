(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('ChannelAlarmSearch', ChannelAlarmSearch);

    ChannelAlarmSearch.$inject = ['$resource'];

    function ChannelAlarmSearch($resource) {
        var resourceUrl =  'api/_search/channel-alarms/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
