(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('ChannelAlarm', ChannelAlarm);

    ChannelAlarm.$inject = ['$resource'];

    function ChannelAlarm ($resource) {
        var resourceUrl =  'api/channel-alarms/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
