(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('channel-alarm', {
            parent: 'entity',
            url: '/channel-alarm',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                },
                pageTitle: 'polsoftApp.channelAlarm.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/channel-alarm/channel-alarms.html',
                    controller: 'ChannelAlarmController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('channelAlarm');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('channel-alarm-detail', {
            parent: 'entity',
            url: '/channel-alarm/{id}',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                },
                pageTitle: 'polsoftApp.channelAlarm.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/channel-alarm/channel-alarm-detail.html',
                    controller: 'ChannelAlarmDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('channelAlarm');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ChannelAlarm', function($stateParams, ChannelAlarm) {
                    return ChannelAlarm.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'channel-alarm',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('channel-alarm-detail.edit', {
            parent: 'channel-alarm-detail',
            url: '/detail/edit',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channel-alarm/channel-alarm-dialog.html',
                    controller: 'ChannelAlarmDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ChannelAlarm', function(ChannelAlarm) {
                            return ChannelAlarm.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('channel-alarm.new', {
            parent: 'channel-alarm',
            url: '/new',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channel-alarm/channel-alarm-dialog.html',
                    controller: 'ChannelAlarmDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                low: null,
                                high: null,
                                lowLow: null,
                                highHigh: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('channel-alarm', null, { reload: 'channel-alarm' });
                }, function() {
                    $state.go('channel-alarm');
                });
            }]
        })
        .state('channel-alarm.edit', {
            parent: 'channel-alarm',
            url: '/{id}/edit',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channel-alarm/channel-alarm-dialog.html',
                    controller: 'ChannelAlarmDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ChannelAlarm', function(ChannelAlarm) {
                            return ChannelAlarm.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('channel-alarm', null, { reload: 'channel-alarm' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('channel-alarm.delete', {
            parent: 'channel-alarm',
            url: '/{id}/delete',
            data: {
            	authorities: ['Administrator'],
                permission: {
                    'resource': 'CHANNEL',
                    'level': 'VIEW'
                }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/channel-alarm/channel-alarm-delete-dialog.html',
                    controller: 'ChannelAlarmDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ChannelAlarm', function(ChannelAlarm) {
                            return ChannelAlarm.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('channel-alarm', null, { reload: 'channel-alarm' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
