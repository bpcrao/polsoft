(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ChannelAlarmDetailController', ChannelAlarmDetailController);

    ChannelAlarmDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ChannelAlarm', 'Dchannel'];

    function ChannelAlarmDetailController($scope, $rootScope, $stateParams, previousState, entity, ChannelAlarm, Dchannel) {
        var vm = this;

        vm.channelAlarm = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:channelAlarmUpdate', function(event, result) {
            vm.channelAlarm = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
