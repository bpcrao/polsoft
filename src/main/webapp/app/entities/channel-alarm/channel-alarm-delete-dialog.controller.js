(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ChannelAlarmDeleteController',ChannelAlarmDeleteController);

    ChannelAlarmDeleteController.$inject = ['$uibModalInstance', 'entity', 'ChannelAlarm'];

    function ChannelAlarmDeleteController($uibModalInstance, entity, ChannelAlarm) {
        var vm = this;

        vm.channelAlarm = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ChannelAlarm.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
