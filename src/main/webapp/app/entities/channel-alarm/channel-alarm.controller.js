(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ChannelAlarmController', ChannelAlarmController);

    ChannelAlarmController.$inject = ['$scope', '$state', 'ChannelAlarm', 'ChannelAlarmSearch'];

    function ChannelAlarmController ($scope, $state, ChannelAlarm, ChannelAlarmSearch) {
        var vm = this;
        
        vm.channelAlarms = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            ChannelAlarm.query(function(result) {
                vm.channelAlarms = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ChannelAlarmSearch.query({query: vm.searchQuery}, function(result) {
                vm.channelAlarms = result;
            });
        }    }
})();
