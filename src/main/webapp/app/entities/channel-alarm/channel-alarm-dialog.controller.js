(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ChannelAlarmDialogController', ChannelAlarmDialogController);

    ChannelAlarmDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ChannelAlarm', 'Dchannel'];

    function ChannelAlarmDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ChannelAlarm, Dchannel) {
        var vm = this;

        vm.channelAlarm = entity;
        vm.clear = clear;
        vm.save = save;
        vm.dchannels = Dchannel.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.channelAlarm.id !== null) {
                ChannelAlarm.update(vm.channelAlarm, onSaveSuccess, onSaveError);
            } else {
                ChannelAlarm.save(vm.channelAlarm, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('polsoftApp:channelAlarmUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
