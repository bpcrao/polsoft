(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ReportTemplateController', ReportTemplateController);

    ReportTemplateController.$inject = ['$scope','$filter', '$state', 'ReportTemplate', 'ReportTemplateSearch', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants'];

    function ReportTemplateController ($scope, $filter,$state, ReportTemplate, ReportTemplateSearch, ParseLinks, AlertService, pagingParams, paginationConstants) {
        var vm = this;
        
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;
        vm.fromDate = null;
        vm.today = today;
        vm.previousMonth = previousMonth;
        vm.toDate = null;
        vm.today();
        vm.download = download;
        vm.previousMonth();
        $scope.isFromOpen = false;
        $scope.isToOpen = false;
        var today = new Date();
         $scope.datepickers = {
            dtFrom: false,
            dtTo: false
        }
         $scope.toggleOpenDatePicker = function ($event, which) {
             $event.preventDefault();
             $event.stopPropagation();
             $scope.datepickers[which] = true;
         };
         $scope.dateOpened = false;
         $scope.format = "dd-MMM-yyyy";
         function today () {
             var today = new Date();
             vm.toDate = new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate() + 1,today.getUTCHours(),today.getUTCMinutes(),today.getUTCSeconds());
         }
         
         function previousMonth () { // what is this code?
             var fromDate = new Date();
             if (fromDate.getUTCMonth() == 0) {
                 fromDate = new Date(fromDate.getUTCFullYear() - 1, 11, fromDate.getUTCDate(),fromDate.getUTCHours(),fromDate.getUTCMinutes(),fromDate.getUTCSeconds());
             } else {
                 fromDate = new Date(fromDate.getUTCFullYear(), fromDate.getUTCMonth() - 1, fromDate.getUTCDate(),fromDate.getUTCHours(),fromDate.getUTCMinutes(),fromDate.getUTCSeconds());
             }
             vm.fromDate = fromDate;
         }
     
        vm.searchQuery = pagingParams.search;
        vm.currentSearch = pagingParams.search;

        loadAll();

        function loadAll () {
            if (pagingParams.search) {
                ReportTemplateSearch.query({
                    query: pagingParams.search,
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                ReportTemplate.query({
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.reportTemplates = data;
                vm.reportTemplates.forEach(function(template,index){
                	template.fromDate = vm.fromDate;
                	template.toDate =vm.toDate;
                });
                
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function search (searchQuery) {
            if (!searchQuery){
                return vm.clear();
            }
            vm.links = null;
            vm.page = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.transition();
        }

        function clear () {
            vm.links = null;
            vm.page = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.currentSearch = null;
            vm.transition();
        }
        function download(reportTemplate)
        {
        	var fromDate = new Date(reportTemplate.fromDate).toISOString();
        	var toDate = new Date(reportTemplate.toDate).toISOString();
        	if(fromDate > toDate)
        	{
        		window.alert("fromdate cannot be greater than toDate");
        		return;
        	}
        	ReportTemplate.download(reportTemplate.id,fromDate,toDate).then(function(result){
        		var headers = result.headers()
                var blob = new Blob([result.data], { type: headers['content-type'],endings:'transparent' })
                var windowUrl = (window.URL || window.webkitURL)
                var downloadUrl = windowUrl.createObjectURL(blob)
                var anchor = document.createElement("a")
                anchor.href = downloadUrl;
                var fileNamePattern = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/
                anchor.download = fileNamePattern.exec(headers['content-disposition'])[1];
                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                anchor.dispatchEvent(clickEvent);
    		});
        	
        }
    }
})();
