(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ReportTemplateDeleteController',ReportTemplateDeleteController);

    ReportTemplateDeleteController.$inject = ['$uibModalInstance', 'entity', 'ReportTemplate'];

    function ReportTemplateDeleteController($uibModalInstance, entity, ReportTemplate) {
        var vm = this;

        vm.reportTemplate = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ReportTemplate.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
