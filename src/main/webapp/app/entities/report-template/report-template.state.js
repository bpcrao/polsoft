(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('report-template', {
            parent: 'entity',
            url: '/report-template?page&sort&search',
            data: {
          	  authorities: [],
              permission: {
                  'resource': 'REPORTTEMPLATE',
                  'level': 'VIEW'
              },
                pageTitle: 'polsoftApp.reportTemplate.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/report-template/report-templates.html',
                    controller: 'ReportTemplateController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reportTemplate');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('report-template-detail', {
            parent: 'entity',
            url: '/report-template/{id}',
            data: {
          	  authorities: [],
              permission: {
                  'resource': 'REPORTTEMPLATE',
                  'level': 'VIEW'
              },
                pageTitle: 'polsoftApp.reportTemplate.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/report-template/report-template-detail.html',
                    controller: 'ReportTemplateDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reportTemplate');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ReportTemplate', function($stateParams, ReportTemplate) {
                    return ReportTemplate.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'report-template',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('report-template-detail.edit', {
            parent: 'report-template-detail',
            url: '/detail/edit',
            data: {
          	  authorities: [],
              permission: {
                  'resource': 'REPORTTEMPLATE',
                  'level': 'VIEW'
              }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-template/report-template-dialog.html',
                    controller: 'ReportTemplateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ReportTemplate', function(ReportTemplate) {
                            return ReportTemplate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('report-template.new', {
            parent: 'report-template',
            url: '/new',
            data: {
          	  authorities: ['Administrator'],
              permission: {
                  'resource': 'REPORTTEMPLATE',
                  'level': 'VIEW'
              }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-template/report-template-dialog.html',
                    controller: 'ReportTemplateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                locations: [],
                                id: null,
                                timeFrame:null,
                                samplingData:null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('report-template', null, { reload: 'report-template' });
                }, function() {
                    $state.go('report-template');
                });
            }]
        })
        .state('report-template.edit', {
            parent: 'report-template',
            url: '/{id}/edit',
            data: {
          	  authorities: [],
              permission: {
                  'resource': 'REPORTTEMPLATE',
                  'level': 'VIEW'
              }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-template/report-template-dialog.html',
                    controller: 'ReportTemplateDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ReportTemplate', function(ReportTemplate) {
                            return ReportTemplate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('report-template', null, { reload: 'report-template' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('report-template.delete', {
            parent: 'report-template',
            url: '/{id}/delete',
            data: {
          	  authorities: ['Administrator'],
              permission: {
                  'resource': 'REPORTTEMPLATE',
                  'level': 'VIEW'
              }
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/report-template/report-template-delete-dialog.html',
                    controller: 'ReportTemplateDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ReportTemplate', function(ReportTemplate) {
                            return ReportTemplate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('report-template', null, { reload: 'report-template' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
