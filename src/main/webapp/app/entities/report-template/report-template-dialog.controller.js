(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ReportTemplateDialogController', ReportTemplateDialogController);

    ReportTemplateDialogController.$inject = ['$timeout', '$scope', '$stateParams','AlertService','$uibModalInstance', 'entity', 'ReportTemplate', 'Locationtree'];

    function ReportTemplateDialogController($timeout, $scope, $stateParams, AlertService ,$uibModalInstance, entity, ReportTemplate, Locationtree) {
        var vm = this;

        vm.reportTemplate = entity;
        if(vm.reportTemplate.settings)
            {
                vm.reportTemplate.locations=vm.reportTemplate.settings.split(',');
            }
            console.log('vm.reportTemplate.locations ',vm.reportTemplate);
            
        vm.clear = clear;
        vm.save = save;
        vm.getemailUsers = getemailUsers;
        vm.users =[];
        vm.emailUsers =[];
        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        vm.getemailUsers();
        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.validateField = function (inputField) {

        }
        
        function save() {
            if (vm.reportTemplate.timeFrame == 'Minutes' && vm.reportTemplate.samplingData > 60) {
                AlertService.error("Minutes Should be less then 60");

            } else {
                vm.isSaving = true;
                if (vm.reportTemplate.id !== null) {
                    ReportTemplate.update(vm.reportTemplate, onSaveSuccess, onSaveError);
                } else {
                    ReportTemplate.save(vm.reportTemplate, onSaveSuccess, onSaveError);
                }
            }

        }

        function onSaveSuccess(result) {
            $scope.$emit('polsoftApp:reportTemplateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        var treedata_avm = [{
            label: 'Plant',
            isChannel: false,
            id: 0,
            noLeaf: true
        }];
        $scope.my_tree = {};

        Locationtree.query(function (result) {
            console.log('result ', result);
            $scope.my_data = [result];
            $timeout(function () { $scope.my_tree.expand_all(); }, 10);
        });

        $scope.my_data = treedata_avm;

        $scope.my_tree_handler = function (branch, isDeselect) {

            console.log('clicked row ', branch);
           
            if (!isDeselect) {
                $scope.selectedLocation = branch;
                vm.reportTemplate.locations.push(branch.id)
            } else {
                var index = vm.reportTemplate.locations.indexOf(branch.id);
                vm.reportTemplate.locations.splice(index, 1);
            }
        };

        function getemailUsers(){
        	ReportTemplate.getUsers().then(function(data){
        		vm.emailUsers = data;
        		}
        	);
        }
    }
})();
