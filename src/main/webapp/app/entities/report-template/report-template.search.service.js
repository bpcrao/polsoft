(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('ReportTemplateSearch', ReportTemplateSearch);

    ReportTemplateSearch.$inject = ['$resource'];

    function ReportTemplateSearch($resource) {
        var resourceUrl =  'api/_search/report-templates/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
