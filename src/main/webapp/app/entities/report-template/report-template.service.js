(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('ReportTemplate', ReportTemplate);

    ReportTemplate.$inject = ['$resource', '$http'];

    function ReportTemplate ($resource,$http) {
        var resourceUrl =  'api/report-templates/:id';

        var service =  $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
        service.getUsers = function(){
        	return $http({
            	method: 'GET',            
                url: 'api/users/emailNotEmpty'
            }).then(function (response) {
                return response.data;
            });
        }
        service.download = function(id,fromDate,todate)
        {
        	return $http({
            	method: 'GET',
            	responseType:"arraybuffer",
                url: 'api/report-templates/'+id+'/download?fromDate="'+fromDate+'"&toDate="'+todate+'"'
            }).then(function (response) {
                return response;
            });
        }
        return service;
    }
})();
