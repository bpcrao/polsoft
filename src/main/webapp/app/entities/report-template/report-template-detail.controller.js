(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ReportTemplateDetailController', ReportTemplateDetailController);

    ReportTemplateDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ReportTemplate'];

    function ReportTemplateDetailController($scope, $rootScope, $stateParams, previousState, entity, ReportTemplate) {
        var vm = this;

        vm.reportTemplate = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('polsoftApp:reportTemplateUpdate', function(event, result) {
            vm.reportTemplate = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
