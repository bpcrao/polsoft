(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('EventsController', EventsController);

    EventsController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'EventsService'];

    function EventsController($scope, Principal, LoginService, $state, EventsService) {

        var vm = this;

        vm.data = EventsService;

        console.log(vm.data);

    }
})();
