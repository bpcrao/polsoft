(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('events', {
            parent: 'app',
            url: '/events',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/events/events.html',
                    controller: 'EventsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('events');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
