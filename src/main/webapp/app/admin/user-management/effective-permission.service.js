(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('UserPermission', UserPermission);

    UserPermission.$inject = ['$resource'];

    function UserPermission ($resource) {
        var resourceUrl =  'api/user-permissions/:login';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},            
            'update': { method:'PUT' }
        });
    }
})();
