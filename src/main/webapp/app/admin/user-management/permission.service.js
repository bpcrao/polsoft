(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('BulkUserPermission', UserPermission);

    UserPermission.$inject = ['$resource'];

    function UserPermission ($resource) {
        var resourceUrl =  'api/bulkuser-permissions/:login';

        return $resource(resourceUrl, {}, {
            'update': { method:'PUT' }
        });
    }
})();
