(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserManagementDialogController',UserManagementDialogController);

    UserManagementDialogController.$inject = ['$stateParams', '$uibModalInstance', 'entity', 'User','Role', 'JhiLanguageService','Systempreferences'];

    function UserManagementDialogController ($stateParams, $uibModalInstance, entity, User, Role, JhiLanguageService, Systempreferences) {
        var vm = this;
        vm.authorities = ['Operator', 'Administrator'];
      //  vm.user.authorities = vm.authorities[0];
      //  vm.authorities = ['Operator'];
        
        Role.query(function(data){
        	vm.authorities = data.map(function(item){
        	return item.name;	
        	});        	
        });
        vm.clear = clear;
        vm.languages = null;
        vm.save = save;
		vm.user = entity;
		vm.user.authorities = vm.authorities[2];
		if (vm.user.login) {
			vm.expiryDate = new Date(vm.user.expiryDate);
		} else {
			vm.expiryDate = new Date();
			vm.expiryDate.setDate(vm.expiryDate.getDate()+90);
		}

        vm.options={minDate : new Date()};


        vm.passwordPattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.ipAddress={};

        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        loadAll();
        function loadAll() {
            Systempreferences.query(function(result) {
                vm.systempreferences = result;
               vm.Passwordlength = parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "Password Minlength"})[0].data)
             //vm.passwordMinLength=parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "password length"})[0].data);
            });
        }
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function onSaveSuccess (result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function save () {
        	var reason = prompt("Please enter a reason : ");
        	if(reason && !(0 === reason.length)){
        	vm.user.reason = reason;
            vm.user.langKey='en';
            vm.isSaving = true;
            vm.user.expiryDate = vm.expiryDate;
           // vm.user.authorities = vm.authorities[0];
            if (vm.user.id !== null) {
                User.update(vm.user, onSaveSuccess, onSaveError);
            } else {
                User.save(vm.user, onSaveSuccess, onSaveError);
            }
            var elmnt = document.getElementById("myUserLabel");
            elmnt.scrollIntoView();
        }
        	else
        		alert("Need to enter a reason to proceed with the action!"); 
        }
        
        
        vm.datePickerOpenStatus.expiryDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();