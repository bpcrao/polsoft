(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserManagementPermissionController', UserManagementPermissionController);

    UserManagementPermissionController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'user', 'permissions', 'User',
        'RoleResources', 'UserPermission', 'BulkUserPermission'
    ];

    function UserManagementPermissionController($scope, $stateParams, $uibModalInstance, entity, user, permissions, User, RoleResources,
        UserPermission, BulkUserPermission) {
        var vm = this;
        vm.userResources = entity;
        vm.resources = [];
        vm.setResources = setResources;
        vm.user = user;
        vm.epermissions = permissions;
        if (entity) vm.setResources();

        var onSaveSuccess = function (result) {
            $scope.$emit('polsoftApp:roleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            BulkUserPermission.save({
                'userPermissions': vm.resources
            }, onSaveSuccess, onSaveError);
        };

        vm.clear = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function setResources() {
            RoleResources.query(function (resources) {
                angular.forEach(resources, function (val, key) {
                    var exists = vm.userResources.filter(function (resource) {
                        return resource.name === val
                    }).length;
                    var ep = 0;
                    var authorityPermission = vm.epermissions.filter(function (resource) {
                        return resource.name === val
                    }).length;
                    if (authorityPermission) {
                        ep = vm.epermissions.filter(function (resource) {
                            return resource.name === val
                        })[0].permission;
                    }
                    if (!exists) {
                        vm.resources.push({
                            name: val,
                            permission: 0,
                            userId: vm.user.id,
                            epermission: ep
                        });
                    } else {
                        var resource = vm.userResources.filter(function (
                            resource) {
                            return resource.name === val
                        })[0];
                        resource.epermission = ep;
                        resource.maxpermission = Math.max(ep, resource.permission);
                        vm.resources.push(resource);
                    }
                });
            });
        };
    }
})();
