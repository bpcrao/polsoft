(function() {
    'use strict';
    angular
        .module('polsoftApp')
        .factory('EffectivePermission', EffectivePermission);

    EffectivePermission.$inject = ['$resource'];

    function EffectivePermission ($resource) {
        var resourceUrl =  'api/effective-permissions/:login';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
