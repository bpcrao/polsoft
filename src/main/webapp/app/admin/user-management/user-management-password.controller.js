(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('UserManagementPasswordController',UserManagementPasswordController);

    UserManagementPasswordController.$inject = ['$stateParams', '$uibModalInstance', 'entity', 'User','Role', 'JhiLanguageService','Auth','ChangePassword','Systempreferences','EncryptService'];

    function UserManagementPasswordController ($stateParams, $uibModalInstance, entity, User, Role, JhiLanguageService, Auth, ChangePassword, Systempreferences,EncryptService) {
        var vm = this;
        vm.authorities = ['Operator', 'Administrator'];
        Role.query(function(data){
        	vm.authorities = data.map(function(item){
        	return item.name;
        	});
        });
        vm.clear = clear;
        vm.languages = null;
        vm.user = entity;
        if (vm.user.login) {
        	vm.expiryDate = new Date(vm.user.expiryDate);
        }
        vm.passwordPattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;
        vm.datePickerOpenStatus = {};

        vm.confirmPassword = '';
        vm.passwordStrength = 0;

        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        loadAll();
        function loadAll() {
            Systempreferences.query(function(result) {
                vm.systempreferences = result;
               vm.Passwordlength = parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "Password Minlength"})[0].data)
             //vm.passwordMinLength=parseInt(vm.systempreferences.filter(function(data){return data.keyproperty == "password length"})[0].data);
            });
        }
        
        function onSaveSuccess (result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    
        vm.changePassword =function() {
            if (vm.user.password !== vm.confirmPassword) {
                vm.error = null;
                vm.success = null;
                vm.doNotMatch = 'ERROR';
            } else {
                vm.doNotMatch = null;
                var reason = prompt("Please enter a reason : ");
            	if(reason && !(0 === reason.length)){
            	vm.user.reason = reason;
                vm.success = 'OK';
                $uibModalInstance.close("password changed successfully");
            	}
            	else
            		alert("Need to enter a reason to proceed with the action!");
            	var key = EncryptService.getPublicKey();
            	key.$get().then(function (result) {
            		var encrypt = new JSEncrypt();
            		encrypt.setPublicKey(result.publicKey);
            		vm.user.password1 = encrypt.encrypt(vm.user.password);
            		ChangePassword.update({'login':vm.user.login,'password': vm.user.password1,'reason': vm.user.reason}).$promise.then(function () {
                        vm.error = null;
                        vm.oldpassword = null; 
                    }).catch(function (error) {
                        vm.success = null;
                        if(error.data==="Password Exists"){
                        	vm.oldpassword = error.data;
                        } else {
                        vm.error = 'ERROR';
                        }
                    });
            	});
            }
        };

    }
})();
