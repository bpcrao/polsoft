(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('BackupNowService', BackupNowService);

    BackupNowService.$inject = ['$resource'];

    function BackupNowService($resource) {
        var resourceUrl =  '/api/backupData';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET'}
        });
    }
})();
