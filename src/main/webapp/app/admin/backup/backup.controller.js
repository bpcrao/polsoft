(function() {
	'use strict';

	angular.module('polsoftApp').controller('BackupController',
			BackupController);

	BackupController.$inject = [ '$scope', '$http', 'BackupService',
			'BackupNowService', 'AlertService', 'JhiConfigurationService','$window' ];

	function BackupController($scope, $http, BackupService, BackupNowService,
			AlertService, JhiConfigurationService, window ) {
		var vm = this, backupNow, restoreDB;

		vm.backupNow = backupNow;
		vm.restoreDB = restoreDB;
		vm.restore = restore;
		vm.isRestore = false;
		BackupService.query({}, onSuccess, onError);
		
		BackupService.verifyFolderName().then(function(data){
       	 vm.verifyFileName = data.data; 
        });
		
		JhiConfigurationService.getEnv().then(function(configuration) {
			vm.isRestore = configuration.profiles.filter(function(profile) {
				return profile.val === 'restore'
			}).length;
		});
		 
		 
		function backupNow() {
			BackupNowService.query({}, onBackupSuccess, onBackupError)
		}

		function restoreDB(folderName) {
			BackupService.restoreDB(folderName).then(onRestoreSuccess);
		}
		
		function restore(folderName) {
			BackupService.restore(folderName, function() {}, onRestoreError)
		}

		function onSuccess(data, headers) {
			vm.backups = data;
		}

		function onBackupSuccess(data, headers) {
			AlertService.success("Backup success");
			BackupService.query({}, onSuccess, onError);
		}

		function onError(error) {
			AlertService.error(error.data.message);
		}

		function onBackupError(data, headers) {
			AlertService.error(error.data.message);
		}
		
		function onRestoreSuccess(data) {
			AlertService.success("Restore success");
			window.open("http://"+window.location.hostname+":8081/polsoft.html");
		}

		function onRestoreError(error) {
			AlertService.error("Restore failed");
		}


	}
})();