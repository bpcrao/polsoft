(function() {
	'use strict';

	angular.module('polsoftApp').factory('BackupService', BackupService);

	BackupService.$inject = [ '$resource', '$http' ];

	function BackupService($resource, $http) {
		var resourceUrl = '/api/backupFolderList';

		var service = $resource(resourceUrl, {}, {
			'query' : {
				method : 'GET',
				isArray : true
			}
		});

		service.restoreDB = function(folderName) {

			return $http.get('api/restoreVerify/' + folderName).then(
					function(response) {
						return response.data;
					});
		};
		service.restore = function(folderName) {

			return $http.get('api/restore/' + folderName).then(
					function(response) {
						return response.data;
					});
		};
		service.verifyFolderName = function() {
			return $http.get('api/verifiedFolderName/').then(
					function(response) {
						return response.data;
					});
		};
		return service;
	}
})();
