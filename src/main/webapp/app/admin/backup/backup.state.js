(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('backup', {
            parent: 'admin',
            url: '/backup',
            data: {
                authorities: ['Administrator'],
                pageTitle: 'backup.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/backup/backup.html',
                    controller: 'BackupController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('backup');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
