(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('SchedulerService', SchedulerService);

    SchedulerService.$inject = ['$resource', '$http'];

    function SchedulerService($resource, $http) {
        var service = $resource('/api/scheduler/schedulejob', {}, {
            'create': { method: 'POST' }
        });
        

        return service;
    }
    
})();