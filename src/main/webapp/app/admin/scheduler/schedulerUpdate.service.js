(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('SchedulerUpdateService', SchedulerUpdateService);

    SchedulerUpdateService.$inject = ['$resource', '$http'];

    function SchedulerUpdateService($resource, $http) {
        var service = $resource('/api/scheduler/updatejob', {}, {
            'update': { method: 'POST' }
        });
        ///api/scheduler/unschedulejob
        service.deleteaction = function(job) {
            return $http({
            	method: 'POST',            
                url: '/api/scheduler/unschedulejob',
                data: job
            }).then(function (response) {
                return response.data;
            });
        };
        return service;
    }
})();
