(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('loadJobClassesService',loadJobClassesService);

    loadJobClassesService.$inject = ['$resource'];

    function loadJobClassesService($resource) {
        var resourceUrl =  '/api/scheduler/getjobclasses';
        var service= $resource(resourceUrl, {}, {
            'query': { method: 'GET',isArray: true}
        });
        
        service.getReportTemplates = $resource('/api/scheduler/getReportTemplates', {}, {
            'query': { method: 'GET'}
        });
        return service;
    }
})();
