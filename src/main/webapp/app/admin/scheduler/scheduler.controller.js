(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('SchedulerController', SchedulerController);

    SchedulerController.$inject = ['$scope', '$http', 'SchedulerService', 'JobsService','SchedulerUpdateService','loadJobClassesService'];

    function SchedulerController($scope, $http, SchedulerService, JobsService,SchedulerUpdateService,loadJobClassesService) {
        var vm = this;
       
        vm.cronExpression = "";
        vm.jobOptions ="";
        vm.scheduleJob = scheduleJob;
        vm.updateJob = updateJob;
        vm.deleteJob = deleteJob;       
        vm.reportTemplates = ""
        
        vm.config = {
            quartz: true,
            options: {
                allowWeek: false,
                allowMonth: false,
                allowYear: false
            }
        };
        
        JobsService.query({
        }, onSuccess, onError);
        
        loadJobClassesService.query({        		
        },loadjoboptions,onError);
        
        loadJobClassesService.getReportTemplates.query({
        }, loadReportTemplates , onError);
        
        function onSuccess(data, headers) {
           vm.jobs = data;
        }
        function onError(error) {        	
            AlertService.error(error.data.message);
            }            
        	
       
        function loadjoboptions(data, headers){
        	vm.jobOptions = data;
        }
        
        function loadReportTemplates(data, headers){
        	vm.reportTemplates =data;
        }
        function scheduleJob() {
        	if(!vm.jobName)
        	{
        		window.alert("please enter job name");
        		return;
        	}
        	if(!vm.selectedJob)
        	{
        		window.alert("please select job type");
        		return;
        	}
        	
        	if(vm.selectedJob=='Reports')
        	{
        		if(!vm.selectedTemplate)
        		{
        			window.alert("please select report name");
            		return;
        		}        		
        	}
        	if(!vm.cronExpression)
        	{
        		window.alert("please select cron expression");
        		return;
        	}
        	
            var jobData ={
            		  "className": vm.selectedJob,
            		  "cronExpression": vm.cronExpression,
            		  "jobDesciption": "sample api",
            		  "jobGroup": vm.selectedJob+'-group',
            		  "jobName": vm.jobName,
            		  "templateid": vm.selectedTemplate,
            		  "lastExceutionTime": "",
            		  "nextExecutionTime": "",
            		  "status": "Active"
            		};

            SchedulerService.create(jobData, function(data){
            	vm.jobs.push(data);
            }, onSaveError);
        }
        function updateJob(job) {
            SchedulerUpdateService.update(job, onSaveSuccess, onSaveError);
        }
        function deleteJob(job) {
            SchedulerUpdateService.deleteaction(job).then(function(data){
            	vm.jobs.pop(data);
            });
        }


        function onSaveSuccess(result) {
            $scope.$emit("Job created", result);
           //$uibModalInstance.close(result);
            vm.isSaving = false;
            vm.jobs = result;
            JobsService.query({
            }, onSuccess, onError);
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();