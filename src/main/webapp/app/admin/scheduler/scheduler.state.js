(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('scheduler', {
            parent: 'admin',
            url: '/scheduler',
            data: {
                authorities: ['Administrator'],
                pageTitle: 'scheduler.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/scheduler/scheduler.html',
                    controller: 'SchedulerController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('scheduler');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
