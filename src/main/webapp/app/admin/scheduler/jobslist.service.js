(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('JobsService', JobsService);

    JobsService.$inject = ['$resource'];

    function JobsService($resource) {
        var resourceUrl =  '/api/scheduler/getAllJobs';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
