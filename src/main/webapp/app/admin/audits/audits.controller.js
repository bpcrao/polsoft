(function () {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('AuditsController', AuditsController);

    AuditsController.$inject = ['$scope', '$filter', 'AuditsService', 'ParseLinks', 'AlertService', 'ObjectDiff', '$uibModal', 'Principal', 'AuditTrackerService'];

    function AuditsController($scope, $filter, AuditsService, ParseLinks, AlertService, ObjectDiff, $uibModal, Principal, AuditTrackerService) {
        var vm = this;
        vm.auditTypes = ['AUTHENTICATION_SUCCESS', 'NEW_Location is Added', 'ALL'];
        vm.predicate = 'auditEventDate';
        vm.id = 'id';
        vm.selectedAuditType = 'ALL';
        vm.audits = null;
        vm.fromDate = null;
        vm.CurrentDateTime = null;
        vm.links = null;
        vm.loadPage = loadPage;
        vm.onChangeDate = onChangeDate;
        vm.page = 1;
        vm.previousMonth = previousMonth;
        vm.toDate = null;
        vm.today = today;
        vm.totalItems = null;

        vm.today();
        vm.previousMonth();
        vm.onChangeDate();
        vm.findAllAudited = findAllAudited;
        vm.openChange = openChange;
        vm.signOn = signOn;
        vm.print = print;

        $scope.isFromOpen = false;
        $scope.isToOpen = false;
        var today = new Date();

        $scope.datepickers = {
            dtFrom: false,
            dtTo: false
        }
        $scope.toggleOpenDatePicker = function ($event, which) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepickers[which] = true;
        };
        
        var today = new Date();
        var dateFormat = 'yyyy-MM-dd';
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        vm.CurrentDateTime = $filter('date')(date+' '+time, dateFormat);
        //  $scope.fromDate = new Date();

        // $scope.fromDate.setHours($scope.fromDate.getHours() - 1);

        // $scope.toDate = today;

        // $scope.maxDate = new Date();



        $scope.dateOpened = false;
        // $scope.hourStep = 1;
        $scope.format = "dd-MMM-yyyy";
        // $scope.minuteStep = 1;
        $scope.showMeridian = true;

        vm.findAllAudited();

        AuditTrackerService.receive().then(null, null, function (data) {
            vm.onChangeDate();
        });



        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
            });
        }


        function findAllAudited() {
            AuditsService.findAllAudited().then(function (data) {
                vm.auditTypes = data;
                vm.auditTypes.push('ALL');
            });
        }

        $('#printName').hide();

        function onChangeDate() {
            var dateFormat = 'yyyy-MM-dd';
            var fromDate = $filter('date')(vm.fromDate, dateFormat);
            var toDate = $filter('date')(vm.toDate, dateFormat);
            var eventTypeFilter = vm.selectedAuditType;
            AuditsService.query({ page: vm.page - 1, size: 20, fromDate: fromDate, toDate: toDate, eventTypeFilter: eventTypeFilter, sort: sort() }, function (result, headers) {
                vm.audits = result;
                if(vm.audits){
                	vm.audits.forEach(function(audit) {
						if (audit.entityValue) {
							audit.entityValue = JSON.parse(audit.entityValue);
						}
					});                	
                }
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
            });
        }

        $scope.getName=function(obj,isCreatedby)
        {
            if(obj != null)
                return isCreatedby?JSON.parse(obj).createdBy:JSON.parse(obj).name;
        } 
        function print() {
            var dateFormat = 'yyyy-MM-dd';
            var fromDate = $filter('date')(vm.fromDate, dateFormat);
            var toDate = $filter('date')(vm.toDate, dateFormat);
            var eventTypeFilter = vm.selectedAuditType;
            AuditsService.findAll(fromDate, toDate, eventTypeFilter, sort()).then(function (response) {
                vm.auditTemp = response.data;
            }).then(function () {
                $("#nonPrintDiv").hide();
                $('#auditTable').hide();
                $('#printIcon').hide();
                $('#printName').show();
                $('#auditTablePrint').show();
                $('.navbar').hide();
                

                setTimeout(function () {
                    // var elemToPrint = document.getElementById("auditTablePrint");
                    // var domClone = elemToPrint.cloneNode(true);
                    // var printSection = document.createElement('div');
                    // // if there is no printing section, create one
                    // printSection.id = 'printSection';
                    // document.body.appendChild(printSection);
                    // document.getElementById('printSection').appendChild(domClone);
                  // return;
                    window.print();
                    $('.navbar').show();
                    $("#nonPrintDiv").show();
                    $('#auditTable').show();
                    $('#printIcon').show();
                     $('#printName').hide();
                    $('#auditTablePrint').hide();
                   // document.body.removeChild(printSection);
                }, 2000);


            });
        }

        function sort() {
            var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            if (vm.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }

        // Date picker configuration
        function today() {
            // Today + 1 day - needed if the current day must be included
            var today = new Date();
            vm.toDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
        }

        function previousMonth() {
            var fromDate = new Date();
            if (fromDate.getMonth() === 0) {
                fromDate = new Date(fromDate.getFullYear() - 1, 11, fromDate.getDate());
            } else {
                fromDate = new Date(fromDate.getFullYear(), fromDate.getMonth() - 1, fromDate.getDate());
            }

            vm.fromDate = fromDate;
        }

        function loadPage(page) {
            vm.page = page;
            vm.onChangeDate();
        }

        ///

        function format(val) {
            if (val)
                return ObjectDiff.objToJsonView(val);
            else return '';
        };

        function isObject(val) {
            return (val && (typeof val) == 'object');
        };

        function isDate(key) {
            return (key && key.indexOf("Date") != -1);
        };

        function openChange(audit) {
            if (audit.commitVersion < 2) {
                AlertService.warning("entityAudit.result.firstAuditEntry");

            } else {
                AuditsService.getPrevVersion(audit.type, audit.entityId, audit.commitVersion).then(function (data) {
                    var previousVersion = JSON.parse(data.entityValue),
                        currentVersion = JSON.parse(audit.entityValue);
                    var diff = ObjectDiff.diffOwnProperties(previousVersion, currentVersion);

                    $uibModal.open({
                        templateUrl: 'app/admin/audits/entity-audit.detail.html',
                        controller: 'AuditDetailMoCtrl',
                        controllerAs: 'vm',
                        size: 'lg',
                        resolve: {
                            diff: function () {
                                return diff;
                            },
                            audit: function () {
                                return audit;
                            }
                        }
                    });
                });
            }
        };

        function signOn() {
            var auditIds = vm.audits.map(audit => audit.id);
            AuditsService.signOn(auditIds);
        };



        function convertDates(obj) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key) && obj[key]) {
                    if (key.indexOf("Date") != -1 && (obj[key] instanceof Date || Object.prototype.toString.call(obj[key]) === '[object Date]' || (new Date(obj[key]) !== "Invalid Date" && !isNaN(new Date(obj[key]))))) {
                        obj[key] = $filter('date')(obj[key]);
                    }
                }
            }
            return obj;
        }
        //
    }
})();
