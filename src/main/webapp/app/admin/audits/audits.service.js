(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .factory('AuditsService', AuditsService);

    AuditsService.$inject = ['$resource','$http'];

    function AuditsService ($resource, $http) {
        var service = $resource('management/jhipster/audits/:id', {}, {
            'get': {
                method: 'GET',
                isArray: true
            },
            'query': {
                method: 'GET',
                isArray: true,
                params: {fromDate: null, toDate: null, eventTypeFilter: null}
            }
            
        });

        service.findAllAudited = function () {
            return $http.get('management/jhipster/audits/types').then(function (response) {
                return response.data;
            });
        };
        
        service.findByEntity = function(entityType, limit) {
            return $http.get('management/jhipster/audits/changes', {
                params: {
                    entityType: entityType,
                    limit: limit
                }
            }).then(function (response) {
                return response.data;
            });
        };
        service.findAll = function(fromDate,toDate,eventTypeFilter,sort) {
        	return $http.get('management/jhipster/audits/getAll', {
                params: {
                	fromDate: fromDate,
                	toDate: toDate,
                	eventTypeFilter: eventTypeFilter,
                	sort: sort
                }
            })
        }
        service.signOn = function(auditIds) {
            return $http.put('management/jhipster/audits/', auditIds).then(function (response) {
                return response.data;
            });
        };
        

        service.getPrevVersion = function(qualifiedName, entityId, commitVersion) {
            return $http.get('management/jhipster/audits/changes/version/previous', {
                params: {
                    qualifiedName: qualifiedName,
                    entityId: entityId,
                    commitVersion: commitVersion
                }
            }).then(function (response) {
                return response.data;
            });
        };
        
        return service;
    }
})();
