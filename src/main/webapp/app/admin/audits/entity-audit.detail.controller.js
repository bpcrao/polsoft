(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('AuditDetailMoCtrl', AuditDetailMoCtrl);

    AuditDetailMoCtrl.$inject = ['$scope', '$uibModalInstance', 'ObjectDiff', 'diff', 'audit'];

    function AuditDetailMoCtrl($scope, $uibModalInstance, ObjectDiff, diff, audit) {
        var vm = this;
        
        vm.diffValue = ObjectDiff.toJsonView(diff);
        vm.diffValueChanges = ObjectDiff.toJsonDiffView(diff);
        vm.audit = audit;
        vm.cancel = cancel;

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    };
})();
