(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('channel', {
            parent: 'app',
            url: '/channel',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/channel/channel.html',
                    controller: 'ChannelController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('channel');
                    return $translate.refresh();
                }]
            }
        })
        .state('channel.alarm', {
            parent: 'channel',
            url: '/alarm',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/channel/alarmoff.html',
                    controller: 'alarmsoffController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                parameter: null,
                                condition: null,
                                value: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('channel', null, { reload: 'channel' });
                }, function() {
                    $state.go('channel');
                });
            }]
        })
        .state('channel.setAlarm', {
            parent: 'channel',
            url: '/setAlarm',
            data: {
                authorities: ['Operator']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/setAlarm/alarmoff.html',
                    controller: 'SetAlarmController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg'
                }).result.then(function() {
                    $state.go('channel', null, {});
                }, function() {
                    $state.go('channel');
                });
            }]
        });
    }
})();
