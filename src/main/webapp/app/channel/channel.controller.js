(function() {
    'use strict';

    angular
        .module('polsoftApp')
        .controller('ChannelController', ChannelController);

    ChannelController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'ChannelService', 'ChannelHelperService', '$timeout', 'Alarm'];

    function ChannelController($scope, Principal, LoginService, $state, ChannelService, ChannelHelperService, $timeout, Alarm) {
        var vm = this;

        vm.alarms = [];

        Alarm.query(function(result) {
            vm.alarms = result;
        });

        $scope.setAlarm = function(cb) {
            // $state.transitionTo('setAlarm', {  
            // })
        };

        $scope.multipleChartConfig = {
            chart: {
                zoomType: 'xy'
            },

            title: {
                text: 'Channel Time Series'
            },
            subtitle: {
                text: ''
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            xAxis: [{
                categories: [],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} Pa',
                    style: {
                        color: 'red'
                    }
                },
                title: {
                    text: 'Pressure',
                    style: {
                        color: 'red'
                    }
                },
                opposite: true

            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Temperature',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} °C',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: []
        };

        $scope.selectedChannels = [];


        $scope.Intervals = [{
            id: 1,
            name: "Hours"
        }, {
            id: 2,
            name: "Days"
        }, {
            id: 3,
            name: "Minutes"
        }];

        $scope.selectedInterval = $scope.Intervals[2];


        $scope.changeIntervalValue = function(d) {
            if (d.name == "Hours") {
                $scope.selectedInterval = $scope.Intervals[0];
            } else if (d.name == "Minutes") {
                $scope.selectedInterval = $scope.Intervals[2];
            } else {
                $scope.selectedInterval = $scope.Intervals[1];
            }
        }

        $scope.zoomLimitTracker = 0;

        $scope.renderChart = function() {

            $scope.isLoading = true;

            var channelList = $scope.channelList || [];
            var startDateTime = $scope.startDateTime;
            var endDateTime = $scope.endDateTime;

            var obj;

            if ($scope.selectedInterval.name == "Minutes") {

                obj = ChannelHelperService.doubleMinutes(startDateTime, endDateTime);

            } else if ($scope.selectedInterval.name == "Hours") {

                obj = ChannelHelperService.doubleHours(startDateTime, endDateTime);

            } else {

                obj = ChannelHelperService.doubleDates(startDateTime, endDateTime);
            }

            $scope.TimeData = {
                startDateTime: obj.startDateTime,
                endDateTime: obj.endDateTime,
                pseudoStartDateTime: obj.pseudoStartDateTime,
                pseudoEndDateTime: obj.pseudoEndDateTime,
                currentStart: startDateTime,
                currentEnd: endDateTime
            };

            $scope.startDateTime = obj.startDateTime;
            $scope.endDateTime = obj.endDateTime;
            $scope.pseudoStartDateTime = obj.pseudoStartDateTime;
            $scope.pseudoEndDateTime = obj.pseudoEndDateTime;
            $scope.getDataForChart(channelList, obj.startDateTime, obj.endDateTime);
        };

        $scope.getDataForChart = function(channelList, startDateTime, endDateTime) {

            ChannelService.getDataFromServer(channelList, startDateTime, endDateTime, $scope.selectedInterval)
                .then(function(serviceObject) {
                        $scope.data = serviceObject.data;

                        $scope.channelList = serviceObject.channelList;

                        var selectedChannels = [];

                        for (var i = 0; i < $scope.channelList.length; i++) {
                            if ($scope.channelList[i].toShow) {
                                selectedChannels.push($scope.channelList[i])
                            }
                        }

                        $scope.initialiseChart($scope.data, selectedChannels, $scope.multipleChartConfig);

                    },
                    function(data) {
                        console.log('Failed to get Data.');
                    });
        };

        $scope.initialiseChart = function(input, selectedChannels, chart) {

            var input = input;

            var chartToRender = chart || $scope.multipleChartConfig;

            makeChart(selectedChannels, input, chartToRender);

            $scope.isLoading = false;
        };

        function makeChart(selectedChannels, requestedChannelsData, chartToRender) {

            $scope.categories = [];
            var seriesTemp;
            var seriesHumidity;

            var tempDS = [];

            for (var i = 0; i < $scope.channelList.length; i++) {

                var temp = {
                    name: $scope.channelList[i].name,
                    values: []
                }

                tempDS.push(temp);
            }

            for (var ch = 0; ch < selectedChannels.length; ch++) {

                var currentChannel = selectedChannels[ch];
                var currentChannelName = selectedChannels[ch].name;
                var currentChannelId = currentChannel.id;
                var series_ = [];

                for (var k = 0; k < requestedChannelsData.length; k++) {

                    series_.push(Number(requestedChannelsData[k][currentChannelName]));

                    if (ch == 0) {
                        seriesTemp = [];
                        seriesHumidity = [];
                        $scope.categories.push(requestedChannelsData[k].logdate);
                    }
                }

                for (var i = 0; i < tempDS.length; i++) {
                    if (tempDS[i].name == currentChannelName) {
                        tempDS[i].values = series_;
                    }
                }
            }

            chartToRender.series = [];

            chartToRender.xAxis[0] = {
                categories: setCategories($scope.categories)
            };

            for (var i = 0; i < tempDS.length; i++) {

                if (isChannelNeeded(tempDS[i].name)) {

                    var currentSeriesData = [];

                    for (var k = 0; k < tempDS[i].values.length; k++) {
                        currentSeriesData.push(tempDS[i].values[k]);
                    }

                    if (getColumnValue(tempDS[i].name) == 1) {

                        chartToRender.series.push({
                            name: tempDS[i].name,
                            data: currentSeriesData,
                            yAxis: 1,
                            tooltip: {
                                valueSuffix: ' mm'
                            }
                        });
                    } else {
                        chartToRender.series.push({
                            name: tempDS[i].name,
                            data: currentSeriesData,
                            yAxis: 0,
                            tooltip: {
                                valueSuffix: ' c'
                            }
                        });
                    }
                }
            }
        }

        function isChannelNeeded(chName) {

            var isNeeded = false;

            for (var i = 0; i < $scope.channelList.length; i++) {
                if ($scope.channelList[i].name == chName) {
                    if ($scope.channelList[i].toShow) {
                        isNeeded = true;
                    }
                    break;
                }
            }

            return isNeeded;
        }

        function getColumnValue(chName) {
            for (var i = 0; i < $scope.channelList.length; i++) {
                if ($scope.channelList[i].name == chName) {
                    return $scope.channelList[i].id < 4 ? 1 : 2;
                }
            }
        }

        $scope.zoomIn = function() {

            var chart = $('#container').highcharts();

            if ($scope.zoomLimitTracker < 3) {

                if ($scope.selectedInterval == $scope.Intervals[2]) { //minutes

                    if (ChannelHelperService.getDifferenceMinutes($scope.startDateTime, $scope.endDateTime) >= 5) {

                        $scope.startDateTime = ChannelHelperService.addMin($scope.startDateTime, 1);

                        $scope.endDateTime = ChannelHelperService.addMin($scope.endDateTime, -1);

                        $scope.renderChart();
                    }
                } else if ($scope.selectedInterval == $scope.Intervals[0]) { //hours

                    if (ChannelHelperService.getDifferenceHours($scope.startDateTime, $scope.endDateTime)) {

                        $scope.startDateTime = ChannelHelperService.addHours($scope.startDateTime, 1);

                        $scope.endDateTime = ChannelHelperService.addHours($scope.endDateTime, -1);

                        $scope.renderChart();
                    }
                } else if ($scope.selectedInterval == $scope.Intervals[1]) { //Days

                    if (ChannelHelperService.getDifferenceDays($scope.startDateTime, $scope.endDateTime)) {

                        $scope.startDateTime = ChannelHelperService.addDays($scope.startDateTime, 1);

                        $scope.endDateTime = ChannelHelperService.addDays($scope.endDateTime, -1);

                        $scope.renderChart();
                    }
                }

                $scope.zoomLimitTracker++;

            } else {

                $scope.showAlert = true;
                $scope.zoomInvalid = true;
                $timeout(closeAlert, 5000);
            }
        };

        $scope.zoomOut = function() {

            var chart = $('#container').highcharts();

            if ($scope.zoomLimitTracker >= -3) {

                if ($scope.selectedInterval == $scope.Intervals[2]) { //minutes

                    if (ChannelHelperService.getDifferenceMinutes($scope.startDateTime, $scope.endDateTime) >= 5) {

                        $scope.startDateTime = ChannelHelperService.addMin($scope.startDateTime, -1);

                        $scope.endDateTime = ChannelHelperService.addMin($scope.endDateTime, 1);

                        $scope.renderChart();

                    }
                } else if ($scope.selectedInterval == $scope.Intervals[0]) { //hours

                    if (ChannelHelperService.getDifferenceHours($scope.startDateTime, $scope.endDateTime)) {

                        $scope.startDateTime = ChannelHelperService.addHours($scope.startDateTime, -1);

                        $scope.endDateTime = ChannelHelperService.addHours($scope.endDateTime, 1);

                        $scope.renderChart();
                    }
                } else if ($scope.selectedInterval == $scope.Intervals[1]) { //Days

                    if (ChannelHelperService.getDifferenceDays($scope.startDateTime, $scope.endDateTime)) {

                        $scope.startDateTime = ChannelHelperService.addDays($scope.startDateTime, -1);

                        $scope.endDateTime = ChannelHelperService.addDays($scope.endDateTime, 1);

                        $scope.renderChart();
                    }
                }

                $scope.zoomLimitTracker--;

            } else {

                $scope.showAlert = true;
                $scope.zoomInvalid = true;
                $timeout(closeAlert, 5000);
            }
        };

        function setCategories(categories) {

            if ($scope.selectedInterval == $scope.Intervals[2]) { //minutes

                return ChannelHelperService.categoriesForMinutes(categories);

            } else if ($scope.selectedInterval == $scope.Intervals[0]) { //hours

                return ChannelHelperService.categoriesForHours(categories);

            } else if ($scope.selectedInterval == $scope.Intervals[1]) { //Days

                return ChannelHelperService.categoriesForDays(categories);

            }
        }

        $scope.submitForm = function(isValid) {

            $scope.zoomLimitTracker = 0;

            if ($scope.selectedInterval == $scope.Intervals[2]) {
                if (isValid && (ChannelHelperService.getDifferenceHours($scope.startDateTime, $scope.endDateTime) <= 24)) {
                    $scope.showAlert = false;
                    $scope.minutesInvalid = false;
                    $scope.hoursInvalid = true;
                    $scope.renderChart();
                } else {
                    $scope.showAlert = true;
                    $scope.minutesInvalid = true;
                    $scope.hoursInvalid = false;
                    $timeout(closeAlert, 5000);
                }
            } else if ($scope.selectedInterval == $scope.Intervals[0]) {
                if (isValid && (ChannelHelperService.getDifferenceDays($scope.startDateTime, $scope.endDateTime) <= 5)) {
                    $scope.showAlert = false;
                    $scope.hoursInvalid = false;
                    $scope.minutesInvalid = true;
                    $scope.renderChart();
                } else {
                    $scope.showAlert = true;
                    $scope.minutesInvalid = false;
                    $scope.hoursInvalid = true;
                    $timeout(closeAlert, 5000);
                }
            } else {
                // alert("Days");
                if (isValid) {
                    $scope.showAlert = false;
                    $scope.minutesInvalid = false;
                    $scope.hoursInvalid = true;
                    $scope.renderChart();
                } else {
                    $scope.showAlert = true;
                    $scope.minutesInvalid = true;
                    $scope.hoursInvalid = false;
                    $timeout(closeAlert, 5000);
                }
            }
        };

        //----------------------------------------------------------------


        var today = new Date('Sat Jan 02 2016 12:00:00 GMT+0530 (IST)');

        var hourago = new Date(today.getTime() - (1000 * 60 * 60));

        $scope.startDateTime = new Date('Sat Jan 02 2016 11:00:00 GMT+0530 (IST)');;

        $scope.endDateTime = new Date('Sat Jan 02 2016 11:30:00 GMT+0530 (IST)');

        $scope.formValid = true;

        $scope.showAlert = false;

        $scope.hoursInvalid = false;

        $scope.minutesInvalid = false;

        $scope.zoomInvalid = false;

        function closeAlert() {
            $scope.showAlert = false;
        }

        $scope.allSelected = false;

        $scope.cbChecked = function() {

            $scope.allSelected = true;

            angular.forEach($scope.channelList, function(v, k) {
                if (!v.toShow) {
                    $scope.allSelected = false;
                }
            });

            $scope.renderChart();
        }

        $scope.toggleAll = function() {

            var bool = true;

            if ($scope.allSelected) {
                bool = false;
            }

            angular.forEach($scope.channelList, function(v, k) {
                v.toShow = !bool;
                $scope.allSelected = !bool;
            });

            $scope.renderChart();
        }

        $('#downloadChart').click(function() {
            var chart = $('#container').highcharts();
            chart.exportChart();
        });

        // $scope.downloadChart = function() {

        //     console.log("Hello");
        //     $('#downloadChart').click(function() {
        //         var chart = $('#container').highcharts();
        //         chart.exportChart();
        //     });
        // }

        ///////////////////////Date Time Settings.

        $scope.dateTimeNow = function() {
            $scope.date = new Date();
        };

        $scope.dateTimeNow();

        // $scope.toggleMinDate = function() {
        //     var minDate = new Date();
        //     // set to yesterday
        //     minDate.setDate(minDate.getDate() - 1);
        //     $scope.dateOptions.minDate = $scope.dateOptions.minDate ? null : minDate;
        // };

        $scope.dateOptions = {
            showWeeks: false,
            startingDay: 0
        };

        // $scope.toggleMinDate();

        $scope.open = function($event, opened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.dateOpened = true;
        };

        $scope.dateOpened = false;
        $scope.hourStep = 1;
        $scope.format = "dd-MMM-yyyy";
        $scope.minuteStep = 15;

        $scope.timeOptions = {
            hourStep: [1, 2, 3],
            minuteStep: [1, 5, 10, 15, 25, 30]
        };

        $scope.showMeridian = true;
        $scope.timeToggleMode = function() {
            $scope.showMeridian = !$scope.showMeridian;
        };

        $scope.resetHours = function() {
            $scope.date.setHours(1);
        };

        $scope.renderChart();

        // var unsubscribe = $rootScope.$on('polsoftApp:AlarmTemplateCreated', function(event, result) {
        //     console.log("Here");
        //     console.log(result);
        // });

        // $scope.$on('$destroy', unsubscribe);



    }
})();