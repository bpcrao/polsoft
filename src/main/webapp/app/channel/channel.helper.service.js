(function() {
	'use strict';

	angular.module('polsoftApp').factory('ChannelHelperService', ChannelHelperService);

	ChannelHelperService.$inject = [ '$q', '$http' ];

	function ChannelHelperService($q, $http) {

		var dataPromise;

		// Interface
		var service = {
			getChannelData : getChannelData,
			doubleMinutes : doubleMinutes,
			doubleHours : doubleHours,
			doubleDates : doubleDates,
			addMin : addMin,
			addHours : addHours,
			addDays : addDays,
			getDifferenceMinutes : getDifferenceMinutes,
			getDifferenceHours : getDifferenceHours,
			getDifferenceDays : getDifferenceDays,
			categoriesForMinutes : categoriesForMinutes,
			categoriesForHours : categoriesForHours,
			categoriesForDays : categoriesForDays
		};

		var timeMinConfigForZoom = 5;

		var timeHourConfigForZoom = 2;

		var timeDayConfigForZoom = 2;

		// Implementation
		function getChannelData(channelList, startDateTime, endDateTime,
				selectedInterval) {
		}

		function doubleHours(startDateTime, endDateTime) {

			var differenceHours = getDifferenceHours(startDateTime, endDateTime) / 2;

			var newStartTime = new Date(startDateTime.getTime()
					- (1000 * 60 * 60 * differenceHours));

			var newEndDateTime = new Date(endDateTime.getTime()
					+ (1000 * 60 * 60 * differenceHours));

			return {
				startDateTime : startDateTime,
				endDateTime : endDateTime,
				pseudoStartDateTime : newStartTime,
				pseudoEndDateTime : newEndDateTime,
				getDifferenceMinutes : getDifferenceMinutes,
				getDifferenceHours : getDifferenceHours,
				getDifferenceDays : getDifferenceDays
			};
		}

		function doubleDates(startDateTime, endDateTime) {

			var differenceDays = getDifferenceDays(startDateTime, endDateTime) / 2;

			var newStartTime = new Date(startDateTime.getTime()
					- (1000 * 60 * 60 * 24 * differenceDays));

			var newEndDateTime = new Date(endDateTime.getTime()
					+ (1000 * 60 * 60 * 24 * differenceDays));

			return {
				startDateTime : startDateTime,
				endDateTime : endDateTime,
				pseudoStartDateTime : newStartTime,
				pseudoEndDateTime : newEndDateTime
			};
		}

		function doubleMinutes(startDateTime, endDateTime) {

			var differenceMinutes = getDifferenceMinutes(startDateTime,
					endDateTime) / 2;

			var newStartTime = new Date(startDateTime.getTime()
					- (1000 * 60 * differenceMinutes));

			var newEndDateTime = new Date(endDateTime.getTime()
					+ (1000 * 60 * differenceMinutes));

			return {
				startDateTime : startDateTime,
				endDateTime : endDateTime,
				pseudoStartDateTime : newStartTime,
				pseudoEndDateTime : newEndDateTime
			};
		}

		function addMin(timeString, operation, timeVal) {

			var tm = new Date(timeString);

			var quantity = timeMinConfigForZoom * operation;

			tm.setMinutes(tm.getMinutes() - quantity);

			return tm;
		}

		function addHours(timeString, operation, timeVal) {

			var tm = new Date(timeString);

			var quantity = timeHourConfigForZoom * operation;

			tm.setHours(tm.getHours() - quantity);

			return tm;
		}

		function addDays(timeString, operation, timeVal) {

			var tm = new Date(timeString);

			var quantity = timeDayConfigForZoom * operation;

			tm.setDate(tm.getDate() - quantity);

			return tm;
		}

		function getDifferenceHours(first, second) {
			var hours = Math.abs(first - second) / 36e5;
			return hours;
		}

		function getDifferenceDays(first, second) {

			return Math.round((second - first) / (1000 * 60 * 60 * 24));
		}

		function getDifferenceMinutes(first, second) {

			var difference = second.getTime() - first.getTime();

			return Math.round(difference / 60000);
		}

		function dateFormattedForMinutes(date) {

			var dt = new Date(date);

			var dateString = dt.getHours() + ':' + dt.getMinutes();

			return dateString;
		}

		function dateFormattedForHours(date) {

			var dt = new Date(date);

			var dateString = dt.getHours();

			return dateString;
		}

		function dateFormattedForDays(date) {

			var dt = new Date(date);

			var dateString = dt.getDate() + '/' + dt.getMonth() + '/'
					+ dt.getFullYear();

			return dateString;
		}

		function categoriesForMinutes(categories) {

			var formattedDates = [];

			for (var i = 0; i < categories.length; i++) {
				formattedDates.push(dateFormattedForMinutes(categories[i]));
			}

			return formattedDates;
		}

		function categoriesForHours(categories) {

			var formattedDates = [];

			for (var i = 0; i < categories.length; i++) {
				formattedDates.push(dateFormattedForHours(categories[i]));
			}

			return formattedDates;
		}

		function categoriesForDays(categories) {

			var formattedDates = [];

			for (var i = 0; i < categories.length; i++) {
				formattedDates.push(dateFormattedForDays(categories[i]));
			}

			return formattedDates;
		}

		return service;
	}
})();
