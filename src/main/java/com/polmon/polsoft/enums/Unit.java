package com.polmon.polsoft.enums;

public enum Unit {
	Temperature_DegC(1,"Temperature DegC"),
	Temperature_DegF(2,"Temperature DegF"),
	Humidity(3,"Humidity %RH"),
	Speed_RPM(4,"Speed rpm"),
	CURRENT_A(5,"Current A"),
	POWER_CONSUMPTION_KW(6,"Power Consumption kW"),
	PRESSURE_MMWC(7,"Pressure mmWc"),
	PRESSURE_MMHG(8,"Pressure mmHg"),
	PRESSURE_KPA(9,"Pressure kPa"),
	PRESSURE_PA(10,"Pressure Pa"),
	PRESSURE_CM_KG2(11,"Pressure Kg/cm2"),
	PRESSURE_PSI(12,"Pressure psi"),
	PRESSURE_BAR(13,"Pressure bar"),
	PRESSURE_AT(14,"Pressure at"),
	PRESSURE_ATM(15,"Pressure atm"),
	PRESSURE_TORR(16,"Pressure Torr"),
	DIFFERENCIAL_PRESSURE_MMWC(17,"Differential Pressure mmWc"),
	DIFFERENCIAL_PRESSURE_MMHG(18,"Differential Pressure mmHg"),
	DIFFERENCIAL_PRESSURE_KPA(19,"Differential Pressure kPa"),
	DIFFERENCIAL_PRESSURE_PA(20,"Differential Pressure Pa"),
	DIFFERENCIAL_PRESSURE_KGCM2(21,"Differential Pressure Kg/cm2"),
	DIFFERENCIAL_PRESSURE_PSI(22,"Differential Pressure psi"),
	DIFFERENCIAL_PRESSURE_BAR(23,"Differential Pressure bar"),
	DIFFERENCIAL_PRESSURE_AT(24,"Differential Pressure at"),
	DIFFERENCIAL_PRESSURE_ATM(25,"Differential Pressure atm"),
	DIFFERENCIAL_PRESSURE_TORR(26,"Differential Pressure Torr");
	
	private int value;
	private String name;
	Unit(int value,String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
