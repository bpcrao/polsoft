package com.polmon.polsoft.config;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.acls.AclPermissionCacheOptimizer;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.security.acls.afterinvocation.AclEntryAfterInvocationCollectionFilteringProvider;
import org.springframework.security.acls.afterinvocation.AclEntryAfterInvocationProvider;
import org.springframework.security.acls.domain.AclAuthorizationStrategy;
import org.springframework.security.acls.domain.AclAuthorizationStrategyImpl;
import org.springframework.security.acls.domain.ConsoleAuditLogger;
import org.springframework.security.acls.domain.DefaultPermissionFactory;
import org.springframework.security.acls.domain.DefaultPermissionGrantingStrategy;
import org.springframework.security.acls.domain.EhCacheBasedAclCache;
import org.springframework.security.acls.jdbc.BasicLookupStrategy;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.jdbc.LookupStrategy;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.PermissionGrantingStrategy;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.polmon.polsoft.permissions.PolsoftPermission;


@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@AutoConfigureAfter(value = { CacheConfiguration.class, DatabaseConfiguration.class })
public class ACLConfig {

@Autowired
public DataSource dataSource;

@Autowired
public EhCacheCacheManager cacheManager;

@Bean
EhCacheBasedAclCache aclCache() {
	net.sf.ehcache.Cache aclcache  = cacheManager.getCacheManager().getCache("aclCache");
    return new EhCacheBasedAclCache(aclcache, permissionGrantingStrategy(), aclAuthorizationStrategy());
}

@Bean
AclAuthorizationStrategy aclAuthorizationStrategy() {
    return new AclAuthorizationStrategyImpl(new SimpleGrantedAuthority("ROLE_ACL_ADMIN"));
}

@Bean
PermissionGrantingStrategy permissionGrantingStrategy(){
    return new DefaultPermissionGrantingStrategy(new ConsoleAuditLogger());
}

@Bean
LookupStrategy lookupStrategy() {
	BasicLookupStrategy lookupStrategy = new BasicLookupStrategy(dataSource, aclCache(), aclAuthorizationStrategy(), permissionGrantingStrategy());
    DefaultPermissionFactory defaultFactory = new DefaultPermissionFactory(PolsoftPermission.class);
    lookupStrategy.setPermissionFactory(defaultFactory);
	//aclPermissionEvaluator.setPermissionFactory(defaultFactory);
	return lookupStrategy;
}

@Bean
JdbcMutableAclService aclService() {
    JdbcMutableAclService service = new JdbcMutableAclService(dataSource, lookupStrategy(), aclCache());
    //service.setClassIdentityQuery("select currval(pg_get_serial_sequence('acl_class', 'id'))");
    //service.setSidIdentityQuery("select currval(pg_get_serial_sequence('acl_sid', 'id'))");
	
	//new AclEntryAfterInvocationCollectionFilteringProvider(service,requirePermission);
    return service;
}

@Bean
PermissionEvaluator permissionEvaluator(){
	AclPermissionEvaluator aclPermissionEvaluator =  new AclPermissionEvaluator(aclService());
	DefaultPermissionFactory defaultFactory = new DefaultPermissionFactory(PolsoftPermission.class);
	aclPermissionEvaluator.setPermissionFactory(defaultFactory);
	return aclPermissionEvaluator;
}

@Bean
AclEntryAfterInvocationCollectionFilteringProvider afterAclCollectionRead(){
    List<Permission> requirePermission = new ArrayList();
	requirePermission.add(PolsoftPermission.READ);
	AclEntryAfterInvocationCollectionFilteringProvider  aclEntryAfterInvocationProvider = new AclEntryAfterInvocationCollectionFilteringProvider(aclService(),requirePermission);
	return aclEntryAfterInvocationProvider;
}


@Bean
AclPermissionCacheOptimizer aclPermissionCacheOptimizer(){
    return new AclPermissionCacheOptimizer(aclService());
}

@Bean
protected MethodSecurityExpressionHandler expressionHandler(){
    DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();

    expressionHandler.setPermissionEvaluator(permissionEvaluator());
    expressionHandler.setPermissionCacheOptimizer(aclPermissionCacheOptimizer());

    return expressionHandler;
}
}