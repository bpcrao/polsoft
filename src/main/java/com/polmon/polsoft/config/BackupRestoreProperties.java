package com.polmon.polsoft.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
@ConfigurationProperties(prefix = "backuprestore", ignoreUnknownFields = false)
public class BackupRestoreProperties {

    private String backupDir;

    private String postgresbackupname;

    public String getBackupDir(){
        return this.backupDir;
    }

    public void setBackupDir(String backupDir){
        this.backupDir = backupDir;
    }

    public void setPostgresbackupname(String postgresbackupname){
        this.postgresbackupname = postgresbackupname;
    }

     public String getPostgresbackupname(){
        return this.postgresbackupname;
    }
}
