package com.polmon.polsoft.config.audit;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashSet;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.polmon.polsoft.domain.AbstractAuditingEntity;
import com.polmon.polsoft.domain.EntityConfig;
import com.polmon.polsoft.domain.PersistentAuditEvent;
import com.polmon.polsoft.repository.EntityConfigRepository;
import com.polmon.polsoft.repository.PersistenceAuditEventRepository;
import com.polmon.polsoft.security.SecurityUtils;

/**
 * Async Entity Audit Event writer This is invoked by Hibernate entity listeners
 * to write audit event for entitities
 */
@Component
public class AsyncEntityAuditEventWriter {

	private final Logger log = LoggerFactory.getLogger(AsyncEntityAuditEventWriter.class);

	@Inject
	private PersistenceAuditEventRepository persistenceAuditEventRepository;

	@Inject
	private ObjectMapper objectMapper; // Jackson object mapper

	private static HashSet<String> filteredClasses = new HashSet<String>();

	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	@Autowired
	private EntityConfigRepository entityConfigRepository;
	
	@Autowired
	private Environment environment;
	
	static {
		filteredClasses.add("DChannel");
	}

	/**
	 * Writes audit events to DB asynchronously in a new thread
	 */
	@Async
	public void writeAuditEvent(Object target, EntityAuditAction action) {
		if(!SecurityUtils.isRestoredApplication(environment))
		{
			log.debug("-------------- Post {} audit  --------------", action.value());
			try {
				this.add(target, action);
			} catch (Exception e) {
				log.error("Exception while persisting audit entity for {} error: {}", target, e);
			}
		}else {
			log.debug("audit skipped");
		}
	}

	public void add(Object target, EntityAuditAction create) {
		Class<?> entityClass = target.getClass(); // Retrieve entity class with
		if (filteredClasses.contains(entityClass.getSimpleName())) {
			return;
		}

		PersistentAuditEvent auditEntity = prepareCustomAuditEntity(target, create);
		EntityConfig entityConfig = entityConfigRepository.findByEntityName(entityClass.getSimpleName());
		if (auditEntity != null &&  entityConfig!=null &&  entityConfig.isEnableAudit()) {
			persistenceAuditEventRepository.save(auditEntity);
			messagingTemplate.convertAndSend("/topic/audits", auditEntity);
		}
	}

	private PersistentAuditEvent prepareCustomAuditEntity(final Object entity, EntityAuditAction action) {
		Class<?> entityClass = entity.getClass(); // Retrieve entity class with
													// reflection
		PersistentAuditEvent auditedEntity = new PersistentAuditEvent();
		auditedEntity.setAuditEventType(entityClass.getSimpleName());
		auditedEntity.setAuditEventSource(action.value());

		Long entityId;
		String entityData;
		try {
			Field privateLongField = entityClass.getDeclaredField("id");
			privateLongField.setAccessible(true);
			entityId = (Long) privateLongField.get(entity);
			privateLongField.setAccessible(false);
			entityData = objectMapper.writeValueAsString(entity);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException
				| IOException e) {
			// returning null as we dont want to raise an application exception
			// here
			return null;
		}
		final AbstractAuditingEntity abstractAuditEntity = (AbstractAuditingEntity) entity;
		auditedEntity.setEntityId(entityId);
		auditedEntity.setEntityValue(entityData);
		if (EntityAuditAction.CREATE.equals(action)) {
			auditedEntity.setPrincipal(abstractAuditEntity.getCreatedBy());
			auditedEntity.setAuditEventDate(abstractAuditEntity.getCreatedDate().toLocalDateTime());
			auditedEntity.setCommitVersion(1);
		} else {
			auditedEntity.setPrincipal(abstractAuditEntity.getLastModifiedBy());
			auditedEntity.setAuditEventDate(abstractAuditEntity.getLastModifiedDate().toLocalDateTime());
			calculateVersion(auditedEntity);
			
		}

		return auditedEntity;
	}

	 private void calculateVersion(PersistentAuditEvent auditedEntity) {
	 log.trace("Version calculation. for update/remove");
	 Integer lastCommitVersion =
			 persistenceAuditEventRepository.findMaxCommitVersion(auditedEntity
	 .getAuditEventType(), auditedEntity.getEntityId());
	 log.trace("Last commit version of entity => {}", lastCommitVersion);
	 if(lastCommitVersion!=null && lastCommitVersion != 0){
	 log.trace("Present. Adding version..");
	 auditedEntity.setCommitVersion(lastCommitVersion + 1);
	 } else {
	 log.trace("No entities.. Adding new version 1");
	 auditedEntity.setCommitVersion(1);
	 }
	 }
}
