package com.polmon.polsoft.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "polsoft", ignoreUnknownFields = false)
public class PolSoftNewInstance {
	private PolSoftService service = new PolSoftService();
	
	private InFluxDBService influx = new InFluxDBService();
		
	public PolSoftService getService() {
		return service;
	}

	public void setService(PolSoftService service) {
		this.service = service;
	}

	public InFluxDBService getInflux() {
		return influx;
	}

	public void setInflux(InFluxDBService influx) {
		this.influx = influx;
	}
	
	public static class PolSoftService {
		private String path;

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}
		
	}
	public static class InFluxDBService {
		private String verifyPath;
		
		private String home;
		
		private String backupDir;
		
		private String path;
		
		private String verifyHome;
		
		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getHome() {
			return home;
		}

		public void setHome(String home) {
			this.home = home;
		}

		public String getBackupDir() {
			return backupDir;
		}

		public void setBackupDir(String backupDir) {
			this.backupDir = backupDir;
		}

		public String getVerifyPath() {
			return verifyPath;
		}

		public void setVerifyPath(String verifyPath) {
			this.verifyPath = verifyPath;
		}

		public String getVerifyHome() {
			return verifyHome;
		}

		public void setVerifyHome(String verifyHome) {
			this.verifyHome = verifyHome;
		}		
	}
}
