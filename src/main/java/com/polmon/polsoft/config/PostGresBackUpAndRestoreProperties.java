package com.polmon.polsoft.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
@ConfigurationProperties(prefix = "postgres", ignoreUnknownFields = false)
public class PostGresBackUpAndRestoreProperties {
	private Dump dump =new Dump();
	private Restore restore = new Restore();
	private String outfile;
	
	public Dump getDump() {
		return dump;
	}
	public void setDump(Dump dump) {
		this.dump = dump;
	}
	public Restore getRestore() {
		return restore;
	}
	public void setRestore(Restore restore) {
		this.restore = restore;
	}
	public String getOutfile() {
		return outfile;
	}
	public void setOutfile(String outfile) {
		this.outfile = outfile;
	}
	public static class Dump {
		private String exepath;
		private String host;
		private String port;
		private String username;
		private String password;
		private String database;

		public String getExepath() {
			return exepath;
		}

		public void setExepath(String exepath) {
			this.exepath = exepath;
		}

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public String getPort() {
			return port;
		}

		public void setPort(String port) {
			this.port = port;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getDatabase() {
			return database;
		}

		public void setDatabase(String database) {
			this.database = database;
		}
		
	}
	public static class Restore {
		private String exepath;
		private String host;
		private String port;
		private String username;
		private String password;
		private String database;

		public String getExepath() {
			return exepath;
		}

		public void setExepath(String exepath) {
			this.exepath = exepath;
		}

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public String getPort() {
			return port;
		}

		public void setPort(String port) {
			this.port = port;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getDatabase() {
			return database;
		}

		public void setDatabase(String database) {
			this.database = database;
		}
	}
}
