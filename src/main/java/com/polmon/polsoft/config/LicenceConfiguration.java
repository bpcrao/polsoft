package com.polmon.polsoft.config;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import com.google.gson.Gson;
import com.polmon.polsoft.domain.Licence;

@Configuration
public class LicenceConfiguration {
	@Autowired
	private ApplicationContext applicationContext;
	@Bean
	public Licence getLicence() throws IOException
	{
		Resource resource = applicationContext.getResource("classpath:polsoft.licence");
		InputStream inputStream = resource.getInputStream();
		StandardPBEStringEncryptor encryptor = applicationContext.getBean(StandardPBEStringEncryptor.class);
		String decryptedJson = encryptor.decrypt(IOUtils.toString(inputStream, Charset.forName("UTF-8")));
		Gson gson = new Gson();
		Licence licence = gson.fromJson(decryptedJson, Licence.class);
		return licence;
	}
}
