package com.polmon.polsoft;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class LicenceGenerator {
	
	//THIs code will be moved to separate class.
	/*public static void main(String[] args) throws Exception{
		getMacAddress();
		generateLicence();
	}*/

	private static void getMacAddress() throws Exception{
		   String command = "ipconfig /all";
	       Process p = Runtime.getRuntime().exec(command);

	       BufferedReader inn = new BufferedReader(new InputStreamReader(p.getInputStream()));
	       Pattern pattern = Pattern.compile(".*Physical Addres.*: (.*)");

	       while (true) {
	            String line = inn.readLine();

		    if (line == null)
		        break;

		    Matcher mm = pattern.matcher(line);
		    if (mm.matches()) {
		        System.out.println(mm.group(1));
		    }
		}
	}

	private static void generateLicence() throws IOException, FileNotFoundException {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setAlgorithm("PBEWithMD5AndTripleDES");
		encryptor.setPassword("jasypt");
		String encrptedString = encryptor.encrypt(IOUtils.toString(new FileInputStream(new File("licenceGenerator.json")),Charset.defaultCharset()));
		System.out.println(encrptedString);
	}
}
