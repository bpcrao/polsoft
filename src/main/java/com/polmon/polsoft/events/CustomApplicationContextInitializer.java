package com.polmon.polsoft.events;

import java.net.InetAddress;
import java.net.NetworkInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.polmon.polsoft.domain.Licence;
@Component
public class CustomApplicationContextInitializer implements ApplicationListener<ApplicationReadyEvent> {
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		try {
			Licence licence = event.getApplicationContext().getBean(Licence.class);
			String macAddress = getMacAdress();
			if(!licence.getMacAddresses().contains(macAddress))
			{
				System.out.println("mac not found");
				eventPublisher.publishEvent(new ApplicationFailedEvent(event.getSpringApplication(), event.getArgs(), event.getApplicationContext(), new Exception("Invalid Licence information")));
				System.exit(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getMacAdress() throws Exception{
		InetAddress ip = InetAddress.getLocalHost();
		NetworkInterface network = NetworkInterface.getByInetAddress(ip);
		byte[] mac = network.getHardwareAddress();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length; i++) {
			sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
		}
		return sb.toString();
	}
}
