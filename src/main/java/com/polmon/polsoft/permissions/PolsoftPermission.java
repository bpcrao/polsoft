package com.polmon.polsoft.permissions;

import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;

public class PolsoftPermission extends BasePermission {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -830601105305941544L;
	
//	public static final Permission READ = new BasePermission(1 << 0, 'R'); // 1
//	public static final Permission WRITE = new BasePermission(1 << 1, 'W'); // 2
//	public static final Permission CREATE = new BasePermission(1 << 2, 'C'); // 4
//	public static final Permission DELETE = new BasePermission(1 << 3, 'D'); // 8
//	public static final Permission ADMINISTRATION = new BasePermission(1 << 4, 'A'); // 16
	public static final Permission MANAGE_ALARM = new PolsoftPermission(1 << 5, 'X'); // 32
	public static final Permission APPLY_THRESHOLD = new PolsoftPermission(1 << 6, 'Y'); // 64	
	public static final Permission SHOW_LOCATION = new PolsoftPermission(1 << 7, 'Z'); // 128

	public PolsoftPermission(int mask) {
		super(mask);
	}

	protected PolsoftPermission(int mask, char code) {
		super(mask, code);
	}
}
