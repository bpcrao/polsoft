package com.polmon.polsoft.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.repository.search.DchannelSearchRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.DchannelDTO;
import com.polmon.polsoft.service.mapper.DchannelMapper;
import com.polmon.polsoft.service.mapper.DchannelSummaryMapper;

/**
 * Service Implementation for managing Dchannel.
 */
@Service
@Transactional
public class DchannelService {

    private final Logger log = LoggerFactory.getLogger(DchannelService.class);
    
    @Inject
    private AuditEventPublisher auditPublisher;
    
    @Inject
    private DchannelRepository dchannelRepository;

    @Inject
    private DchannelMapper dchannelMapper;
    
    @Inject
    private DchannelSummaryMapper dchannelSummaryMapper;

    @Inject
    private DchannelSearchRepository dchannelSearchRepository;

    /**
     * Save a dchannel.
     *
     * @param dchannelDTO the entity to save
     * @return the persisted entity
     */
    public DchannelDTO save(DchannelDTO dchannelDTO) {
        log.debug("Request to save Dchannel : {}", dchannelDTO);
        Dchannel dchannel = dchannelMapper.dchannelDTOToDchannel(dchannelDTO);
        dchannel = dchannelRepository.save(dchannel);
        DchannelDTO result = dchannelMapper.dchannelToDchannelDTO(dchannel);
        dchannelSearchRepository.save(dchannel);
        auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "New_Channel Is Added"));
        return result;
    }

    /**
     *  Get all the dchannels.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<DchannelDTO> findAll() {
        log.debug("Request to get all Dchannels");
        List<DchannelDTO> result = dchannelRepository.findAll().stream()
            .map(dchannelMapper::dchannelToDchannelDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }


    /**
     *  get all the dchannels where Location is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<DchannelDTO> findAllWhereLocationIsNull() {
        log.debug("Request to get all dchannels where Location is null");
        return StreamSupport
            .stream(dchannelRepository.findAll().spliterator(), false)
            .filter(dchannel -> dchannel.getLocation() == null)
            .map(dchannelMapper::dchannelToDchannelDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    @Transactional(readOnly = true) 
	public List<DchannelDTO> findAllByDevice(Device device, Boolean detailed) {    	
    	if (detailed) {
    		return StreamSupport
                    .stream(dchannelRepository.findAllByDevice(device).spliterator(), false)
                    .map(dchannelMapper::dchannelToDchannelDTO)
                    .collect(Collectors.toCollection(LinkedList::new));
    	}
    	return StreamSupport
                .stream(dchannelRepository.findAllByDevice(device).spliterator(), false)
                .filter(dchannel -> dchannel.getLocation() == null)
                .map(dchannelSummaryMapper::dchannelToDchannelDTOSummary)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one dchannel by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DchannelDTO findOne(Long id) {
        log.debug("Request to get Dchannel : {}", id);
        Dchannel dchannel = dchannelRepository.findOne(id);
        DchannelDTO dchannelDTO = dchannelMapper.dchannelToDchannelDTO(dchannel);
        return dchannelDTO;
    }

    /**
     *  Delete the  dchannel by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Dchannel : {}", id);
        dchannelRepository.delete(id);
        dchannelSearchRepository.delete(id);
    }

    /**
     * Search for the dchannel corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DchannelDTO> search(String query) {
        log.debug("Request to search Dchannels for query {}", query);
        return StreamSupport
            .stream(dchannelSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(dchannelMapper::dchannelToDchannelDTO)
            .collect(Collectors.toList());
    }
}
