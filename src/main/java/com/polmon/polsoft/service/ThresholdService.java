package com.polmon.polsoft.service;

import com.polmon.polsoft.domain.Threshold;
import com.polmon.polsoft.repository.ThresholdRepository;
import com.polmon.polsoft.repository.search.ThresholdSearchRepository;
import com.polmon.polsoft.service.dto.ThresholdDTO;
import com.polmon.polsoft.service.mapper.ThresholdMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Threshold.
 */
@Service
@Transactional
public class ThresholdService {

    private final Logger log = LoggerFactory.getLogger(ThresholdService.class);
    
    @Inject
    private ThresholdRepository thresholdRepository;

    @Inject
    private ThresholdMapper thresholdMapper;

    @Inject
    private ThresholdSearchRepository thresholdSearchRepository;

    /**
     * Save a threshold.
     *
     * @param thresholdDTO the entity to save
     * @return the persisted entity
     */
    public ThresholdDTO save(ThresholdDTO thresholdDTO) {
        log.debug("Request to save Threshold : {}", thresholdDTO);
        Threshold threshold = thresholdMapper.thresholdDTOToThreshold(thresholdDTO);
        threshold = thresholdRepository.save(threshold);
        ThresholdDTO result = thresholdMapper.thresholdToThresholdDTO(threshold);
        thresholdSearchRepository.save(threshold);
        return result;
    }

    /**
     *  Get all the thresholds.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ThresholdDTO> findAll() {
        log.debug("Request to get all Thresholds");
        List<ThresholdDTO> result = thresholdRepository.findAll().stream()
            .map(thresholdMapper::thresholdToThresholdDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one threshold by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ThresholdDTO findOne(Long id) {
        log.debug("Request to get Threshold : {}", id);
        Threshold threshold = thresholdRepository.findOne(id);
        ThresholdDTO thresholdDTO = thresholdMapper.thresholdToThresholdDTO(threshold);
        return thresholdDTO;
    }

    /**
     *  Delete the  threshold by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Threshold : {}", id);
        thresholdRepository.delete(id);
        thresholdSearchRepository.delete(id);
    }

    /**
     * Search for the threshold corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ThresholdDTO> search(String query) {
        log.debug("Request to search Thresholds for query {}", query);
        return StreamSupport
            .stream(thresholdSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(thresholdMapper::thresholdToThresholdDTO)
            .collect(Collectors.toList());
    }
}
