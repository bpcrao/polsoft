package com.polmon.polsoft.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.EntityConfig;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.domain.PersistentAuditEvent;
import com.polmon.polsoft.permissions.PolsoftPermission;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.repository.EntityConfigRepository;
import com.polmon.polsoft.repository.LocationRepository;
import com.polmon.polsoft.repository.PersistenceAuditEventRepository;
import com.polmon.polsoft.repository.search.LocationSearchRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.AlarmsDTO;
import com.polmon.polsoft.service.dto.LocationDTO;
import com.polmon.polsoft.service.eventsender.DeviceEventSender;
import com.polmon.polsoft.service.mapper.LocationMapper;

/**
 * Service Implementation for managing Location.
 */
@Service
@Transactional
public class LocationService {

	private final Logger log = LoggerFactory.getLogger(LocationService.class);

	@Inject
	private LocationRepository locationRepository;

	@Inject
	private LocationMapper locationMapper;

	@Inject
	private LocationSearchRepository locationSearchRepository;

	@Inject
	private JdbcMutableAclService aclService;

	@Inject
	private AlarmsService alarmsService;

	@Autowired
	private PersistenceAuditEventRepository persistenceAuditEventRepository;

	@Autowired
	private EntityConfigRepository entityConfigRepository;

	@Inject
	private ObjectMapper objectMapper;

	@Inject
	DeviceEventSender deviceEventSender;

	@Inject
	DchannelRepository dchannelRepository;
	
	@Autowired
	private Environment environment;

	/**
	 * Save a location.
	 *
	 * @param locationDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public LocationDTO save(LocationDTO locationDTO) {
		log.debug("Request to save Location : {}", locationDTO);
		Location location = locationMapper.locationDTOToLocation(locationDTO);
		Location oldLocation = null;
		boolean linkOrUnlink = false;
		boolean oldLocationdata = false;
		Long deviceID = null;
		if (location.getId() != null) {
			EntityConfig entityConfig = entityConfigRepository.findByEntityName(Location.class.getSimpleName());
			if (entityConfig != null && entityConfig.isEnableAudit()) {
				oldLocation = locationRepository.findOne(location.getId());
				PersistentAuditEvent auditedEntity = new PersistentAuditEvent();
				try {
					auditedEntity.setEntityValue(objectMapper.writeValueAsString(location));
					auditedEntity.setPrincipal(SecurityContextHolder.getContext().getAuthentication().getName());
					auditedEntity.setAuditEventDate(LocalDateTime.now());
					auditedEntity.setCommitVersion(1);
					auditedEntity.setAuditEventType(Location.class.getSimpleName());
				} catch (JsonProcessingException e) {
					// skip
				}
			
				if(oldLocation != null){
				if (oldLocation.getDchannel() == null && location.getDchannel() != null) {
					Dchannel dchannel = dchannelRepository.findOne(location.getDchannel().getId());
					auditedEntity.setAuditEventSource("Channel Linked: " + dchannel.getName());
					linkOrUnlink = true;
					
				}
				if (oldLocation.getDchannel() != null && location.getDchannel() == null) {
					auditedEntity.setAuditEventSource("Channel UnLinked: " + oldLocation.getDchannel().getName());					
					linkOrUnlink = true;
					oldLocationdata = true;
					deviceID = oldLocation.getDchannel().getDevice().getId();
				}
				}
				if(!SecurityUtils.isRestoredApplication(environment))
				{
					persistenceAuditEventRepository.save(auditedEntity);
				}				
			}
		}
		if (location.getDchannel() != null) {
			location.getDchannel().setLocation(location);
			location.getDchannel().getDevice();

		}
		location = locationRepository.save(location);
		setPermissions(location);
		LocationDTO result = locationMapper.locationTLocationDTO(location);
		locationSearchRepository.save(location);
		result.setLinkUnlinkLocation(linkOrUnlink);
		if(oldLocationdata && deviceID != null)
			result.setOldLocationDeviceID(deviceID);
		return result;
	}

	private void setPermissions(Location location) {
		// --------------------
		ObjectIdentity oi = new ObjectIdentityImpl(Location.class, new Long(location.getId()));
		Sid sid = new PrincipalSid(SecurityUtils.getCurrentUserLogin()!=null?SecurityUtils.getCurrentUserLogin():"admin");
		// Create or update the relevant ACL
		MutableAcl acl = null;
		try {
			acl = (MutableAcl) aclService.readAclById(oi);
		} catch (NotFoundException nfe) {
			acl = aclService.createAcl(oi);
		}
		aclService.readAclById(oi);
		// ACL PARENT
		acl.insertAce(acl.getEntries().size(), PolsoftPermission.MANAGE_ALARM, sid, true);
		acl.insertAce(acl.getEntries().size(), PolsoftPermission.APPLY_THRESHOLD, sid, true);
		acl.insertAce(acl.getEntries().size(), PolsoftPermission.SHOW_LOCATION, sid, true);
		acl.insertAce(acl.getEntries().size(), PolsoftPermission.ADMINISTRATION, sid, true);

		if (location.getParent() != null && location.getParent().getId() != null) {
			Location parent = locationRepository.findOne(location.getParent().getId());
			parent.getChildren().add(location);
			locationRepository.save(parent);
			// ACL PARENT
			acl.setEntriesInheriting(true);
			List<Sid> sids = new ArrayList<Sid>();
			sids.add(sid);
			acl.setParent(aclService
					.readAclById(new ObjectIdentityImpl(Location.class, new Long(location.getParent().getId())), sids));
		}
		aclService.updateAcl(acl);
	}

	/**
	 * Get all the locations.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	@PostFilter("hasPermission(filterObject,'SHOW_LOCATION') or hasPermission(filterObject,'ADMINISTRATION')")
	public List<Location> findAll() {
		log.debug("Request to get all Locations");
		return locationRepository.findAll();
	}

	/**
	 * Get one location by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public LocationDTO findOne(Long id) {
		log.debug("Request to get Location : {}", id);
		Location location = locationRepository.findOne(id);
		LocationDTO locationDTO = locationMapper.locationTLocationDTO(location);
		return locationDTO;
	}

	/**
	 * Delete the location by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) throws Exception {
		// log.debug("Request to delete Location : {}", id);
		try {
			Location deleteLocation = locationRepository.getOne(id);
			if (deleteLocation.getHasData() != null && deleteLocation.getHasData()) {
				throw new IllegalStateException("cannot delete location with data");
			}
			deleteLocation.getParent().getChildren().remove(deleteLocation);
			locationRepository.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		locationSearchRepository.delete(id);
		log.debug("Request to delete Location : {}", id);
	}

	/**
	 * Search for the location corresponding to the query.
	 *
	 * @param query
	 *            the query of the search
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<LocationDTO> search(String query) {
		log.debug("Request to search Locations for query {}", query);
		return StreamSupport.stream(locationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
				.map(locationMapper::locationTLocationDTO).collect(Collectors.toList());
	}

	@PostFilter("hasPermission(#location,'SHOW_LOCATION')")
	private void recursivegetChildren(Location location) {
		log.info(location.getName());
		List<AlarmsDTO> alarmsActive = alarmsService.findAllByStatus(location.getDchannel(), "active");
		location.setAlarmsActive(alarmsActive);
		if (location.getChildren() != null) {
			log.info("-->");
			location.getChildren().stream().forEach(locationS -> recursivegetChildren(locationS));
		}
	}
 
	public LocationDTO findRootNode(Set<Location> accessibleLocations) {
		log.debug("Request to get Location Tree: ");
		Location location = locationRepository.findOneByParent(null);

		recursivegetChildren(location);
		LocationDTO locationDTO = locationMapper.locationTAccessibleLocationDTO(location, accessibleLocations);
		return locationDTO;
	}
}