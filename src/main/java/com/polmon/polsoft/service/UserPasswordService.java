package com.polmon.polsoft.service;

import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.UserPassword;
import com.polmon.polsoft.repository.UserPasswordRepository;
import com.polmon.polsoft.repository.search.UserPasswordSearchRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.UserPasswordDTO;
import com.polmon.polsoft.service.mapper.UserPasswordMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UserPassword.
 */
@Service
@Transactional
public class UserPasswordService {

    private final Logger log = LoggerFactory.getLogger(UserPasswordService.class);
    
    @Inject
    private UserPasswordRepository userPasswordRepository;

    @Inject
    private UserPasswordMapper userPasswordMapper;

    @Inject
    private UserPasswordSearchRepository userPasswordSearchRepository;

    /**
     * Save a userPassword.
     *
     * @param userPasswordDTO the entity to save
     * @return the persisted entity
     */
    public UserPasswordDTO save(UserPasswordDTO userPasswordDTO) {
        log.debug("Request to save UserPassword : {}", userPasswordDTO);
        UserPassword userPassword = userPasswordMapper.userPasswordDTOToUserPassword(userPasswordDTO);
        userPassword = userPasswordRepository.save(userPassword);
        UserPasswordDTO result = userPasswordMapper.userPasswordToUserPasswordDTO(userPassword);
        userPasswordSearchRepository.save(userPassword);
        return result;
    }

    /**
     *  Get all the userPasswords.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<UserPasswordDTO> findAll() {
        log.debug("Request to get all UserPasswords");
        List<UserPasswordDTO> result = userPasswordRepository.findAll().stream()
            .map(userPasswordMapper::userPasswordToUserPasswordDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one userPassword by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public UserPasswordDTO findOne(Long id) {
        log.debug("Request to get UserPassword : {}", id);
        UserPassword userPassword = userPasswordRepository.findOne(id);
        UserPasswordDTO userPasswordDTO = userPasswordMapper.userPasswordToUserPasswordDTO(userPassword);
        return userPasswordDTO;
    }

    /**
     *  Delete the  userPassword by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserPassword : {}", id);
        userPasswordRepository.delete(id);
        userPasswordSearchRepository.delete(id);
    }

    /**
     * Search for the userPassword corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<UserPasswordDTO> search(String query) {
        log.debug("Request to search UserPasswords for query {}", query);
        return StreamSupport
            .stream(userPasswordSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(userPasswordMapper::userPasswordToUserPasswordDTO)
            .collect(Collectors.toList());
    }
}
