package com.polmon.polsoft.service;

import com.polmon.polsoft.service.dto.EntityConfigDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing EntityConfig.
 */
public interface EntityConfigService {

    /**
     * Save a entityConfig.
     *
     * @param entityConfigDTO the entity to save
     * @return the persisted entity
     */
    EntityConfigDTO save(EntityConfigDTO entityConfigDTO);

    /**
     *  Get all the entityConfigs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<EntityConfigDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" entityConfig.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EntityConfigDTO findOne(Long id);

    /**
     *  Delete the "id" entityConfig.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the entityConfig corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<EntityConfigDTO> search(String query, Pageable pageable);
}
