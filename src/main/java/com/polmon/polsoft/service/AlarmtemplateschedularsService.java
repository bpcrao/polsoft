package com.polmon.polsoft.service;

import com.polmon.polsoft.service.dto.AlarmtemplateschedularsDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Alarmtemplateschedulars.
 */
public interface AlarmtemplateschedularsService {

    /**
     * Save a alarmtemplateschedulars.
     *
     * @param alarmtemplateschedularsDTO the entity to save
     * @return the persisted entity
     */
    AlarmtemplateschedularsDTO save(AlarmtemplateschedularsDTO alarmtemplateschedularsDTO);

    /**
     *  Get all the alarmtemplateschedulars.
     *  
     *  @return the list of entities
     */
    List<AlarmtemplateschedularsDTO> findAll();

    /**
     *  Get the "id" alarmtemplateschedulars.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AlarmtemplateschedularsDTO findOne(Long id);

    /**
     *  Delete the "id" alarmtemplateschedulars.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the alarmtemplateschedulars corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<AlarmtemplateschedularsDTO> search(String query);
}
