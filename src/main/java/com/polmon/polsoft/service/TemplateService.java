package com.polmon.polsoft.service;

import com.polmon.polsoft.domain.Template;
import com.polmon.polsoft.repository.TemplateRepository;
import com.polmon.polsoft.repository.search.TemplateSearchRepository;
import com.polmon.polsoft.service.dto.TemplateDTO;
import com.polmon.polsoft.service.mapper.TemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Template.
 */
@Service
@Transactional
public class TemplateService {

    private final Logger log = LoggerFactory.getLogger(TemplateService.class);
    
    @Inject
    private TemplateRepository templateRepository;

    @Inject
    private TemplateMapper templateMapper;

    @Inject
    private TemplateSearchRepository templateSearchRepository;

    /**
     * Save a template.
     *
     * @param templateDTO the entity to save
     * @return the persisted entity
     */
    public TemplateDTO save(TemplateDTO templateDTO) {
        log.debug("Request to save Template : {}", templateDTO);
        Template template = templateMapper.templateDTOToTemplate(templateDTO);
        template = templateRepository.save(template);
        TemplateDTO result = templateMapper.templateToTemplateDTO(template);
        templateSearchRepository.save(template);
        return result;
    }

    /**
     *  Get all the templates.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<TemplateDTO> findAll() {
        log.debug("Request to get all Templates");
        List<TemplateDTO> result = templateRepository.findAll().stream()
            .map(templateMapper::templateToTemplateDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one template by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public TemplateDTO findOne(Long id) {
        log.debug("Request to get Template : {}", id);
        Template template = templateRepository.findOne(id);
        TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(template);
        return templateDTO;
    }

    /**
     *  Delete the  template by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Template : {}", id);
        templateRepository.delete(id);
        templateSearchRepository.delete(id);
    }

    /**
     * Search for the template corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TemplateDTO> search(String query) {
        log.debug("Request to search Templates for query {}", query);
        return StreamSupport
            .stream(templateSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(templateMapper::templateToTemplateDTO)
            .collect(Collectors.toList());
    }
}
