package com.polmon.polsoft.service;

import com.polmon.polsoft.config.JHipsterProperties;
import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.constants.EmailConfiguration;
import com.polmon.polsoft.domain.Alarms;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.repository.SystempreferencesRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.AlarmsDTO;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;


import javax.inject.Inject;
import javax.mail.internet.MimeMessage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Service for sending e-mails.
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";
    private static final String BASE_URL = "baseUrl";

    @Inject
    private JHipsterProperties jHipsterProperties;
    
    @Inject
    private AuditEventPublisher auditPublisher;

    @Inject
    private JavaMailSenderImpl javaMailSender;

    @Inject
    private MessageSource messageSource;

    @Inject
    private SpringTemplateEngine templateEngine;
    
    @Autowired
    private SystempreferencesRepository systempreferencesRepository;

    @Async
    public void sendEmail(List<String> toEmailId, String subject, String content, boolean isMultipart, boolean isHtml,File... attachments) {
        log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, toEmailId, subject, content);
        setMailPropertiesFromDatabase();
        String[] toMailArray = new String[toEmailId.size()];
        toEmailId.toArray(toMailArray);
        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);            
            message.setTo(toMailArray);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject("Report Scheduler");
            message.setText("Your Scheduled report is ready.", isHtml);

            if(attachments!=null && attachments.length>0)
            {
            	for(int i=0;i<attachments.length;i++)
            	{
            		File file = attachments[i];
            		message.addAttachment(file.getName(), file);
            	}
            }
            try {
				javaMailSender.send(mimeMessage);
			} catch (Exception e) {
				javaMailSender.getJavaMailProperties().put("mail.smtp.auth", true);
				javaMailSender.getJavaMailProperties().put("mail.smtp.starttls.enable", true);
				javaMailSender.send(mimeMessage);
				e.printStackTrace();
			}
            log.debug("Sent e-mail to User '{}'", toEmailId);
            toEmailId.forEach(emailId ->{
            	auditPublisher.publish(new AuditEvent(jHipsterProperties.getMail().getFrom(), "EMAIL_SENT", emailId));
            });
            
        } catch (Exception e) {
            log.warn("E-mail could not be sent to user '{}', exception is: {}", toEmailId, e.getMessage());
        }
    }

    @Async
    public void sendActivationEmail(User user, String baseUrl) {
        log.debug("Sending activation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, baseUrl);
        String content = templateEngine.process("activationEmail", context);
        List<String> emailIds = new ArrayList<>();
        emailIds.add(user.getEmail());
        String subject = messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(emailIds, subject, content, false, true);
        auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "ACTIVATION_EMAIL_SENT", user.getLogin()));
    }

    @Async
    public void sendCreationEmail(User user, String baseUrl) {
        log.debug("Sending creation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, baseUrl);
        String content = templateEngine.process("creationEmail", context);
        String subject = messageSource.getMessage("email.activation.title", null, locale);
        List<String> emailIds = new ArrayList<>();
        emailIds.add(user.getEmail());
        sendEmail(emailIds, subject, content, false, true);
        auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "ACCOUNT_CREATED_EMAIL_SENT", user.getLogin()));
    }

    @Async
    public void sendPasswordResetMail(User user, String baseUrl) {
        log.debug("Sending password reset e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, baseUrl);
        String content = templateEngine.process("passwordResetEmail", context);
        String subject = messageSource.getMessage("email.reset.title", null, locale);
        List<String> emailIds = new ArrayList<>();
        emailIds.add(user.getEmail());
        sendEmail(emailIds, subject, content, false, true);
        auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "RESET_EMAIL_SENT", user.getLogin()));
    }
    
    private void setMailPropertiesFromDatabase()
    {
    	systempreferencesRepository.findAll().forEach(systempreferences->{
    		switch(systempreferences.getKeyproperty())
    		{
    		case EmailConfiguration.EMAIL_HOST:{
    			javaMailSender.setHost(systempreferences.getData());
    			break;
			  }
    		case EmailConfiguration.EMAIL_PORT:{
    			javaMailSender.setPort(Integer.parseInt(systempreferences.getData()));
    			break;
			  }
    		case EmailConfiguration.EMAIL_USER_NAME:{
    			javaMailSender.setUsername(systempreferences.getData());
    			break;
			  }
    		case EmailConfiguration.EMAIL_PASSWORD:{
    			javaMailSender.setPassword(systempreferences.getData());
    			break;
			  }
    		}
    	});;
    }
}
