package com.polmon.polsoft.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.domain.UserPermission;
import com.polmon.polsoft.repository.UserPermissionRepository;
import com.polmon.polsoft.repository.search.UserPermissionSearchRepository;
import com.polmon.polsoft.service.dto.UserPermissionDTO;
import com.polmon.polsoft.service.mapper.UserPermissionMapper;

/**
 * Service Implementation for managing UserPermission.
 */
@Service
@Transactional
public class UserPermissionService {

    private final Logger log = LoggerFactory.getLogger(UserPermissionService.class);

    @Inject
    private UserPermissionRepository userPermissionRepository;

    @Inject
    private UserPermissionMapper userPermissionMapper;

    @Inject
    private UserPermissionSearchRepository userPermissionSearchRepository;

    /**
     * Save a userPermission.
     *
     * @param userPermissionDTO the entity to save
     * @return the persisted entity
     */
    public UserPermissionDTO save(UserPermissionDTO userPermissionDTO) {
        log.debug("Request to save UserPermission : {}", userPermissionDTO);
        UserPermission userPermission = userPermissionMapper.userPermissionDTOToUserPermission(userPermissionDTO);
        userPermission = userPermissionRepository.save(userPermission);
        UserPermissionDTO result = userPermissionMapper.userPermissionToUserPermissionDTO(userPermission);
//      userPermissionSearchRepository.save(userPermission);
        return result;
    }

    /**
     *  Get all the userPermissions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserPermissionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserPermissions");
        Page<UserPermission> result = userPermissionRepository.findAll(pageable);
        return result.map(userPermission -> userPermissionMapper.userPermissionToUserPermissionDTO(userPermission));
    }

    /**
     *  Get one userPermission by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserPermissionDTO findOne(Long id) {
        log.debug("Request to get UserPermission : {}", id);
        UserPermission userPermission = userPermissionRepository.findOne(id);
        UserPermissionDTO userPermissionDTO = userPermissionMapper.userPermissionToUserPermissionDTO(userPermission);
        return userPermissionDTO;
    }

    /**
     *  Delete the  userPermission by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserPermission : {}", id);
        userPermissionRepository.delete(id);
        userPermissionSearchRepository.delete(id);
    }

    /**
     * Search for the userPermission corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserPermissionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserPermissions for query {}", query);
        Page<UserPermission> result = userPermissionSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(userPermission -> userPermissionMapper.userPermissionToUserPermissionDTO(userPermission));
    }

    @Transactional(readOnly = true)
	public Page<UserPermissionDTO> findAllByUser(User u,Pageable pageable) {
		 log.debug("Request to get all UserPermissions => ",u.getLogin());
	        Page<UserPermission> result =  userPermissionRepository.findAllByUser(u,pageable);
	        return result.map(userPermission -> userPermissionMapper.userPermissionToUserPermissionDTO(userPermission));
	}
     
    @Transactional(readOnly = true)
   	public  Set<UserPermission> findAllByUser(User user) {
   		 log.debug("Request to get all UserPermissions => ",user.getLogin());
   	        List<UserPermission> result =  userPermissionRepository.findAllByUser(user);
   	     return result.stream().collect(Collectors.toSet());//.stream().map(e -> userPermissionMapper.userPermissionToUserPermissionDTO(e)).collect(Collectors.toSet())
   	}
}
