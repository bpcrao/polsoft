package com.polmon.polsoft.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.repository.ChannelAlarmRepository;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.repository.DeviceRepository;
import com.polmon.polsoft.repository.search.DeviceSearchRepository;
import com.polmon.polsoft.service.dto.DeviceDTO;
import com.polmon.polsoft.service.mapper.DeviceMapper;
import com.polmon.polsoft.service.eventsender.DeviceEventSender;
/**
 * Service Implementation for managing Device.
 */
@Service
@Transactional
public class DeviceService {

	private final Logger log = LoggerFactory.getLogger(DeviceService.class);

	@Inject
	private DeviceRepository deviceRepository;

	@Inject
	private DchannelRepository dchannelRepository;

	@Inject
	private ChannelAlarmRepository channelAlarmRepository;

	@Inject 
	private DeviceEventSender deviceEventSender;
	
	@Inject
	private DeviceMapper deviceMapper;

	@Inject
	private DeviceSearchRepository deviceSearchRepository;

	/**
	 * Save a device.
	 *
	 * @param deviceDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public DeviceDTO save(DeviceDTO deviceDTO) {
		log.debug("Request to save Device : {}", deviceDTO);
		Device device = deviceMapper.deviceDTOToDevice(deviceDTO);
		final Device sdevice = deviceRepository.save(device);
		List<Dchannel> channels = Collections.emptyList();
		if (deviceDTO.getId() == null || deviceDTO.getId() == 0) {
			if(deviceDTO.getSelectedChannel() != null && deviceDTO.isApplyall()){
				channels = deviceDTO.getChannels().stream()
						.map(dchannel -> new Dchannel(dchannel.get("name"), deviceDTO.getSelectedChannel().get("channeldata"), sdevice, dchannel.get("unit")))
						.collect(Collectors.toList());
			}
			else{
			channels = deviceDTO.getChannels().stream()
					.map(dchannel -> new Dchannel(dchannel.get("name"), dchannel.get("channeldata"), sdevice, dchannel.get("unit")))
					.collect(Collectors.toList());
			}
			dchannelRepository.save(channels);
			channelAlarmRepository.save(channels.stream().map(channel -> channel.getChannelAlarms())
					.flatMap(l -> l.stream()).collect(Collectors.toSet()));

		} else {
			channels = deviceDTO.getChannels().stream()
					.map(dchannel -> new Dchannel(dchannel.get("name"), dchannel.get("channeldata"), sdevice, dchannel.get("unit")))
					.collect(Collectors.toList());
			Set<ObjectNode> deviceChannels = deviceDTO.getChannels();
			int i=0;
			for (ObjectNode objectNode : deviceChannels) {
				Dchannel channel = dchannelRepository.getOne(objectNode.get("id").asLong());
				if (channel.getChanneldata() != objectNode.get("channeldata").toString()) {
					channel.setChanneldata(objectNode.get("channeldata").toString());
					dchannelRepository.save(channel);
				}
				channels.get(i).setChanneldata( objectNode.get("channeldata").toString());
				i++;
			} 
		}

		deviceEventSender.sendDeviceCardData(channels,device);
		DeviceDTO result = deviceMapper.deviceToDeviceDTO(device);
		deviceSearchRepository.save(device);
		return result;
	}

	/**
	 * Get all the devices.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<DeviceDTO> findAll() {
		log.debug("Request to get all Devices");
		List<DeviceDTO> result = deviceRepository.findAll().stream().map(deviceMapper::deviceToDeviceDTO)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}
	
	public Device findByDeviceBySerial(String serialNumber){
		return deviceRepository.findOneBySerial(serialNumber);		
	}

	/**
	 * Get one device by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public DeviceDTO findOne(Long id) {
		log.debug("Request to get Device : {}", id);
		Device device = deviceRepository.findOne(id);
		DeviceDTO deviceDTO = deviceMapper.deviceToDeviceDTO(device);
		return deviceDTO;
	}

	/**
	 * Delete the device by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Device : {}", id);
		Device device = deviceRepository.findOne(id);
		
		dchannelRepository.deleteInBatch(device.getChannels());
		deviceRepository.delete(id);
		deviceSearchRepository.delete(id);
	}

	/**
	 * Search for the device corresponding to the query.
	 *
	 * @param query
	 *            the query of the search
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<DeviceDTO> search(String query) {
		log.debug("Request to search Devices for query {}", query);
		return StreamSupport.stream(deviceSearchRepository.search(queryStringQuery(query)).spliterator(), false)
				.map(deviceMapper::deviceToDeviceDTO).collect(Collectors.toList());
	}	
	
//	@Subscribe
//	public void handleLinkChannelEvent(LinkChannelEvent channelLinkedEvent) {
//		Long deviceId = channelLinkedEvent.getDevice();
//		Device device = deviceRepository.findOne(deviceId);
//		List<Long> linkedChannels = device.getChannels().stream()
//				.map(channel -> channel.getLocation() == null ? 0l : 1l).collect(Collectors.toList());
//	}
}
