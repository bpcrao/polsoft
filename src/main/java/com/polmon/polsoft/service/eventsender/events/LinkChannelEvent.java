package com.polmon.polsoft.service.eventsender.events;

public class LinkChannelEvent {
	private Long channel;
	private Long device;

	public LinkChannelEvent(Long channel,Long device){
		this.channel = channel;
		this.device = device;		
	}
	
	public Long getChannel() {
		return channel;
	}

	public void setChannel(Long channel) {
		this.channel = channel;
	}

	public Long getDevice() {
		return device;
	}

	public void setDevice(Long device) {
		this.device = device;
	}

}
