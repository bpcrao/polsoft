package com.polmon.polsoft.service.eventsender;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.polmon.polsoft.domain.Alarmtemplate;
import com.polmon.polsoft.domain.Alarmtemplatelimit;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.repository.AlarmtemplateRepository;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.repository.DeviceRepository;
import com.polmon.polsoft.sms.GSMConnect;

@Component
public class DeviceEventSender {
	private final EventBus eventBus;

	@Inject
	DeviceRepository deviceRepository;
	
	@Autowired
	private DchannelRepository dchannelRepository;
	
	@Inject
	private AlarmtemplateRepository alarmTemplateRepository;

	@Autowired
	public DeviceEventSender(final EventBus eventBus) {
		this.eventBus = eventBus;
	}

	final Logger log = LoggerFactory.getLogger(DeviceEventSender.class);
	
	public void sendEvents(Long deviceId) {

		Device device = deviceRepository.findOne(deviceId);
		byte[] channelsLinked = new byte[161];
		byte startCode = (byte) 0xAA;
		channelsLinked[0] = (byte) startCode;
		List<Dchannel> channels = dchannelRepository.findAll();
		for (Iterator<Dchannel> iterator = channels.iterator(); iterator.hasNext();) {
			Dchannel dchannel = iterator.next();
			channelsLinked[dchannel.getChannelNumber()] = (byte) (dchannel.getLocation() != null ? 0 : 1);
		}
		boolean status = false;
		try {
			status = sendDataToDataLogger(device, channelsLinked, startCode);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateDeviceHealth(device, "channelsLinked", status);		
	}

	private void updateDeviceHealth(Device device, String parameter, boolean status) {
		try {
			JSONObject deviceHealth = new JSONObject(device.getDeviceHealth()!=null? device.getDeviceHealth(): "{}");
			deviceHealth.put(parameter, status);
			device.setDeviceHealth(deviceHealth.toString());
			deviceRepository.save(device);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private boolean sendDataToDataLogger(Device device, byte[] channelsLinked, byte startCode) throws IOException {
		boolean status = false;
		Socket client = null;
		try {
			
			client = new Socket(device.getIp(), 8023); 
		}
			catch (IOException e) {			
				e.printStackTrace();}	
	//		ExecutorService executor = Executors.newFixedThreadPool(1);
	//		Future<Socket> future = executor.submit(() -> {
	//		    return new Socket(device.getIp(), 8023);
	//		});
			
		if(client!=null){
			try {
				//time-out
			//	client = future.get(2, TimeUnit.SECONDS);
				if(client.getRemoteSocketAddress() != null){
				System.out.println("Just connected to " + client.getRemoteSocketAddress());
				OutputStream outToServer = client.getOutputStream();
				DataOutputStream out = new DataOutputStream(outToServer);
				DataInputStream in = new DataInputStream(client.getInputStream());

				out.write(channelsLinked); 
				out.flush();			
				byte[] inData = new byte[1];
				in.read(inData);
		        if(inData[0] == -86 || inData[0] == -69 || inData[0] == -52) { // this is a blocking call!!            	
		            System.out.println("reading..."+inData[0]);
		        }
		        out.close();
				client.close();
				status = (inData[0] == startCode ? true : false);
				}
			}// catch (InterruptedException e) {
			//	e.printStackTrace();
		//		log.warn("interrupted sending data to DL ", e );
		//	} //catch (ExecutionException e) {				
				//e.printStackTrace();
				//log.warn("ExecutionException sending data to DL ", e );
		//	} //catch (TimeoutException e) {				
				//e.printStackTrace();
			//	log.warn("Timeout sending data to DL more than 2 seconds", e );
			//}
			
		//}
	catch (IOException e) {			
			e.printStackTrace();
		}		

		}

		return status;
	}
/*	public byte[] sendAlarmEvent(Long deviceId) {
		Device device = deviceRepository.findOne(deviceId);
		byte[] channelsAlarms = new byte[161];
		byte startCode = (byte)0xBB;
		channelsAlarms[0] = (byte) startCode;
		List<Dchannel> channels = dchannelRepository.findAll();
		for (Iterator<Dchannel> iterator = channels.iterator(); iterator.hasNext();) {
			Dchannel dchannel = iterator.next();
			channelsAlarms[dchannel.getChannelNumber()- 1] = (byte) (dchannel.getChannelAlarmTemplates().size() > 0 ? 1 : 0);
		}
		boolean status = sendDataToDataLogger(device, channelsAlarms, startCode);		 
		updateDeviceHealth(device, "alarmsLinked", status);
		return channelsAlarms;
	}*/
	
	public void sendAlarmMinMaxEvent(Long deviceId) {
		Device device = deviceRepository.findOne(deviceId);
		byte[] alarmLowLimitData = new byte[320];
		byte[] alarmHighLimitData = new byte[320];
		byte[] channelsAlarms = new byte[161]; 
		List<Integer> lowLimits = new ArrayList<Integer>(Collections.nCopies(160, 0));
		List<Integer> highLimits = new ArrayList<Integer>(Collections.nCopies(160, 0));
		List<Dchannel> channels = dchannelRepository.findAll();
		byte startCode = (byte) 0xBB;
		channelsAlarms[0] = (byte) startCode;
		for (Iterator<Dchannel> iterator = channels.iterator(); iterator.hasNext();) {
			Dchannel dchannel = iterator.next();
			Set<Alarmtemplate> channelAlarms = alarmTemplateRepository.findAllByDchannel(dchannel);
			channelsAlarms[dchannel.getChannelNumber()] = (byte) (channelAlarms.size() > 0 ? 1 : 0);
			if(channelAlarms.size()>0)
			{
				channelAlarms.forEach(alarmTemplate ->{
					Set<Alarmtemplatelimit> alarmLimits = alarmTemplate.getAlarmtemplatelimits();
					if(alarmTemplate.getAlarmtemplatelimits().size()>0)
					{
						System.out.println(dchannel.getChannelNumber());
						alarmLimits.stream().filter(alarmlimit -> "warning".equalsIgnoreCase(alarmlimit.getType()) && alarmlimit.getTriggerset()==1)
						.forEach(alarmlimit -> {
							highLimits.set(dchannel.getChannelNumber()-1,new Double(alarmlimit.getHighlimit()*10).intValue());
							lowLimits.set(dchannel.getChannelNumber()-1,new Double(alarmlimit.getLowlimit()*10).intValue());
						});
					}else {
						highLimits.set(dchannel.getChannelNumber()-1,new Double(0).intValue());
						lowLimits.set(dchannel.getChannelNumber()-1,new Double(0).intValue()); // i added the index part...
					}
				});
			}else {
				highLimits.set(dchannel.getChannelNumber()-1,new Double(0).intValue());
				lowLimits.set(dchannel.getChannelNumber()-1,new Double(0).intValue());
			}
		}
		try {
			for(int i=0;i<alarmLowLimitData.length;i=i+2)
			{
				ByteBuffer b = ByteBuffer.allocate(2);
				b.putShort(lowLimits.get((i/2)).shortValue());
				byte[] test = b.array();
				alarmLowLimitData[i] = test[0];
				alarmLowLimitData[i+1] = test[1];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			for(int i=0;i<alarmHighLimitData.length;i=i+2)
			{
				ByteBuffer b = ByteBuffer.allocate(2);
				b.putShort(highLimits.get((i/2)).shortValue());
				byte[] test = b.array();
				alarmHighLimitData[i] = test[0];
				alarmHighLimitData[i+1] = test[1];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		byte[] finalData = ArrayUtils.addAll(ArrayUtils.addAll(channelsAlarms, alarmLowLimitData),alarmHighLimitData);

		boolean status = false;
		try {
			status = sendDataToDataLogger(device, finalData, startCode);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
	    updateDeviceHealth(device, "alarmsLinked", status);
	}

	public void sendDeviceCardData(List<Dchannel> channels, Device device) {

		Map<Integer, String> map = channels.stream()
				.collect(Collectors.toMap(Dchannel::getChannelNumber, Dchannel::getChanneldata));
		String[] channelsData = new String[160];
		JSONObject[] channelsJsonData = new JSONObject[160];
		map.entrySet().stream().forEach(entrySet -> channelsData[entrySet.getKey() - 1] = entrySet.getValue());

		for (int index = 0; index < channelsData.length; index++) {
			try {
				if (channelsData[index] == null) {
					channelsData[index] = "{}";
				}
				channelsJsonData[index] = new JSONObject(channelsData[index]);
			} catch (JSONException e) {
				channelsJsonData[index] = null;
			}
		}

		byte startCode = (byte) 0xCC;

		ByteBuffer allBytes = ByteBuffer.allocate(1153);
		allBytes.put(startCode);

		ByteBuffer allMulfactorBytes = ByteBuffer.allocate(256);
		ByteBuffer allOffsetBytes = ByteBuffer.allocate(256);
		ByteBuffer prgInputBytes = ByteBuffer.allocate(16);
		ByteBuffer prgSubInputBytes = ByteBuffer.allocate(16);
		ByteBuffer maLowBytes = ByteBuffer.allocate(32);
		ByteBuffer maHighBytes = ByteBuffer.allocate(32);
		ByteBuffer mvLowBytes = ByteBuffer.allocate(32);
		ByteBuffer mvHighBytes = ByteBuffer.allocate(32);
		int index ;
		
		for (index = 0; index < channelsJsonData.length; index++) {
			float mulfactor = 0;
			float offset = 0;
			if ((index >= 0 && index <= 15)|| (index >= 48 && index <= 63) || (index >= 96 && index <= 111)|| (index >= 144 && index <= 159) ){ 
				mulfactor = extracted(channelsJsonData[index], "mulfactor");
				offset = extracted(channelsJsonData[index], "offset");
				allMulfactorBytes.putFloat(mulfactor);
				allOffsetBytes.putFloat(offset);
			}
			
		}
		
		allBytes.put(allOffsetBytes.array()).put(allMulfactorBytes.array());
		
		for (index = 0; index < 48; index++) {
			if ((index >= 0 && index <= 15)){ 
				prgInputBytes.put(getIntByte(channelsJsonData, index, "prginput"));
				prgSubInputBytes.put(getIntByte(channelsJsonData, index, "prgsubinput"));
				maLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "maLow")*10));
				maHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "maHigh")*10));
				mvLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvLow")*10));
				mvHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvHigh")*10));
			}
		}
			allBytes.put(prgInputBytes.array())
			.put(prgSubInputBytes.array()).put(maLowBytes.array()).put(maHighBytes.array()).put(mvLowBytes.array())
			.put(mvHighBytes.array());
			
			prgInputBytes.clear();
			prgSubInputBytes.clear();
			maLowBytes.clear();
			maHighBytes.clear();
			mvLowBytes.clear();
			mvHighBytes.clear();
			
		for(index = 48; index < 96; index++)
		{
			if((index >= 48 && index <= 63))
			{

			prgInputBytes.put(getIntByte(channelsJsonData, index, "prginput"));
			prgSubInputBytes.put(getIntByte(channelsJsonData, index, "prgsubinput"));
			maLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "maLow")*10));
			maHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "maHigh")*10));
			mvLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvLow")*10));
			mvHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvHigh")*10));
		
			}
		}
		allBytes.put(prgInputBytes.array())
		.put(prgSubInputBytes.array()).put(maLowBytes.array()).put(maHighBytes.array()).put(mvLowBytes.array())
		.put(mvHighBytes.array());

		prgInputBytes.clear();
		prgSubInputBytes.clear();
		maLowBytes.clear();
		maHighBytes.clear();
		mvLowBytes.clear();
		mvHighBytes.clear();
		
		for(index = 96; index < 144; index++)
		{
			if((index >= 96 && index <= 111))
			{
				prgInputBytes.put(getIntByte(channelsJsonData, index, "prginput"));
				prgSubInputBytes.put(getIntByte(channelsJsonData, index, "prgsubinput"));
				maLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "maLow")*10));
				maHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "maHigh")*10));
				mvLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvLow")*10));
				mvHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvHigh")*10));
			}
		}
		allBytes.put(prgInputBytes.array()).put(prgSubInputBytes.array()).put(maLowBytes.array()).put(maHighBytes.array()).put(mvLowBytes.array())
		.put(mvHighBytes.array());
		

		prgInputBytes.clear();
		prgSubInputBytes.clear();
		maLowBytes.clear();
		maHighBytes.clear();
		mvLowBytes.clear();
		mvHighBytes.clear();
		
		for(index = 144; index < 160; index++)
		{
			if(index >= 144 && index <= 159)
			{
				prgInputBytes.put(getIntByte(channelsJsonData, index, "prginput"));
				prgSubInputBytes.put(getIntByte(channelsJsonData, index, "prgsubinput"));
				maLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "maLow")*10));
				maHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "maHigh")*10));
				mvLowBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvLow")*10));
				mvHighBytes.putShort((short) (getShortInt(channelsJsonData, index, "mvHigh")*10));
				
			}
		}
		allBytes.put(prgInputBytes.array())
		.put(prgSubInputBytes.array()).put(maLowBytes.array()).put(maHighBytes.array()).put(mvLowBytes.array())
		.put(mvHighBytes.array());
		
		boolean status = false;
		try {
			status = sendDataToDataLogger(device, allBytes.array(), startCode);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
		updateDeviceHealth(device, "deviceconfig", status);
	}

	private float extracted(JSONObject channelsJsonData, String key) {

		try {
			if (channelsJsonData.has("sensor") && new JSONObject(channelsJsonData.get("sensor").toString()).has(key)) {
				return Float.parseFloat((String) new JSONObject(channelsJsonData.get("sensor").toString()).get(key));
			}
		} catch (NumberFormatException | JSONException e) {
			e.printStackTrace();
			return 0;
			}

		return 0;
	}

	private short getShortInt(JSONObject[] channelsJsonData, int index, String key) {
		short maLow = 0;
		try {
			JSONObject jsonObject = new JSONObject(
					channelsJsonData[index] != null ? channelsJsonData[index].toString() : "{}");
			if (channelsJsonData[index] != null && jsonObject.has(key)) {
				maLow = (short) Integer.parseInt(jsonObject.get(key).toString());
			}
		} catch (NumberFormatException | JSONException e) {
			maLow = 0;
			e.printStackTrace();
		}
		return maLow;
                               
		
	}

	private byte getIntByte(JSONObject[] channelsJsonData, int index, String key) {
		byte programInput = 0;
		try {
			JSONObject jsonObject = new JSONObject(
					channelsJsonData[index] != null ? channelsJsonData[index].toString() : "{}");
			if (channelsJsonData[index] != null && jsonObject.has(key))
				programInput = (byte) Integer.parseInt(jsonObject.get(key).toString());
		} catch (NumberFormatException | JSONException e) {
			programInput = (byte) 0;
		}
		return programInput;
	}}