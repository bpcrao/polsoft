package com.polmon.polsoft.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.stereotype.Service;

import com.polmon.polsoft.constants.EmailConfiguration;
import com.polmon.polsoft.domain.Systempreferences;
import com.polmon.polsoft.repository.SystempreferencesRepository;

@Service
public class InfluxDbService {
	
    @Autowired
    private SystempreferencesRepository systempreferencesRepository;
    
    @Autowired
    private InfluxDBTemplate<Point> influxDBTemplate;
    
	public void cleanUp(){
		Systempreferences systempreferences = systempreferencesRepository.findByKeyproperty(EmailConfiguration.CLEAN_PERIOD);
		if(systempreferences!=null)
		{
			try{
				long days = Long.valueOf(systempreferences.getData());
				LocalDateTime localDateTime =LocalDateTime.now().minusDays(days);
				Query query = new Query("delete from data where time <'"+localDateTime.toInstant(ZoneOffset.UTC)+"'", influxDBTemplate.getDatabase());
				influxDBTemplate.query(query);
			}catch(Exception e)
			{
				e.printStackTrace();
			}			
		}		
	}
}
