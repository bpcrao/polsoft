/**
 * 
 */
package com.polmon.polsoft.service.util;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author admin
 *
 */
public class ResponseParser {

	public static Pattern startAddress = Pattern.compile("\"Start Address is\":\"(.*)\"\\,");
	public static Pattern datePattern = Pattern.compile("\"Date Is \":\"(.*)\"\\,");
	public static Pattern timePattern = Pattern.compile("\"Time Is \":\"(.*)\"\\,");
	public static Pattern dataPattern = Pattern.compile("\"Data Is \":\\[(.*)");

	public List<JSONObject> parseData(List strData) throws JSONException {
		int index = 0;
		List<JSONObject> datas= new ArrayList<JSONObject>();
		while (index < strData.size() && !strData.get(index).toString().trim().equals("{") ) {
			index = index + 1;
		}
		while (index < strData.size()) {
			if (strData.get(index).equals("{")) {
				JSONObject data = new JSONObject();
				datas.add(data);
				
				Matcher startAddressMatcher = startAddress.matcher(strData.get(index + 1).toString());
				startAddressMatcher.find();
					
				Matcher dateMatcher = datePattern.matcher(strData.get(index + 2).toString());
				dateMatcher.find();
				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");
				
//26/01/17
				Matcher timeMatcher = timePattern.matcher(strData.get(index + 3).toString());
				timeMatcher.find();

				Matcher dataMatcher = dataPattern.matcher(strData.get(index + 4).toString());
				dataMatcher.find();

				data.put("startAddress", startAddressMatcher.group(1));
				
				data.put("date", ZonedDateTime.of(LocalDateTime.parse(dateMatcher.group(1)+" "+timeMatcher.group(1),formatter),ZoneId.systemDefault()));
				JSONObject channelData = new JSONObject();
				String[] channelDat = dataMatcher.group(1).toString().split(" ");
				int i = 0;
				for (String value : channelDat) {
					try {
						channelData.put("ch" + i, !value.equals("null") ? new BigDecimal(value): "null");
						i++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				data.put("data", channelData.toString());
				System.out.println(data.toString());
				while ( index < strData.size() && !strData.get(index).equals("}")) {
					index = index + 1;
				}
				
				while (index < strData.size() && !strData.get(index).equals("{") ) {
					index = index + 1;
				}
			}
		}
		return datas;
	}
}
