package com.polmon.polsoft.service.util;

import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.SECOND;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.influxdb.dto.QueryResult.Series;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;
import com.polmon.polsoft.constants.EmailConfiguration;
import com.polmon.polsoft.domain.Alarmtemplatelimit;
import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.domain.ReportTemplate;
import com.polmon.polsoft.domain.Systempreferences;
import com.polmon.polsoft.repository.AlarmtemplateRepository;
import com.polmon.polsoft.repository.AlarmtemplatelimitRepository;
import com.polmon.polsoft.repository.LocationRepository;
import com.polmon.polsoft.repository.SystempreferencesRepository;
import com.polmon.polsoft.service.AlarmtemplatelimitService;




@Component
public class PdfGeneratorUtil {

	@Autowired
	private TemplateEngine templateEngine;

	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;

	@Inject
	private LocationRepository locationRepository;

	@Inject
	private AlarmtemplateRepository alarmtemplateRepository;
	
	@Autowired
	private SystempreferencesRepository systempreferencesRepository;

	public static DecimalFormat df = new DecimalFormat("#.##");

	static {
		df.setRoundingMode(RoundingMode.CEILING);

	}

	public byte[] createPdf(String templateName, List allData, List<String> locations, String reportName,
			Date fTime, Date tTime, TreeMap<String, TreeMap<String, Double>> minmaxvalues, Set<String> deviceSerial, double numberOfPages, String userName) {
		Assert.notNull(templateName, "The templateName can not be null");
		FileOutputStream os = null;
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String fromTime = df.format(fTime);
		String toTime = df.format(tTime);
		Map locs = new LinkedHashMap();
		for(String long1: locations){
			locs.put(long1, long1);
		}
		
		


		ArrayList<Object> minimumvalues = new ArrayList<Object>();
		ArrayList<Object> maximumvalues = new ArrayList<Object>();
		if(minmaxvalues != null){
		for (Entry<String, TreeMap<String, Double>> entry : minmaxvalues .entrySet()) {
			minimumvalues.add(entry.getValue().get("minimum"));
			maximumvalues.add(entry.getValue().get("maximum"));
			System.out.println("Key:" + entry.getKey() + ", Value:" + entry.getValue().get("minimum"));
		}
		}
		
		
		String fileName1 = reportName.substring(0,1).toUpperCase()+reportName.substring(1);
		String fileName = reportName.substring(0, 1).toUpperCase() + reportName.substring(1) + ""
				+ fromTime.replaceAll("[^a-zA-Z0-9\\._]+", "") + "_" + toTime.replaceAll("[^a-zA-Z0-9\\._]+", "");
		String deviceSerialString = deviceSerial.stream().collect(Collectors.joining(","));
		Systempreferences reportFolder = systempreferencesRepository
				.findByKeyproperty(EmailConfiguration.REPORT_DOWNLOAD_FOLDER);
		Systempreferences companyLogo = systempreferencesRepository.findByKeyproperty(EmailConfiguration.COMPANY_LOGO);
		try {
			// final File outputFile = new File("/home/polmon/Desktop/reports/"
			// C:/polsoft/reports/" + fileName + ".pdf
			// + fileName + ".pdf");
			final File outputFile = new File(reportFolder.getData() + "//" + fileName + ".pdf");
			if (outputFile.getParentFile() != null) {
				outputFile.getParentFile().mkdirs();
			}
			os = new FileOutputStream(outputFile);
			String processedHtml = "";

			
			Context ctx = new Context();
			int index = 0;
			ctx.setVariable("allData", allData);
			ctx.setVariable("locs", locs);
			ctx.setVariable("isHeader", index == 0);
			ctx.setVariable("from_time", fromTime);
			ctx.setVariable("to_time", toTime);
			ctx.setVariable("fileName1", fileName1);
			ctx.setVariable("maximumvalues", maximumvalues);
			ctx.setVariable("minimumvalues", minimumvalues);
			ctx.setVariable("deviceSerial", deviceSerialString);
			ctx.setVariable("numberOfPages", numberOfPages);
			ctx.setVariable("userName", userName);
			ctx.setVariable("companyLogo", companyLogo.getData());
			ctx.setVariable("up", "&#8657;");
			String tempgHtml = templateEngine.process(templateName, ctx);
			processedHtml = processedHtml + tempgHtml;
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(processedHtml);
			renderer.layout();
			renderer.createPDF(os, false);
			renderer.finishPDF();
			int byteLength = (int) outputFile.length();

			FileInputStream fileInputStream = new FileInputStream(outputFile);
			byte[] filecontent = new byte[byteLength];
			fileInputStream.read(filecontent, 0, byteLength);
			return filecontent;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					/* ignore */ }
			}
		}
		return null;

	}
	

	public byte[] forEachReport(ReportTemplate report, String fromDate, String toDate) throws ParseException {

		List<Long> locationsLinked = new ArrayList<>();
		List<String> locationNames = new ArrayList<>();
		Set<String> deviceSerial = new HashSet<>();
		List<Device> locationLinkeddevices = new ArrayList<>();
		TreeMap<String,TreeMap<String,Double>> minmaxvalues = new TreeMap<>();
	//	TreeMap<String,TreeMap<String,Double>> maximumvalues = new TreeMap<>();
		List<Long> locationsOfReport = new ArrayList<>();
		List<String> locationsOfReport1 = Arrays.stream(report.getSettings().split(",")).map(i -> new String(i))
				.collect(Collectors.toList());
		Collections.sort(locationsOfReport1);
		for(String obj : locationsOfReport1) {
		    locationsOfReport.add(Long.parseLong(obj));
		}

		 /*Alarmtemplatelimit alarmtemplatelimit = alarmtemplateRepository.findAllByDchannelId(l);
				 .stream()
				 .map(i-> new Long())
		            .collect(Collectors.toCollection(LinkedList::new));
		 */
		String reportName = report.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		DateFormat influxDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime now = LocalDateTime.now();
		now = now.minusMinutes(330);
		
		LocalDateTime fromTime = null;

		Query query = null;
		Query queryLastValue = null;
		Query alarmQuery = null;
		Date From_time = null, To_time = null;

		if (fromDate == null || toDate == null) {
			if (fromDate == null && report.getTimeFrame().equalsIgnoreCase("minutes")) {
				
				fromTime = now.minusMinutes(report.getReportDuration());
				fromDate = influxDateFormat.format(dateFormat.parse(fromTime.toString()));
				toDate = influxDateFormat.format(dateFormat.parse(now.toString()));
			} else if (report.getTimeFrame().equalsIgnoreCase("hours")) {
				fromTime = now.minusHours(report.getReportDuration()).minusNanos(now.getNano());
				Integer.toString(report.getSamplingData() * 60);
				fromDate = influxDateFormat.format(dateFormat.parse(fromTime.toString()));
				toDate = influxDateFormat.format(dateFormat.parse(now.toString()));
			}
		} else {
			From_time = trim(dateFormat.parse(fromDate));
			To_time = trim(dateFormat.parse(toDate));
		}

		if (locationsOfReport.size() > 0) {
			List<Location> locationObjects = locationRepository.findAll(locationsOfReport).stream()
					.filter(location -> location.getDchannel() != null).collect(Collectors.toList());
			
			Collections.sort(locationObjects, new Comparator() {

	        public int compare(Object arg0, Object arg1) {


	            Location pers0 = (Location)arg0;
	            Location pers1 = (Location)arg1;


	            // COMPARE NOW WHAT YOU WANT
	            // Thanks to Steve Kuo for your comment!
	            return pers0.getId().toString().compareToIgnoreCase(pers1.getId().toString());
	        }
	    });
			
			for (Location locationObj : locationObjects) {
				locationsLinked.add(locationObj.getId());
				if (locationObj.getDchannel() != null) {
					locationLinkeddevices.add(locationObj.getDchannel().getDevice());
				}
				if (locationObj.getUnit() != null) {
					if (locationObj.getUnit().equals("Temperature DegC"))
						locationNames.add(locationObj.getName() + "(°C)");
					else if (locationObj.getUnit().equals("Humidity %RH"))
						locationNames.add(locationObj.getName() + "(%RH)");
					else
						locationNames.add(locationObj.getName() + "(-)");
				} else
					locationNames.add(locationObj.getName() + "(-)");
				// locationNames.add(locationObj.getName()+"("+(null!=locationObj.getUnit()?locationObj.getUnit():"-")+")");
			}

			deviceSerial = locationLinkeddevices.stream().filter(device -> device.getSerial() != null)
					.map(device -> device.getSerial()).collect(Collectors.toSet());

		}

		String queryLoc = getQueryCondition(locationsLinked);

		//getMinAndMaxValues(queryLoc, maximumvalues, minimumvalues, fromDate, toDate, report.getReportDuration());
/*		query = new Query(
				"SELECT first(*) FROM data where location =~ " + queryLoc + "/ and time >= '" + fromDate
						+ "'and time<='" + toDate + "' group by time(" + report.getSamplingData()
						+ report.getTimeFrame().toLowerCase().charAt(0) + "), location_tag order by time desc",
				influxDBTemplate.getDatabase());*/
		query = new Query(
				"SELECT * FROM data where " + queryLoc + " and time >= '" + fromDate
						+ "'and time<='" + toDate + "' group by location_tag order by time asc",
				influxDBTemplate.getDatabase());
		
		queryLastValue = new Query("SELECT * FROM data where " + queryLoc + "and time ='" + toDate
				+ "' group by location_tag", influxDBTemplate.getDatabase());

		alarmQuery = new Query("SELECT * FROM data where isReports = 'true' and (" + queryLoc
				+ ") and time >= '" + fromDate + "'and time <='" + toDate + "' group by location_tag",
				influxDBTemplate.getDatabase());

		QueryResult result = influxDBTemplate.query(query);
		QueryResult lastResult = influxDBTemplate.query(queryLastValue);
		QueryResult alarmResult = influxDBTemplate.query(alarmQuery);
		List<Result> resultList = result.getResults();
		List<Result> lastResultList = lastResult.getResults();
		List<Result> AlarmResultList = alarmResult.getResults();
		// NOT SURE
		//resultList.addAll(lastResultList);

		if (From_time == null) {
			From_time = dateFormat.parse(fromDate);
			To_time = dateFormat.parse(toDate);
		}

		TreeMap<Long, TreeMap> linkedHash = new TreeMap<Long, TreeMap>();
		TreeMap<Long, TreeMap> linkedHash_Alarms = new TreeMap<Long, TreeMap>();
		
		if (resultList.get(0).getSeries() != null) {

			List<Series> multipleSeries = resultList.get(0).getSeries();

			for (Iterator<Series> iterator = multipleSeries.iterator(); iterator.hasNext();) {
				Series series = iterator.next();
				List<List<Object>> seriesData = series.getValues();
				for (List<Object> rowData : seriesData) {
					if (rowData.size() > 6) {
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
						Date times = df.parse(rowData.get(0).toString());
						
						if ((times.getTime() - From_time.getTime() + 19800000)%(report.getSamplingData()*60000) == 0) {
							times.setTime(times.getTime() + 19800000);
						if (rowData.get(6) != null) {

							Map<String, Object> channelValue = createChannelValue(rowData);

							// if there is no entry for timestam create it
							if (!linkedHash.containsKey(times.getTime())) {
								TreeMap value = new TreeMap();
								linkedHash.put(times.getTime(), value);
							}
							Map rowMap = linkedHash.get(times.getTime());
							rowMap.put(rowData.get(3), channelValue);

						}

						// set values "-" if there is no value in DB
						Map rowMap = linkedHash.get(times.getTime());
						if (rowMap != null) {
							for (Long locationId : locationsLinked) {
								if (!rowMap.containsKey(locationId.toString())) {
									Map channelValue = new TreeMap();
									channelValue.put("value", "-");
									channelValue.put("alarm", "warning");
									channelValue.put("isReports", true);
									rowMap.put(locationId.toString(), channelValue);
								}
							}
						}
						
					}
					}
					
				}
				
			}
		}

		if (From_time != null) {
			long from_Seconds = From_time.getTime();
			long to_Seconds = To_time.getTime();
			if ((to_Seconds - from_Seconds) % (report.getSamplingData() * 60000) != 0) {
				if (lastResultList.get(0).getSeries() != null) {
					List<Series> multipleSeries = lastResultList.get(0).getSeries();
					for (Iterator<Series> iterator = multipleSeries.iterator(); iterator.hasNext();) {
						Series series = iterator.next();
						List<List<Object>> seriesData = series.getValues();
						for (List<Object> rowData : seriesData) {
							if (rowData.size() > 6) {
								DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
								Date times = df.parse(rowData.get(0).toString());
								times.setTime(times.getTime() + 19800000);
								if (rowData.get(6) != null) {
									Map<String, Object> channelValue = createChannelValue(rowData);
									// if there is no entry for timestamp create
									// it
									if (!linkedHash.containsKey(times.getTime())) {
										TreeMap value = new TreeMap();
										linkedHash.put(times.getTime(), value);
									}
									Map rowMap = linkedHash.get(times.getTime());
									rowMap.put(rowData.get(3), channelValue);
								}
								// set values "-" if there is no value in DB
								Map rowMap = linkedHash.get(times.getTime());
								if (rowMap != null) {
									for (Long locationId : locationsLinked) {
										if (!rowMap.containsKey(locationId.toString())) {
											Map channelValue = new TreeMap();
											channelValue.put("value", "-");
											channelValue.put("alarm", "warning");
											channelValue.put("isReports", true);
											rowMap.put(locationId.toString(), channelValue);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		

		if (AlarmResultList.get(0).getSeries() != null) {
			List<Series> multipleSeries = AlarmResultList.get(0).getSeries();
			for (Iterator<Series> iterator = multipleSeries.iterator(); iterator.hasNext();) {
				Series series = iterator.next();
				List<List<Object>> seriesData = series.getValues();
				for (List<Object> rowData : seriesData) {
					if (rowData.size() > 6) {
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
						Date times = df.parse(rowData.get(0).toString());
						times.setTime(times.getTime() + 19800000);
						if (rowData.get(6) != null) {
							Map<String, Object> channelValue = createChannelValue(rowData);
							// if there is no entry for timestam create it
							if (!linkedHash_Alarms.containsKey(times.getTime())) {
								TreeMap value = new TreeMap();
								linkedHash_Alarms.put(times.getTime(), value);
							}
							Map rowMap = linkedHash_Alarms.get(times.getTime());
							rowMap.put(rowData.get(3), channelValue);
						}
						// set values "-" if there is no value in DB
						Map rowMap = linkedHash_Alarms.get(times.getTime());
						if (rowMap != null) {
							for (Long locationId : locationsLinked) {
								if (!rowMap.containsKey(locationId.toString())) {
									Map channelValue = new TreeMap();
									channelValue.put("value", "-");
									channelValue.put("alarm", "warning");
									channelValue.put("isReports", true);
									rowMap.put(locationId.toString(), channelValue);
								}
							}
						}
					}
				}
			}
		}
		
	
		boolean removeKey;
		for (Map.Entry<Long, TreeMap> entry : new TreeMap<Long, TreeMap>(linkedHash_Alarms).entrySet()) {
			TreeMap<String, TreeMap> value = entry.getValue();
			Long key = entry.getKey();
			removeKey = true;
			for (Map.Entry<String, TreeMap> entry1 : value.entrySet()) {
				String location = entry1.getKey();
				
				TreeMap<String, Object> data = entry1.getValue();	
				 if ((data.containsValue("alarm") ||  data.containsValue("warning") || data.containsValue("-"))) {
					 removeKey = false;
					 break;
				 }
			}

			if (removeKey)
				linkedHash_Alarms.remove(key);
		}
		
		// will it replace or add ??
		for (Map.Entry<Long, TreeMap> entry : new TreeMap<Long, TreeMap>(linkedHash_Alarms).entrySet()) {
			linkedHash.put(entry.getKey(), entry.getValue());
		}
		
		double min = 0, max = 0;
		for (Map.Entry<Long, TreeMap> entry : new TreeMap<Long, TreeMap>(linkedHash).entrySet()) {
			TreeMap<String, TreeMap> value = entry.getValue();
			Long key = entry.getKey();
		
			for (Map.Entry<String, TreeMap> entry1 : value.entrySet()) {
				String location = entry1.getKey();
				TreeMap<String, Object> data = entry1.getValue();				
				if(!data.get("value").getClass().getName().equalsIgnoreCase("java.lang.String")){	
					TreeMap<String, Double> x = new TreeMap<>();
					if(minmaxvalues.get(location) == null)
					{
						min = (double) data.get("value");
						max = (double) data.get("value");
					}
					else{
					min = minmaxvalues.get(location).get("minimum") < (double) data.get("value") ? minmaxvalues.get(location).get("minimum") : (double) data.get("value");
					max = minmaxvalues.get(location).get("maximum") > (double) data.get("value") ? minmaxvalues.get(location).get("maximum") : (double) data.get("value");
					}

				x.put("minimum", min);
				x.put("maximum", max);
				minmaxvalues.put(location, x);
				}	
			}
		}

		List<Map> allData = new ArrayList<Map>();

		int dataPointsSize = linkedHash.size();
		double contentSize = 24d;
		int contentSiz = 24;
		double numberOfPages = Math.ceil(dataPointsSize / contentSize) - 1;
		Long[] keyss = linkedHash.keySet().toArray(new Long[linkedHash.keySet().size()]);

		for (int page = 0; page <= numberOfPages && dataPointsSize > (page * contentSiz); page++) {
			int startSize = ((page) * contentSiz) > (int) dataPointsSize ? ((int) dataPointsSize) - 1
					: ((page) * contentSiz);
			int endSize = ((page + 1) * contentSiz) - 1 > (int) dataPointsSize ? ((int) dataPointsSize) - 1
					: ((page + 1) * contentSiz) - 2;
			if (endSize == dataPointsSize - 1) {
				endSize = endSize - 1;
			}
			if (endSize == 0) {
				break;
			}
			allData.add(linkedHash.subMap(keyss[startSize], true, keyss[endSize + 1], true));
		}

		SecurityContext securityContext = SecurityContextHolder.getContext();
		String userName = "";
		if (securityContext != null && securityContext.getAuthentication() != null) {
			userName = securityContext.getAuthentication().getName();
		}
		if (StringUtils.isBlank(userName)) {
			userName = StringUtils.defaultIfBlank(report.getOwner(), "");
		}
		//DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return createPdf("report", allData, locationNames, reportName, From_time, To_time,
				minmaxvalues, deviceSerial, numberOfPages + 1, userName);
	}

	/**
	 * creates the channel value
	 * 
	 * @param rowData
	 * @return channel
	 */
	private Map<String, Object> createChannelValue(List<Object> rowData) {
		String object = rowData.get(6).toString().split("[.]").length > 1
				? rowData.get(6).toString().split("[.]")[0] + "." + rowData.get(6).toString().split("[.]")[1]
						.substring(0, rowData.get(6).toString().split("[.]")[1].length())
				: rowData.get(6).toString();

		Map<String, Object> channelValue = new TreeMap<String, Object>();
		float pv_value = Float.parseFloat(object);

		if (pv_value == -8999) {
			channelValue.put("value", "Err1");
		} else if (pv_value == -9000) {
			channelValue.put("value", "Err0");
		} else if (pv_value == -9003) {
			channelValue.put("value", "Err3");
			
		} else {
			BigDecimal bd = new BigDecimal(object.toString());
			bd = bd.setScale(1, RoundingMode.HALF_UP);

			channelValue.put("value", bd.doubleValue());
		}
		channelValue.put("alarm", rowData.get(1) != null ? rowData.get(1).toString() : "");
		channelValue.put("isReports", rowData.get(2));
		
		return channelValue;
	}

	private String getQueryCondition(List<Long> locationsLinked) {
		String queryLoc = "";
		int index = 0;
		for (Long locationId : locationsLinked) {
			queryLoc = queryLoc + (index == 0 ? "location = '" : "OR location = '") + locationId +"'";
			index++;
			
		}
		return queryLoc;
	}

	public static Date trim(Date date) {
		Calendar cal = Calendar.getInstance();
		TimeZone tz = TimeZone.getTimeZone("GMT");
		cal.setTime(date);
		cal.setTimeZone(tz);
		// cal.set(HOUR_OF_DAY, 0);
		// cal.set(MINUTE, 0);
		cal.set(SECOND, 0);
		cal.set(MILLISECOND, 0);
		return cal.getTime();
	}

	/*private void getMinAndMaxValues(String locations, List<Object> maximumvalues, List<Object> minimumvalues,
			String fromDate, String toDate, int duration) {
		String max_value = null;
		String min_value = null;
		List<Result> results = null;

		if (fromDate != null && toDate != null) {

			// need to check for null case of fromDate and toDate

			Query maxMinQuery = new Query(
					"SELECT MAX(value),MIN(value) FROM data where location =~ " + locations + "/ and time >= '"
							+ fromDate + "' and time<='" + toDate + "' and value > -8999 group by location_tag",
					influxDBTemplate.getDatabase());

			QueryResult result = influxDBTemplate.query(maxMinQuery);
			results = result.getResults();
			List<Series> seriesData = results.get(0).getSeries();
			if (seriesData != null) {

				for (Series series : seriesData) {
					List<List<Object>> data2 = series.getValues();
					for (List<Object> list2 : data2) {
						max_value = list2.get(1).toString().split("[.]").length > 1
								? list2.get(1).toString().split("[.]")[0] + "."
										+ list2.get(1).toString().split("[.]")[1].substring(0,
												list2.get(1).toString().split("[.]")[1].length())
								: list2.get(1).toString();
						min_value = list2.get(2).toString().split("[.]").length > 1
								? list2.get(2).toString().split("[.]")[0] + "."
										+ list2.get(2).toString().split("[.]")[1].substring(0,
												list2.get(2).toString().split("[.]")[1].length())
								: list2.get(2).toString();
					}

					BigDecimal bd = new BigDecimal(max_value);
					bd = bd.setScale(1, RoundingMode.HALF_UP);
					maximumvalues.add(bd.doubleValue());
					BigDecimal bd1 = new BigDecimal(min_value);
					bd1 = bd1.setScale(1, RoundingMode.HALF_UP);
					minimumvalues.add(bd1.doubleValue());
				}
			}
		}
	}*/
}
