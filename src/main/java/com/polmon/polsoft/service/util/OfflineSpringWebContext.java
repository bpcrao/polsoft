//package com.polmon.polsoft.service.util;
//import static org.thymeleaf.spring4.expression.ThymeleafEvaluationContext.THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME;
//import static org.thymeleaf.spring4.naming.SpringContextVariableNames.SPRING_REQUEST_CONTEXT;
//
//import java.util.HashMap;
//import java.util.Locale;
//
//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.springframework.context.ApplicationContext;
//import org.springframework.core.convert.ConversionService;
//import org.springframework.web.servlet.support.RequestContext;
//import org.springframework.web.servlet.support.RequestContextUtils;
//import org.thymeleaf.context.IWebContext;
//import org.thymeleaf.context.VariablesMap;
//import org.thymeleaf.spring4.context.SpringWebContext;
//import org.thymeleaf.spring4.expression.ThymeleafEvaluationContext;
//
///**
// * Decorator for SpringWebContext performing all required initializations.
// */
//public class OfflineSpringWebContext implements IWebContext {
//    
//    private final SpringWebContext springWebContext;
//
//    public OfflineSpringWebContext(
//            final HttpServletRequest request, 
//            final HttpServletResponse response,
//            final ServletContext servletContext,
//            final ApplicationContext applicationContext,
//            final ConversionService conversionService) {
//        // Create delegating SpringWebContext
//        final Locale locale = RequestContextUtils.getLocale(request);
//        springWebContext = new SpringWebContext(
//            request, response, servletContext, locale, new HashMap(), applicationContext);
//        // Perform initializations
//        initialize(conversionService);
//    }
//
//    private void initialize(final ConversionService conversionService) {
//        createRequestContext();
//        createEvaluationContext(conversionService);
//        createBindings();
//    }
//    
//    private void createRequestContext() {
//        RequestContext requestContext = new RequestContext(
//            springWebContext.getHttpServletRequest(), springWebContext.getHttpServletResponse(), springWebContext.getServletContext(), springWebContext.getVariables());
//        springWebContext.setVariable(SPRING_REQUEST_CONTEXT, requestContext);
//    }
//
//    private void createEvaluationContext(final ConversionService conversionService) {
//        ThymeleafEvaluationContext evaluationContext = new ThymeleafEvaluationContext(springWebContext.getApplicationContext(), conversionService);
//        //evaluationContext.setTypeLocator(new WhitelistTypeLocator());
//        springWebContext.setVariable(THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME, evaluationContext);
//    }
//
//    private void createBindings() {
//        // TODO
//    }
//
//    public void addContextExecutionInfo(String string) {
//        springWebContext.addContextExecutionInfo(string);
//    }
//
//    public HttpServletRequest getHttpServletRequest() {
//        return springWebContext.getHttpServletRequest();
//    }
//
//    public HttpServletResponse getHttpServletResponse() {
//        return springWebContext.getHttpServletResponse();
//    }
//
//    public HttpSession getHttpSession() {
//        return springWebContext.getHttpSession();
//    }
//
//    public ServletContext getServletContext() {
//        return springWebContext.getServletContext();
//    }
//
//    @Deprecated
//    public VariablesMap<String, String[]> getRequestParameters() {
//        return springWebContext.getRequestParameters();
//    }
//
//    @Deprecated
//    public VariablesMap<String, Object> getRequestAttributes() {
//        return springWebContext.getRequestAttributes();
//    }
//
//    @Deprecated
//    public VariablesMap<String, Object> getSessionAttributes() {
//        return springWebContext.getSessionAttributes();
//    }
//
//    @Deprecated
//    public VariablesMap<String, Object> getApplicationAttributes() {
//        return springWebContext.getApplicationAttributes();
//    }
//
//    public VariablesMap<String, Object> getVariables() {
//        return springWebContext.getVariables();
//    }
//
//    public Locale getLocale() {
//        return springWebContext.getLocale();
//    }
//}
