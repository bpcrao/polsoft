package com.polmon.polsoft.service;

import com.polmon.polsoft.domain.Systempreferences;
import com.polmon.polsoft.repository.SystempreferencesRepository;
import com.polmon.polsoft.repository.search.SystempreferencesSearchRepository;
import com.polmon.polsoft.service.dto.SystempreferencesDTO;
import com.polmon.polsoft.service.mapper.SystempreferencesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Systempreferences.
 */
@Service
@Transactional
public class SystempreferencesService {

    private final Logger log = LoggerFactory.getLogger(SystempreferencesService.class);
    
    @Inject
    private SystempreferencesRepository systempreferencesRepository;

    @Inject
    private SystempreferencesMapper systempreferencesMapper;

    @Inject
    private SystempreferencesSearchRepository systempreferencesSearchRepository;

    /**
     * Save a systempreferences.
     *
     * @param systempreferencesDTO the entity to save
     * @return the persisted entity
     */
    public List<SystempreferencesDTO> save(List<SystempreferencesDTO> systempreferencesDTO) {
        log.debug("Request to save Systempreferences : {}", systempreferencesDTO);
        List<Systempreferences> systempreferences = systempreferencesMapper.systempreferencesDTOsToSystempreferences(systempreferencesDTO);
        systempreferences = systempreferencesRepository.save(systempreferences);
         List<SystempreferencesDTO> result = systempreferencesMapper.systempreferencesToSystempreferencesDTOs(systempreferences);
       // systempreferencesSearchRepository.save(systempreferences);
        return result;
    }

    /**
     *  Get all the systempreferences.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<SystempreferencesDTO> findAll() {
        log.debug("Request to get all Systempreferences");
        List<SystempreferencesDTO> result = systempreferencesRepository.findAll().stream()
            .map(systempreferencesMapper::systempreferencesToSystempreferencesDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one systempreferences by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public SystempreferencesDTO findOne(Long id) {
        log.debug("Request to get Systempreferences : {}", id);
        Systempreferences systempreferences = systempreferencesRepository.findOne(id);
        SystempreferencesDTO systempreferencesDTO = systempreferencesMapper.systempreferencesToSystempreferencesDTO(systempreferences);
        return systempreferencesDTO;
    }

    /**
     *  Delete the  systempreferences by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Systempreferences : {}", id);
        systempreferencesRepository.delete(id);
        systempreferencesSearchRepository.delete(id);
    }

    /**
     * Search for the systempreferences corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SystempreferencesDTO> search(String query) {
        log.debug("Request to search Systempreferences for query {}", query);
        return StreamSupport
            .stream(systempreferencesSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(systempreferencesMapper::systempreferencesToSystempreferencesDTO)
            .collect(Collectors.toList());
    }
}
