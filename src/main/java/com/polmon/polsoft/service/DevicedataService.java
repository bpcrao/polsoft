package com.polmon.polsoft.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.polmon.polsoft.domain.Devicedata;
import com.polmon.polsoft.repository.DevicedataRepository;
import com.polmon.polsoft.repository.search.DevicedataSearchRepository;
import com.polmon.polsoft.service.dto.DevicedataDTO;
import com.polmon.polsoft.service.mapper.DevicedataMapper;

/**
 * Service Implementation for managing Devicedata.
 */
@Service
@Transactional
public class DevicedataService {

    private final Logger log = LoggerFactory.getLogger(DevicedataService.class);
    
    @Inject
    private DevicedataRepository devicedataRepository;

    @Inject
    private DevicedataMapper devicedataMapper;

    @Inject
    private DevicedataSearchRepository devicedataSearchRepository;

    /**
     * Save a devicedata.
     *
     * @param devicedataDTO the entity to save
     * @return the persisted entity
     */
    public DevicedataDTO save(DevicedataDTO devicedataDTO) {
        log.debug("Request to save Devicedata : {}", devicedataDTO);
        Devicedata devicedata = devicedataMapper.devicedataDTOToDevicedata(devicedataDTO);
        devicedata = devicedataRepository.save(devicedata);
        DevicedataDTO result = devicedataMapper.devicedataToDevicedataDTO(devicedata);
        devicedataSearchRepository.save(devicedata);
        return result;
    }

    /**
     *  Get all the devicedata.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<DevicedataDTO> findAll() {
        log.debug("Request to get all Devicedata");
        List<DevicedataDTO> result = devicedataRepository.findAll().stream()
            .map(devicedataMapper::devicedataToDevicedataDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }
    
    
    /**
     *  Get all the devicedata.
     *  
     *  @return the list of entities
     * @throws JsonProcessingException 
     */
    @Transactional(readOnly = true) 
    public String findAllForCharts() throws JsonProcessingException {
        log.debug("Request to get all Devicedata");        
        List<ZonedDateTime> xSeries = devicedataRepository.findAll().stream().map(Devicedata::getTimeCollected).collect(Collectors.toList());
        
        List<JsonObject> result = devicedataRepository.findAll().stream()
                .map(Devicedata::getChannelDataJSON)
                .collect(Collectors.toCollection(LinkedList::new));
        
        Map finslResult = result.stream()
        .flatMap(map -> map.entrySet().stream())
        .collect(
                Collectors.groupingBy(
                        Map.Entry::getKey, 
                        Collectors.mapping( obj -> 
                                Integer.valueOf(obj.getValue().toString()), 
                                Collectors.toList()
                        )
                )
        );  
        return  new ObjectMapper().writeValueAsString(finslResult);
    }

    /**
     *  Get one devicedata by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DevicedataDTO findOne(Long id) {
        log.debug("Request to get Devicedata : {}", id);
        Devicedata devicedata = devicedataRepository.findOne(id);
        DevicedataDTO devicedataDTO = devicedataMapper.devicedataToDevicedataDTO(devicedata);
        return devicedataDTO;
    }

    /**
     *  Delete the  devicedata by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Devicedata : {}", id);
        devicedataRepository.delete(id);
        devicedataSearchRepository.delete(id);
    }

    /**
     * Search for the devicedata corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DevicedataDTO> search(String query) {
        log.debug("Request to search Devicedata for query {}", query);
        return StreamSupport
            .stream(devicedataSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(devicedataMapper::devicedataToDevicedataDTO)
            .collect(Collectors.toList());
    }
}
