package com.polmon.polsoft.service;

import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.ChannelAlarm;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.repository.ChannelAlarmRepository;
import com.polmon.polsoft.repository.search.ChannelAlarmSearchRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.ChannelAlarmDTO;
import com.polmon.polsoft.service.mapper.ChannelAlarmMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ChannelAlarm.
 */
@Service
@Transactional
public class ChannelAlarmService {

    private final Logger log = LoggerFactory.getLogger(ChannelAlarmService.class);
    
    @Inject
    private AuditEventPublisher auditPublisher;
    
    @Inject
    private ChannelAlarmRepository channelAlarmRepository;

    @Inject
    private ChannelAlarmMapper channelAlarmMapper;

    @Inject
    private ChannelAlarmSearchRepository channelAlarmSearchRepository;

    /**
     * Save a channelAlarm.
     *
     * @param channelAlarmDTO the entity to save
     * @return the persisted entity
     */
    public ChannelAlarmDTO save(ChannelAlarmDTO channelAlarmDTO) {
        log.debug("Request to save ChannelAlarm : {}", channelAlarmDTO);
        ChannelAlarm channelAlarm = channelAlarmMapper.channelAlarmDTOToChannelAlarm(channelAlarmDTO);
        channelAlarm = channelAlarmRepository.save(channelAlarm);
        ChannelAlarmDTO result = channelAlarmMapper.channelAlarmToChannelAlarmDTO(channelAlarm);
        channelAlarmSearchRepository.save(channelAlarm);
        auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "ALARM IS ADDED"));
        return result;
    }

    /**
     *  Get all the channelAlarms.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ChannelAlarmDTO> findAll() {
        log.debug("Request to get all ChannelAlarms");
        List<ChannelAlarmDTO> result = channelAlarmRepository.findAll().stream()
            .map(channelAlarmMapper::channelAlarmToChannelAlarmDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one channelAlarm by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ChannelAlarmDTO findOne(Long id) {
        log.debug("Request to get ChannelAlarm : {}", id);
        ChannelAlarm channelAlarm = channelAlarmRepository.findOne(id);
        ChannelAlarmDTO channelAlarmDTO = channelAlarmMapper.channelAlarmToChannelAlarmDTO(channelAlarm);
        return channelAlarmDTO;
    }

    /**
     *  Delete the  channelAlarm by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ChannelAlarm : {}", id);
        channelAlarmRepository.delete(id);
        channelAlarmSearchRepository.delete(id);
    }

    /**
     * Search for the channelAlarm corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ChannelAlarmDTO> search(String query) {
        log.debug("Request to search ChannelAlarms for query {}", query);
        return StreamSupport
            .stream(channelAlarmSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(channelAlarmMapper::channelAlarmToChannelAlarmDTO)
            .collect(Collectors.toList());
    }

	public Set<ChannelAlarm> findAllByChannel(Dchannel ch) {
		return channelAlarmRepository.findAllByDchannel(ch);
	}
}
