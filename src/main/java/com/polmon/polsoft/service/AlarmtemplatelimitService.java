package com.polmon.polsoft.service;

import com.polmon.polsoft.service.dto.AlarmtemplateDTO;
import com.polmon.polsoft.service.dto.AlarmtemplatelimitDTO;

import java.util.LinkedList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

/**
 * Service Interface for managing Alarmtemplatelimit.
 */
public interface AlarmtemplatelimitService {

    /**
     * Save a alarmtemplatelimit.
     *
     * @param alarmtemplatelimitDTO the entity to save
     * @return the persisted entity
     */
    AlarmtemplatelimitDTO save(AlarmtemplatelimitDTO alarmtemplatelimitDTO);

    /**
     *  Get all the alarmtemplatelimits.
     *  
     *  @return the list of entities
     */
    List<AlarmtemplatelimitDTO> findAll();

    /**
     *  Get the "id" alarmtemplatelimit.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AlarmtemplatelimitDTO findOne(Long id);

    /**
     *  Delete the "id" alarmtemplatelimit.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the alarmtemplatelimit corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<AlarmtemplatelimitDTO> search(String query);
}
