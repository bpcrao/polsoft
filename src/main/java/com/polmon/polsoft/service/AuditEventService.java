package com.polmon.polsoft.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.config.audit.AuditEventConverter;
import com.polmon.polsoft.constants.EmailConfiguration;
import com.polmon.polsoft.domain.PersistentAuditEvent;
import com.polmon.polsoft.domain.Systempreferences;
import com.polmon.polsoft.repository.PersistenceAuditEventRepository;
import com.polmon.polsoft.repository.SystempreferencesRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.CustomAuditEvent;

/**
 * Service for managing audit events.
 * <p>
 * This is the default implementation to support SpringBoot Actuator AuditEventRepository
 * </p>
 */
@Service
@Transactional
public class AuditEventService {

    private PersistenceAuditEventRepository persistenceAuditEventRepository;

    private AuditEventConverter auditEventConverter;
    
    @Autowired
    private SystempreferencesRepository systempreferencesRepository;

    @Inject
    public AuditEventService(
        PersistenceAuditEventRepository persistenceAuditEventRepository,
        AuditEventConverter auditEventConverter) {

        this.persistenceAuditEventRepository = persistenceAuditEventRepository;
        this.auditEventConverter = auditEventConverter;
    }

    public Page<CustomAuditEvent> findAll(Pageable pageable) {
        return persistenceAuditEventRepository.findAll(pageable)
            .map(persistentAuditEvents -> auditEventConverter.convertToAuditEvent(persistentAuditEvents));
    }

    public Page<CustomAuditEvent> findByDates(LocalDateTime fromDate, LocalDateTime toDate, Pageable pageable) {
        return persistenceAuditEventRepository.findAllByAuditEventDateBetween(fromDate, toDate, pageable)
            .map(persistentAuditEvents -> auditEventConverter.convertToAuditEvent(persistentAuditEvents));
    }
    
    public List<CustomAuditEvent> findByDates(LocalDateTime fromDate, LocalDateTime toDate) {
        return persistenceAuditEventRepository.findAllByAuditEventDateBetween(fromDate, toDate).stream()
            .map(persistentAuditEvents -> auditEventConverter.convertToAuditEvent(persistentAuditEvents)).collect(Collectors.toList());
    }

    public Optional<CustomAuditEvent> find(Long id) {
        return Optional.ofNullable(persistenceAuditEventRepository.findOne(id)).map
            (auditEventConverter::convertToAuditEvent);
    }

	public List<String> findAllEntityTypes() {
		return persistenceAuditEventRepository.findAllEntityTypes();
	}

	public Page<CustomAuditEvent> findByDatesAndEventSource(LocalDateTime fromDate, LocalDateTime toDate, String eventTypeFilter,
			Pageable pageable) {
        return persistenceAuditEventRepository.findAllByAuditEventDateBetweenAndAuditEventTypeEquals(fromDate, toDate, eventTypeFilter, pageable)
                .map(persistentAuditEvents -> auditEventConverter.convertToAuditEvent(persistentAuditEvents));
	}
	
	public List<CustomAuditEvent> findByDatesAndEventSource(LocalDateTime fromDate, LocalDateTime toDate, String eventTypeFilter) {
        return persistenceAuditEventRepository.findAllByAuditEventDateBetweenAndAuditEventTypeEquals(fromDate, toDate, eventTypeFilter)
                .stream().map(persistentAuditEvents -> auditEventConverter.convertToAuditEvent(persistentAuditEvents)).collect(Collectors.toList());
	}

	public List<String> findAllAuditEventSource() {
		return persistenceAuditEventRepository.findAllAuditEventSource();
	}

	public Page<CustomAuditEvent> findAllByAuditEventType(String entityType, Pageable pageRequest) {
		// TODO Auto-generated method stub
		return persistenceAuditEventRepository.findAllByAuditEventType(entityType, pageRequest);
	}

	public CustomAuditEvent findOneByAuditEventTypeAndEntityIdAndCommitVersion(String qualifiedName, Long entityId,
			Integer commitVersion) {
		// TODO Auto-generated method stub
		return auditEventConverter.convertToAuditEvent(persistenceAuditEventRepository.findOneByAuditEventTypeAndEntityIdAndCommitVersion(qualifiedName,entityId,commitVersion));
	}

	public CustomAuditEvent save(CustomAuditEvent auditData) {
		PersistentAuditEvent pAuditData = persistenceAuditEventRepository.findOneById(auditData.getId());
		pAuditData.setReviewedBy(SecurityUtils.getCurrentUserLogin());
		PersistentAuditEvent data = persistenceAuditEventRepository.save(pAuditData);
		return auditEventConverter.convertToAuditEvent(data);
	}
	
	public List<CustomAuditEvent> findAll(List<Long> auditIds) {
       return persistenceAuditEventRepository.findByIdIn(auditIds).stream().map(persistentAuditEvent ->
        		auditEventConverter.convertToAuditEvent(persistentAuditEvent)
        		).collect(Collectors.toList());
    }
	
	public void cleanUp(){
		Systempreferences systempreferences = systempreferencesRepository.findByKeyproperty(EmailConfiguration.CLEAN_PERIOD);
		if(systempreferences!=null)
		{
			try{
				long days = Long.valueOf(systempreferences.getData());
				persistenceAuditEventRepository.deleteAllByAuditEventDateBefore(LocalDateTime.now().minusDays(days));
			}catch(Exception e)
			{
				e.printStackTrace();
			}			
		}		
	}
}
