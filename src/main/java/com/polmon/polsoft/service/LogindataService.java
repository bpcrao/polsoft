package com.polmon.polsoft.service;

import com.polmon.polsoft.domain.Logindata;
import com.polmon.polsoft.repository.LogindataRepository;
import com.polmon.polsoft.repository.search.LogindataSearchRepository;
import com.polmon.polsoft.service.dto.LogindataDTO;
import com.polmon.polsoft.service.mapper.LogindataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Logindata.
 */
@Service
@Transactional
public class LogindataService {

    private final Logger log = LoggerFactory.getLogger(LogindataService.class);
    
    @Inject
    private LogindataRepository logindataRepository;

    @Inject
    private LogindataMapper logindataMapper;

    @Inject
    private LogindataSearchRepository logindataSearchRepository;

    /**
     * Save a logindata.
     *
     * @param logindataDTO the entity to save
     * @return the persisted entity
     */
    public LogindataDTO save(LogindataDTO logindataDTO) {
        log.debug("Request to save Logindata : {}", logindataDTO);
        Logindata logindata = logindataMapper.logindataDTOToLogindata(logindataDTO);
        logindata = logindataRepository.save(logindata);
        LogindataDTO result = logindataMapper.logindataToLogindataDTO(logindata);
        logindataSearchRepository.save(logindata);
        return result;
    }

    /**
     *  Get all the logindata.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<LogindataDTO> findAll() {
        log.debug("Request to get all Logindata");
        List<LogindataDTO> result = logindataRepository.findAll().stream()
            .map(logindataMapper::logindataToLogindataDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one logindata by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public LogindataDTO findOne(Long id) {
        log.debug("Request to get Logindata : {}", id);
        Logindata logindata = logindataRepository.findOne(id);
        LogindataDTO logindataDTO = logindataMapper.logindataToLogindataDTO(logindata);
        return logindataDTO;
    }
    
    /**
     *  Get one logindata by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public LogindataDTO findOneByUserId(Long userId) {
        log.debug("Request to get Logindata for userId: {}", userId);
        Logindata logindata = logindataRepository.findOneByUserId(userId);
        LogindataDTO logindataDTO = logindataMapper.logindataToLogindataDTO(logindata);
        return logindataDTO;
    }

    /**
     *  Delete the  logindata by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Logindata : {}", id);
        logindataRepository.delete(id);
        logindataSearchRepository.delete(id);
    }

    /**
     * Search for the logindata corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<LogindataDTO> search(String query) {
        log.debug("Request to search Logindata for query {}", query);
        return StreamSupport
            .stream(logindataSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(logindataMapper::logindataToLogindataDTO)
            .collect(Collectors.toList());
    }

	public void resetLoginData(Long id) {
		Logindata loginData = logindataRepository.findOneByUserId(id);
		 LogindataDTO loginDataDto = new LogindataDTO();
		 loginDataDto.setUserId(id);
		 LocalDate now = LocalDate.now();
		 loginDataDto.setFailedAttempts(0);
		 loginDataDto.setLastActiveDate(now);		 
		 if(loginData!=null){
			loginDataDto.setId(loginData.getId());			
		 }
		 this.save(loginDataDto);		
	}
}
