package com.polmon.polsoft.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.Authority;
import com.polmon.polsoft.domain.Licence;
import com.polmon.polsoft.domain.Resource;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.domain.UserPassword;
import com.polmon.polsoft.domain.UserPermission;
import com.polmon.polsoft.repository.AuthorityRepository;
import com.polmon.polsoft.repository.PersistentTokenRepository;
import com.polmon.polsoft.repository.UserPasswordRepository;
import com.polmon.polsoft.repository.UserRepository;
import com.polmon.polsoft.repository.search.UserSearchRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.ManagedUserDTO;
import com.polmon.polsoft.service.dto.UserDTO;
import com.polmon.polsoft.service.dto.UserPasswordDTO;
import com.polmon.polsoft.service.dto.UserPermissionDTO;
import com.polmon.polsoft.service.mapper.UserPermissionMapper;
import com.polmon.polsoft.service.util.RandomUtil;
import com.polmon.polsoft.web.rest.util.PasswordUtil;
import com.polmon.polsoft.service.dto.LogindataDTO;
/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private AuditEventPublisher auditPublisher;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserSearchRepository userSearchRepository;

  	@Inject
  	private  UserPasswordRepository userPasswordRepository;

    @Inject
    private LogindataService loginDataService;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private UserPermissionService userPermissionService;

    @Inject
    private UserPermissionMapper userPermissionMapper;
    
    String reason = null;
    public Optional<User> activateRegistration(String key, String reason) {
        log.debug("Activating user for activation key {}", key);
   //     String reason = null;
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
            	Map data= new HashMap<String, Object>();
            	data.put("user",user.getLogin()+" Reason:" +reason);
            	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "User is activated.", data));
                user.setActivated(true);
                user.setActivationKey(null);
                userRepository.save(user);
                userSearchRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public void deactivateRegistration(Long userId, String reason) {
    
        log.debug("Deactivating user for activation key {}", userId);
        userRepository.findOneById(userId)
            .map(user -> {
                user.setActivated(false);
                LogindataDTO loginDataDto = new LogindataDTO();
    			loginDataDto.setUserId(user.getId());
                user.setActivationKey(RandomUtil.generateActivationKey());
                userRepository.save(user);
                Map data= new HashMap<String, Object>();
                //data.put("user", user.getLogin());
                if(reason != null){
                	data.put("user", user.getLogin()+" Reason: "+ reason);
                }
                else 
                	data.put("user", user.getLogin()+" Reason: Maximum no.of attempts reached ");
            	auditPublisher.publish(new AuditEvent("admin","User Deactivated.", data));
                log.debug("Deactivated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
            .filter(user -> {
                ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
                return user.getResetDate().isAfter(oneDayAgo);
           })
           .map(user -> {
                user.setPassword(passwordEncoder.encode(PasswordUtil.getDecodedPassword(newPassword)));
                user.setResetKey(null);
                user.setResetDate(null);
                LocalDate today = LocalDate.now();
                if(user.getExpiryDate()==null || !user.getExpiryDate().minusDays(90).isAfter(today)){
                	user.setExpiryDate(today.plusDays(90));
                }
                userRepository.save(user);
                Map data = new HashMap<String, Object>();
                data.put("user", user.getLogin());
            	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "RESET_USER_PASSWORD", data));
                return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmail(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(ZonedDateTime.now());
                userRepository.save(user);
                return user;
            });
    }

    public Optional<User> requestPasswordResetById(Long userId) {
        return userRepository.findOneById(userId)
            .filter(User::getActivated)
            .map(user -> {
            	if(user.getResetKey() == null || user.getResetKey().isEmpty()){
	                user.setResetKey(RandomUtil.generateResetKey());
	                user.setResetDate(ZonedDateTime.now());
            	}
                return userRepository.save(user);
            });
    }

    public User createUserInformation(String login, String password, String firstName, String lastName, String email,
        String langKey) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne("Operator");
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(false);
        newUser.setExpiryDate(LocalDate.now().plusDays(365));
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }
    
    private void setOptimisticUserPermissions(User user, Set<Authority> authorities){
    	Set<UserPermission> userPermisions = new HashSet<UserPermission>();
    	Map<String,UserPermission> userPermissionMap= new HashMap<String,UserPermission>();
		authorities.forEach(authority -> {
			authority.getResources().forEach( resource -> {				
				if(!userPermissionMap.containsKey(resource.getName())){
					UserPermission up = new UserPermission();
					up.setName(resource.getName());
					up.setUser(user);
					up.setPermission(resource.getPermission());
					userPermisions.add(up);
				} else {
					UserPermission userPermission = userPermissionMap.get(resource.getName());
					if(userPermission.getPermission()<resource.getPermission()){
						userPermission.setPermission(resource.getPermission());
					}
				}
			});			
		});
		user.setUserPermissions(userPermisions);
    }

    public User createUser(ManagedUserDTO managedUserDTO) {
        User user = new User();
        user.setLogin(managedUserDTO.getLogin());
        user.setFirstName(managedUserDTO.getFirstName());
        user.setLastName(managedUserDTO.getLastName());
        user.setEmail(managedUserDTO.getEmail());
        if (managedUserDTO.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(managedUserDTO.getLangKey());
        }
        if (managedUserDTO.getAuthorities() != null) {
            Set<Authority> authorities = new HashSet<>();
            managedUserDTO.getAuthorities().stream().forEach(
                authority -> authorities.add(authorityRepository.findOne(authority))
            );
            user.setAuthorities(authorities);
            setOptimisticUserPermissions(user,authorities);            
        }
        
        String encryptedPassword = passwordEncoder.encode(managedUserDTO.getPassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(ZonedDateTime.now());
        user.setActivated(true);
        user.setExpiryDate(managedUserDTO.getExpiryDate());
        user.setIpaddresses(managedUserDTO.getIpaddresses());
    	UserPassword up = new UserPassword();
		up.setPassword(encryptedPassword);
		up.setUser(user);
		up.setCreatedDate(LocalDate.now());
		userPasswordRepository.save(up);

        userRepository.save(user);
        userSearchRepository.save(user);
        log.debug("Created Information for User: {}", user);
        Map data = new HashMap<String, Object>();
        data.put("user", user.getLogin()+" Reason: "+managedUserDTO.getReason());
    	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "New user created. ", data));
        return user;
    }

    public void updateUserInformation(String firstName, String lastName, String email, String langKey, LocalDate expiryDate, String ipaddress, 
    		String reason) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setLangKey(langKey);
            user.setExpiryDate(expiryDate);
            user.setIpaddresses(ipaddress);
            userRepository.save(user);
            userSearchRepository.save(user);
            Map data = new HashMap<String, Object>();
            data.put("user", user.getLogin()+" Reason: "+reason);
            auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "Updated user details", data));
            log.debug("Changed Information for User: {}", user);
        });
    }

    public void deleteUserInformation(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            userSearchRepository.delete(user);
            log.debug("Deleted User: {}", user);
            Map data = new HashMap<String, Object>();
            data.put("user", user.getLogin());
        	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "DELETED_USER", data));
           // auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "DELETED_USER", "Login ="+user.getLogin()));
        });
    }

	public String changePassword(String loginName, String password, String reason) {
		User user = userRepository.findOneByLogin(loginName).get();
		return checkRecentPasswordAndChange(password, user, true, reason);
	}

	public String changePassword(String password, String reason) {
		User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
		return checkRecentPasswordAndChange(password, user, true, reason);
	}
				
	public String checkRecentPasswordAndChange(String password, User user, Boolean doNotCheck, String reason) {
		if (user != null) {
			String encryptedPassword = passwordEncoder.encode(PasswordUtil.getDecodedPassword(password));
			List<UserPassword> userPasswords = userPasswordRepository.findAllByUserOrderByIdDesc(user);
			List<UserPassword> recentUserPasswords = userPasswords.size()>=3 ? userPasswords.subList(0, 2): userPasswords;
			List<UserPassword> filterPasswords = recentUserPasswords.stream()
					.filter(userPassword -> passwordEncoder.matches(password, userPassword.getPassword()))
					.collect(Collectors.toList());
			if (filterPasswords.size() == 0 || doNotCheck) {
				user.setPassword(encryptedPassword);
        if(doNotCheck && (user.getResetKey() == null || user.getResetKey().isEmpty())){
              user.setResetKey(RandomUtil.generateResetKey());
              user.setResetDate(ZonedDateTime.now());
              loginDataService.resetLoginData(user.getId());
        }
				userRepository.save(user);
				log.debug("Changed password for User: {}", user);
				UserPassword up = new UserPassword();
				up.setPassword(encryptedPassword);
				up.setUser(user);
				up.setCreatedDate(LocalDate.now());
				userPasswordRepository.save(up);
				Map data = new HashMap<String, Object>();
				if(reason != null){
                data.put("user", user.getLogin()+" Reason: "+reason);
				}
				else
					data.put("user", user.getLogin());
				auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "UPDATED_PASSWORD", data));
				return "OK";
			} else {
				Map data = new HashMap<String, Object>();
                data.put("user", user.getLogin());
            	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "PASSWORD_EXIST", data));
				//auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "PASSWORD EXIST", user.getLogin()));
				return "Password Exists";
			}
		}
		return "Failed";
	}


    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login).map(u -> {
            u.getAuthorities().size();
            return u;
        });
    }

    @Transactional(readOnly = true)
    public List<String> getUserWithEmailIds() {
        return userRepository.findByEmailIsNotNull().stream().map(user-> user.getLogin()).collect(Collectors.toList());
    }
    
    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        User user = userRepository.findOne(id);
        user.getAuthorities().size(); // eagerly load the association
        return user;
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
        user.getAuthorities().size(); // eagerly load the association
        user.setUserPermissions(userPermissionService.findAllByUser(user));
        user.getUserPermissions().size();
        user.getUserPermissions().stream().map(e -> userPermissionMapper.userPermissionToUserPermissionDTO(e)).collect(Collectors.toSet());
        return user;
    }

    @Transactional(readOnly = true)
    public UserDTO getUserWithAuthoritiesAndPermissions() {
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
        user.getAuthorities().size(); // eagerly load the association

        Set<UserPermissionDTO> userPermissions = userPermissionService.findAllByUser(user).stream().map(e -> userPermissionMapper.userPermissionToUserPermissionDTO(e)).collect(Collectors.toSet());
        UserDTO userDto = new UserDTO(user);

        Stream<Resource> resources = user.getAuthorities().stream().map(Authority::getResources).flatMap(Collection::stream);
        List<UserPermissionDTO> rolePermissions = resources.map(userPermissionMapper::resourceToUserPermissionDTO).collect(Collectors.toList());

		rolePermissions.stream().forEach(rolePermission -> {
			userPermissions.stream().filter(userPermission -> rolePermission.getName().equals(userPermission.getName())).forEach(userPerm -> {
						if (rolePermission.getPermission() > userPerm.getPermission()) {
							userPerm.setPermission(rolePermission.getPermission());
						}
					});
		});

        userDto.setUserPermissions(userPermissions);
        return userDto;
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     * </p>
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).stream().forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     * </p>
     */
    /*@Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        ZonedDateTime now = ZonedDateTime.now();
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
            userSearchRepository.delete(user);
        }
        auditPublisher.publish(new AuditEvent("SCHEDULER", "DELETED_INACTIVE_USER", 
        		users.stream().map(user-> {return user.getLogin();}).collect(Collectors.toList()).toString()));
    }

*/
	public void activateUser(Long id, String reason) {	
		User user = userRepository.findOne(id);
        user.setActivated(true); 
        Map data= new HashMap<String, Object>();
    	data.put("user",user.getLogin()+" Reason: "+reason);
    	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "User is activated", data));
	}
}