package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.EntityConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EntityConfig and its DTO EntityConfigDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EntityConfigMapper extends EntityMapper <EntityConfigDTO, EntityConfig> {
    
    
    default EntityConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        EntityConfig entityConfig = new EntityConfig();
        entityConfig.setId(id);
        return entityConfig;
    }
}
