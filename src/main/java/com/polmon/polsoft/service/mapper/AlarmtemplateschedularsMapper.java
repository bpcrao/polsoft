package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.AlarmtemplateschedularsDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Alarmtemplateschedulars and its DTO AlarmtemplateschedularsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AlarmtemplateschedularsMapper {

    AlarmtemplateschedularsDTO alarmtemplateschedularsToAlarmtemplateschedularsDTO(Alarmtemplateschedulars alarmtemplateschedulars);

    List<AlarmtemplateschedularsDTO> alarmtemplateschedularsToAlarmtemplateschedularsDTOs(List<Alarmtemplateschedulars> alarmtemplateschedulars);

    @Mapping(target = "alarmtemplates", ignore = true)
    Alarmtemplateschedulars alarmtemplateschedularsDTOToAlarmtemplateschedulars(AlarmtemplateschedularsDTO alarmtemplateschedularsDTO);

    List<Alarmtemplateschedulars> alarmtemplateschedularsDTOsToAlarmtemplateschedulars(List<AlarmtemplateschedularsDTO> alarmtemplateschedularsDTOs);
}
