package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.ThresholdDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Threshold and its DTO ThresholdDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ThresholdMapper {

    ThresholdDTO thresholdToThresholdDTO(Threshold threshold);

    List<ThresholdDTO> thresholdsToThresholdDTOs(List<Threshold> thresholds);

    Threshold thresholdDTOToThreshold(ThresholdDTO thresholdDTO);

    List<Threshold> thresholdDTOsToThresholds(List<ThresholdDTO> thresholdDTOs);
}
