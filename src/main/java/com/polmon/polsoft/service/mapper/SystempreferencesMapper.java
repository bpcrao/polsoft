package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.SystempreferencesDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Systempreferences and its DTO SystempreferencesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SystempreferencesMapper {

    SystempreferencesDTO systempreferencesToSystempreferencesDTO(Systempreferences systempreferences);

    List<SystempreferencesDTO> systempreferencesToSystempreferencesDTOs(List<Systempreferences> systempreferences);

    Systempreferences systempreferencesDTOToSystempreferences(SystempreferencesDTO systempreferencesDTO);

    List<Systempreferences> systempreferencesDTOsToSystempreferences(List<SystempreferencesDTO> systempreferencesDTOs);
}
