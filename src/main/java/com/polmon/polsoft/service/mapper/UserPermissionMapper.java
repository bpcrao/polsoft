package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.ResourceDTO;
import com.polmon.polsoft.service.dto.UserPermissionDTO;

import org.mapstruct.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for the entity UserPermission and its DTO UserPermissionDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface UserPermissionMapper {

    @Mapping(source = "user.id", target = "userId")
    UserPermissionDTO userPermissionToUserPermissionDTO(UserPermission userPermission);
    
    UserPermissionDTO resourceToUserPermissionDTO(Resource resource);

    @Mapping(source = "userId", target = "user")
    UserPermission userPermissionDTOToUserPermission(UserPermissionDTO userPermissionDTO);

    List<UserPermission> userPermissionDTOsToUserPermissions(List<UserPermissionDTO> userPermissionDTOs);
    
    
   
}
