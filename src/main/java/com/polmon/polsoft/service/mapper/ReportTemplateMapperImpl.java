package com.polmon.polsoft.service.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.polmon.polsoft.domain.ReportTemplate;
import com.polmon.polsoft.service.dto.ReportTemplateDTO;

@Component
public class ReportTemplateMapperImpl implements ReportTemplateMapper {

    @Override
    public ReportTemplateDTO reportTemplateToReportTemplateDTO(ReportTemplate reportTemplate) {
        if ( reportTemplate == null ) {
            return null;
        }

        ReportTemplateDTO reportTemplateDTO = new ReportTemplateDTO();

        reportTemplateDTO.setCreatedBy( reportTemplate.getCreatedBy() );
        reportTemplateDTO.setCreatedDate( reportTemplate.getCreatedDate() );
        reportTemplateDTO.setLastModifiedBy( reportTemplate.getLastModifiedBy() );
        reportTemplateDTO.setLastModifiedDate( reportTemplate.getLastModifiedDate() );
        reportTemplateDTO.setId( reportTemplate.getId() );
        reportTemplateDTO.setName( reportTemplate.getName() );
        reportTemplateDTO.setTimeFrame(reportTemplate.getTimeFrame());
        reportTemplateDTO.setSamplingData(reportTemplate.getSamplingData());
        //reportTemplateDTO.setLocations(Arrays.asList(reportTemplate.getSettings().split(",")).stream().map(str ->Long.parseLong(str)).collect(Collectors.toList()));
        reportTemplateDTO.setSettings(reportTemplate.getSettings());
        reportTemplateDTO.setSendEmail(Boolean.valueOf(reportTemplate.getSendEmail()));
        reportTemplateDTO.setReportDuration(reportTemplate.getReportDuration());
        reportTemplateDTO.setOwner(reportTemplate.getOwner());
        if(reportTemplate.getUsers()!=null)
        {
        	reportTemplateDTO.setUsers(Arrays.asList(reportTemplate.getUsers().split(",")).stream().collect(Collectors.toList()));
        }        
        return reportTemplateDTO;
    }

    @Override
    public List<ReportTemplateDTO> reportTemplatesToReportTemplateDTOs(List<ReportTemplate> reportTemplates) {
        if ( reportTemplates == null ) {
            return null;
        }

        List<ReportTemplateDTO> list = new ArrayList<ReportTemplateDTO>();
        for ( ReportTemplate reportTemplate : reportTemplates ) {
            list.add( reportTemplateToReportTemplateDTO( reportTemplate ) );
        }

        return list;
    }

    @Override
    public ReportTemplate reportTemplateDTOToReportTemplate(ReportTemplateDTO reportTemplateDTO) {
        if ( reportTemplateDTO == null ) {
            return null;
        }

        ReportTemplate reportTemplate = new ReportTemplate();

        reportTemplate.setCreatedBy( reportTemplateDTO.getCreatedBy() );
        reportTemplate.setCreatedDate( reportTemplateDTO.getCreatedDate() );
        reportTemplate.setLastModifiedBy( reportTemplateDTO.getLastModifiedBy() );
        reportTemplate.setLastModifiedDate( reportTemplateDTO.getLastModifiedDate() );
        reportTemplate.setId( reportTemplateDTO.getId() );
        reportTemplate.setName( reportTemplateDTO.getName() );
        String locationsString = reportTemplateDTO.getLocations().stream().map(Object::toString).collect(Collectors.joining(","));
        reportTemplate.setSettings(locationsString);
        reportTemplate.setSamplingData(reportTemplateDTO.getSamplingData());
        reportTemplate.setTimeFrame(reportTemplateDTO.getTimeFrame());
        reportTemplate.setSendEmail(Boolean.valueOf(reportTemplateDTO.getSendEmail()));
        reportTemplate.setReportDuration(reportTemplateDTO.getReportDuration());
        reportTemplate.setOwner(reportTemplateDTO.getOwner());
        if(reportTemplateDTO.getUsers()!=null)
        {
        	 String users =reportTemplateDTO.getUsers().stream().map(Object::toString).collect(Collectors.joining(","));
             reportTemplate.setUsers(users);
        }       
        return reportTemplate;
    }

    @Override
    public List<ReportTemplate> reportTemplateDTOsToReportTemplates(List<ReportTemplateDTO> reportTemplateDTOs) {
        if ( reportTemplateDTOs == null ) {
            return null;
        }

        List<ReportTemplate> list = new ArrayList<ReportTemplate>();
        for ( ReportTemplateDTO reportTemplateDTO : reportTemplateDTOs ) {
            list.add( reportTemplateDTOToReportTemplate( reportTemplateDTO ) );
        }

        return list;
    }
}
