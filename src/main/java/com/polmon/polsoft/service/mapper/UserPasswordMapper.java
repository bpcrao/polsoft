package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.UserPasswordDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity UserPassword and its DTO UserPasswordDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface UserPasswordMapper {

    @Mapping(source = "user.id", target = "userId")
    UserPasswordDTO userPasswordToUserPasswordDTO(UserPassword userPassword);

    List<UserPasswordDTO> userPasswordsToUserPasswordDTOs(List<UserPassword> userPasswords);

    @Mapping(source = "userId", target = "user")
    UserPassword userPasswordDTOToUserPassword(UserPasswordDTO userPasswordDTO);

    List<UserPassword> userPasswordDTOsToUserPasswords(List<UserPasswordDTO> userPasswordDTOs);
}
