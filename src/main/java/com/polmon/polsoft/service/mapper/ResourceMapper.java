package com.polmon.polsoft.service.mapper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Authority;
import com.polmon.polsoft.domain.Resource;
import com.polmon.polsoft.service.dto.ResourceDTO;

/**
 * Mapper for the entity Resource and its DTO ResourceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ResourceMapper {

    ResourceDTO resourceToResourceDTO(Resource resource);

    Set<ResourceDTO> resourcesToResourceDTOs(Set<Resource> resources);

    default Resource resourceDTOToResource(ResourceDTO resourceDTO) {
    	Resource res = new Resource();
        res.setId(resourceDTO.getId());
        res.setName(resourceDTO.getName());
        res.setPermission(resourceDTO.getPermission());

        return res;
    }

    default Set<Resource> resoursesFromResourceDTOs(Set<ResourceDTO> resourceDTOs) {
        return resourceDTOs.stream().map(resourceDTO -> {
            return resourceDTOToResource(resourceDTO);
        }).collect(Collectors.toSet());
    }
}