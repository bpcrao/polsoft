package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.AlarmtemplatelimitDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Alarmtemplatelimit and its DTO AlarmtemplatelimitDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AlarmtemplatelimitMapper {

    AlarmtemplatelimitDTO alarmtemplatelimitToAlarmtemplatelimitDTO(Alarmtemplatelimit alarmtemplatelimit);

    List<AlarmtemplatelimitDTO> alarmtemplatelimitsToAlarmtemplatelimitDTOs(List<Alarmtemplatelimit> alarmtemplatelimits);

    @Mapping(target = "alarmtemplates", ignore = true)
    Alarmtemplatelimit alarmtemplatelimitDTOToAlarmtemplatelimit(AlarmtemplatelimitDTO alarmtemplatelimitDTO);

    List<Alarmtemplatelimit> alarmtemplatelimitDTOsToAlarmtemplatelimits(List<AlarmtemplatelimitDTO> alarmtemplatelimitDTOs);
}
