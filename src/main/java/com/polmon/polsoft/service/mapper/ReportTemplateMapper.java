package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.ReportTemplateDTO;

import java.util.List;

/**
 * Mapper for the entity ReportTemplate and its DTO ReportTemplateDTO.
 */

public interface ReportTemplateMapper {

    ReportTemplateDTO reportTemplateToReportTemplateDTO(ReportTemplate reportTemplate);

    List<ReportTemplateDTO> reportTemplatesToReportTemplateDTOs(List<ReportTemplate> reportTemplates);

    ReportTemplate reportTemplateDTOToReportTemplate(ReportTemplateDTO reportTemplateDTO);

    List<ReportTemplate> reportTemplateDTOsToReportTemplates(List<ReportTemplateDTO> reportTemplateDTOs);
}
