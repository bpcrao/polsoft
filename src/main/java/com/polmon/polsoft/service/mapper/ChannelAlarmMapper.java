package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.ChannelAlarmDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ChannelAlarm and its DTO ChannelAlarmDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChannelAlarmMapper {

    @Mapping(source = "dchannel.id", target = "dchannelId")
    ChannelAlarmDTO channelAlarmToChannelAlarmDTO(ChannelAlarm channelAlarm);

    List<ChannelAlarmDTO> channelAlarmsToChannelAlarmDTOs(List<ChannelAlarm> channelAlarms);

    @Mapping(source = "dchannelId", target = "dchannel")
    ChannelAlarm channelAlarmDTOToChannelAlarm(ChannelAlarmDTO channelAlarmDTO);

    List<ChannelAlarm> channelAlarmDTOsToChannelAlarms(List<ChannelAlarmDTO> channelAlarmDTOs);

    default Dchannel dchannelFromId(Long id) {
        if (id == null) {
            return null;
        }
        Dchannel dchannel = new Dchannel();
        dchannel.setId(id);
        return dchannel;
    }
}
