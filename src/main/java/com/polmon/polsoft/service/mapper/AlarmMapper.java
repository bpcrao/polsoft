package com.polmon.polsoft.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Alarm;
import com.polmon.polsoft.service.dto.AlarmDTO;

/**
 * Mapper for the entity Alarm and its DTO AlarmDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface AlarmMapper {

    @Mapping(source = "user.id", target = "userId")
    AlarmDTO alarmToAlarmDTO(Alarm alarm);

    List<AlarmDTO> alarmsToAlarmDTOs(List<Alarm> alarms);

    @Mapping(source = "userId", target = "user")
    Alarm alarmDTOToAlarm(AlarmDTO alarmDTO);

    List<Alarm> alarmDTOsToAlarms(List<AlarmDTO> alarmDTOs);
   
}