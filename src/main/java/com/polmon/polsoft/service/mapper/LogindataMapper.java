package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.LogindataDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Logindata and its DTO LogindataDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface LogindataMapper {

    @Mapping(source = "user.id", target = "userId")
    LogindataDTO logindataToLogindataDTO(Logindata logindata);

    List<LogindataDTO> logindataToLogindataDTOs(List<Logindata> logindata);

    @Mapping(source = "userId", target = "user")
    Logindata logindataDTOToLogindata(LogindataDTO logindataDTO);

    List<Logindata> logindataDTOsToLogindata(List<LogindataDTO> logindataDTOs);
}
