package com.polmon.polsoft.service.mapper;

import org.mapstruct.Mapper;

import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.service.dto.DchannelDTO;

/**
 * Mapper for the entity Dchannel and its DTO DchannelDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public class DchannelSummaryMapper {

	public DchannelDTO dchannelToDchannelDTOSummary(Dchannel dchannel){
    	DchannelDTO channel = new DchannelDTO();
    	channel.setId(dchannel.getId());
    	channel.setName(dchannel.getName());
    	channel.setDeviceId(dchannel.getDevice().getId());
		return channel;    	
    }

}
