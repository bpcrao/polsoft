
package com.polmon.polsoft.service.mapper;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.service.dto.LocationDTO;

/**
 * Mapper for the entity Location and its DTO LocationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LocationMapper {

	default LocationDTO locationTLocationDTO(Location location) {
		LocationDTO locationDto = new LocationDTO();
		locationDto.setName(location.getName());
		locationDto.setHasData(location.getHasData());
		locationDto.setId(location.getId());
		locationDto.setLeaf(location.isLeaf());
		if (location.getParent() != null) {
			locationDto.setParentId(location.getParent().getId());
		}

		if (location.getDchannel() != null) {
			locationDto.setDchannelId(location.getDchannel().getId());
			locationDto.setChannelName(location.getDchannel().getName());
			locationDto.setChannelUnit(location.getDchannel().getUnit());
		}

		Set<LocationDTO> locations = Collections.emptySet();
		if (location.getChildren() != null) {
			locations = location.getChildren().stream().map(locatione -> this.locationTLocationDTO(locatione))
					.collect(Collectors.toSet());
		}
		if (location.getDchannel() != null && location.getDchannel().getDevice() != null) {
			Long deviceId = location.getDchannel().getDevice().getId();
			locationDto.setDeviceid(deviceId);
		}
		locationDto.setChildren(locations);
		locationDto.setUnit(location.getUnit());
		return locationDto;

	}

	default LocationDTO locationTAccessibleLocationDTO(Location location, Set<Location> accessibleLocations) {
		Set<Long> aIds = accessibleLocations.stream().map(locatione -> locatione.getId()).collect(Collectors.toSet());
		LocationDTO locationDto = new LocationDTO();
		locationDto.setName(location.getName());
		locationDto.setId(location.getId());
		locationDto.setLeaf(location.isLeaf());
		locationDto.setHasData(location.getHasData());
		if (location.getParent() != null) {
			locationDto.setParentId(location.getParent().getId());
		}
		if (location.getDchannel() != null) {
			locationDto.setDchannelId(location.getDchannel().getId());
			locationDto.setChannelName(location.getDchannel().getName());
			locationDto.setChannelUnit(location.getDchannel().getUnit());
			locationDto.setAlarmsActive(location.getAlarmsActive());
		}

		Set<LocationDTO> locations = Collections.emptySet();
		if (location.getChildren() != null) {
			locations = location.getChildren().stream().filter(locatione -> aIds.contains(locatione.getId()))
					.map(locatione -> this.locationTAccessibleLocationDTO(locatione, accessibleLocations))
					.collect(Collectors.toSet());
		}
		locationDto.setChildren(locations);
		locationDto.setUnit(location.getUnit());
		return locationDto;
	}

	List<LocationDTO> locationsToLocationDTOs(List<Location> locations);

	@Mapping(source = "parentId", target = "parent")
	@Mapping(source = "dchannelId", target = "dchannel")
	Location locationDTOToLocation(LocationDTO locationDTO);

	List<Location> locationDTOsToLocations(List<LocationDTO> locationDTOs);

	default Location locationFromId(Long id) {
		if (id == null) {
			return null;
		}
		Location location = new Location();
		location.setId(id);
		return location;
	}

	default Dchannel dchannelFromId(Long id) {
		if (id == null) {
			return null;
		}
		Dchannel dchannel = new Dchannel();
		dchannel.setId(id);
		return dchannel;
	}

}