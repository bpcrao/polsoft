package com.polmon.polsoft.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Alarmtemplate;
import com.polmon.polsoft.domain.Alarmtemplatelimit;
import com.polmon.polsoft.domain.Alarmtemplateschedulars;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.service.dto.AlarmtemplateDTO;

/**
 * Mapper for the entity Alarmtemplate and its DTO AlarmtemplateDTO.
 */
@Mapper(componentModel = "spring", uses = {AlarmtemplateschedularsMapper.class, AlarmtemplatelimitMapper.class, DchannelMapper.class})
public interface AlarmtemplateMapper {
	
	 @Mapping(source = "dchannel.id", target = "dchannelId") 
    AlarmtemplateDTO alarmtemplateToAlarmtemplateDTO(Alarmtemplate alarmtemplate);

    List<AlarmtemplateDTO> alarmtemplatesToAlarmtemplateDTOs(List<Alarmtemplate> alarmtemplates);

    @Mapping(source = "dchannelId", target = "dchannel")
    Alarmtemplate alarmtemplateDTOToAlarmtemplate(AlarmtemplateDTO alarmtemplateDTO);

    List<Alarmtemplate> alarmtemplateDTOsToAlarmtemplates(List<AlarmtemplateDTO> alarmtemplateDTOs);

    default Alarmtemplateschedulars alarmtemplateschedularsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Alarmtemplateschedulars alarmtemplateschedulars = new Alarmtemplateschedulars();
        alarmtemplateschedulars.setId(id);
        return alarmtemplateschedulars;
    }

    default Alarmtemplatelimit alarmtemplatelimitFromId(Long id) {
        if (id == null) {
            return null;
        }
        Alarmtemplatelimit alarmtemplatelimit = new Alarmtemplatelimit();
        alarmtemplatelimit.setId(id);
        return alarmtemplatelimit;
    }
    
    default Dchannel dchannelFromId(Long id) {
        if (id == null) {
            return null;
        }
        Dchannel dchannel = new Dchannel();
        dchannel.setId(id);
        return dchannel;
    }
}
