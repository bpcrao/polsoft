package com.polmon.polsoft.service.mapper;

import com.polmon.polsoft.domain.*;
import com.polmon.polsoft.service.dto.TemplateDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Template and its DTO TemplateDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TemplateMapper {

    TemplateDTO templateToTemplateDTO(Template template);

    List<TemplateDTO> templatesToTemplateDTOs(List<Template> templates);

    Template templateDTOToTemplate(TemplateDTO templateDTO);

    List<Template> templateDTOsToTemplates(List<TemplateDTO> templateDTOs);
}
