package com.polmon.polsoft.service.mapper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Authority;
import com.polmon.polsoft.domain.Resource;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.domain.UserPermission;
import com.polmon.polsoft.service.dto.ResourceDTO;
import com.polmon.polsoft.service.dto.UserDTO;
import com.polmon.polsoft.service.dto.UserPermissionDTO;

/**
 * Mapper for the entity User and its DTO UserDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserMapper {

    UserDTO userToUserDTO(User user);

    List<UserDTO> usersToUserDTOs(List<User> users);
    
//    List<UserPermissionDTO> userPermissionsToUserPermissionDTOs(List<UserPermission> users);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "persistentTokens", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "activationKey", ignore = true)    
    @Mapping(target = "resetDate", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "userPermissions", ignore = true)
    User userDTOToUser(UserDTO userDTO);

    List<User> userDTOsToUsers(List<UserDTO> userDTOs);

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

    default Set<String> stringsFromAuthorities (Set<Authority> authorities) {
        return authorities.stream().map(Authority::getName)
            .collect(Collectors.toSet());
    }

    default Set<Authority> authoritiesFromStrings(Set<String> strings) {
        return strings.stream().map(string -> {
            Authority auth = new Authority();
            auth.setName(string);
            return auth;
        }).collect(Collectors.toSet());
    }
    
    default Set<UserPermissionDTO> userPermissionsToUserPermissionDTOs(Set<UserPermission> userpermissions){
    	return userpermissions.stream().map(userPermission -> {
            return userPermissionToUserPermissionDTO(userPermission);
        }).collect(Collectors.toSet());
    }
    
    default UserPermissionDTO userPermissionToUserPermissionDTO(UserPermission userPermission){
    	UserPermissionDTO userPermissionDTO = new UserPermissionDTO();
    	userPermissionDTO.setId(userPermission.getId());
    	userPermissionDTO.setName(userPermission.getName());
    	userPermissionDTO.setPermission(userPermission.getPermission());
    	//userPermissionDTO.setUserD(userPermission.getUser());
    	return userPermissionDTO;
    }
//    
//    default Set<UserPermission> userPermissionsFromUserPermissionDTOs(Set<UserPermissionDTO> userpermissions){
//    	return userpermissions.stream().map(userPermission -> {
//            return userPermissionDTOToUserPermission(userPermission);
//        }).collect(Collectors.toSet());
//    }
//    
//    default UserPermissionDTO userPermissionToUserPermissionDTO(UserPermission userPermission) {
//    	UserPermissionDTO res = new UserPermissionDTO();
//        res.setId(resourceDTO.getId());
//        res.setName(resourceDTO.getName());
//        res.setPermission(resourceDTO.getPermission());
//        res.setAuthority(auth);
//
//        return res;
//    }
}
