package com.polmon.polsoft.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Alarms;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.service.dto.AlarmsDTO;

/**
 * Mapper for the entity Alarms and its DTO AlarmsDTO.
 */
@Mapper(componentModel = "spring", uses = {DchannelMapper.class})
public interface AlarmsMapper {
	
	@Mapping(source = "dchannel.id", target = "dchannelId")
    AlarmsDTO alarmsToAlarmsDTO(Alarms alarms);
	  
    List<AlarmsDTO> alarmsToAlarmsDTOs(List<Alarms> alarms);

    @Mapping(source = "dchannelId", target = "dchannel")
    Alarms alarmsDTOToAlarms(AlarmsDTO alarmsDTO);
    
    List<Alarms> alarmsDTOsToAlarms(List<AlarmsDTO> alarmsDTOs);
    
    default Dchannel dchannelFromId(Long id) {
        if (id == null) {
            return null;
        }
        Dchannel dchannel = new Dchannel();
        dchannel.setId(id);
        return dchannel;
    }
}
