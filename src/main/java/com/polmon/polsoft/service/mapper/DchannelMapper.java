package com.polmon.polsoft.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.service.dto.DchannelDTO;

/**
 * Mapper for the entity Dchannel and its DTO DchannelDTO.
 */
@Mapper(componentModel = "spring", uses = {ChannelAlarmMapper.class,AlarmtemplateMapper.class,AlarmsMapper.class})
public interface DchannelMapper {
	
 
    @Mapping(source = "device.id", target = "deviceId")
    @Mapping(target = "channelAlarms",source = "channelAlarms")
    DchannelDTO dchannelToDchannelDTO(Dchannel dchannel);
    

    List<DchannelDTO> dchannelsToDchannelDTOs(List<Dchannel> dchannels);

    @Mapping(source = "deviceId", target = "device")
    @Mapping(target = "channelAlarms",source = "channelAlarms")
    @Mapping(target = "channelAlarmTemplates",source = "channelAlarmTemplates")
    @Mapping(target = "location", ignore = true)
    Dchannel dchannelDTOToDchannel(DchannelDTO dchannelDTO);

    List<Dchannel> dchannelDTOsToDchannels(List<DchannelDTO> dchannelDTOs);

    default Device deviceFromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }

//	Set<Dchannel> dchannelsToDchannelDTOs(Set<Dchannel> channels);
    
    
}
