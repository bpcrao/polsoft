package com.polmon.polsoft.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.domain.Devicedata;
import com.polmon.polsoft.service.dto.DevicedataDTO;

/**
 * Mapper for the entity Devicedata and its DTO DevicedataDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DevicedataMapper {

    @Mapping(source = "device.id", target = "deviceId")
    DevicedataDTO devicedataToDevicedataDTO(Devicedata devicedata);

    List<DevicedataDTO> devicedataToDevicedataDTOs(List<Devicedata> devicedata);

    @Mapping(source = "deviceId", target = "device")
    Devicedata devicedataDTOToDevicedata(DevicedataDTO devicedataDTO) ;

    List<Devicedata> devicedataDTOsToDevicedata(List<DevicedataDTO> devicedataDTOs);

    default Device deviceFromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }
}
