package com.polmon.polsoft.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.domain.Resource;
import com.polmon.polsoft.repository.ResourceRepository;
import com.polmon.polsoft.repository.search.ResourceSearchRepository;
import com.polmon.polsoft.service.dto.ResourceDTO;
import com.polmon.polsoft.service.mapper.ResourceMapper;

/**
 * Service Implementation for managing Resource.
 */
@Service
@Transactional
public class ResourceService {

    private final Logger log = LoggerFactory.getLogger(ResourceService.class);
    
    @Inject
    private ResourceRepository resourceRepository;

    @Inject
    private ResourceMapper resourceMapper;

    @Inject
    private ResourceSearchRepository resourceSearchRepository;

    /**
     * Save a resource.
     *
     * @param resourceDTO the entity to save
     * @return the persisted entity
     */
    public ResourceDTO save(ResourceDTO resourceDTO) {
        log.debug("Request to save Resource : {}", resourceDTO);
        Resource resource = resourceMapper.resourceDTOToResource(resourceDTO);
        resource = resourceRepository.save(resource);
        ResourceDTO result = resourceMapper.resourceToResourceDTO(resource);
        resourceSearchRepository.save(resource);
        return result;
    }

    /**
     *  Get all the resources.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ResourceDTO> findAll() {
        log.debug("Request to get all Resources");
        List<ResourceDTO> result = resourceRepository.findAll().stream()
            .map(resourceMapper::resourceToResourceDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one resource by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ResourceDTO findOne(Long id) {
        log.debug("Request to get Resource : {}", id);
        Resource resource = resourceRepository.findOne(id);
        ResourceDTO resourceDTO = resourceMapper.resourceToResourceDTO(resource);
        return resourceDTO;
    }

    /**
     *  Delete the  resource by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Resource : {}", id);
        resourceRepository.delete(id);
        resourceSearchRepository.delete(id);
    }

    /**
     * Search for the resource corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ResourceDTO> search(String query) {
        log.debug("Request to search Resources for query {}", query);
        return StreamSupport
            .stream(resourceSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(resourceMapper::resourceToResourceDTO)
            .collect(Collectors.toList());
    }
}
