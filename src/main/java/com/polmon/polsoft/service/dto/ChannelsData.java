package com.polmon.polsoft.service.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ChannelsData {
	public ChannelsData(String deviceSerial, String date, String time) {
		this.deviceSerial = deviceSerial;
		this.channelValues = new HashMap<String, BigDecimal>();
		this.time = getTime(date, time);
	}

	public ChannelsData(String deviceSerial, String dTime) {
		this.deviceSerial = deviceSerial;
		this.channelValues = new HashMap<String, BigDecimal>();
		this.time = getTime(dTime);
	}

	private long getTime(String date, String time) {
		String toParseDate = date.split("=")[1].trim() + " " + time.split("=")[1].trim();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");
		LocalDateTime formatDateTime = LocalDateTime.parse(toParseDate, formatter);
		ZonedDateTime zdt = formatDateTime.atZone(ZoneId.systemDefault());
		formatDateTime.minusHours(5);
		formatDateTime.minusMinutes(30);
		
		this.created = zdt;

		this.day = zdt.getDayOfWeek().toString();
		this.hour = zdt.getHour();
		this.minute = zdt.getMinute();
		return zdt.toInstant().toEpochMilli();
	}

	private long getTime(String dateTime) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");
		LocalDateTime formatDateTime = LocalDateTime.parse(dateTime, formatter);
		ZonedDateTime zdt = formatDateTime.atZone(ZoneId.systemDefault());
		this.created = zdt;

		this.day = zdt.getDayOfWeek().toString();
		this.hour = zdt.getHour();
		this.minute = zdt.getMinute();
		return zdt.toInstant().toEpochMilli();
	}

	String deviceSerial;
	public Map<String, BigDecimal> channelValues;
	public long time;
	public String day;
	public int hour;
	public int minute;
	public ZonedDateTime created;

	public String getDeviceSerial() {
		// TODO Auto-generated method stub
		return this.deviceSerial;
	}
}