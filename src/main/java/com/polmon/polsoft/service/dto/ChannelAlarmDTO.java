package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;


/**
 * A DTO for the ChannelAlarm entity.
 */
public class ChannelAlarmDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private BigDecimal low;

    private BigDecimal high;

    private BigDecimal lowLow;

    private BigDecimal highHigh;


    private Long dchannelId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {
        this.low = low;
    }
    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {
        this.high = high;
    }
    public BigDecimal getLowLow() {
        return lowLow;
    }

    public void setLowLow(BigDecimal lowLow) {
        this.lowLow = lowLow;
    }
    public BigDecimal getHighHigh() {
        return highHigh;
    }

    public void setHighHigh(BigDecimal highHigh) {
        this.highHigh = highHigh;
    }

    public Long getDchannelId() {
        return dchannelId;
    }

    public void setDchannelId(Long dchannelId) {
        this.dchannelId = dchannelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChannelAlarmDTO channelAlarmDTO = (ChannelAlarmDTO) o;

        if ( ! Objects.equals(id, channelAlarmDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ChannelAlarmDTO{" +
            "id=" + id +
            ", low='" + low + "'" +
            ", high='" + high + "'" +
            ", lowLow='" + lowLow + "'" +
            ", highHigh='" + highHigh + "'" +
            '}';
    }
}
