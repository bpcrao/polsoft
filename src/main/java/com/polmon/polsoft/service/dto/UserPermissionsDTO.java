package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.List;


/**
 * A DTO for the UserPermission entity.
 */
public class UserPermissionsDTO implements Serializable {

	 private List<UserPermissionDTO> userPermissions;
	 
	

	public List<UserPermissionDTO> getUserPermissions() {
		return userPermissions;
	}	

	public void setUserPermissions(List<UserPermissionDTO> userPermissions) {
		this.userPermissions = userPermissions;
	}
	
	public void addUserPermission(UserPermissionDTO userPermission) {
		this.userPermissions.add(userPermission);
	}	
	
	
}
