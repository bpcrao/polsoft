package com.polmon.polsoft.service.dto;

import java.util.Date;
import java.util.List;

public class JobData {

	private String jobName;
	
	private String jobGroup;
	
	private String jobDesciption;
	
	private  String cronExpression;
	
	private  Date lastExceutionTime;
	
	private Date nextExecutionTime;
	
	private String className;
	
	private String status;
	
	private String  templateid ;


	public String getJobName() {
		return jobName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobDesciption() {
		return jobDesciption;
	}

	public void setJobDesciption(String jobDesciption) {
		this.jobDesciption = jobDesciption;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Date getLastExceutionTime() {
		return lastExceutionTime;
	}

	public void setLastExceutionTime(Date lastExceutionTime) {
		this.lastExceutionTime = lastExceutionTime;
	}

	public Date getNextExecutionTime() {
		return nextExecutionTime;
	}

	public void setNextExecutionTime(Date nextExecutionTime) {
		this.nextExecutionTime = nextExecutionTime;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTemplateid() {
		return templateid;
	}

	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}

}
