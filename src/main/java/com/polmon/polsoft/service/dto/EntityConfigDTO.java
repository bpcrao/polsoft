package com.polmon.polsoft.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EntityConfig entity.
 */
public class EntityConfigDTO implements Serializable {

    private Long id;

    private String entityName;

    private Boolean enableAudit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Boolean isEnableAudit() {
        return enableAudit;
    }

    public void setEnableAudit(Boolean enableAudit) {
        this.enableAudit = enableAudit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EntityConfigDTO entityConfigDTO = (EntityConfigDTO) o;
        if(entityConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), entityConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EntityConfigDTO{" +
            "id=" + getId() +
            ", entityName='" + getEntityName() + "'" +
            ", enableAudit='" + isEnableAudit() + "'" +
            "}";
    }
}
