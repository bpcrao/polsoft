package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 * A DTO for the Alarmtemplate entity.
 */
public class AlarmtemplateDTO implements Serializable {

    private Long id;

    private Boolean ackrequired;

    private Boolean commentsrequired;

    private String logicalmeasurepoint;

    private String logicaldefaultvalue;

    private Boolean activetimechecked;
    
    private boolean linkUnlinkalarmtemplate;
    
    public boolean isLinkUnlinkalarmtemplate() {
		return linkUnlinkalarmtemplate;
	}

	public void setLinkUnlinkalarmtemplate(boolean linkUnlinkalarmtemplate) {
		this.linkUnlinkalarmtemplate = linkUnlinkalarmtemplate;
	}

	private String name;

    private Long dchannelId;
    
	private Long dchannel_id;
    
    @ManyToMany(cascade = {CascadeType.ALL}) 
    @JoinTable
    private Set<AlarmtemplateschedularsDTO> alarmtemplateschedulars = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.ALL}) 
    @JoinTable
    private Set<AlarmtemplatelimitDTO> alarmtemplatelimits = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Boolean getAckrequired() {
        return ackrequired;
    }

    public void setAckrequired(Boolean ackrequired) {
        this.ackrequired = ackrequired;
    }
    public Boolean getCommentsrequired() {
        return commentsrequired;
    }

    public void setCommentsrequired(Boolean commentsrequired) {
        this.commentsrequired = commentsrequired;
    }
    public String getLogicalmeasurepoint() {
        return logicalmeasurepoint;
    }

    public void setLogicalmeasurepoint(String logicalmeasurepoint) {
        this.logicalmeasurepoint = logicalmeasurepoint;
    }
    public String getLogicaldefaultvalue() {
        return logicaldefaultvalue;
    }

    public void setLogicaldefaultvalue(String logicaldefaultvalue) {
        this.logicaldefaultvalue = logicaldefaultvalue;
    }
    public Boolean getActivetimechecked() {
        return activetimechecked;
    }

    public void setActivetimechecked(Boolean activetimechecked) {
        this.activetimechecked = activetimechecked;
    }

    public Set<AlarmtemplateschedularsDTO> getAlarmtemplateschedulars() {
        return alarmtemplateschedulars;
    }

    public void setAlarmtemplateschedulars(Set<AlarmtemplateschedularsDTO> alarmtemplateschedulars) {
        this.alarmtemplateschedulars = alarmtemplateschedulars;
    }

    public Set<AlarmtemplatelimitDTO> getAlarmtemplatelimits() {
        return alarmtemplatelimits;
    }

    public void setAlarmtemplatelimits(Set<AlarmtemplatelimitDTO> alarmtemplatelimits) {
        this.alarmtemplatelimits = alarmtemplatelimits;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AlarmtemplateDTO alarmtemplateDTO = (AlarmtemplateDTO) o;

        if ( ! Objects.equals(id, alarmtemplateDTO.id)) return false;

        return true;
    }

    public Long getDchannelId() {
        return dchannelId;
    }

    public void setDchannelId(Long dchannelId) {
        this.dchannelId = dchannelId;
        this.dchannel_id = dchannelId;
    }

    public Long getDchannel_id() {
		return dchannelId;
	}

	public void setDchannel_id() {
		this.dchannel_id = dchannelId;
	}
	
    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AlarmtemplateDTO{" +
            "id=" + id +
            ", ackrequired='" + ackrequired + "'" +
            ", commentsrequired='" + commentsrequired + "'" +
            ", logicalmeasurepoint='" + logicalmeasurepoint + "'" +
            ", logicaldefaultvalue='" + logicaldefaultvalue + "'" +
            ", activetimechecked='" + activetimechecked + "'" +
            '}';
    }
}
