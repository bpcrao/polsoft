package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the UserPermission entity.
 */
public class UserPermissionDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private Integer permission;

    private String name;


    private Long userId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getPermission() {
        return permission;
    }

    public void setPermission(Integer permission) {
        this.permission = permission;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserPermissionDTO userPermissionDTO = (UserPermissionDTO) o;

        if ( ! Objects.equals(id, userPermissionDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "UserPermissionDTO{" +
            "id=" + id +
            ", permission='" + permission + "'" +
            ", name='" + name + "'" +
            '}';
    }
}
