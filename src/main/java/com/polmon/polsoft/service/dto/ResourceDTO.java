package com.polmon.polsoft.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Resource entity.
 */
public class ResourceDTO extends AbstractAuditingDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6662054694951862840L;

	private Long id;

    @NotNull
    private String name;

    private Integer permission;
    
    private String authority_name;
    
    public String getAuthority_name() {
		return authority_name;
	}

	public void setAuthority_name(String authority_name) {
		this.authority_name = authority_name;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Integer getPermission() {
        return permission;
    }

    public void setPermission(Integer permission) {
        this.permission = permission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ResourceDTO resourceDTO = (ResourceDTO) o;

        if ( ! Objects.equals(name, resourceDTO.name) || ! Objects.equals(authority_name, resourceDTO.authority_name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ResourceDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", permission='" + permission + "'" +
            '}';
    }
}
