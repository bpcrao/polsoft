package com.polmon.polsoft.service.dto;

import java.math.BigDecimal;

public class LocationOverviewDTO {
	private Long locationId;
	private double minValue;
	private double maxValue;
	private double stdDevValue;
	private double meanValue;
	private double currValue;

	private String time;
	private Long samples;
	
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public double getMinValue() {
		return minValue;
	}
	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}
	public double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}
	public double getStdDevValue() {
		return stdDevValue;
	}
	public void setStdDevValue(double stdDevValue) {
		this.stdDevValue = stdDevValue;
	}
	public double getMeanValue() {
		return meanValue;
	}
	public void setMeanValue(double meanValue) {
		this.meanValue = meanValue;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Long getSamples() {
		return samples;
	}
	public void setSamples(Long samples) {
		this.samples = samples;
	}
	public double getCurrValue() {
		return currValue;
	}
	public void setCurrValue(double currValue) {
		this.currValue = currValue;
	}

}
