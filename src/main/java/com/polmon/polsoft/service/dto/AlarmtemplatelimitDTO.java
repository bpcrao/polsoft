package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Alarmtemplatelimit entity.
 */
public class AlarmtemplatelimitDTO implements Serializable {

    private Long id;

    private Double highlimit;

    private Double lowlimit;

    private Double roc;

    private String schedular;

    private Long delay;

    private Boolean email;
    private Boolean sms;
    
    private String emailids;
    private String mobilenumbers;
    
    private String type;
    
    private Integer triggerset;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Double getHighlimit() {
        return highlimit;
    }

    public void setHighlimit(Double highlimit) {
        this.highlimit = highlimit;
    }
    public Double getLowlimit() {
        return lowlimit;
    }

    public void setLowlimit(Double lowlimit) {
        this.lowlimit = lowlimit;
    }
    public Double getRoc() {
    	if(roc == null)
    		return 0.0;
        return roc;
    }

    public void setRoc(Double roc) {
        this.roc = roc;
    }
    public String getSchedular() {
        return schedular;
    }

    public void setSchedular(String schedular) {
        this.schedular = schedular;
    }
    public Long getDelay() {
        return delay;
    }

    public void setDelay(Long delay) {
        this.delay = delay;
    }

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTriggerset() {
		return triggerset;
	}

	public void setTriggerset(Integer triggerset) {
		this.triggerset = triggerset;
	}

	@Override
    public boolean equals(Object o) {
		if(id == null)
			return false;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AlarmtemplatelimitDTO alarmtemplatelimitDTO = (AlarmtemplatelimitDTO) o;

        if ( ! Objects.equals(id, alarmtemplatelimitDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    public Boolean getEmail() {
		return email;
	}

	public void setEmail(Boolean email) {
		this.email = email;
	}

	public Boolean getSms() {
		return sms;
	}

	public void setSms(Boolean sms) {
		this.sms = sms;
	}

	public String getEmailids() {
		return emailids;
	}

	public void setEmailids(String emailids) {
		this.emailids = emailids;
	}

	public String getMobilenumbers() {
		return mobilenumbers;
	}

	public void setMobilenumbers(String mobilenumbers) {
		this.mobilenumbers = mobilenumbers;
	}

	@Override
    public String toString() {
        return "AlarmtemplatelimitDTO{" +
            "id=" + id +
            ", highlimit='" + highlimit + "'" +
            ", lowlimit='" + lowlimit + "'" +
            ", roc='" + roc + "'" +
            ", schedular='" + schedular + "'" +
            ", delay='" + delay + "'" +
            ", type ='" + type + "'" +
            ", triggerset ='" + triggerset + "'" +
            '}';
    }
}
