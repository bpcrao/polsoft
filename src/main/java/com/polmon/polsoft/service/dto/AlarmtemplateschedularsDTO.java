package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Alarmtemplateschedulars entity.
 */
public class AlarmtemplateschedularsDTO implements Serializable {

    private Long id;

    private String day;

    private String starttime;

    private String endtime;
    private boolean checked = true;

    public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = true;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
    
    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }
    
    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    @Override
    public boolean equals(Object o) {
    	if(id == null)
    		return false;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AlarmtemplateschedularsDTO alarmtemplateschedularsDTO = (AlarmtemplateschedularsDTO) o;

        if ( ! Objects.equals(id, alarmtemplateschedularsDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AlarmtemplateschedularsDTO{" +
            "id=" + id +
            ", day='" + day + "'" +
            ", starttime='" + starttime + "'" +
            ", endtime='" + endtime + "'" +
            '}';
    }
}
