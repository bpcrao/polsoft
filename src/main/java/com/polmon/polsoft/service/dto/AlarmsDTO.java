package com.polmon.polsoft.service.dto;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import com.polmon.polsoft.domain.AlarmSpecialValues;

/**
 * A DTO for the Alarms entity.
 */
public class AlarmsDTO extends AbstractAuditingDTO implements Serializable, Cloneable{

	private Long id;

	private String type;

	private String description;

	private String smsDescritption;
	
	private String duration = "0hr 0min 0sec";

	private ZonedDateTime created;
	private ZonedDateTime updated;
	private ZonedDateTime deleted;

	private String source;

	private String action;

	private String comments;

	private String username;

	private String status;

	private String severity;

	private String ackStatus;

	private ZonedDateTime acknowledged;

	private Long delay;

	private Boolean ackrequired;

	private Boolean commentsrequired;

	private Long dchannelId;

	private String alarmdata;

	private ZonedDateTime generated;
	private ZonedDateTime ended;

	private String emailIds;
	private String mobileNumbers;

	private String state;
	
	private Boolean special = false;
	
	public Boolean getSpecial() {
		return special;
	}

	public void setSpecial(Boolean special) {
		this.special = special;
	}

	public ZonedDateTime getEnded() {
		return ended;
	}

	public void setEnded(ZonedDateTime ended) {
		this.ended = ended;
	}

	public ZonedDateTime getGenerated() {
		return generated;
	}

	public void setGenerated(ZonedDateTime generated) {
		this.generated = generated;
	}

	public String getAlarmdata() {
		return alarmdata;
	}

	public void setAlarmdata(String alarmdata) {
		this.alarmdata = alarmdata;
	}

	public Long getDchannelId() {
		return dchannelId;
	}

	public void setDchannelId(Long dchannelId) {
		this.dchannelId = dchannelId;
	}

	private Long dchannel_id;

	public Long getDchannel_id() {
		return dchannelId;
	}

	public void setDchannel_id(Long dchannel_id) {
		this.dchannel_id = dchannel_id;
		this.dchannelId = dchannel_id;
	}

	public Long getDelay() {
		return delay;
	}

	public void setDelay(Long delay) {
		this.delay = delay;
	}

	public Boolean getAckrequired() {
		return ackrequired;
	}

	public void setAckrequired(Boolean ackrequired) {
		this.ackrequired = ackrequired;
	}

	public Boolean getCommentsrequired() {
		return commentsrequired;
	}

	public void setCommentsrequired(Boolean commentsrequired) {
		this.commentsrequired = commentsrequired;
	}

	public ZonedDateTime getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(ZonedDateTime acknowledged) {
		this.acknowledged = acknowledged;
	}

	public String getAckStatus() {
		return ackStatus;
	}

	public void setAckStatus(String ackStatus) {
		this.ackStatus = ackStatus;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getSmsDescritption() {
		return smsDescritption;
	}

	public void setSmsDescritption(String smsDescritption) {
		this.smsDescritption = smsDescritption;
	}
	
	public String getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(String emailIds) {
		this.emailIds = emailIds;
	}

	public String getMobileNumbers() {
		return mobileNumbers;
	}

	public void setMobileNumbers(String mobileNumbers) {
		this.mobileNumbers = mobileNumbers;
	}

	public String getDuration() {
		if (duration == null) {
			return getCalculatedDuration();
		}
		return duration;
	}

	private String getCalculatedDuration() {
		ZonedDateTime now = ZonedDateTime.now();
		long elapsedTime = Duration.between(generated, ended==null ? now : ended).toMillis();
		elapsedTime = elapsedTime / 1000;
		if(elapsedTime <= 0)
			elapsedTime = 0;
		int seconds = (int)elapsedTime % 60;
		int hours = (int)elapsedTime / 3600; 
		int minutes = ((int)elapsedTime %3600)/60; 
		int seconds_output = (seconds% 3600)%60; 
		String time =  Integer.toString(hours) + "hr " + Integer.toString(minutes) + "min " 
		+ Integer.toString(seconds_output) + "sec";
		
		return time;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AlarmsDTO alarmsDTO = (AlarmsDTO) o;

		if (!Objects.equals(id, alarmsDTO.id))
			return false;

		return true;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public ZonedDateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(ZonedDateTime deleted) {
		this.deleted = deleted;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "AlarmsDTO [id=" + id + ", type=" + type + ", description=" + description + ", duration=" + duration
				+ ", created=" + created + ", updated=" + updated + ", deleted=" + deleted + ", source=" + source
				+ ", action=" + action + ", comments=" + comments + ", username=" + username + ", status=" + status
				+ ", severity=" + severity + ", ackStatus=" + ackStatus + ", acknowledged=" + acknowledged + ", delay="
				+ delay + ", ackrequired=" + ackrequired + ", commentsrequired=" + commentsrequired + ", dchannelId="
				+ dchannelId + ", alarmdata=" + alarmdata + ", generated=" + generated + ", ended=" + ended
				+ ", emailIds=" + emailIds + ", mobileNumbers=" + mobileNumbers + ", state=" + state + ", dchannel_id="
				+ dchannel_id + "]";
	}
	
	public String getSummary() {
	return  "status=" + status
			+ ", severity=" + severity 
			+ ", ackStatus=" + ackStatus 
			+ ", acknowledged=" + acknowledged 
			+ ", location=" + this.getSource(); 	
	}
	
	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	public String calculateDuration() {
		ZonedDateTime now = ZonedDateTime.now();
		long elapsedTime = Duration.between(generated, ended==null ? now : ended).toMillis();
		elapsedTime = elapsedTime / 1000;
		if(elapsedTime <= 0)
			elapsedTime = 0;
		int seconds = (int)elapsedTime % 60;
		int hours = (int)elapsedTime / 3600; 
		int minutes = ((int)elapsedTime %3600)/60; 
		int seconds_output = (seconds% 3600)%60; 
		String time =  Integer.toString(hours) + "hr " + Integer.toString(minutes) + "min " 
		+ Integer.toString(seconds_output) + "sec";
		
		return time;
	}

	public void setCalculatedDuration() {
		setDuration(calculateDuration());		
	}

	public boolean isActive() {
		return this.getStatus().equalsIgnoreCase("active");
	}

	public boolean isSpecial() {
		return this.getState().equalsIgnoreCase("special");
	}

	public boolean isDescription(AlarmSpecialValues specialValues) {
		return this.getDescription().equalsIgnoreCase(specialValues.getDescription());
	}
}
