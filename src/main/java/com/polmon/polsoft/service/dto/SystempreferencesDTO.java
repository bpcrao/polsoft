package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Systempreferences entity.
 */
public class SystempreferencesDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String data;

    private String keyproperty;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    public String getKeyproperty() {
        return keyproperty;
    }

    public void setKeyproperty(String keyproperty) {
        this.keyproperty = keyproperty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SystempreferencesDTO systempreferencesDTO = (SystempreferencesDTO) o;

        if ( ! Objects.equals(id, systempreferencesDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SystempreferencesDTO{" +
            "id=" + id +
            ", data='" + data + "'" +
            ", keyproperty='" + keyproperty + "'" +
            '}';
    }
}
