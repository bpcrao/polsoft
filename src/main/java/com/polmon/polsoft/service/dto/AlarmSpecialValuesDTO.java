package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class AlarmSpecialValuesDTO implements Serializable {

	private Long id;
	
	private String name;
	
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private Long value;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AlarmSpecialValuesDTO alarmsDTO = (AlarmSpecialValuesDTO) o;

		if (!Objects.equals(id, alarmsDTO.id))
			return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}
}
