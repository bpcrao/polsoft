package com.polmon.polsoft.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Template entity.
 */
public class TemplateDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private String templateName;

    private String thresholds;

    private String sms;

    private String emails;

    @NotNull
    private Boolean acknowledgeWithComment;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
    public String getThresholds() {
        return thresholds;
    }

    public void setThresholds(String thresholds) {
        this.thresholds = thresholds;
    }
    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }
    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }
    public Boolean getAcknowledgeWithComment() {
        return acknowledgeWithComment;
    }

    public void setAcknowledgeWithComment(Boolean acknowledgeWithComment) {
        this.acknowledgeWithComment = acknowledgeWithComment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TemplateDTO templateDTO = (TemplateDTO) o;

        if ( ! Objects.equals(id, templateDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TemplateDTO{" +
            "id=" + id +
            ", templateName='" + templateName + "'" +
            ", thresholds='" + thresholds + "'" +
            ", sms='" + sms + "'" +
            ", emails='" + emails + "'" +
            ", acknowledgeWithComment='" + acknowledgeWithComment + "'" +
            '}';
    }
}
