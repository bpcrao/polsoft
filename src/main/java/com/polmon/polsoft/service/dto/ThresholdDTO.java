package com.polmon.polsoft.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Threshold entity.
 */
public class ThresholdDTO implements Serializable {

    private Long id;

    @NotNull
    private String thresholdName;

    @NotNull
    private String parameter;

    @NotNull
    private Double value;

    @NotNull
    private String severity;

    @NotNull
    private Float lowerLimitAlarm;

    @NotNull
    private Float upperLimitAlarm;

    @NotNull
    private Float lowerLimitWarning;

    @NotNull
    private Float upperLimitWarning;

    @NotNull
    private Boolean warningEnabled;

    @NotNull
    private Boolean alarmEnabled;

    @NotNull
    private Float warningHysteresis;

    @NotNull
    private Float alarmHysteresis;

    @NotNull
    private Float alarmDelay;

    private Float warningDelay;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getThresholdName() {
        return thresholdName;
    }

    public void setThresholdName(String thresholdName) {
        this.thresholdName = thresholdName;
    }
    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }
    public Float getLowerLimitAlarm() {
        return lowerLimitAlarm;
    }

    public void setLowerLimitAlarm(Float lowerLimitAlarm) {
        this.lowerLimitAlarm = lowerLimitAlarm;
    }
    public Float getUpperLimitAlarm() {
        return upperLimitAlarm;
    }

    public void setUpperLimitAlarm(Float upperLimitAlarm) {
        this.upperLimitAlarm = upperLimitAlarm;
    }
    public Float getLowerLimitWarning() {
        return lowerLimitWarning;
    }

    public void setLowerLimitWarning(Float lowerLimitWarning) {
        this.lowerLimitWarning = lowerLimitWarning;
    }
    public Float getUpperLimitWarning() {
        return upperLimitWarning;
    }

    public void setUpperLimitWarning(Float upperLimitWarning) {
        this.upperLimitWarning = upperLimitWarning;
    }
    public Boolean getWarningEnabled() {
        return warningEnabled;
    }

    public void setWarningEnabled(Boolean warningEnabled) {
        this.warningEnabled = warningEnabled;
    }
    public Boolean getAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(Boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }
    public Float getWarningHysteresis() {
        return warningHysteresis;
    }

    public void setWarningHysteresis(Float warningHysteresis) {
        this.warningHysteresis = warningHysteresis;
    }
    public Float getAlarmHysteresis() {
        return alarmHysteresis;
    }

    public void setAlarmHysteresis(Float alarmHysteresis) {
        this.alarmHysteresis = alarmHysteresis;
    }
    public Float getAlarmDelay() {
        return alarmDelay;
    }

    public void setAlarmDelay(Float alarmDelay) {
        this.alarmDelay = alarmDelay;
    }
    public Float getWarningDelay() {
        return warningDelay;
    }

    public void setWarningDelay(Float warningDelay) {
        this.warningDelay = warningDelay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ThresholdDTO thresholdDTO = (ThresholdDTO) o;

        if ( ! Objects.equals(id, thresholdDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ThresholdDTO{" +
            "id=" + id +
            ", thresholdName='" + thresholdName + "'" +
            ", parameter='" + parameter + "'" +
            ", value='" + value + "'" +
            ", severity='" + severity + "'" +
            ", lowerLimitAlarm='" + lowerLimitAlarm + "'" +
            ", upperLimitAlarm='" + upperLimitAlarm + "'" +
            ", lowerLimitWarning='" + lowerLimitWarning + "'" +
            ", upperLimitWarning='" + upperLimitWarning + "'" +
            ", warningEnabled='" + warningEnabled + "'" +
            ", alarmEnabled='" + alarmEnabled + "'" +
            ", warningHysteresis='" + warningHysteresis + "'" +
            ", alarmHysteresis='" + alarmHysteresis + "'" +
            ", alarmDelay='" + alarmDelay + "'" +
            ", warningDelay='" + warningDelay + "'" +
            '}';
    }
}
