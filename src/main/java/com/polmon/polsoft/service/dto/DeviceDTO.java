package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.databind.node.ObjectNode;


/**
 * A DTO for the Device entity.
 */
public class DeviceDTO extends AbstractAuditingDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6638570488297059971L;

	private Long id;
    
    private Integer deviceChannelsStart;

	private String name;

    private String type;

    private String settings;

    private Boolean status;

    private ZonedDateTime lastRefresh;

    private String serial;

    private String location;

    private Set<ObjectNode> channels;
    
    private boolean applyall;
  
    private ObjectNode selectedChannel;
    
    private String ipAddress;
    
    private String deviceHealth;

	public String getDeviceHealth() {
		return deviceHealth;
	}

	public void setDeviceHealth(String deviceHealth) {
		this.deviceHealth = deviceHealth;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Set<ObjectNode> getChannels() {
		return channels;
	}

	public void setChannels(Set<ObjectNode> channels) {
		this.channels = channels;
	}

	private Long blockId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    public ZonedDateTime getLastRefresh() {
        return lastRefresh;
    }

    public void setLastRefresh(ZonedDateTime lastRefresh) {
        this.lastRefresh = lastRefresh;
    }
    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }
    
    public Integer getDeviceChannelsStart() {
		return deviceChannelsStart;
	}

	public void setDeviceChannelsStart(Integer deviceChannelsStart) {
		this.deviceChannelsStart = deviceChannelsStart;
	}
 
	 public boolean isApplyall() {
			return applyall;
		}

		public void setApplyall(boolean applyall) {
			this.applyall = applyall;
		}

		public ObjectNode getSelectedChannel() {
			return selectedChannel;
		}

		public void setSelectedChannel(ObjectNode selectedChannel) {
			this.selectedChannel = selectedChannel;
		}

		@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceDTO deviceDTO = (DeviceDTO) o;

        if ( ! Objects.equals(id, deviceDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DeviceDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", type='" + type + "'" +
            ", settings='" + settings + "'" +
            ", status='" + status + "'" +
            ", lastRefresh='" + lastRefresh + "'" +
            ", serial='" + serial + "'" +
            ", location='" + location + "'" +
            '}';
    }
}
