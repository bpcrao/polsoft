package com.polmon.polsoft.service.dto;

import java.util.Date;
import java.util.Map;

import org.springframework.boot.actuate.audit.AuditEvent;

public class CustomAuditEvent extends AuditEvent {
	
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private String auditEventSource;

	private Long entityId;
	
    private String entityValue;

	private Integer commitVersion;

	private String reviewedBy;

	public CustomAuditEvent(Date timestamp, String principal, String type, Map<String, Object> data) {
		super(timestamp, principal, type, data);
	}

	public String getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public Integer getCommitVersion() {
		return commitVersion;
	}

	public void setCommitVersion(Integer commitVersion) {
		this.commitVersion = commitVersion;
	}

	public String getAuditEventSource() {
		return auditEventSource;
	}

	public void setAuditEventSource(String auditEventSource) {
		this.auditEventSource = auditEventSource;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityValue() {
		return entityValue;
	}

	public void setEntityValue(String entityValue) {
		this.entityValue = entityValue;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4140293481866154535L;

}
