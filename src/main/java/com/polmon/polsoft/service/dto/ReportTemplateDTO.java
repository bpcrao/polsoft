package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;


/**
 * A DTO for the ReportTemplate entity.
 */
public class ReportTemplateDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String name;

    private String timeFrame;
    
    private int samplingData;
    
    private List<Long> locations;
    
    private String settings;
    
    private int reportDuration;
    
    private Boolean sendEmail = Boolean.FALSE;
    
    private List<String> users;
    
    private String owner;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeFrame() {
		return timeFrame;
	}

	public void setTimeFrame(String timeFrame) {
		this.timeFrame = timeFrame;
	}

	public int getSamplingData() {
		return samplingData;
	}

	public void setSamplingData(int samplingData) {
		this.samplingData = samplingData;
	}

	public List<Long> getLocations() {
		return locations;
	}

	public void setLocations(List<Long> locations) {
		this.locations = locations;
	}

	public String getSettings() {
		return settings;
	}

	public void setSettings(String settings) {
		this.settings = settings;
	}

	public int getReportDuration() {
		return reportDuration;
	}

	public void setReportDuration(int reportDuration) {
		this.reportDuration = reportDuration;
	}

	public Boolean getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}	
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportTemplateDTO reportTemplateDTO = (ReportTemplateDTO) o;

        if ( ! Objects.equals(id, reportTemplateDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ReportTemplateDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", " +
            '}';
    }
}
