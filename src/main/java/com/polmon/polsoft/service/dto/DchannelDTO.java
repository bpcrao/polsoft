package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * A DTO for the Dchannel entity.
 */
public class DchannelDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String name;

    private String unit;

    private String channeldata;

    private Set<ChannelAlarmDTO> channelAlarms = new HashSet<>();
    private Set<AlarmtemplateDTO> channelAlarmTemplates = new HashSet<>();

	public Set<AlarmtemplateDTO> getChannelAlarmTemplates() {
		return channelAlarmTemplates;
	}

	public void setChannelAlarmTemplates(Set<AlarmtemplateDTO> channelAlarmTemplates) {
		this.channelAlarmTemplates = channelAlarmTemplates;
	}

	public Set<ChannelAlarmDTO> getChannelAlarms() {
		return channelAlarms;
	}

	public void setChannelAlarms(Set<ChannelAlarmDTO> channelAlarms) {
		this.channelAlarms = channelAlarms;
	}

	private Long deviceId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public String getChanneldata() {
        return channeldata;
    }

    public void setChanneldata(String channeldata) {
        this.channeldata = channeldata;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DchannelDTO dchannelDTO = (DchannelDTO) o;

        if ( ! Objects.equals(id, dchannelDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
	public String toString() {
		return "DchannelDTO [id=" + id + ", name=" + name + ", unit=" + unit + ", channeldata=" + channeldata
				+ ", channelAlarms=" + channelAlarms + ", channelAlarmTemplates=" + channelAlarmTemplates + ", deviceId=" + deviceId + "]";
	}
}
