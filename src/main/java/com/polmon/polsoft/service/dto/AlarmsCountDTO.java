package com.polmon.polsoft.service.dto;

public class AlarmsCountDTO {

	private String severity;
	private Long alarms_count;

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public Long getAlarms_count() {
		return alarms_count;
	}

	public void setAlarms_count(Long alarms_count) {
		this.alarms_count = alarms_count;
	}
}
