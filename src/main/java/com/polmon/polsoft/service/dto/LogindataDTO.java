package com.polmon.polsoft.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Logindata entity.
 */
public class LogindataDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private Integer failedAttempts;

    private LocalDate lastActiveDate;


    private Long userId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getFailedAttempts() {
        return failedAttempts;
    }

    public void setFailedAttempts(Integer failedAttempts) {
        this.failedAttempts = failedAttempts;
    }
    public LocalDate getLastActiveDate() {
        return lastActiveDate;
    }

    public void setLastActiveDate(LocalDate lastActiveDate) {
        this.lastActiveDate = lastActiveDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LogindataDTO logindataDTO = (LogindataDTO) o;

        if ( ! Objects.equals(id, logindataDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "LogindataDTO{" +
            "id=" + id +
            ", failedAttempts='" + failedAttempts + "'" +
            ", lastActiveDate='" + lastActiveDate + "'" +
            '}';
    }
}
