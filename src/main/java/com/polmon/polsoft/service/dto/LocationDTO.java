package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Transient;

import com.polmon.polsoft.domain.Location;

/**
 * A DTO for the Location entity.
 */
public class LocationDTO extends AbstractAuditingDTO implements Serializable {

	private Long id;

	private String name;

	private Boolean leaf;
	
	private Boolean hasData;

	private String unit;

	private boolean linkUnlinkLocation;
	
	private Long oldLocationDeviceID;
	
	public Long getOldLocationDeviceID() {
		return this.oldLocationDeviceID;
		
	}
	
	public boolean isLinkUnlinkLocation() {
		return linkUnlinkLocation;
	}

	public void setLinkUnlinkLocation(boolean linkUnlinkLocation) {
		this.linkUnlinkLocation = linkUnlinkLocation;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Boolean getHasData() {
		return hasData;
	}

	public void setHasData(Boolean hasData) {
		this.hasData = hasData;
	}

	private Long parentId;

	private String label;
	
	private List<AlarmsDTO> alarmsActive;
	

	public List<AlarmsDTO> getAlarmsActive() {
		return alarmsActive;
	}

	public void setAlarmsActive(List<AlarmsDTO> alarmsActive) {
		this.alarmsActive = alarmsActive;
	}

	private Long dchannelId;

	private String channelName;

	private String channelUnit;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelUnit() {
		return channelUnit;
	}

	public void setChannelUnit(String channelUnit) {
		this.channelUnit = channelUnit;
	}

	private Set<LocationDTO> children;

	private String devicename;

	private Long deviceId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getLeaf() {
		return leaf;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public String getLabel() {
		return name;
	}

	public Long getParentId() {
		return parentId;
	}

	
	public void setParentId(Long locationId) {
		this.parentId = locationId;
	}

	public Long getDchannelId() {
		return dchannelId;
	}

	public void setDchannelId(Long dchannelId) {
		this.dchannelId = dchannelId;
	}

	public Set<LocationDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<LocationDTO> children) {
		this.children = children;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		LocationDTO locationDTO = (LocationDTO) o;

		if (!Objects.equals(id, locationDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "LocationDTO{" + "id=" + id + ", name='" + name + "'" + ", leaf='" + leaf + "'" + '}';
	}

	public void setDevicename(String devicename) {
		this.devicename = devicename;
		
	}
	public String getDevicename(){
		return this.devicename;
	}

	public void setDeviceid(Long deviceId) {
		this.deviceId=deviceId;
	}
	public Long getDeviceid(){
		return this.deviceId;
	}

	public void setOldLocationDeviceID(Long id) {
		// TODO Auto-generated method stub
		this.oldLocationDeviceID = id;
	}

}
