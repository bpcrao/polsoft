package com.polmon.polsoft.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A DTO for the Devicedata entity.
 */
public class DevicedataDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private Long deviceid;

    private JSONObject channelData;

    private ZonedDateTime timeCollected;


    private Long deviceId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(Long deviceid) {
        this.deviceid = deviceid;
    }
    public String getChannelData() {
        return channelData.toString();
    }
    


    public void setChannelData(String channelData){
        try {
			this.channelData = new JSONObject(channelData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public ZonedDateTime getTimeCollected() {
        return timeCollected;
    }

    public void setTimeCollected(ZonedDateTime timeCollected) {
        this.timeCollected = timeCollected;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DevicedataDTO devicedataDTO = (DevicedataDTO) o;

        if ( ! Objects.equals(id, devicedataDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DevicedataDTO{" +
            "id=" + id +
            ", deviceid='" + deviceid + "'" +
            ", channelData='" + channelData + "'" +
            ", timeCollected='" + timeCollected + "'" +
            '}';
    }
}
