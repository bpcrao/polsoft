package com.polmon.polsoft.service;

import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.domain.ReportTemplate;
import com.polmon.polsoft.repository.LocationRepository;
import com.polmon.polsoft.repository.ReportTemplateRepository;
import com.polmon.polsoft.repository.search.ReportTemplateSearchRepository;
import com.polmon.polsoft.service.dto.ReportTemplateDTO;
import com.polmon.polsoft.service.mapper.ReportTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ReportTemplate.
 */
@Service
@Transactional
public class ReportTemplateService {

    private final Logger log = LoggerFactory.getLogger(ReportTemplateService.class);
    
    @Inject
    private ReportTemplateRepository reportTemplateRepository;

    @Inject
    private ReportTemplateMapper reportTemplateMapper;
    @Autowired
    private LocationRepository locationRepository;

    @Inject
    private ReportTemplateSearchRepository reportTemplateSearchRepository;

    /**
     * Save a reportTemplate.
     *
     * @param reportTemplateDTO the entity to save
     * @return the persisted entity
     */
    public ReportTemplateDTO save(ReportTemplateDTO reportTemplateDTO) {
        log.debug("Request to save ReportTemplate : {}", reportTemplateDTO);
        ReportTemplate reportTemplate = reportTemplateMapper.reportTemplateDTOToReportTemplate(reportTemplateDTO);
        reportTemplate = reportTemplateRepository.save(reportTemplate);
        ReportTemplateDTO result = reportTemplateMapper.reportTemplateToReportTemplateDTO(reportTemplate);
        reportTemplateSearchRepository.save(reportTemplate);
        return result;
    }

    /**
     *  Get all the reportTemplates.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<ReportTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ReportTemplates");
        Page<ReportTemplate> result = reportTemplateRepository.findAll(pageable);
        result.forEach(template ->{
        	List<Location> locations = locationRepository.findByIdIn(Arrays.stream( template.getSettings().split(",")).map(i -> new Long(i)).collect(Collectors.toList()));
        	template.setSettings(locations.stream().map((Location location) ->location.getName()).collect(Collectors.joining(",")));
        });
        return result.map(reportTemplate -> reportTemplateMapper.reportTemplateToReportTemplateDTO(reportTemplate));
    }
    
    /**
     *  Get all the reportTemplates.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ReportTemplate> findAllReports() {
        log.debug("Request to get all ReportTemplates");
        List<ReportTemplate> result = reportTemplateRepository.findAll();
        return result;
    }


    /**
     *  Get one reportTemplate by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ReportTemplateDTO findOne(Long id) {
        log.debug("Request to get ReportTemplate : {}", id);
        ReportTemplate reportTemplate = reportTemplateRepository.findOne(id);
        ReportTemplateDTO reportTemplateDTO = reportTemplateMapper.reportTemplateToReportTemplateDTO(reportTemplate);
        return reportTemplateDTO;
    }
    
    @Transactional(readOnly = true) 
    public ReportTemplate findOneReportTemplate(Long id) {
        log.debug("Request to get ReportTemplate : {}", id);
        ReportTemplate reportTemplate = reportTemplateRepository.findOne(id);
        return reportTemplate;
    }

    /**
     *  Delete the  reportTemplate by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ReportTemplate : {}", id);
        reportTemplateRepository.delete(id);
        reportTemplateSearchRepository.delete(id);
    }

    /**
     * Search for the reportTemplate corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReportTemplateDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ReportTemplates for query {}", query);
        Page<ReportTemplate> result = reportTemplateSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(reportTemplate -> reportTemplateMapper.reportTemplateToReportTemplateDTO(reportTemplate));
    }
}
