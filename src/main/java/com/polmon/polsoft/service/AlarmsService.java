package com.polmon.polsoft.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.polmon.polsoft.domain.AlarmSpecialValues;
import com.polmon.polsoft.domain.AlarmsCount;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.service.dto.AlarmsDTO;

/**
 * Service Interface for managing Alarms.
 */
public interface AlarmsService {
 
    /**
     * Save a alarms.
     *
     * @param alarmsDTO the entity to save
     * @return the persisted entity
     */
    AlarmsDTO save(AlarmsDTO alarmsDTO);

    List<AlarmsDTO> save(List<AlarmsDTO> alarms);
    /**
     *  Get all the alarms.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<AlarmsDTO> findAll(Pageable pageable);
    
    /**
     *  Get all the alarms.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    List<AlarmsDTO> findAll();

    /**
     *  Get the "id" alarms.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AlarmsDTO findOne(Long id);

    /**
     *  Delete the "id" alarms.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the alarms corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<AlarmsDTO> search(String query, Pageable pageable);
    
    /**
     *  Get all the alarms with channelId.
     *  
     *  @return the list of entities
     */
    List<AlarmsDTO> findAllWithChannel(Dchannel ch);
    
    List<AlarmsDTO> findAllWithChannelAndStatus(Set<Dchannel> ch,String status);
    
    /**
     *  Get all the alarms with channelId.
     *  
     *  @return the list of entities
     */        
    List<AlarmsDTO> findAllByStatus(Dchannel ch,String status);
    
    List<AlarmsDTO> findAllWithStatus(String status);
    /**
     *  Get all the alarms with active status and delay is not null
     *  
     *  @return the list of entities
     */
    List<AlarmsDTO> findAllByActiveStatusWithDelay(String status);

	Page<AlarmsDTO> findAllBetweenDates(LocalDate fromDate, LocalDate toDate, Pageable pageable);

	List<AlarmsDTO> findAllBetweenDates(LocalDate fromDate, LocalDate toDate);

	
	List<AlarmsCount> countAlarms();
	
	void deleteAllBefore(ZonedDateTime beforeDate);
	
	void cleanup();

	List<AlarmsDTO> getAlarmsToSave(Dchannel ch, Location loc, float channelValue, ZonedDateTime dataPointTime,
			String day, int hour, int minute, ZonedDateTime now, String serialNumber, List<AlarmsDTO> activeAlarms,
			List<AlarmSpecialValues> alarmSpecialList, boolean sendSMS);
}
