package com.polmon.polsoft.service;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.polmon.polsoft.domain.Alarmtemplate;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.service.dto.AlarmtemplateDTO;

/**
 * Service Interface for managing Alarmtemplate.
 */
public interface AlarmtemplateService {

    /**
     * Save a alarmtemplate.
     *
     * @param alarmtemplateDTO the entity to save
     * @return the persisted entity
     */
    AlarmtemplateDTO save(AlarmtemplateDTO alarmtemplateDTO);

    /**
     *  Get all the alarmtemplates.
     *  
     *  @return the list of entities
     */
    List<AlarmtemplateDTO> findAll();
    
	HashMap<Long, AlarmtemplateDTO> findAllAsMap(Set<Dchannel> dchannels);

    /**
     *  Get the "id" alarmtemplate.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AlarmtemplateDTO findOne(Long id);

    /**
     *  Delete the "id" alarmtemplate.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the alarmtemplate corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<AlarmtemplateDTO> search(String query);
    

    /**
     *  Get all the alarmtemplates with channelId.
     *  
     *  @return the list of entities
     */
     List<AlarmtemplateDTO> findAllWithOutChannelId();
     
     /**
      *  Get all the alarmtemplates with channel.
      *  
      *  @return the list of entities
      */
     List<AlarmtemplateDTO> findAllWithChannel(Dchannel ch);
     
     /**
      *  Get all the alarmtemplates with channelId.
      *  
      *  @return the list of entities
      */
     List<AlarmtemplateDTO> findAllWithChannelId(Long dchannelId);
}
