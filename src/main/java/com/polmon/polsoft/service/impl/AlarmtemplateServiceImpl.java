package com.polmon.polsoft.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.domain.Alarmtemplate;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.repository.AlarmtemplateRepository;
import com.polmon.polsoft.repository.search.AlarmtemplateSearchRepository;
import com.polmon.polsoft.service.AlarmtemplateService;
import com.polmon.polsoft.service.dto.AlarmtemplateDTO;
import com.polmon.polsoft.service.mapper.AlarmtemplateMapper; 

/**
 * Service Implementation for managing Alarmtemplate.
 */
@Service
@Transactional
public class AlarmtemplateServiceImpl implements AlarmtemplateService{

    private final Logger log = LoggerFactory.getLogger(AlarmtemplateServiceImpl.class);
    
    @Inject
    private AlarmtemplateRepository alarmtemplateRepository;

    @Inject
    private AlarmtemplateMapper alarmtemplateMapper;

    @Inject
    private AlarmtemplateSearchRepository alarmtemplateSearchRepository;    
   

    /**
     * Save a alarmtemplate.
     *
     * @param alarmtemplateDTO the entity to save
     * @return the persisted entity
     */
	public AlarmtemplateDTO save(AlarmtemplateDTO alarmtemplateDTO) {
		log.debug("Request to save Alarmtemplate : {}", alarmtemplateDTO);
		Alarmtemplate alarmtemplate = alarmtemplateMapper.alarmtemplateDTOToAlarmtemplate(alarmtemplateDTO);
		alarmtemplate = alarmtemplateRepository.save(alarmtemplate);
		AlarmtemplateDTO result = alarmtemplateMapper.alarmtemplateToAlarmtemplateDTO(alarmtemplate);
		alarmtemplate = alarmtemplateSearchRepository.save(alarmtemplate);	
		//result.setLinkUnlinkalarmtemplate(true);
		return result;
	}

    /**
     *  Get all the alarmtemplates.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AlarmtemplateDTO> findAll() {
        log.debug("Request to get all Alarmtemplates");
        List<AlarmtemplateDTO> result = alarmtemplateRepository.findAllWithEagerRelationships().stream()
            .map(alarmtemplateMapper::alarmtemplateToAlarmtemplateDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get all the alarmtemplates with channelId.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AlarmtemplateDTO> findAllWithOutChannelId() {
        log.debug("Request to get all Alarmtemplates");
        List<AlarmtemplateDTO> result = alarmtemplateRepository.findAllWithoutDchannel().stream()
            .map(alarmtemplateMapper::alarmtemplateToAlarmtemplateDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }
    
    
    /**
     *  Get all the alarmtemplates with channel.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AlarmtemplateDTO> findAllWithChannel(Dchannel ch) {
        log.debug("Request to get all Alarmtemplates");
        List<AlarmtemplateDTO> result = alarmtemplateRepository.findAllByDchannel(ch).stream()
            .map(alarmtemplateMapper::alarmtemplateToAlarmtemplateDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }
    
    /**
     *  Get all the alarmtemplates with channelId.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AlarmtemplateDTO> findAllWithChannelId(Long dchannelId) {
        log.debug("Request to get all Alarmtemplates");
        List<AlarmtemplateDTO> result = alarmtemplateRepository.findAllByDchannelId(dchannelId).stream()
            .map(alarmtemplateMapper::alarmtemplateToAlarmtemplateDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }
    
    /**
     *  Get one alarmtemplate by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public AlarmtemplateDTO findOne(Long id) {
        log.debug("Request to get Alarmtemplate : {}", id);
        Alarmtemplate alarmtemplate = alarmtemplateRepository.findOneWithEagerRelationships(id);
        AlarmtemplateDTO alarmtemplateDTO = alarmtemplateMapper.alarmtemplateToAlarmtemplateDTO(alarmtemplate);
        return alarmtemplateDTO;
    }

    /**
     *  Delete the  alarmtemplate by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Alarmtemplate : {}", id);
        alarmtemplateRepository.delete(id);
        alarmtemplateSearchRepository.delete(id);
    }

    /**
     * Search for the alarmtemplate corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AlarmtemplateDTO> search(String query) {
        log.debug("Request to search Alarmtemplates for query {}", query);
        return StreamSupport
            .stream(alarmtemplateSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(alarmtemplateMapper::alarmtemplateToAlarmtemplateDTO)
            .collect(Collectors.toList());
    }

	@Override
	public HashMap<Long, AlarmtemplateDTO> findAllAsMap(Set<Dchannel> dchannels) {
		List<AlarmtemplateDTO> alarmtemplateDTOs = alarmtemplateRepository.findAllByDchannelIn(dchannels).stream()
	            .map(alarmtemplateMapper::alarmtemplateToAlarmtemplateDTO)
	            .collect(Collectors.toCollection(LinkedList::new));
		HashMap<Long, AlarmtemplateDTO> hashMap = new HashMap<>();
		for(AlarmtemplateDTO alarmtemplateDTO : alarmtemplateDTOs){
			hashMap.put(alarmtemplateDTO.getDchannel_id(), alarmtemplateDTO);
		}
		return hashMap;
	}
}
