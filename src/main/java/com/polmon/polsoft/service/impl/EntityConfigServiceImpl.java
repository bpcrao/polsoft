package com.polmon.polsoft.service.impl;

import com.polmon.polsoft.service.EntityConfigService;
import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.EntityConfig;
import com.polmon.polsoft.repository.EntityConfigRepository;
import com.polmon.polsoft.repository.search.EntityConfigSearchRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.dto.EntityConfigDTO;
import com.polmon.polsoft.service.mapper.EntityConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Service Implementation for managing EntityConfig.
 */
@Service
@Transactional
public class EntityConfigServiceImpl implements EntityConfigService{

	@Inject
    private AuditEventPublisher auditPublisher;
	
    private final Logger log = LoggerFactory.getLogger(EntityConfigServiceImpl.class);

    private final EntityConfigRepository entityConfigRepository;

    private final EntityConfigMapper entityConfigMapper;

    private final EntityConfigSearchRepository entityConfigSearchRepository;

    public EntityConfigServiceImpl(EntityConfigRepository entityConfigRepository, EntityConfigMapper entityConfigMapper, EntityConfigSearchRepository entityConfigSearchRepository) {
        this.entityConfigRepository = entityConfigRepository;
        this.entityConfigMapper = entityConfigMapper;
        this.entityConfigSearchRepository = entityConfigSearchRepository;
    }

    /**
     * Save a entityConfig.
     *
     * @param entityConfigDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EntityConfigDTO save(EntityConfigDTO entityConfigDTO) {
        log.debug("Request to save EntityConfig : {}", entityConfigDTO);
        EntityConfig entityConfig = entityConfigMapper.toEntity(entityConfigDTO);
        entityConfig = entityConfigRepository.save(entityConfig);
        EntityConfigDTO result = entityConfigMapper.toDto(entityConfig);
        Map data = new HashMap<String, Object>();
        data.put("entity", entityConfig.getEntityName());
        if(entityConfig.isEnableAudit()){
        	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "Audit entity enabled", data));
        }
        else{
        	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "Audit entity disabled", data));
        }
        entityConfigSearchRepository.save(entityConfig);
        return result;
    }

    /**
     *  Get all the entityConfigs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EntityConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EntityConfigs");
        return entityConfigRepository.findAll(pageable)
            .map(entityConfigMapper::toDto);
    }

    /**
     *  Get one entityConfig by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EntityConfigDTO findOne(Long id) {
        log.debug("Request to get EntityConfig : {}", id);
        EntityConfig entityConfig = entityConfigRepository.findOne(id);
        return entityConfigMapper.toDto(entityConfig);
    }

    /**
     *  Delete the  entityConfig by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EntityConfig : {}", id);
        entityConfigRepository.delete(id);
        entityConfigSearchRepository.delete(id);
    }

    /**
     * Search for the entityConfig corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EntityConfigDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of EntityConfigs for query {}", query);
        Page<EntityConfig> result = entityConfigSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(entityConfigMapper::toDto);
    }
}
