package com.polmon.polsoft.service.impl;

import com.polmon.polsoft.service.AlarmtemplatelimitService;
import com.polmon.polsoft.domain.Alarmtemplatelimit;
import com.polmon.polsoft.repository.AlarmtemplatelimitRepository;
import com.polmon.polsoft.repository.search.AlarmtemplatelimitSearchRepository;
import com.polmon.polsoft.service.dto.AlarmtemplatelimitDTO;
import com.polmon.polsoft.service.mapper.AlarmtemplatelimitMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Alarmtemplatelimit.
 */
@Service
@Transactional
public class AlarmtemplatelimitServiceImpl implements AlarmtemplatelimitService{

    private final Logger log = LoggerFactory.getLogger(AlarmtemplatelimitServiceImpl.class);
    
    @Inject
    private AlarmtemplatelimitRepository alarmtemplatelimitRepository;

    @Inject
    private AlarmtemplatelimitMapper alarmtemplatelimitMapper;

    @Inject
    private AlarmtemplatelimitSearchRepository alarmtemplatelimitSearchRepository;

    /**
     * Save a alarmtemplatelimit.
     *
     * @param alarmtemplatelimitDTO the entity to save
     * @return the persisted entity
     */
    public AlarmtemplatelimitDTO save(AlarmtemplatelimitDTO alarmtemplatelimitDTO) {
        log.debug("Request to save Alarmtemplatelimit : {}", alarmtemplatelimitDTO);
        Alarmtemplatelimit alarmtemplatelimit = alarmtemplatelimitMapper.alarmtemplatelimitDTOToAlarmtemplatelimit(alarmtemplatelimitDTO);
        alarmtemplatelimit = alarmtemplatelimitRepository.save(alarmtemplatelimit);
        AlarmtemplatelimitDTO result = alarmtemplatelimitMapper.alarmtemplatelimitToAlarmtemplatelimitDTO(alarmtemplatelimit);
        alarmtemplatelimitSearchRepository.save(alarmtemplatelimit);
        return result;
    }

    /**
     *  Get all the alarmtemplatelimits.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AlarmtemplatelimitDTO> findAll() {
        log.debug("Request to get all Alarmtemplatelimits");
        List<AlarmtemplatelimitDTO> result = alarmtemplatelimitRepository.findAll().stream()
            .map(alarmtemplatelimitMapper::alarmtemplatelimitToAlarmtemplatelimitDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one alarmtemplatelimit by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public AlarmtemplatelimitDTO findOne(Long id) {
        log.debug("Request to get Alarmtemplatelimit : {}", id);
        Alarmtemplatelimit alarmtemplatelimit = alarmtemplatelimitRepository.findOne(id);
        AlarmtemplatelimitDTO alarmtemplatelimitDTO = alarmtemplatelimitMapper.alarmtemplatelimitToAlarmtemplatelimitDTO(alarmtemplatelimit);
        return alarmtemplatelimitDTO;
    }

    /**
     *  Delete the  alarmtemplatelimit by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Alarmtemplatelimit : {}", id);
        alarmtemplatelimitRepository.delete(id);
        alarmtemplatelimitSearchRepository.delete(id);
    }

    /**
     * Search for the alarmtemplatelimit corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AlarmtemplatelimitDTO> search(String query) {
        log.debug("Request to search Alarmtemplatelimits for query {}", query);
        return StreamSupport
            .stream(alarmtemplatelimitSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(alarmtemplatelimitMapper::alarmtemplatelimitToAlarmtemplatelimitDTO)
            .collect(Collectors.toList());
    }
}
