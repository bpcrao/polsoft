package com.polmon.polsoft.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.constants.EmailConfiguration;
import com.polmon.polsoft.domain.AlarmSpecialValues;
import com.polmon.polsoft.domain.Alarms;
import com.polmon.polsoft.domain.AlarmsCount;
import com.polmon.polsoft.domain.Alarmtemplate;
import com.polmon.polsoft.domain.Alarmtemplatelimit;
import com.polmon.polsoft.domain.Alarmtemplateschedulars;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.domain.Systempreferences;
import com.polmon.polsoft.repository.AlarmsRepository;
import com.polmon.polsoft.repository.AlarmtemplateRepository;
import com.polmon.polsoft.repository.SystempreferencesRepository;
import com.polmon.polsoft.repository.search.AlarmsSearchRepository;
import com.polmon.polsoft.service.AlarmsService;
import com.polmon.polsoft.service.dto.AlarmsDTO;
import com.polmon.polsoft.service.dto.AlarmtemplateDTO;
import com.polmon.polsoft.service.mapper.AlarmsMapper;
import com.polmon.polsoft.sms.GSMProxy;

/**
 * Service Implementation for managing Alarms.
 */
@Service
@Transactional
public class AlarmsServiceImpl implements AlarmsService {

	private final Logger log = LoggerFactory.getLogger(AlarmsServiceImpl.class);

	@Inject
	private AlarmsRepository alarmsRepository;

	@Inject
	private AlarmtemplateRepository alarmtemplateRepository;

	@Inject
	private AlarmsMapper alarmsMapper;

	@Inject
	private AlarmsSearchRepository alarmsSearchRepository;
	
	@Inject
	private GSMProxy gsmConnect;

	@Autowired
	private SystempreferencesRepository systempreferencesRepository;
	
	/**
	 * Save a alarms.
	 *
	 * @param alarmsDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AlarmsDTO save(AlarmsDTO alarmsDTO) {
		log.debug("Request to save Alarms : {}", alarmsDTO);
		Alarms alarms = alarmsMapper.alarmsDTOToAlarms(alarmsDTO);
		alarms = alarmsRepository.save(alarms);
		AlarmsDTO result = alarmsMapper.alarmsToAlarmsDTO(alarms);
		alarmsSearchRepository.save(alarms);
		return result;
	}

	/**
	 * Get all the alarms.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<AlarmsDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Alarms");
		Page<Alarms> result = alarmsRepository.findAll(pageable);
		return result.map(alarms -> alarmsMapper.alarmsToAlarmsDTO(alarms));
	}

	/**
	 * Get all the alarms.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<AlarmsDTO> findAll() {
		log.debug("Request to get all Alarms");
		List<Alarms> result = alarmsRepository.findAll();
		return result.stream().map(alarms -> alarmsMapper.alarmsToAlarmsDTO(alarms)).collect(Collectors.toList());
	}

	/**
	 * Get one alarms by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public AlarmsDTO findOne(Long id) {
		log.debug("Request to get Alarms : {}", id);
		Alarms alarms = alarmsRepository.findOne(id);
		AlarmsDTO alarmsDTO = alarmsMapper.alarmsToAlarmsDTO(alarms);
		return alarmsDTO;
	}

	/**
	 * Delete the alarms by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Alarms : {}", id);
		alarmsRepository.delete(id);
		alarmsSearchRepository.delete(id);
	}

	/**
	 * Search for the alarms corresponding to the query.
	 *
	 * @param query
	 *            the query of the search
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<AlarmsDTO> search(String query, Pageable pageable) {
		log.debug("Request to search for a page of Alarms for query {}", query);
		Page<Alarms> result = alarmsSearchRepository.search(queryStringQuery(query), pageable);
		return result.map(alarms -> alarmsMapper.alarmsToAlarmsDTO(alarms));
	}

	/**
	 * Get all the alarms with channelId.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<AlarmsDTO> findAllWithChannel(Dchannel ch) {
		log.debug("Request to get all Alarms on channel " + ch);
		List<AlarmsDTO> result = alarmsRepository.findAllByDchannel(ch).stream().map(alarmsMapper::alarmsToAlarmsDTO)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	/**
	 * Get all the alarms with status.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<AlarmsDTO> findAllByStatus(Dchannel ch, String status) {
		log.debug("Request to get all Alarms on channel " + ch);
		List<AlarmsDTO> result = alarmsRepository
				.findAllByDchannelAndStatusStartingWithIgnoreCaseAndSeverityNotNull(ch, status).stream()
				.map(alarmsMapper::alarmsToAlarmsDTO).collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	/**
	 * Get all the alarms with status.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<AlarmsDTO> findAllByActiveStatusWithDelay(String status) {
		log.debug("Request to get all Alarms on channel with active status and delay not null " + status);
		List<AlarmsDTO> result = alarmsRepository.findByStatusStartingWithIgnoreCaseAndDelayNotNull(status).stream()
				.map(alarmsMapper::alarmsToAlarmsDTO).collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	/**
	 * Get all the alarms with status.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<AlarmsDTO> findAllBetweenDates(LocalDate fromDate, LocalDate toDate, Pageable pageable) {
		log.debug("Request to get all Alarms ");
		ZonedDateTime zonedFromDate = fromDate.atStartOfDay(ZoneOffset.UTC);
		ZonedDateTime zonedToDate = toDate.atStartOfDay(ZoneOffset.UTC);
		Page<AlarmsDTO> result = alarmsRepository.findAllByGeneratedBetween(zonedFromDate, zonedToDate, pageable)
				.map(alarmsMapper::alarmsToAlarmsDTO);
		return result;
	}

	/**
	 * Get all the alarms with status.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<AlarmsDTO> findAllBetweenDates(LocalDate fromDate, LocalDate toDate) {
		log.debug("Request to get all Alarms ");
		ZonedDateTime zonedFromDate = fromDate.atStartOfDay(ZoneOffset.UTC);
		ZonedDateTime zonedToDate = toDate.atStartOfDay(ZoneOffset.UTC);
		List<AlarmsDTO> result = alarmsRepository.findAllByGeneratedBetween(zonedFromDate, zonedToDate).stream()
				.map(alarmsMapper::alarmsToAlarmsDTO).collect(Collectors.toList());
		return result;
	}

	/**
	 * count alarms and warnings.
	 *
	 * @return count alarms and warnings
	 */
	@Override
	public List<AlarmsCount> countAlarms() {
		List<AlarmsCount> alarmsCounts= new ArrayList<>(2);
		
		AlarmsCount alarmCount = new AlarmsCount();
		alarmCount.setSeverity("alarm");
		alarmCount.setAlarms_count(alarmsRepository.getAlarmCount());
		alarmsCounts.add(alarmCount);
		
		AlarmsCount warningCount = new AlarmsCount();
		warningCount.setSeverity("warning");
		warningCount.setAlarms_count(alarmsRepository.getWarningCount());
		alarmsCounts.add(warningCount);
		
		return alarmsCounts;
	}

	@Override
	public List<AlarmsDTO> save(List<AlarmsDTO> alarmsDTOs) {
		log.debug("Request to save Alarms : {}", alarmsDTOs);

		List<Alarms> alarms = alarmsMapper.alarmsDTOsToAlarms(alarmsDTOs);

		alarms = alarmsRepository.save(alarms);
		List<AlarmsDTO> result = alarmsMapper.alarmsToAlarmsDTOs(alarms);

		return result;
	}

	@Override
	public List<AlarmsDTO> findAllWithStatus(String status) {
		log.debug("Request to get all Alarms on channel with active status " + status);
		List<AlarmsDTO> result = alarmsRepository.findByStatusStartingWithIgnoreCase(status).stream()
				.map(alarmsMapper::alarmsToAlarmsDTO).collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	@Override
	public List<AlarmsDTO> findAllWithChannelAndStatus(Set<Dchannel> ch, String status) {
		log.debug("Request to get all Alarms on channel " + ch);
		List<AlarmsDTO> result = alarmsRepository.findAllByDchannelInAndStatusStartingWithIgnoreCase(ch, status)
				.stream().map(alarmsMapper::alarmsToAlarmsDTO).collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	@Override
	public void deleteAllBefore(ZonedDateTime beforeDate) {
		alarmsRepository.deleteAllByCreatedDateBefore(beforeDate);
	}

	public void cleanup() {
		Systempreferences systempreferences = systempreferencesRepository
				.findByKeyproperty(EmailConfiguration.CLEAN_PERIOD);
		if (systempreferences != null) {
			try {
				long days = Long.valueOf(systempreferences.getData());
				alarmsRepository.deleteAllByStatusAndGeneratedBefore("inactive",ZonedDateTime.now().minusDays(days));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<AlarmsDTO> getAlarmsToSave(Dchannel ch, Location loc, float channelValue, ZonedDateTime dataPointTime,
			String day, int hour, int minute, ZonedDateTime now, String serialNumber, List<AlarmsDTO> activeAlarms,
			List<AlarmSpecialValues> alarmSpecialList, boolean sendSMS) {
		Map<String,String> smsMessages = new HashMap<>();
		List<AlarmsDTO> alarms = new ArrayList<AlarmsDTO>();

		Set<Alarmtemplate> alarmTemplates = alarmtemplateRepository.findAllByDchannel(ch);
		if (alarmTemplates.size() == 0) {
			return Collections.emptyList();
		}	
		Alarmtemplate alarmTemplate = alarmTemplates.stream().findFirst().get();	
		
		Set<Alarmtemplatelimit> alarmLimits = alarmTemplate.getAlarmtemplatelimits();
		Set<Alarmtemplateschedulars> triggerConditions = alarmTemplate.getAlarmtemplateschedulars();

		// Finding the trigger-set and fetching the correct template Limits
		List<Alarmtemplateschedulars> matchedTriggerCondition = triggerConditions.stream()
				.filter(condition -> condition.getDay().equalsIgnoreCase(day)).collect(Collectors.toList());
		int triggerSetIndex = isTriggerSetOne(alarmTemplate, matchedTriggerCondition, hour, minute);

		// filter by selected index
		List<Alarmtemplatelimit> matchedAlarmTemplateLimits = alarmLimits.stream()
				.filter(item -> (item.getTriggerset() == triggerSetIndex)).collect(Collectors.toList());

		// for each alarm limit check if there is a violation and create alarm
		Map<Object, Object> alarmToTriggerSetMap = matchedAlarmTemplateLimits.stream()
				.collect(Collectors.toMap(limit -> limit.getType().toLowerCase(), limit -> limit));
		
		// SPECIAL ALARMS
				// this method is tooo bad
				if (!specialAlarmsHandler(ch, loc, channelValue, dataPointTime, now, serialNumber, activeAlarms, alarmSpecialList,
						alarms,smsMessages,alarmTemplate, alarmToTriggerSetMap, "alarm", sendSMS)) {
					// if there are special alarms means the value is Special and no
					// more alarms
					if(sendSMS){
					sendSMSs(smsMessages);	
					}
					return alarms;
				}
				

		// Create alarm and warning
		checkAndCreateEvent(ch, loc, channelValue, dataPointTime, now, serialNumber, alarmToTriggerSetMap, alarms,
				"alarm", activeAlarms,smsMessages,sendSMS);
		checkAndCreateEvent(ch, loc, channelValue, dataPointTime, now, serialNumber, alarmToTriggerSetMap, alarms,
				"warning", activeAlarms,smsMessages,sendSMS);
		if(sendSMS){
			sendSMSs(smsMessages);		
		}
		// Save
		return alarms;
	}

	private void sendSMSs(Map<String, String> smsMessages) {
		if(smsMessages.size()>0)
		{
			try {
				gsmConnect.sendMessageMap(smsMessages);
			} catch (Exception e) {
				log.error("error sending sms", e);
			}
		}
	}

	private boolean specialAlarmsHandler(Dchannel ch, Location loc, float channelValue, ZonedDateTime dataPointTime,
			ZonedDateTime now, String serialNumber, List<AlarmsDTO> activeAlarms,List<AlarmSpecialValues> alarmSpecialList, List<AlarmsDTO> alarms,Map<String,String> smsMessages, Alarmtemplate alarmTemplate,
			Map<Object, Object> alarmToTriggerSetMap, String eventType, boolean sendSMS) {
		HashMap<Long, AlarmSpecialValues> alarmSpecialValues = new HashMap<Long, AlarmSpecialValues>();
		
		Alarmtemplatelimit limit = (Alarmtemplatelimit) alarmToTriggerSetMap.get(eventType);
		
		for (AlarmSpecialValues specialValues : alarmSpecialList) {
			alarmSpecialValues.put(specialValues.getValue(), specialValues);
		}
		
		locUnit = loc.getUnit();
		if(locUnit.equals("Temperature DegC"))
			locUnit = locUnit.replace("Temperature DegC", "C");
		else if (locUnit.equals("Humidity %RH"))
			locUnit = locUnit.replace("Humidity %RH","%RH");
		
		AlarmSpecialValues specialValues = alarmSpecialValues.get(new Float(channelValue).longValue());
		if (alarmSpecialValues.get(new Float(channelValue).longValue()) != null) {
			if (activeAlarms != null) {

				// clear special alarms no more relevant
				List<AlarmsDTO> specialAlarmsToClear = activeAlarms.stream()
						.filter(activeAlarm -> activeAlarm.isSpecial() && !activeAlarm.isDescription(specialValues))
						.collect(Collectors.toList());
				alarms.addAll(makeAlarmInactive(ch, specialAlarmsToClear, dataPointTime, "-"));
				
				specialAlarmsToClear.stream().forEach(alarmDTO -> {
				String timeStamp = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma").format(ZonedDateTime.now());
				
				if(StringUtils.isNotBlank(limit.getMobilenumbers()) && sendSMS)
				{
					if(smsMessages.get(limit.getMobilenumbers()) != null)
						smsMessages.put(limit.getMobilenumbers(),smsMessages.get(limit.getMobilenumbers())+"\n"+loc.getName()+" : Alarm Deactivated. PV : "+channelValue+locUnit);
					else
						smsMessages.put(limit.getMobilenumbers(),timeStamp+ "\n"+loc.getName()+" : Alarm Deactivated. PV : "+channelValue+locUnit);
				}
				});
				// special alarms active and relevant
				List<AlarmsDTO> specialAlarms = activeAlarms.stream()
						.filter(activeAlarm -> activeAlarm.isSpecial() && activeAlarm.isDescription(specialValues))
						.collect(Collectors.toList());

				// there are no special alarms relevant so creating new one
				if (specialAlarms.size() == 0) {
					AlarmsDTO specialAlarm = createAlarmDTO(ch, loc, dataPointTime, now, null,
							ch.getName() + "," + serialNumber, "special", limit);
					specialAlarm.setType("Device");
					specialAlarm.setDescription(specialValues.getDescription());
					specialAlarm.setSmsDescritption(specialValues.getDescription());
					specialAlarm.setSeverity("-");
					specialAlarm.setSpecial(true);
					specialAlarm.setState("special");
					specialAlarm.setMobileNumbers(limit.getMobilenumbers());
					String timeStamp = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma").format(specialAlarm.getGenerated());
					if(StringUtils.isNotBlank(specialAlarm.getMobileNumbers()) && sendSMS)
					{
						if(smsMessages.get(specialAlarm.getMobileNumbers()) != null)
							smsMessages.put(specialAlarm.getMobileNumbers(),smsMessages.get(specialAlarm.getMobileNumbers())+"\n" + loc.getName()+" : Alarm "+specialAlarm.getSmsDescritption());
						else
							smsMessages.put(specialAlarm.getMobileNumbers(),timeStamp+ "\n"+ loc.getName()+" : Alarm "+specialAlarm.getSmsDescritption());
					}			
					alarms.add(specialAlarm);
				}
			}
			return false;
		} 
		else {
			// clear all special alarms since we got proper value
			List<AlarmsDTO> specialAlarmsToClear = activeAlarms.stream().filter(activeAlarm -> activeAlarm.isSpecial())
					.collect(Collectors.toList());
			
			makeAlarmInactive(ch, specialAlarmsToClear, dataPointTime, "-");
			alarms.addAll(specialAlarmsToClear);
			specialAlarmsToClear.stream().forEach(alarmDTO -> {
				Alarmtemplatelimit limits = (Alarmtemplatelimit) alarmToTriggerSetMap.get(eventType);
				Long channelId = ch.getId();
				String timeStamp = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma").format(ZonedDateTime.now());
				if(StringUtils.isNotBlank(limits.getMobilenumbers()) && alarmDTO.getDchannel_id().equals(channelId) && sendSMS)
				{
					if(smsMessages.get(limits.getMobilenumbers()) != null)
						smsMessages.put(limits.getMobilenumbers(),smsMessages.get(limits.getMobilenumbers())+"\n"+loc.getName()+" : Alarm Deactivated. PV : "+channelValue+locUnit);
					else
						smsMessages.put(limits.getMobilenumbers(),timeStamp+ "\n"+loc.getName() +" : Alarm Deactivated. PV : "+channelValue+locUnit);
				}
			 });
			return true;
		}
	}

	String locUnit;
	private void checkAndCreateEvent(Dchannel ch, Location loc, float channelValue, ZonedDateTime dataPointTime,
			ZonedDateTime now, String serialNumber, Map<Object, Object> alarmToTriggerSetMap, List<AlarmsDTO> alarms,
			String eventType, List<AlarmsDTO> activeAlarms,Map<String,String> smsMessages, boolean sendSMS) {
		Alarmtemplatelimit limit = (Alarmtemplatelimit) alarmToTriggerSetMap.get(eventType);
		if (alarmToTriggerSetMap.containsKey(eventType)) {

			final AlarmState alarmState = limit.checkAlarmState(channelValue, false);

				List<AlarmsDTO> activeAlarmsToUpdate = activeAlarms.stream()
						.filter(activeAlarm -> activeAlarm.getDchannelId().equals(ch.getId())
								&& activeAlarm.getSeverity().equalsIgnoreCase(eventType))
						.collect(Collectors.toList());
				if (activeAlarmsToUpdate.size() == 0 && alarmState != AlarmState.NORMAL) {
					AlarmsDTO alarm = createAlarmDTO(ch, loc, dataPointTime, now, null, ch.getName() + "," + serialNumber,
							eventType, limit);
					alarm.setDescription(limit.getValue(alarmState, channelValue, alarm.getSeverity(), loc.getUnit()));
					alarm.setSmsDescritption(limit.getsmsValue(alarmState, channelValue, alarm.getSeverity(), loc.getUnit()));
					alarm.setState(alarmState.name());
					alarm.setCalculatedDuration();
					alarm.setMobileNumbers(limit.getMobilenumbers());
					String timeStamp = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma").format(alarm.getGenerated());					
					if(StringUtils.isNotBlank(alarm.getMobileNumbers()) && dataPointTime.toEpochSecond() + 120 > now.toEpochSecond() && sendSMS)
					{
						if(smsMessages.get(alarm.getMobileNumbers()) != null)
							smsMessages.put(alarm.getMobileNumbers(),smsMessages.get(alarm.getMobileNumbers())+"\n"+loc.getName()+" : "+alarm.getSmsDescritption());
						else
							smsMessages.put(alarm.getMobileNumbers(),timeStamp+ "\n"+loc.getName()+" : "+alarm.getSmsDescritption());
					}
					alarms.add(alarm);
				} else {
					locUnit = loc.getUnit();
					if(locUnit.equals("Temperature DegC"))
						locUnit = locUnit.replace("Temperature DegC", "C");
					else if (locUnit.equals("Humidity %RH"))
						locUnit = locUnit.replace("Humidity %RH","%RH");
					
					activeAlarmsToUpdate.stream().forEach(alarmDTO -> {
						if (!alarmState.name().equalsIgnoreCase(alarmDTO.getState()) && alarmState != AlarmState.NORMAL) { // are we creating alarms for normal state also?
							AlarmsDTO alarm = createAlarmDTO(ch, loc, dataPointTime, now, null,
									ch.getName() + "," + serialNumber, eventType, limit);
							alarm.setDescription(limit.getValue(alarmState, channelValue, alarm.getSeverity(), loc.getUnit()));
							alarm.setSmsDescritption(limit.getsmsValue(alarmState, channelValue, alarm.getSeverity(), loc.getUnit()));
							alarm.setState(alarmState.name());
							alarm.setCalculatedDuration();
							alarm.setMobileNumbers(limit.getMobilenumbers());
							alarms.add(alarm);
							
							String timeStamp = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma").format(alarm.getGenerated());
							if(StringUtils.isNotBlank(alarm.getMobileNumbers()) && dataPointTime.toEpochSecond() + 120 > now.toEpochSecond() && sendSMS)
							{
								if(smsMessages.get(alarm.getMobileNumbers()) != null)
									smsMessages.put(alarm.getMobileNumbers(),smsMessages.get(alarm.getMobileNumbers())+"\n"+loc.getName()+" : "+alarm.getSmsDescritption());
								else
									smsMessages.put(alarm.getMobileNumbers(),timeStamp+ "\n"+loc.getName()+" : "+alarm.getSmsDescritption());
							}						
						} else {
							activeAlarmsToUpdate.stream().forEach(item -> item.setCalculatedDuration());
						}
						AlarmState alarmState2 = limit.checkAlarmState(channelValue, true);
						if (!alarmState2.name().equalsIgnoreCase(alarmDTO.getState())) {
							alarmDTO.setStatus("inactive");
							alarmDTO.setEnded(now);
							alarmDTO.setCalculatedDuration();
							alarmDTO.setMobileNumbers(limit.getMobilenumbers());
							alarmDTO.setUpdated(now);
							alarms.add(alarmDTO);
							String timeStamp = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma").format(now);
							if(StringUtils.isNotBlank(limit.getMobilenumbers()) && sendSMS)
							{
								if(smsMessages.get(limit.getMobilenumbers()) != null)
									smsMessages.put(limit.getMobilenumbers(),smsMessages.get(limit.getMobilenumbers())+"\n"+loc.getName()+" : "+ alarmDTO.getSeverity().substring(0,1).toUpperCase()+alarmDTO.getSeverity().substring(1) +" Deactivated. PV : "+channelValue+locUnit);
								else
									smsMessages.put(limit.getMobilenumbers(),timeStamp+ "\n"+loc.getName()+" : "+ alarmDTO.getSeverity().substring(0,1).toUpperCase()+alarmDTO.getSeverity().substring(1) +" Deactivated. PV : "+channelValue+""+locUnit);
							}
						}

					}

					);
				}
		
		}
	}

	/**
	 * Clearing active alarms
	 * 
	 * @param ch
	 * 
	 * @param activeAlarms
	 * @param dataPointTime
	 * @return
	 */
	private List<AlarmsDTO> makeAlarmInactive(Dchannel ch, List<AlarmsDTO> activeAlarms, ZonedDateTime dataPointTime,
			String eventType) {
		List<AlarmsDTO> clearedActiveALarms = activeAlarms.stream()
				.filter(activeAlarm -> activeAlarm.getDchannelId().equals(ch.getId())
						&& activeAlarm.getSeverity().equalsIgnoreCase(eventType))
				.collect(Collectors.toList());
		clearedActiveALarms.stream().forEach(alarmDto -> {
			alarmDto.setStatus("inactive");
			alarmDto.setEnded(dataPointTime);
			alarmDto.setCalculatedDuration();
			alarmDto.setUpdated(ZonedDateTime.now());
		});
		return clearedActiveALarms;
	}

	private AlarmsDTO createAlarmDTO(Dchannel ch, Location loc, ZonedDateTime generated, ZonedDateTime now,
			AlarmtemplateDTO template, String alarmData, String severity, Alarmtemplatelimit limit) {
		AlarmsDTO alarmsDTO = new AlarmsDTO();
		alarmsDTO.setDchannel_id(ch.getId());
		alarmsDTO.setStatus("active");
		alarmsDTO.setGenerated(generated);
		alarmsDTO.setCreated(now);
		alarmsDTO.setAlarmdata(alarmData);
		alarmsDTO.setSeverity(severity);
	//	alarmsDTO.setSource(loc.getPath(true) + loc.getName());
		alarmsDTO.setSource(loc.getName());
		if (template != null) {
			alarmsDTO.setCommentsrequired(template.getCommentsrequired());
			alarmsDTO.setAckrequired(template.getAckrequired());
		}
		alarmsDTO.setType("Threshold");
		if(limit != null){
		alarmsDTO.setEmailIds(limit.getEmailids());
		alarmsDTO.setMobileNumbers(limit.getMobilenumbers());
		}
		return alarmsDTO;
	}

	/**
	 * 
	 * @param alarmTemplateOfChannel 
	 * @param triggerCondition
	 * @param minute 
	 * @param dataPointsHour,
	 *            indicates the hour
	 * @return
	 */
	private int isTriggerSetOne(Alarmtemplate alarmTemplateOfChannel, List<Alarmtemplateschedulars> triggerCondition, int dataPointsHour, int dataPointsMinute) {
		//only when active is selected we check which trigger else just one
		if(alarmTemplateOfChannel.isActivetimechecked() == null || !alarmTemplateOfChannel.isActivetimechecked()){
			return 1;
		}
		boolean isOne = triggerCondition.size() > 0 && triggerCondition.get(0) != null
				&& triggerCondition.get(0).getStarttime() != null && triggerCondition.get(0).getEndtime() != null
				&& !triggerCondition.get(0).getStarttime().isEmpty() && !triggerCondition.get(0).getEndtime().isEmpty();
		if (!isOne) {
			return 2;
		}
		LocalTime startTime = LocalTime.parse(triggerCondition.get(0).getStarttime().split("\\s")[4]);
		LocalTime endTime = LocalTime.parse(triggerCondition.get(0).getEndtime().split("\\s")[4]);
		return startTime.isBefore(LocalTime.of(dataPointsHour, dataPointsMinute)) && endTime.isAfter(LocalTime.of(dataPointsHour, dataPointsMinute))
				? 1 : 2;
	}

}
