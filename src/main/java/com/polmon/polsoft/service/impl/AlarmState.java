package com.polmon.polsoft.service.impl;

public enum AlarmState {
	HIGH,
	LOW,
	NORMAL
}
