package com.polmon.polsoft.service.impl;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.polmon.polsoft.service.AlarmsService;
import com.polmon.polsoft.service.dto.AlarmsDTO;

@Component
public class AlarmsSchedular {

	private static final Logger log = LoggerFactory.getLogger(AlarmsSchedular.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Inject
	private AlarmsService alarmsService;
	
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	//@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() {
		//log.info("The time is now {}", dateFormat.format(new Date()));
		List<AlarmsDTO> alarmsActive = alarmsService.findAllByActiveStatusWithDelay("active");
		for (AlarmsDTO alarmsDTO2 : alarmsActive) {
			ZonedDateTime created = alarmsDTO2.getCreated();
			ZonedDateTime addedDelay = created.plusMinutes(alarmsDTO2.getDelay());
			long elapsedTime = Duration.between(ZonedDateTime.now(), addedDelay).toMillis();
			if (elapsedTime <= 0) {
				alarmsDTO2.setDelay(null);
				alarmsDTO2.setUpdated(ZonedDateTime.now());
				alarmsService.save(alarmsDTO2);
				messagingTemplate.convertAndSend("/topic/alarms", alarmsDTO2);
			}
		}
	}
}
