package com.polmon.polsoft.service.impl;

import com.polmon.polsoft.service.AlarmtemplateschedularsService;
import com.polmon.polsoft.domain.Alarmtemplateschedulars;
import com.polmon.polsoft.repository.AlarmtemplateschedularsRepository;
import com.polmon.polsoft.repository.search.AlarmtemplateschedularsSearchRepository;
import com.polmon.polsoft.service.dto.AlarmtemplateschedularsDTO;
import com.polmon.polsoft.service.mapper.AlarmtemplateschedularsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Alarmtemplateschedulars.
 */
@Service
@Transactional
public class AlarmtemplateschedularsServiceImpl implements AlarmtemplateschedularsService{

    private final Logger log = LoggerFactory.getLogger(AlarmtemplateschedularsServiceImpl.class);
    
    @Inject
    private AlarmtemplateschedularsRepository alarmtemplateschedularsRepository;

    @Inject
    private AlarmtemplateschedularsMapper alarmtemplateschedularsMapper;

    @Inject
    private AlarmtemplateschedularsSearchRepository alarmtemplateschedularsSearchRepository;

    /**
     * Save a alarmtemplateschedulars.
     *
     * @param alarmtemplateschedularsDTO the entity to save
     * @return the persisted entity
     */
    public AlarmtemplateschedularsDTO save(AlarmtemplateschedularsDTO alarmtemplateschedularsDTO) {
        log.debug("Request to save Alarmtemplateschedulars : {}", alarmtemplateschedularsDTO);
        Alarmtemplateschedulars alarmtemplateschedulars = alarmtemplateschedularsMapper.alarmtemplateschedularsDTOToAlarmtemplateschedulars(alarmtemplateschedularsDTO);
        alarmtemplateschedulars = alarmtemplateschedularsRepository.save(alarmtemplateschedulars);
        AlarmtemplateschedularsDTO result = alarmtemplateschedularsMapper.alarmtemplateschedularsToAlarmtemplateschedularsDTO(alarmtemplateschedulars);
        alarmtemplateschedularsSearchRepository.save(alarmtemplateschedulars);
        return result;
    }

    /**
     *  Get all the alarmtemplateschedulars.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AlarmtemplateschedularsDTO> findAll() {
        log.debug("Request to get all Alarmtemplateschedulars");
        List<AlarmtemplateschedularsDTO> result = alarmtemplateschedularsRepository.findAll().stream()
            .map(alarmtemplateschedularsMapper::alarmtemplateschedularsToAlarmtemplateschedularsDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one alarmtemplateschedulars by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public AlarmtemplateschedularsDTO findOne(Long id) {
        log.debug("Request to get Alarmtemplateschedulars : {}", id);
        Alarmtemplateschedulars alarmtemplateschedulars = alarmtemplateschedularsRepository.findOne(id);
        AlarmtemplateschedularsDTO alarmtemplateschedularsDTO = alarmtemplateschedularsMapper.alarmtemplateschedularsToAlarmtemplateschedularsDTO(alarmtemplateschedulars);
        return alarmtemplateschedularsDTO;
    }

    /**
     *  Delete the  alarmtemplateschedulars by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Alarmtemplateschedulars : {}", id);
        alarmtemplateschedularsRepository.delete(id);
        alarmtemplateschedularsSearchRepository.delete(id);
    }

    /**
     * Search for the alarmtemplateschedulars corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AlarmtemplateschedularsDTO> search(String query) {
        log.debug("Request to search Alarmtemplateschedulars for query {}", query);
        return StreamSupport
            .stream(alarmtemplateschedularsSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(alarmtemplateschedularsMapper::alarmtemplateschedularsToAlarmtemplateschedularsDTO)
            .collect(Collectors.toList());
    }
}
