package com.polmon.polsoft.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.session.ConcurrentSessionControlAuthenticationStrategy;

public class CustomConcurrentSessionStrategy extends ConcurrentSessionControlAuthenticationStrategy {
	
	public CustomConcurrentSessionStrategy(SessionRegistry sessionRegistry) {
		super(sessionRegistry);
		this.setExceptionIfMaximumExceeded(true);
	}

	/**
	 * Overriding the default
	 */
	protected int getMaximumSessionsForThisUser(Authentication authentication) {
		return 1;
	}
}
