package com.polmon.polsoft.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.polmon.polsoft.web.rest.util.PasswordUtil;

public class CustomPasswordEncoder extends BCryptPasswordEncoder {

  @Override
  public boolean matches(CharSequence rawPassword, String encodedPassword) {
    String credentials = PasswordUtil.getDecodedPassword(rawPassword.toString());
    return super.matches(credentials, encodedPassword);
  }

}
