package com.polmon.polsoft.security;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.polmon.polsoft.domain.Licence;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.web.rest.EncryptionController;
import com.polmon.polsoft.web.rest.errors.ConcurrentSessionLimitExceededException;
import com.polmon.polsoft.web.rest.errors.IPAdressUserLoginException;


public class IPAddressBasedAuthenticationProvider extends DaoAuthenticationProvider {

  @Autowired
  private SessionRegistry sessionRegistry;

  @Autowired
  private Licence license;

  @Autowired
  EncryptionController encryptionController;

  @Override
  protected void additionalAuthenticationChecks(UserDetails userDetails,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    super.additionalAuthenticationChecks(userDetails, authentication);
    if (userDetails instanceof CustomUserDetails) {
      User user = ((CustomUserDetails) userDetails).getUser();
      if (user.getIpaddresses() != null) {
        List<String> ipAddresses = Arrays.asList(user.getIpaddresses().split(","));
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        String requestIpAddress = null;
        if (requestAttributes instanceof ServletRequestAttributes) {
          HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
          requestIpAddress = request.getRemoteAddr();
        }
        if (ipAddresses.size() > 0 && !ipAddresses.contains(requestIpAddress)) {
          throw new IPAdressUserLoginException(
              "user noto allowed to login using ipaddress: " + requestIpAddress);
        }
      }
      if (sessionRegistry.getAllSessions(userDetails, false).size() >= 2) {
        throw new SessionAuthenticationException("user session already exists");
      }
      int maxConcurrentUsers = sessionRegistry.getAllPrincipals().size();
      if (maxConcurrentUsers >= license.getNoofconcurrentsession()) {
        throw new ConcurrentSessionLimitExceededException(
            "maximum concurrent users allowed are: " + license.getNoofconcurrentsession());
      }
    }
  }
}
