package com.polmon.polsoft.security;

import java.io.IOException;
import java.time.LocalDate;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.polmon.polsoft.repository.LogindataRepository;
import com.polmon.polsoft.repository.UserRepository;
import com.polmon.polsoft.service.LogindataService;
import com.polmon.polsoft.service.UserService;
import com.polmon.polsoft.service.dto.LogindataDTO;

/**
 * Spring Security success handler, specialized for Ajax requests.
 */
@Component
public class AjaxAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	@Inject
	LogindataRepository loginDataRepository;
	
	@Inject
	LogindataService loginDataService;
	
	@Inject
	UserRepository userRepository;
	
	@Inject
	UserService userService;

	
    public final Integer SESSION_TIMEOUT_IN_SECONDS = 60 * 15;

	
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
        Authentication authentication)
        throws IOException, ServletException {
		LogindataDTO loginDataDto = new LogindataDTO();
    	 userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
    		 
    		 if(user.getExpiryDate()!=null && !user.getExpiryDate().isAfter(LocalDate.now())){    			 
    			 user = userService.requestPasswordResetById(user.getId()).get();
    			 loginDataDto.setUserId(user.getId());
    			 loginDataDto.setFailedAttempts(0);
    		 }
    		 loginDataService.resetLoginData(user.getId());
    		 if(user.getResetKey()!=null){
    			 response.setHeader("resetKey", user.getResetKey());
    		 }    		
    		 request.getSession().setMaxInactiveInterval(SESSION_TIMEOUT_IN_SECONDS);
         });
    	
    	response.setStatus(HttpServletResponse.SC_OK);
    }
    
}