package com.polmon.polsoft.security;


import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Component;

import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.Logindata;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.repository.LogindataRepository;
import com.polmon.polsoft.repository.UserRepository;
import com.polmon.polsoft.service.LogindataService;
import com.polmon.polsoft.service.UserService;
import com.polmon.polsoft.service.dto.LogindataDTO;
import com.polmon.polsoft.web.rest.errors.ConcurrentSessionLimitExceededException;
import com.polmon.polsoft.web.rest.errors.IPAdressUserLoginException;

/**
 * Returns a 401 error code (Unauthorized) to the client, when Ajax
 * authentication fails.
 */
@Component
public class AjaxAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Inject
	private AuditEventPublisher auditPublisher;
	 
	@Inject
	LogindataRepository loginDataRepository;

	@Inject
	LogindataService loginDataService;

	@Inject
	UserRepository userRepository;

	@Inject
	UserService userService;

	private String getActivationURL(HttpServletRequest request) {
		return request.getScheme() + // "http"
				"://" + // "://"
				request.getServerName() + // "myhost"
				":" + // ":"
				request.getServerPort() + // "80"
				request.getContextPath();
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		
		if(exception instanceof ConcurrentSessionLimitExceededException){
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"userSessionExceeded");
			return;
		}
		if(exception instanceof SessionAuthenticationException){
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"SessionExists");
			return;
		}
		
		if(exception instanceof IPAdressUserLoginException){
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"UserloggedinIPNotAllowed");
			return;
		}
		Optional<User> data1 = userRepository.findOneByLogin(request.getParameter("j_username"));
		data1.ifPresent(user -> {
			Logindata loginData = loginDataRepository.findOneByUserId(user.getId());
			LogindataDTO loginDataDto = new LogindataDTO();
			loginDataDto.setUserId(user.getId());
			LocalDate now = LocalDate.now();
			loginDataDto.setLastActiveDate(now);
			try {
				if (loginData == null) {
					loginDataDto.setFailedAttempts(1);
					loginDataService.save(loginDataDto);
				} else {
					loginDataDto.setId(loginData.getId());
					loginDataDto.setFailedAttempts(loginData.getFailedAttempts() + 1);
					if (loginDataDto.getFailedAttempts() >= 3) {
						userService.deactivateRegistration(user.getId(), "");
						response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Deactivated");
					} else if (!user.getActivated()) {
						response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
								"Deactivated");
					}
					else {
						loginDataService.save(loginDataDto);
						response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
								(3 - loginDataDto.getFailedAttempts()) + " more attempts left.");
					}
				}
				
				if(loginDataDto.getFailedAttempts() == 3){
					 Map data = new HashMap<String, Object>();
				     data.put("userdeactivated", user.getLogin());
					auditPublisher.publish(new AuditEvent("system", "Maximum password attempts reached", data));
					}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		if (!data1.isPresent()) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "we do not know such user");
		}
	}
}