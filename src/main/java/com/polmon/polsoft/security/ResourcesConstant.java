package com.polmon.polsoft.security;

import java.util.Arrays;
import java.util.List;

/**
* Constants for secured resources.
*/
public final class ResourcesConstant {
  public enum Resource {
	    ALARMS,
	    LOCATION_MANAGER,
	    LOCATION,
	    DEVICE
	   // REPORTS,
	  //  PREFERENCES
    
    /* jhipster-needle-resource-add-item */
  }

  public static List<Resource> getAllResources() {
    return Arrays.asList(Resource.values());
  }
}
