package com.polmon.polsoft.security;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.polmon.polsoft.domain.Resource;


public class CustomUserDetails extends User {

    private Set<Resource> resources;
	private com.polmon.polsoft.domain.User user;

    public CustomUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, Set<Resource> resources) {
        super(username, password, authorities);
        this.resources = resources;
    }

    public CustomUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, Set<Resource> resources, com.polmon.polsoft.domain.User user) {
        super(username, password, authorities);
        this.resources = resources; 
        this.user = user;
	}

	public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }

	public com.polmon.polsoft.domain.User getUser() {
		return user;
	}

	public void setUser(com.polmon.polsoft.domain.User user) {
		this.user = user;
	}
    
}
