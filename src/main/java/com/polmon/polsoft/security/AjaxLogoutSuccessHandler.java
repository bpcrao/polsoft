package com.polmon.polsoft.security;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.polmon.polsoft.config.audit.AuditEventPublisher;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * Spring Security logout handler, specialized for Ajax requests.
 */
@Component
public class AjaxLogoutSuccessHandler extends AbstractAuthenticationTargetUrlRequestHandler
    implements LogoutSuccessHandler {
	
	@Inject
	private AuditEventPublisher auditPublisher;
	
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
        Authentication authentication)
        throws IOException, ServletException {    	
    	request.getSession().invalidate();
    	response.setStatus(HttpServletResponse.SC_OK);
    	if(authentication != null){
    		Map data = new HashMap<String, Object>();
            data.put("logout", authentication.getName());
    		auditPublisher.publish(new AuditEvent(authentication.getName(), "Session logout ", data));
    	}
      
    }
}
