package com.polmon.polsoft.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * An authority (a security role) used by Spring Security.
 */
@Entity
@Table(name = "channeldata")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Channeldata implements Serializable {
	

    /**
	 * 
	 */
	private static final long serialVersionUID = -2282522679724926730L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
	private BigDecimal ch01;
	
	private BigDecimal ch02;
	
	private BigDecimal ch03;
	
	private BigDecimal ch04;
	
	private BigDecimal ch05;
	
	private BigDecimal ch06;
	
	private BigDecimal ch07;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((logdate == null) ? 0 : logdate.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Channeldata other = (Channeldata) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (logdate == null) {
			if (other.logdate != null)
				return false;
		} else if (!logdate.equals(other.logdate))
			return false;
		return true;
	}

	private BigDecimal ch08;
	
	private Integer hourflag;
	
	private Integer dayflag;

    @NotNull
    @Column(nullable = false)
	private Date logdate;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the ch01
	 */
	public BigDecimal getCh01() {
		return ch01;
	}

	/**
	 * @param ch01 the ch01 to set
	 */
	public void setCh01(BigDecimal ch01) {
		this.ch01 = ch01;
	}

	/**
	 * @return the ch02
	 */
	public BigDecimal getCh02() {
		return ch02;
	}

	/**
	 * @param ch02 the ch02 to set
	 */
	public void setCh02(BigDecimal ch02) {
		this.ch02 = ch02;
	}

	/**
	 * @return the ch03
	 */
	public BigDecimal getCh03() {
		return ch03;
	}

	/**
	 * @param ch03 the ch03 to set
	 */
	public void setCh03(BigDecimal ch03) {
		this.ch03 = ch03;
	}

	/**
	 * @return the ch04
	 */
	public BigDecimal getCh04() {
		return ch04;
	}

	/**
	 * @param ch04 the ch04 to set
	 */
	public void setCh04(BigDecimal ch04) {
		this.ch04 = ch04;
	}

	/**
	 * @return the ch05
	 */
	public BigDecimal getCh05() {
		return ch05;
	}

	/**
	 * @param ch05 the ch05 to set
	 */
	public void setCh05(BigDecimal ch05) {
		this.ch05 = ch05;
	}

	/**
	 * @return the ch06
	 */
	public BigDecimal getCh06() {
		return ch06;
	}

	/**
	 * @param ch06 the ch06 to set
	 */
	public void setCh06(BigDecimal ch06) {
		this.ch06 = ch06;
	}

	/**
	 * @return the ch07
	 */
	public BigDecimal getCh07() {
		return ch07;
	}

	/**
	 * @param ch07 the ch07 to set
	 */
	public void setCh07(BigDecimal ch07) {
		this.ch07 = ch07;
	}

	/**
	 * @return the ch08
	 */
	public BigDecimal getCh08() {
		return ch08;
	}

	/**
	 * @param ch08 the ch08 to set
	 */
	public void setCh08(BigDecimal ch08) {
		this.ch08 = ch08;
	}

	/**
	 * @return the hourflag
	 */
	public Integer getHourflag() {
		return hourflag;
	}

	/**
	 * @param hourflag the hourflag to set
	 */
	public void setHourflag(Integer hourflag) {
		this.hourflag = hourflag;
	}

	/**
	 * @return the dayflag
	 */
	public Integer getDayflag() {
		return dayflag;
	}

	/**
	 * @param dayflag the dayflag to set
	 */
	public void setDayflag(Integer dayflag) {
		this.dayflag = dayflag;
	}

	/**
	 * @return the logdate
	 */
	public Date getLogdate() {
		return logdate;
	}

	/**
	 * @param logdate the logdate to set
	 */
	public void setLogdate(Date logdate) {
		this.logdate = logdate;
	}
}
