package com.polmon.polsoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Logindata.
 */
@Entity
@Table(name = "logindata")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "logindata")
public class Logindata extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "failed_attempts")
    private Integer failedAttempts;

    @Column(name = "last_active_date")
    private LocalDate lastActiveDate;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFailedAttempts() {
        return failedAttempts;
    }

    public Logindata failedAttempts(Integer failedAttempts) {
        this.failedAttempts = failedAttempts;
        return this;
    }

    public void setFailedAttempts(Integer failedAttempts) {
        this.failedAttempts = failedAttempts;
    }

    public LocalDate getLastActiveDate() {
        return lastActiveDate;
    }

    public Logindata lastActiveDate(LocalDate lastActiveDate) {
        this.lastActiveDate = lastActiveDate;
        return this;
    }

    public void setLastActiveDate(LocalDate lastActiveDate) {
        this.lastActiveDate = lastActiveDate;
    }

    public User getUser() {
        return user;
    }

    public Logindata user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Logindata logindata = (Logindata) o;
        if(logindata.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, logindata.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Logindata{" +
            "id=" + id +
            ", failedAttempts='" + failedAttempts + "'" +
            ", lastActiveDate='" + lastActiveDate + "'" +
            '}';
    }
}
