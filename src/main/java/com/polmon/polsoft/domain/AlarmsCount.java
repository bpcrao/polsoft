package com.polmon.polsoft.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "alarmscount")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "alarmscount")
public class AlarmsCount implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "severity")
	private String severity;
	
	@Column(name = "alarms_count")
	private Long alarms_count;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public Long getAlarms_count() {
		return alarms_count;
	}

	public void setAlarms_count(Long alarms_count) {
		this.alarms_count = alarms_count;
	}
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AlarmsCount alarms = (AlarmsCount) o;
		if (alarms.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, alarms.id);
	}

	@Override
	public String toString() {
		return "AlarmsCount [id=" + id + ", severity=" + severity + ", alarms_count=" + alarms_count + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

}
