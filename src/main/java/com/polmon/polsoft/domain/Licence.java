package com.polmon.polsoft.domain;

import java.util.List;

public class Licence {
	private int noofconcurrentsession;
	private List<String> macAddresses;
	
	public int getNoofconcurrentsession() {
		return noofconcurrentsession;
	}
	public void setNoofconcurrentsession(int noofconcurrentsession) {
		this.noofconcurrentsession = noofconcurrentsession;
	}
	public List<String> getMacAddresses() {
		return macAddresses;
	}
	public void setMacAddresses(List<String> macAddresses) {
		this.macAddresses = macAddresses;
	}
}