package com.polmon.polsoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A EntityConfig.
 */
@Entity
@Table(name = "entityauditconfiguration")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "entityconfig")
public class EntityConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "entity_name")
    private String entityName;

    @Column(name = "enable_audit")
    private Boolean enableAudit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntityName() {
        return entityName;
    }

    public EntityConfig entityName(String entityName) {
        this.entityName = entityName;
        return this;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Boolean isEnableAudit() {
        return enableAudit;
    }

    public EntityConfig enableAudit(Boolean enableAudit) {
        this.enableAudit = enableAudit;
        return this;
    }

    public void setEnableAudit(Boolean enableAudit) {
        this.enableAudit = enableAudit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EntityConfig entityConfig = (EntityConfig) o;
        if (entityConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), entityConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EntityConfig{" +
            "id=" + getId() +
            ", entityName='" + getEntityName() + "'" +
            ", enableAudit='" + isEnableAudit() + "'" +
            "}";
    }
}
