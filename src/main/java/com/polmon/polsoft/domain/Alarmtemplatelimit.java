package com.polmon.polsoft.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.polmon.polsoft.service.impl.AlarmState;

/**
 * A Alarmtemplatelimit.
 */
@Entity
@Table(name = "alarmtemplatelimit")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "alarmtemplatelimit")
public class Alarmtemplatelimit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "highlimit")
	private Double highlimit;

	@Column(name = "lowlimit")
	private Double lowlimit;

	@Column(name = "roc")
	private Double roc;

	@Column(name = "schedular")
	private String schedular;

	@Column(name = "delay")
	private Long delay;

	@Column(name = "email")
	private Boolean email;

	@Column(name = "sms")
	private Boolean sms;

	@Column(name = "emailids")
	private String emailids;

	@Column(name = "mobilenumbers")
	private String mobilenumbers;

	@Column(name = "action")
	private String action;

	@Column(name = "type")
	private String type;

	@Column(name = "triggerset")
	private Integer triggerset;

	@ManyToMany(mappedBy = "alarmtemplatelimits")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Alarmtemplate> alarmtemplates = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getHighlimit() {
		return highlimit;
	}

	public Alarmtemplatelimit highlimit(Double highlimit) {
		this.highlimit = highlimit;
		return this;
	}

	public void setHighlimit(Double highlimit) {
		this.highlimit = highlimit;
	}

	public Double getLowlimit() {
		return lowlimit;
	}

	public Alarmtemplatelimit lowlimit(Double lowlimit) {
		this.lowlimit = lowlimit;
		return this;
	}

	public void setLowlimit(Double lowlimit) {
		this.lowlimit = lowlimit;
	}

	public Double getRoc() {
		return roc;
	}

	public Alarmtemplatelimit roc(Double roc) {
		this.roc = roc;
		return this;
	}

	public void setRoc(Double roc) {
		this.roc = roc;
	}

	public String getSchedular() {
		return schedular;
	}

	public Alarmtemplatelimit schedular(String schedular) {
		this.schedular = schedular;
		return this;
	}

	public void setSchedular(String schedular) {
		this.schedular = schedular;
	}

	public Long getDelay() {
		return delay;
	}

	public Alarmtemplatelimit delay(Long delay) {
		this.delay = delay;
		return this;
	}

	public void setDelay(Long delay) {
		this.delay = delay;
	}

	public String getAction() {
		return action;
	}

	public Alarmtemplatelimit action(String action) {
		this.action = action;
		return this;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getType() {
		return type;
	}

	public Alarmtemplatelimit type(String type) {
		this.type = type;
		return this;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<Alarmtemplate> getAlarmtemplates() {
		return alarmtemplates;
	}

	public Alarmtemplatelimit alarmtemplates(Set<Alarmtemplate> alarmtemplates) {
		this.alarmtemplates = alarmtemplates;
		return this;
	}

	public Alarmtemplatelimit addAlarmtemplate(Alarmtemplate alarmtemplate) {
		alarmtemplates.add(alarmtemplate);
		alarmtemplate.getAlarmtemplatelimits().add(this);
		return this;
	}

	public Alarmtemplatelimit removeAlarmtemplate(Alarmtemplate alarmtemplate) {
		alarmtemplates.remove(alarmtemplate);
		alarmtemplate.getAlarmtemplatelimits().remove(this);
		return this;
	}

	public void setAlarmtemplates(Set<Alarmtemplate> alarmtemplates) {
		this.alarmtemplates = alarmtemplates;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Alarmtemplatelimit alarmtemplatelimit = (Alarmtemplatelimit) o;
		if (alarmtemplatelimit.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, alarmtemplatelimit.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	public Boolean isEmail() {
		return email;
	}
	
	public Boolean getEmail() {
		return email;
	}

	public void setEmail(Boolean email) {
		this.email = email;
	}

	public Boolean getSms() {
		return sms;
	}

	public void setSms(Boolean sms) {
		this.sms = sms;
	}

	public String getEmailids() {
		return emailids;
	}

	public void setEmailids(String emailids) {
		this.emailids = emailids;
	}

	@Override
	public String toString() {
		return "Alarmtemplatelimit{" + "id=" + id + ", highlimit='" + highlimit + "'" + ", lowlimit='" + lowlimit + "'"
				+ ", roc='" + roc + "'" + ", schedular='" + schedular + "'" + ", delay='" + delay + "'" + ", action='"
				+ action + "'" + ", type='" + type + "'" + ", triggerset ='" + triggerset + "'" + '}';
	}

	public Integer getTriggerset() {
		return triggerset;
	}

	public void setTriggerset(Integer triggerset) {
		this.triggerset = triggerset;
	}

	public String getMobilenumbers() {
		return mobilenumbers;
	}

	public void setMobilenumbers(String mobilenumbers) {
		this.mobilenumbers = mobilenumbers;
	}

	public String getValue(AlarmState alarmState, float channelValue, String severity, String locUnit ) {
		if(locUnit != null){
			if(locUnit.equals("Temperature DegC"))
				locUnit = locUnit.replace("Temperature DegC", "°C");
			else if (locUnit.equals("Humidity %RH"))
				locUnit = locUnit.replace("Humidity %RH","%RH");
		}
		if (alarmState.equals(AlarmState.LOW)) {
			String value = "PV of (" + channelValue + locUnit + ") is Less than " + severity + " setpoint of ";
			return value + "(" + this.getLowlimit().toString() + locUnit +")";
		} else if (alarmState.equals(AlarmState.HIGH)) {
			 String value1 = "PV of (" + channelValue + locUnit + ") is Greater than " + severity + " setpoint of ";
			return value1 + "(" + this.getHighlimit().toString() + locUnit +")";
		}
		return "Normal";
	}
	
	public String getsmsValue(AlarmState alarmState, float channelValue, String severity, String locUnit) {
		if(locUnit != null){
			if(locUnit.equals("Temperature DegC"))
				locUnit = locUnit.replace("Temperature DegC", "C");
			else if (locUnit.equals("Humidity %RH"))
				locUnit = locUnit.replace("Humidity %RH","%RH");
		}
		if (alarmState.equals(AlarmState.LOW)) {
			String smsDescription = severity.substring(0,1).toUpperCase()+severity.substring(1)+" - PV : "+ channelValue + locUnit + " < SP : ";
			return smsDescription + this.getLowlimit().toString() + locUnit;
		} else if (alarmState.equals(AlarmState.HIGH)) {
			String smsDescription = severity.substring(0,1).toUpperCase()+severity.substring(1)+" - PV : "+ channelValue + locUnit + " > SP : ";
			return smsDescription + this.getHighlimit().toString() + locUnit;
		}
		return "Normal";
	}

	public AlarmState checkAlarmState(float channelValue, boolean considerHysterisis) {
		// Create High or Low ALarms
		if (this.getHighlimit() - (considerHysterisis ? this.getRoc() :0  )  >= channelValue && this.getLowlimit() + (considerHysterisis ? this.getRoc() :0  ) <= channelValue)
			return AlarmState.NORMAL;
		else if (this.getLowlimit() + (considerHysterisis ? this.getRoc() :0  ) >= channelValue)
			return AlarmState.LOW;
		else if (this.getHighlimit() - (considerHysterisis ? this.getRoc() :0  )  <= channelValue)
			return AlarmState.HIGH;
		
		return AlarmState.NORMAL;
	}

	public boolean isAlarm() {
		return this.getType().equals("alarm");
	}
}
