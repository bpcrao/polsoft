package com.polmon.polsoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Alarmtemplate.
 */
@Entity
@Table(name = "alarmtemplate")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Document(indexName = "alarmtemplate")
public class Alarmtemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ackrequired")
    private Boolean ackrequired;

    @Column(name = "commentsrequired")
    private Boolean commentsrequired;

    @Column(name = "logicalmeasurepoint")
    private String logicalmeasurepoint;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "logicaldefaultvalue")
    private String logicaldefaultvalue;

    @Column(name = "activetimechecked")
    private Boolean activetimechecked;

    @ManyToOne
    private Dchannel dchannel;
    
    @ManyToMany(cascade = {CascadeType.ALL}) 
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "alarmtemplate_alarmtemplateschedulars",
               joinColumns = @JoinColumn(name="alarmtemplates_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="alarmtemplateschedulars_id", referencedColumnName="ID"))
    private Set<Alarmtemplateschedulars> alarmtemplateschedulars = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER) 
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "alarmtemplate_alarmtemplatelimit",
               joinColumns = @JoinColumn(name="alarmtemplates_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="alarmtemplatelimits_id", referencedColumnName="ID"))
    private Set<Alarmtemplatelimit> alarmtemplatelimits = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isAckrequired() {
        return ackrequired;
    }

    public Alarmtemplate ackrequired(Boolean ackrequired) {
        this.ackrequired = ackrequired;
        return this;
    }

    public void setAckrequired(Boolean ackrequired) {
        this.ackrequired = ackrequired;
    }

    public Boolean isCommentsrequired() {
        return commentsrequired;
    }

    public Alarmtemplate commentsrequired(Boolean commentsrequired) {
        this.commentsrequired = commentsrequired;
        return this;
    }

    public void setCommentsrequired(Boolean commentsrequired) {
        this.commentsrequired = commentsrequired;
    }

    public String getLogicalmeasurepoint() {
        return logicalmeasurepoint;
    }

    public Alarmtemplate logicalmeasurepoint(String logicalmeasurepoint) {
        this.logicalmeasurepoint = logicalmeasurepoint;
        return this;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setLogicalmeasurepoint(String logicalmeasurepoint) {
        this.logicalmeasurepoint = logicalmeasurepoint;
    }

    public String getLogicaldefaultvalue() {
        return logicaldefaultvalue;
    }

    public Alarmtemplate logicaldefaultvalue(String logicaldefaultvalue) {
        this.logicaldefaultvalue = logicaldefaultvalue;
        return this;
    }

    public void setLogicaldefaultvalue(String logicaldefaultvalue) {
        this.logicaldefaultvalue = logicaldefaultvalue;
    }

    public Boolean isActivetimechecked() {
        return activetimechecked;
    }

    public Alarmtemplate activetimechecked(Boolean activetimechecked) {
        this.activetimechecked = activetimechecked;
        return this;
    }

    public void setActivetimechecked(Boolean activetimechecked) {
        this.activetimechecked = activetimechecked;
    }

    public Set<Alarmtemplateschedulars> getAlarmtemplateschedulars() {
        return alarmtemplateschedulars;
    }

    public Alarmtemplate alarmtemplateschedulars(Set<Alarmtemplateschedulars> alarmtemplateschedulars) {
        this.alarmtemplateschedulars = alarmtemplateschedulars;
        return this;
    }

    public Alarmtemplate addAlarmtemplateschedulars(Alarmtemplateschedulars alarmtemplateschedular) {
        alarmtemplateschedulars.add(alarmtemplateschedular);
        alarmtemplateschedular.getAlarmtemplates().add(this);
        return this;
    }

    public Alarmtemplate removeAlarmtemplateschedulars(Alarmtemplateschedulars alarmtemplateschedular) {
        alarmtemplateschedulars.remove(alarmtemplateschedular);
        alarmtemplateschedular.getAlarmtemplates().remove(this);
        return this;
    }

    public void setAlarmtemplateschedulars(Set<Alarmtemplateschedulars> alarmtemplateschedulars) {
        this.alarmtemplateschedulars = alarmtemplateschedulars;
    }

    public Set<Alarmtemplatelimit> getAlarmtemplatelimits() {
        return alarmtemplatelimits;
    }

    public Alarmtemplate alarmtemplatelimits(Set<Alarmtemplatelimit> alarmtemplatelimits) {
        this.alarmtemplatelimits = alarmtemplatelimits;
        return this;
    }

    public Alarmtemplate addAlarmtemplatelimit(Alarmtemplatelimit alarmtemplatelimit) {
        alarmtemplatelimits.add(alarmtemplatelimit);
        alarmtemplatelimit.getAlarmtemplates().add(this);
        return this;
    }

    public Alarmtemplate removeAlarmtemplatelimit(Alarmtemplatelimit alarmtemplatelimit) {
        alarmtemplatelimits.remove(alarmtemplatelimit);
        alarmtemplatelimit.getAlarmtemplates().remove(this);
        return this;
    }

    public void setAlarmtemplatelimits(Set<Alarmtemplatelimit> alarmtemplatelimits) {
        this.alarmtemplatelimits = alarmtemplatelimits;
    }

    public Dchannel getDchannel() {
        return dchannel;
    }
    
    public Alarmtemplate dchannel(Dchannel dchannel) {
        this.dchannel = dchannel;
        return this;
    }

    public void setDchannel(Dchannel dchannel) {
        this.dchannel = dchannel;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Alarmtemplate alarmtemplate = (Alarmtemplate) o;
        if(alarmtemplate.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, alarmtemplate.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Alarmtemplate{" +
            "id=" + id +
            ", ackrequired='" + ackrequired + "'" +
            ", commentsrequired='" + commentsrequired + "'" +
            ", logicalmeasurepoint='" + logicalmeasurepoint + "'" +
            ", logicaldefaultvalue='" + logicaldefaultvalue + "'" +
            ", activetimechecked='" + activetimechecked + "'" +
            '}';
    }
}
