package com.polmon.polsoft.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.elasticsearch.annotations.Document;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * A Devicedata.
 */
@Entity
@Table(name = "devicedata")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "devicedata")
public class Devicedata extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "deviceid")
    private Long deviceid;

    @Column(name = "channel_data")
    private String channelData;

    @Column(name = "time_collected")
    private ZonedDateTime timeCollected;

    @ManyToOne
    private Device device;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeviceid() {
        return deviceid;
    }

    public Devicedata deviceid(Long deviceid) {
        this.deviceid = deviceid;
        return this;
    }

    public void setDeviceid(Long deviceid) {
        this.deviceid = deviceid;
    }

    public String getChannelData() {
        return channelData;
    }
    

	public JsonObject getChannelDataJSON() {
		return (new JsonParser()).parse(this.getChannelData()).getAsJsonObject();
	}

    public Devicedata channelData(String channelData) {
        this.channelData = channelData;
        return this;
    }

    public void setChannelData(String channelData) {
        this.channelData = channelData;
    }

    public ZonedDateTime getTimeCollected() {
        return timeCollected;
    }

    public Devicedata timeCollected(ZonedDateTime timeCollected) {
        this.timeCollected = timeCollected;
        return this;
    }

    public void setTimeCollected(ZonedDateTime timeCollected) {
        this.timeCollected = timeCollected;
    }

    public Device getDevice() {
        return device;
    }

    public Devicedata device(Device device) {
        this.device = device;
        return this;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Devicedata devicedata = (Devicedata) o;
        if(devicedata.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, devicedata.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Devicedata{" +
            "id=" + id +
            ", deviceid='" + deviceid + "'" +
            ", channelData='" + channelData + "'" +
            ", timeCollected='" + timeCollected + "'" +
            '}';
    }
}
