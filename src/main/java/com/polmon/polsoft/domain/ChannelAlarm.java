package com.polmon.polsoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A ChannelAlarm.
 */
@Entity
@Table(name = "channel_alarm")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "channelalarm")
public class ChannelAlarm extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "low", precision=10, scale=2)
    private BigDecimal low;

    @Column(name = "high", precision=10, scale=2)
    private BigDecimal high;

    @Column(name = "low_low", precision=10, scale=2)
    private BigDecimal lowLow;

    @Column(name = "high_high", precision=10, scale=2)
    private BigDecimal highHigh;

    @ManyToOne
    private Dchannel dchannel;

    public ChannelAlarm(){
    	
    }
    
    public ChannelAlarm(BigDecimal lowLow, BigDecimal low, BigDecimal high, BigDecimal highHigh,Dchannel dchannel) {
    	this.low = low;
    	this.lowLow = lowLow;
    	this.high = high;
    	this.highHigh = highHigh;
    	this.dchannel = dchannel;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getLow() {
        return low;
    }

    public ChannelAlarm low(BigDecimal low) {
        this.low = low;
        return this;
    }

    public void setLow(BigDecimal low) {
        this.low = low;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public ChannelAlarm high(BigDecimal high) {
        this.high = high;
        return this;
    }

    public void setHigh(BigDecimal high) {
        this.high = high;
    }

    public BigDecimal getLowLow() {
        return lowLow;
    }

    public ChannelAlarm lowLow(BigDecimal lowLow) {
        this.lowLow = lowLow;
        return this;
    }

    public void setLowLow(BigDecimal lowLow) {
        this.lowLow = lowLow;
    }

    public BigDecimal getHighHigh() {
        return highHigh;
    }

    public ChannelAlarm highHigh(BigDecimal highHigh) {
        this.highHigh = highHigh;
        return this;
    }

    public void setHighHigh(BigDecimal highHigh) {
        this.highHigh = highHigh;
    }

    public Dchannel getDchannel() {
        return dchannel;
    }

    public ChannelAlarm dchannel(Dchannel dchannel) {
        this.dchannel = dchannel;
        return this;
    }

    public void setDchannel(Dchannel dchannel) {
        this.dchannel = dchannel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChannelAlarm channelAlarm = (ChannelAlarm) o;
        if(channelAlarm.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, channelAlarm.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ChannelAlarm{" +
            "id=" + id +
            ", low='" + low + "'" +
            ", high='" + high + "'" +
            ", lowLow='" + lowLow + "'" +
            ", highHigh='" + highHigh + "'" +
            '}';
    }
}
