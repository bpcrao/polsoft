package com.polmon.polsoft.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the acl_object_identity database table.
 * 
 */
@Entity
@Table(name="acl_object_identity")
@NamedQuery(name="AclObjectIdentity.findAll", query="SELECT a FROM AclObjectIdentity a")
public class AclObjectIdentity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(name="entries_inheriting", nullable=false)
	private Boolean entriesInheriting;

	@Column(name="object_id_identity", nullable=false)
	private Long objectIdIdentity;

	//bi-directional many-to-one association to AclEntry
	@OneToMany(mappedBy="aclObjectIdentityBean")
	private List<AclEntry> aclEntries;

	//bi-directional many-to-one association to AclClass
	@ManyToOne
	@JoinColumn(name="object_id_class", nullable=false)
	private AclClass aclClass;

	//bi-directional many-to-one association to AclObjectIdentity
	@ManyToOne
	@JoinColumn(name="parent_object")
	private AclObjectIdentity aclObjectIdentity;

	//bi-directional many-to-one association to AclObjectIdentity
	@OneToMany(mappedBy="aclObjectIdentity")
	private List<AclObjectIdentity> aclObjectIdentities;

	//bi-directional many-to-one association to AclSid
	@ManyToOne
	@JoinColumn(name="owner_sid")
	private AclSid aclSid;

	public AclObjectIdentity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getEntriesInheriting() {
		return this.entriesInheriting;
	}

	public void setEntriesInheriting(Boolean entriesInheriting) {
		this.entriesInheriting = entriesInheriting;
	}

	public Long getObjectIdIdentity() {
		return this.objectIdIdentity;
	}

	public void setObjectIdIdentity(Long objectIdIdentity) {
		this.objectIdIdentity = objectIdIdentity;
	}

	public List<AclEntry> getAclEntries() {
		return this.aclEntries;
	}

	public void setAclEntries(List<AclEntry> aclEntries) {
		this.aclEntries = aclEntries;
	}

	public AclEntry addAclEntry(AclEntry aclEntry) {
		getAclEntries().add(aclEntry);
		aclEntry.setAclObjectIdentityBean(this);

		return aclEntry;
	}

	public AclEntry removeAclEntry(AclEntry aclEntry) {
		getAclEntries().remove(aclEntry);
		aclEntry.setAclObjectIdentityBean(null);

		return aclEntry;
	}

	public AclClass getAclClass() {
		return this.aclClass;
	}

	public void setAclClass(AclClass aclClass) {
		this.aclClass = aclClass;
	}

	public AclObjectIdentity getAclObjectIdentity() {
		return this.aclObjectIdentity;
	}

	public void setAclObjectIdentity(AclObjectIdentity aclObjectIdentity) {
		this.aclObjectIdentity = aclObjectIdentity;
	}

	public List<AclObjectIdentity> getAclObjectIdentities() {
		return this.aclObjectIdentities;
	}

	public void setAclObjectIdentities(List<AclObjectIdentity> aclObjectIdentities) {
		this.aclObjectIdentities = aclObjectIdentities;
	}

	public AclObjectIdentity addAclObjectIdentity(AclObjectIdentity aclObjectIdentity) {
		getAclObjectIdentities().add(aclObjectIdentity);
		aclObjectIdentity.setAclObjectIdentity(this);

		return aclObjectIdentity;
	}

	public AclObjectIdentity removeAclObjectIdentity(AclObjectIdentity aclObjectIdentity) {
		getAclObjectIdentities().remove(aclObjectIdentity);
		aclObjectIdentity.setAclObjectIdentity(null);

		return aclObjectIdentity;
	}

	public AclSid getAclSid() {
		return this.aclSid;
	}

	public void setAclSid(AclSid aclSid) {
		this.aclSid = aclSid;
	}

}