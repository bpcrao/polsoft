package com.polmon.polsoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Alarms.
 */
@Entity
@Table(name = "alarms")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "alarms")
public class Alarms extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "type")
	private String type;

	@Column(name = "description")
	private String description;

	@Column(name = "duration")
	private String duration;

	@Column(name = "created")
	private ZonedDateTime created;

	@Column(name = "updated")
	private ZonedDateTime updated;

	@Column(name = "deleted")
	private ZonedDateTime deleted;

	@Column(name = "source")
	private String source;

	@Column(name = "action")
	private String action;

	@Column(name = "comments")
	private String comments;

	@Column(name = "username")
	private String username;

	@Column(name = "status")
	private String status;

	@Column(name = "state")
	private String state;
	
	@Column(name = "mobilenumbers")
    private String mobileNumbers;

	public ZonedDateTime getGenerated() {
		return generated;
	}

	public void setGenerated(ZonedDateTime generated) {
		this.generated = generated;
	}

	@Column(name = "ackrequired")
	private Boolean ackrequired;

	@Column(name = "generated")
	private ZonedDateTime generated;

	private ZonedDateTime ended;

	public ZonedDateTime getEnded() {
		return ended;
	}

	public void setEnded(ZonedDateTime ended) {
		this.ended = ended;
	}

	@Column(name = "commentsrequired")
	private Boolean commentsrequired;

	@Column(name = "delay")
	private Long delay;

	@Column(name = "severity")
	private String severity;

	@Column(name = "acknowledged")
	private ZonedDateTime acknowledged;

	@Column(name = "alarmdata")
	private String alarmdata;

	public String getAlarmdata() {
		return alarmdata;
	}

	public void setAlarmdata(String alarmdata) {
		this.alarmdata = alarmdata;
	}

	public ZonedDateTime getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(ZonedDateTime acknowledged) {
		this.acknowledged = acknowledged;
	}

	@ManyToOne
	private Dchannel dchannel;

	public Dchannel getDchannel() {
		return dchannel;
	}

	public void setDchannel(Dchannel dchannel) {
		this.dchannel = dchannel;
	}

	public Long getDelay() {
		return delay;
	}

	public void setDelay(Long delay) {
		this.delay = delay;
	}

	public Boolean getAckrequired() {
		return ackrequired;
	}

	public void setAckrequired(Boolean ackrequired) {
		this.ackrequired = ackrequired;
	}

	public Boolean getCommentsrequired() {
		return commentsrequired;
	}

	public void setCommentsrequired(Boolean commentsrequired) {
		this.commentsrequired = commentsrequired;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public Alarms type(String type) {
		this.type = type;
		return this;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public Alarms description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDuration() {
		return duration;
	}

	public Alarms duration(String duration) {
		this.duration = duration;
		return this;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public Alarms created(ZonedDateTime created) {
		this.created = created;
		return this;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public String getSource() {
		return source;
	}

	public Alarms source(String source) {
		this.source = source;
		return this;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAction() {
		return action;
	}

	public Alarms action(String action) {
		this.action = action;
		return this;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getComments() {
		return comments;
	}

	public Alarms comments(String comments) {
		this.comments = comments;
		return this;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUsername() {
		return username;
	}

	public Alarms username(String username) {
		this.username = username;
		return this;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public Alarms status(String status) {
		this.status = status;
		return this;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public String getState() {
		return state;
	}

	public Alarms state(String state) {
		this.state = state;
		return this;
	}
	
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Alarms alarms = (Alarms) o;
		if (alarms.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, alarms.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Alarms [id=" + id + ", type=" + type + ", description=" + description + ", duration=" + duration
				+ ", created=" + created + ", updated=" + updated + ", deleted=" + deleted + ", source=" + source
				+ ", action=" + action + ", comments=" + comments + ", username=" + username + ", status=" + status
				+ ", state=" + state + ", ackrequired=" + ackrequired + ", generated=" + generated + ", ended=" + ended
				+ ", commentsrequired=" + commentsrequired + ", delay=" + delay + ", severity=" + severity
				+ ", acknowledged=" + acknowledged + ", alarmdata=" + alarmdata + ", dchannel=" + dchannel + "]";
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public ZonedDateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(ZonedDateTime deleted) {
		this.deleted = deleted;
	}

	public String getMobileNumbers() {
		return mobileNumbers;
	}

	public void setMobileNumbers(String mobileNumbers) {
		this.mobileNumbers = mobileNumbers;
	}
}
