package com.polmon.polsoft.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.polmon.polsoft.service.dto.AlarmsDTO;

/**
 * A Location.
 */
@Entity
@Table(name = "location")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "location")
public class Location extends AbstractAuditingEntity implements Serializable, Comparable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEQ_GEN", sequenceName="SEQ_JUST_FOR_LOCATION", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN")
	/*@GeneratedValue(strategy = GenerationType.AUTO)*/
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "leaf")
	private Boolean leaf;

	@Column(name = "has_data")
	private Boolean hasData;

	@Column(name = "unit")
	private String unit;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Boolean getHasData() {
		return hasData;
	}

	public void setHasData(Boolean hasData) {
		this.hasData = hasData;
		if (hasData != null && hasData == true && this.parent != null) {
			this.getParent().setHasData(hasData);
		}
	}

	@ManyToOne
	private Location parent;

	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "parent")
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Location> children = new HashSet<>();

	@OneToOne
	@JoinColumn(unique = true)
	private Dchannel dchannel;
	// if channel is there it is linked

	@Transient
	private List<AlarmsDTO> alarmsActive;

	public List<AlarmsDTO> getAlarmsActive() {
		return alarmsActive;
	}

	public void setAlarmsActive(List<AlarmsDTO> alarmsActive) {
		this.alarmsActive = alarmsActive;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Location name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isLeaf() {
		return leaf;
	}

	public Location leaf(Boolean leaf) {
		this.leaf = leaf;
		return this;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public Location getParent() {
		return parent;
	}

	public Location parent(Location location) {
		this.parent = location;
		return this;
	}

	public void setParent(Location location) {
		this.parent = location;
	}

	public Dchannel getDchannel() {
		return dchannel;
	}

	public Location dchannel(Dchannel dchannel) {
		this.dchannel = dchannel;
		return this;
	}

	public void setDchannel(Dchannel dchannel) {
		this.dchannel = dchannel;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Location location = (Location) o;
		if (location.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, location.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Location{" + "id=" + id + ", name='" + name + "'" + ", leaf='" + leaf + "'" + '}';
	}

	public Set<Location> getChildren() {
		return children;
	}

	public void setChildren(Set<Location> children) {
		this.children = children;
	}

	public String getPath(boolean b) {
		String path = "";
		Location node = this;
		while (node.getParent() != null) {
			path = b ? this.getParent().getName().toString() : this.getParent().getId().toString();
			if (node != this) {
				path = path + "/";
			}
			node = node.getParent();
		}
		return path;
	}

	@Override
	public int compareTo(Object arg0) {
		if (((Location) arg0).getId() > this.getId()) {
			return -1;
		} else if (((Location) arg0).getId() < this.getId()) {
			return 1;
		}
		return 0;
	}

}