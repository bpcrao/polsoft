package com.polmon.polsoft.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Device.
 */
@Entity
@Table(name = "device")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "device")
public class Device extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "settings")
    private String settings;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "last_refresh")
    private ZonedDateTime lastRefresh;

    @Column(name = "serial")
    private String serial;

    @Column(name = "location")
    private String location;
    
    @Column(name = "ip")
    private String ip;
    
    @Column(name = "device_health")
    private String deviceHealth;


    public String getDeviceHealth() {
		return deviceHealth;
	}

	public void setDeviceHealth(String deviceHealth) {
		this.deviceHealth = deviceHealth;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@OneToMany(mappedBy = "device")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Dchannel> channels = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Device name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public Device type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSettings() {
        return settings;
    }

    public Device settings(String settings) {
        this.settings = settings;
        return this;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public Boolean isStatus() {
        return status;
    }

    public Device status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ZonedDateTime getLastRefresh() {
        return lastRefresh;
    }

    public Device lastRefresh(ZonedDateTime lastRefresh) {
        this.lastRefresh = lastRefresh;
        return this;
    }

    public void setLastRefresh(ZonedDateTime lastRefresh) {
        this.lastRefresh = lastRefresh;
    }

    public String getSerial() {
        return serial;
    }

    public Device serial(String serial) {
        this.serial = serial;
        return this;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getLocation() {
        return location;
    }

    public Device location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Set<Dchannel> getChannels() {
        return channels;
    }

    public Device channels(Set<Dchannel> dchannels) {
        this.channels = dchannels;
        return this;
    }

    public Device addDchannel(Dchannel dchannel) {
        channels.add(dchannel);
        dchannel.setDevice(this);
        return this;
    }

    public Device removeDchannel(Dchannel dchannel) {
        channels.remove(dchannel);
        dchannel.setDevice(null);
        return this;
    }

    public void setChannels(Set<Dchannel> dchannels) {
        this.channels = dchannels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Device device = (Device) o;
        if(device.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, device.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Device{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", type='" + type + "'" +
            ", settings='" + settings + "'" +
            ", status='" + status + "'" +
            ", lastRefresh='" + lastRefresh + "'" +
            ", serial='" + serial + "'" +
            ", location='" + location + "'" +
            '}';
    }
}
