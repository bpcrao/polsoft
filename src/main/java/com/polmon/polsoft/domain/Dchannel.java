package com.polmon.polsoft.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * A Dchannel.
 */
@Entity
@Table(name = "channel")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Document(indexName = "dchannel")
public class Dchannel extends AbstractAuditingEntity implements Serializable, Comparable<Dchannel> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "unit")
    private String unit;
    
    @Transient
    private int channelNumber;

    public int getChannelNumber() {
		return Integer.parseInt(this.getName().substring(2));
	}

	public void setChannelNumber(int channelNumber) {
		this.channelNumber = channelNumber;
	}

	@Column(name = "channeldata")
    private String channeldata;

    @ManyToOne
    private Device device;

    @OneToMany(mappedBy = "dchannel" , cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)    
    private Set<ChannelAlarm> channelAlarms = new HashSet<>();

    @OneToMany(mappedBy = "dchannel" , cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonIgnore    
    private Set<Alarmtemplate> channelAlarmtemplates = new HashSet<>();

    @OneToOne(mappedBy = "dchannel")
    @JsonIgnore
    private Location location;


	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Dchannel() {
	}

	public Dchannel(JsonNode jsonNode, JsonNode jsonNode2, Device device, JsonNode unit) {
		this.name = jsonNode.asText();
		this.channeldata = jsonNode2.toString();
		this.device = device;
		this.unit = unit.asText();
		this.channelAlarms.add(new ChannelAlarm(BigDecimal.ZERO,BigDecimal.valueOf(100),BigDecimal.valueOf(100),BigDecimal.valueOf(100),this));
	}

	public Long getId() {
		return id;
	}

	public Set<Alarmtemplate> getChannelAlarmtemplates() {
		return channelAlarmtemplates;
	}

	public void setChannelAlarmtemplates(Set<Alarmtemplate> channelAlarmtemplates) {
		this.channelAlarmtemplates = channelAlarmtemplates;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Dchannel name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public Dchannel unit(String unit) {
		this.unit = unit;
		return this;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getChanneldata() {
		return channeldata;
	}

	public Dchannel channeldata(String channeldata) {
		this.channeldata = channeldata;
		return this;
	}

	public void setChanneldata(String channeldata) {
		this.channeldata = channeldata;
	}

	public Device getDevice() {
		return device;
	}

	public Dchannel device(Device device) {
		this.device = device;
		return this;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Set<Alarmtemplate> getChannelAlarmTemplates() {
		return channelAlarmtemplates;
	}

	public Dchannel channelAlarmTemplates(Set<Alarmtemplate> channelAlarmTemplates) {
		this.channelAlarmtemplates = channelAlarmTemplates;
		return this;
	}

	public Dchannel addChannelAlarmTempalte(Alarmtemplate channelAlarmTemplate) {
		channelAlarmtemplates.add(channelAlarmTemplate);
		channelAlarmTemplate.setDchannel(this);
		return this;
	}

	public Dchannel removeChannelAlarmTempalte(Alarmtemplate channelAlarmTemplate) {
		channelAlarmtemplates.remove(channelAlarmTemplate);
		channelAlarmTemplate.setDchannel(null);
		return this;
	}

	public void setChannelAlarmTemplates(Set<Alarmtemplate> channelAlarmTemplates) {
		this.channelAlarmtemplates = channelAlarmTemplates;
	}
	
	public Set<ChannelAlarm> getChannelAlarms() {
		return channelAlarms;
	}

	public Dchannel channelAlarms(Set<ChannelAlarm> channelAlarms) {
		this.channelAlarms = channelAlarms;
		return this;
	}

	public Dchannel addChannelAlarm(ChannelAlarm channelAlarm) {
		channelAlarms.add(channelAlarm);
		channelAlarm.setDchannel(this);
		return this;
	}

	public Dchannel removeChannelAlarm(ChannelAlarm channelAlarm) {
		channelAlarms.remove(channelAlarm);
		channelAlarm.setDchannel(null);
		return this;
	}

	public void setChannelAlarms(Set<ChannelAlarm> channelAlarms) {
		this.channelAlarms = channelAlarms;
	}
	
	

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Dchannel dchannel = (Dchannel) o;
		if (dchannel.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, dchannel.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public int compareTo(Dchannel o) {		
		return this.getChannelNumber() - o.getChannelNumber();
	}

	@Override
	public String toString() {
		return "Dchannel{" + "id=" + id + ", name='" + name + "'" + ", unit='" + unit + "'" + ", channeldata='"
				+ channeldata + "'" + '}';
	}
}
