package com.polmon.polsoft.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.ReportAsSingleViolation;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A Alarmtemplateschedulars.
 */
@Entity
@Table(name = "alarmtemplateschedulars")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "alarmtemplateschedulars")
public class Alarmtemplateschedulars implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "day")
    private String day;

    @Column(name = "starttime")
    private String starttime;

    @Column(name = "endtime")
    private String endtime;

    @ManyToMany(mappedBy = "alarmtemplateschedulars")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Alarmtemplate> alarmtemplates = new HashSet<>();

    @Transient
    private boolean checked = true;

    public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = true;
	}
	
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public Alarmtemplateschedulars day(String day) {
        this.day = day;
        return this;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStarttime() {
        return starttime;
    }

    public Alarmtemplateschedulars starttime(String starttime) {
        this.starttime = starttime;
        return this;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public Alarmtemplateschedulars endtime(String endtime) {
        this.endtime = endtime;
        return this;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public Set<Alarmtemplate> getAlarmtemplates() {
        return alarmtemplates;
    }

    public Alarmtemplateschedulars alarmtemplates(Set<Alarmtemplate> alarmtemplates) {
        this.alarmtemplates = alarmtemplates;
        return this;
    }

    public Alarmtemplateschedulars addAlarmtemplate(Alarmtemplate alarmtemplate) {
        alarmtemplates.add(alarmtemplate);
        alarmtemplate.getAlarmtemplateschedulars().add(this);
        return this;
    }

    public Alarmtemplateschedulars removeAlarmtemplate(Alarmtemplate alarmtemplate) {
        alarmtemplates.remove(alarmtemplate);
        alarmtemplate.getAlarmtemplateschedulars().remove(this);
        return this;
    }

    public void setAlarmtemplates(Set<Alarmtemplate> alarmtemplates) {
        this.alarmtemplates = alarmtemplates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Alarmtemplateschedulars alarmtemplateschedulars = (Alarmtemplateschedulars) o;
        if(alarmtemplateschedulars.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, alarmtemplateschedulars.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Alarmtemplateschedulars{" +
            "id=" + id +
            ", day='" + day + "'" +
            ", starttime='" + starttime + "'" +
            ", endtime='" + endtime + "'" +
            '}';
    }
}
