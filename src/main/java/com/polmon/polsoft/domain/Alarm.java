package com.polmon.polsoft.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Alarm.
 */
@Entity
@Table(name = "alarm")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "alarm")
public class Alarm implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
   /* @GeneratedValue(strategy = GenerationType.AUTO)*/
    @SequenceGenerator(name="SEQ_GEN", sequenceName="SEQ_JUST_FOR_ALARM", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN")
    private Long id;

    @Column(name = "device")
    private String device;

    @Column(name = "channel")
    private String channel;

    @Column(name = "message")
    private String message;

    @Column(name = "severity")
    private Integer severity;

    @Column(name = "event_time")
    private LocalDate eventTime;

    @Column(name = "status")
    private String status;

    @Column(name = "comments")
    private String comments;

    @ManyToOne
    private User user;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDevice() {
        return device;
    }

    public Alarm device(String device) {
        this.device = device;
        return this;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getChannel() {
        return channel;
    }

    public Alarm channel(String channel) {
        this.channel = channel;
        return this;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public Alarm message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSeverity() {
        return severity;
    }

    public Alarm severity(Integer severity) {
        this.severity = severity;
        return this;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }

    public LocalDate getEventTime() {
        return eventTime;
    }

    public Alarm eventTime(LocalDate eventTime) {
        this.eventTime = eventTime;
        return this;
    }

    public void setEventTime(LocalDate eventTime) {
        this.eventTime = eventTime;
    }

    public String getStatus() {
        return status;
    }

    public Alarm status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public Alarm comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public Alarm user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Alarm alarm = (Alarm) o;
        if(alarm.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, alarm.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Alarm{" +
            "id=" + id +
            ", device='" + device + "'" +
            ", channel='" + channel + "'" +
            ", message='" + message + "'" +
            ", severity='" + severity + "'" +
            ", eventTime='" + eventTime + "'" +
            ", status='" + status + "'" +
            ", comments='" + comments + "'" +
            '}';
    }
}