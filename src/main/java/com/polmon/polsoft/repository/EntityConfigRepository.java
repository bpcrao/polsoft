package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.EntityConfig;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the EntityConfig entity.
 */
@Repository
public interface EntityConfigRepository extends JpaRepository<EntityConfig,Long> {
	
	EntityConfig findByEntityName(String entityName);
}
