package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Resource;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Resource entity.
 */
@SuppressWarnings("unused")
public interface ResourceRepository extends JpaRepository<Resource,Long> {

}
