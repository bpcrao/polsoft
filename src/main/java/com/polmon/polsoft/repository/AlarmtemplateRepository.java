package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Alarms;
import com.polmon.polsoft.domain.Alarmtemplate;
import com.polmon.polsoft.domain.ChannelAlarm;
import com.polmon.polsoft.domain.Dchannel;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the Alarmtemplate entity.
 */
@SuppressWarnings("unused")
public interface AlarmtemplateRepository extends JpaRepository<Alarmtemplate,Long> {

    @Query("select distinct alarmtemplate from Alarmtemplate alarmtemplate left join fetch alarmtemplate.alarmtemplateschedulars left join fetch alarmtemplate.alarmtemplatelimits")
    List<Alarmtemplate> findAllWithEagerRelationships();

    @Query("select alarmtemplate from Alarmtemplate alarmtemplate left join fetch alarmtemplate.alarmtemplateschedulars left join fetch alarmtemplate.alarmtemplatelimits where alarmtemplate.id =:id")
    Alarmtemplate findOneWithEagerRelationships(@Param("id") Long id);
    
    //@Query("select distinct alarmtemplate from Alarmtemplate alarmtemplate left join fetch alarmtemplate.alarmtemplateschedulars left join fetch alarmtemplate.alarmtemplatelimits where alarmtemplate.dchannel is not null")
    Set<Alarmtemplate> findAllByDchannel(Dchannel ch);
    Set<Alarmtemplate> findAllByDchannelId(Long dchannelId);
    
    @Query("select distinct alarmtemplate from Alarmtemplate alarmtemplate left join fetch alarmtemplate.alarmtemplateschedulars left join fetch alarmtemplate.alarmtemplatelimits where alarmtemplate.dchannel is null")
    Set<Alarmtemplate> findAllWithoutDchannel();
    
    List<Alarmtemplate> findAllByDchannelIn(@Param("dchannel") Set<Dchannel> ch);

   /* @Query("select new map (a.dchannel, a) FROM alarmtemplate a")
	HashMap<Dchannel ,Alarmtemplate> findAllAsMap();*/
}
