package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.domain.UserPassword;

import org.springframework.data.jpa.repository.*;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the UserPassword entity.
 */
@SuppressWarnings("unused")
public interface UserPasswordRepository extends JpaRepository<UserPassword,Long> {

    @Query("select userPassword from UserPassword userPassword where userPassword.user.login = ?#{principal.username}")
    List<UserPassword> findByUserIsCurrentUser();

    List<UserPassword> findAllByUserOrderByIdDesc(User user);

}
