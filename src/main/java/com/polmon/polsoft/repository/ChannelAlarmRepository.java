package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.ChannelAlarm;
import com.polmon.polsoft.domain.Dchannel;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the ChannelAlarm entity.
 */
@SuppressWarnings("unused")
public interface ChannelAlarmRepository extends JpaRepository<ChannelAlarm,Long> {

	Set<ChannelAlarm> findAllByDchannel(Dchannel ch);

}
