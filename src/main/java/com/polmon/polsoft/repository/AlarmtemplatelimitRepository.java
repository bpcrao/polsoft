package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Alarmtemplatelimit;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Alarmtemplatelimit entity.
 */
@SuppressWarnings("unused")
public interface AlarmtemplatelimitRepository extends JpaRepository<Alarmtemplatelimit,Long> {

}
