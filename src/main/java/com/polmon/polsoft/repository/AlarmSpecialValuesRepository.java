package com.polmon.polsoft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.polmon.polsoft.domain.AlarmSpecialValues;

public interface AlarmSpecialValuesRepository extends JpaRepository<AlarmSpecialValues,Long> {
	
	List<AlarmSpecialValues> findAll();
	List<AlarmSpecialValues> findAllByValue(Long channelVallue);
}
