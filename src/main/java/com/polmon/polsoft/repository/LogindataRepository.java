package com.polmon.polsoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.polmon.polsoft.domain.Logindata;

/**
 * Spring Data JPA repository for the Logindata entity.
 */
@SuppressWarnings("unused")
public interface LogindataRepository extends JpaRepository<Logindata,Long> {

	Logindata findOneByUserId(Long userId);

	
}
