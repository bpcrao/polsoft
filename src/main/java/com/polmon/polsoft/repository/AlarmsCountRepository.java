package com.polmon.polsoft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.polmon.polsoft.domain.Alarms;
import com.polmon.polsoft.domain.AlarmsCount;
/**
 * Spring Data JPA repository for the Alarms entity.
 */
@SuppressWarnings("unused")
public interface AlarmsCountRepository extends JpaRepository<AlarmsCount,Long> {
	List<AlarmsCount> findAll();
}
