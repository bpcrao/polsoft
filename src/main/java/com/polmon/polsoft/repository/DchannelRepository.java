package com.polmon.polsoft.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Device;

/**
 * Spring Data JPA repository for the Dchannel entity.
 */
@SuppressWarnings("unused")
public interface DchannelRepository extends JpaRepository<Dchannel,Long> {
	
	Set<Dchannel> findAllByDevice(Device device);
	
	Set<Dchannel> findAllByDeviceAndLocationNotNull(Device device);

}
