package com.polmon.polsoft.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.polmon.polsoft.domain.PersistentAuditEvent;
import com.polmon.polsoft.service.dto.CustomAuditEvent;

/**
 * Spring Data JPA repository for the PersistentAuditEvent entity.
 */
public interface PersistenceAuditEventRepository extends JpaRepository<PersistentAuditEvent, Long> {

    List<PersistentAuditEvent> findByPrincipal(String principal);

    List<PersistentAuditEvent> findByAuditEventDateAfter(LocalDateTime after);

    List<PersistentAuditEvent> findByPrincipalAndAuditEventDateAfter(String principal, LocalDateTime after);

    List<PersistentAuditEvent> findByPrincipalAndAuditEventDateAfterAndAuditEventType(String principle, LocalDateTime after, String type);

    Page<PersistentAuditEvent> findAllByAuditEventDateBetweenOrderByAuditEventDateDesc(LocalDateTime fromDate, LocalDateTime toDate, Pageable pageable);
    
    Page<PersistentAuditEvent> findAllByAuditEventDateBetweenAndAuditEventTypeEquals(LocalDateTime fromDate, LocalDateTime toDate, String eventTypeFilter, Pageable pageable);
    
    List<PersistentAuditEvent> findAllByAuditEventDateBetweenAndAuditEventTypeEquals(LocalDateTime fromDate, LocalDateTime toDate, String eventTypeFilter);

    Page<PersistentAuditEvent> findAllByAuditEventDateBetween(LocalDateTime fromDate, LocalDateTime toDate, Pageable pageable);
    
    List<PersistentAuditEvent> findAllByAuditEventDateBetween(LocalDateTime fromDate, LocalDateTime toDate);
    
    @Query("SELECT DISTINCT (a.auditEventType) from PersistentAuditEvent a")
    List<String> findAllEntityTypes();

	Page<PersistentAuditEvent> findAllByAuditEventDateBetweenAndAuditEventTypeEqualsOrderByAuditEventDateDesc(
			LocalDateTime fromDate, LocalDateTime toDate, String eventTypeFilter, Pageable pageable);

    @Query("SELECT DISTINCT (a.auditEventSource) from PersistentAuditEvent a")
	List<String> findAllAuditEventSource();

    @Query("SELECT max(a.commitVersion) FROM PersistentAuditEvent a where a.auditEventType = :auditEventType and a.entityId = :entityId")
	Integer findMaxCommitVersion(@Param("auditEventType") String auditEventType,@Param("entityId") Long entityId);

	Page<CustomAuditEvent> findAllByAuditEventType(String entityType, Pageable pageable);

    PersistentAuditEvent findOneByAuditEventTypeAndEntityIdAndCommitVersion(String qualifiedName, Long entityId,
			Integer commitVersion);

    PersistentAuditEvent findOneById(Long id);
    
    List<PersistentAuditEvent> findByIdIn(List<Long> auditsIds);
    
	void deleteAllByAuditEventDateBefore(LocalDateTime localDateTime);

}
