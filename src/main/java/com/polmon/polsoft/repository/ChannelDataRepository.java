package com.polmon.polsoft.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.polmon.polsoft.domain.Channeldata;

@Repository
public interface ChannelDataRepository extends JpaRepository<Channeldata, Long> {

	List<Channeldata> findAllByLogdateGreaterThanAndLogdateLessThanOrderByLogdateAsc(Date startLogDate,Date endLogDate);
	
	List<Channeldata> findAllByLogdateGreaterThanAndLogdateLessThanAndHourflagOrderByLogdateAsc(Date startLogDate,Date endLogDate,Integer hourflag);
	
	List<Channeldata> findAllByLogdateGreaterThanAndLogdateLessThanAndDayflagOrderByLogdateAsc(Date startLogDate,Date endLogDate,Integer dayflag);

}
