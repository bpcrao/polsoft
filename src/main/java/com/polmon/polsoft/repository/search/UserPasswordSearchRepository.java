package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.UserPassword;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the UserPassword entity.
 */
public interface UserPasswordSearchRepository extends ElasticsearchRepository<UserPassword, Long> {
}
