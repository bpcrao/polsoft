package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.EntityConfig;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the EntityConfig entity.
 */
public interface EntityConfigSearchRepository extends ElasticsearchRepository<EntityConfig, Long> {
}
