package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Dchannel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Dchannel entity.
 */
public interface DchannelSearchRepository extends ElasticsearchRepository<Dchannel, Long> {
}
