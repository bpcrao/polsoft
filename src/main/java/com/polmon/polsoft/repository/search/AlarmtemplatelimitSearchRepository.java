package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Alarmtemplatelimit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Alarmtemplatelimit entity.
 */
public interface AlarmtemplatelimitSearchRepository extends ElasticsearchRepository<Alarmtemplatelimit, Long> {
}
