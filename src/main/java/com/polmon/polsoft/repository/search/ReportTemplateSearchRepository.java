package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.ReportTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ReportTemplate entity.
 */
public interface ReportTemplateSearchRepository extends ElasticsearchRepository<ReportTemplate, Long> {
}
