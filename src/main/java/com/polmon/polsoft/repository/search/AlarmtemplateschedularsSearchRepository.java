package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Alarmtemplateschedulars;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Alarmtemplateschedulars entity.
 */
public interface AlarmtemplateschedularsSearchRepository extends ElasticsearchRepository<Alarmtemplateschedulars, Long> {
}
