package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Logindata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Logindata entity.
 */
public interface LogindataSearchRepository extends ElasticsearchRepository<Logindata, Long> {
}
