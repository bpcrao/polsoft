package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.ChannelAlarm;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ChannelAlarm entity.
 */
public interface ChannelAlarmSearchRepository extends ElasticsearchRepository<ChannelAlarm, Long> {
}
