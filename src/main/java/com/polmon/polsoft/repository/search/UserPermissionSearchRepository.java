package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.UserPermission;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the UserPermission entity.
 */
public interface UserPermissionSearchRepository extends ElasticsearchRepository<UserPermission, Long> {
}
