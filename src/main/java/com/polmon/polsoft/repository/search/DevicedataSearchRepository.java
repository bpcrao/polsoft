package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Devicedata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Devicedata entity.
 */
public interface DevicedataSearchRepository extends ElasticsearchRepository<Devicedata, Long> {
}
