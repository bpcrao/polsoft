package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Threshold;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Threshold entity.
 */
public interface ThresholdSearchRepository extends ElasticsearchRepository<Threshold, Long> {
}
