package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Alarms;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Alarms entity.
 */
public interface AlarmsSearchRepository extends ElasticsearchRepository<Alarms, Long> {
}
