package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Systempreferences;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Systempreferences entity.
 */
public interface SystempreferencesSearchRepository extends ElasticsearchRepository<Systempreferences, Long> {
}
