package com.polmon.polsoft.repository.search;

import com.polmon.polsoft.domain.Alarmtemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Alarmtemplate entity.
 */
public interface AlarmtemplateSearchRepository extends ElasticsearchRepository<Alarmtemplate, Long> {
}
