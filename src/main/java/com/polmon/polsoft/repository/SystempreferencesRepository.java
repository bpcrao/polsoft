package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Systempreferences;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Systempreferences entity.
 */
@SuppressWarnings("unused")
public interface SystempreferencesRepository extends JpaRepository<Systempreferences,Long> {
	Systempreferences findByKeyproperty(String key);
}
