package com.polmon.polsoft.repository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.polmon.polsoft.config.audit.AuditEventConverter;
import com.polmon.polsoft.domain.PersistentAuditEvent;
import com.polmon.polsoft.security.SecurityUtils;

/**
 * An implementation of Spring Boot's AuditEventRepository.
 */
@Repository
public class CustomAuditEventRepository implements AuditEventRepository {
	
	private final Logger log = LoggerFactory.getLogger(CustomAuditEventRepository.class);

	private static final String AUTHORIZATION_FAILURE = "AUTHORIZATION_FAILURE";

	private static final String ANONYMOUS_USER = "anonymoususer";

	@Inject
	private PersistenceAuditEventRepository persistenceAuditEventRepository;

	@Inject
	private AuditEventConverter auditEventConverter;
	
	@Autowired
	private Environment environment;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void add(AuditEvent event) {
		if(!SecurityUtils.isRestoredApplication(environment))
		{
			if (!AUTHORIZATION_FAILURE.equals(event.getType()) && !ANONYMOUS_USER.equals(event.getPrincipal().toString())) {
				PersistentAuditEvent persistentAuditEvent = new PersistentAuditEvent();
				persistentAuditEvent.setPrincipal(event.getPrincipal());
				persistentAuditEvent.setAuditEventType(event.getType());
				Instant instant = Instant.ofEpochMilli(event.getTimestamp().getTime());
				persistentAuditEvent.setAuditEventDate(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
				persistentAuditEvent.setData(auditEventConverter.convertDataToStrings(event.getData()));
				persistenceAuditEventRepository.save(persistentAuditEvent);
			}
		}else {
			log.debug("audit skipped");
		}
		
	}

	@Override
	public List<AuditEvent> find(Date after) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuditEvent> find(String principal, Date after) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuditEvent> find(String principal, Date after, String type) {
		// TODO Auto-generated method stub
		return null;
	}

}
