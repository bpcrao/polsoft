package com.polmon.polsoft.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.polmon.polsoft.domain.Alarms;
import com.polmon.polsoft.domain.Dchannel;
/**
 * Spring Data JPA repository for the Alarms entity.
 */
@SuppressWarnings("unused")
public interface AlarmsRepository extends JpaRepository<Alarms,Long> {
	List<Alarms> findAllByDchannel(Dchannel ch);
	
    //@Query("select distinct alarms from Alarms alarms left join fetch alarms.dchannel where alarmtemplate.dchannel is not null")
	List<Alarms> findAllByDchannelAndStatusStartingWithIgnoreCaseAndSeverityNotNull(@Param("dchannel") Dchannel ch,@Param("status")String status);
	List<Alarms> findAllByStatusAndDelayNotNull(@Param("status")String status);
	List<Alarms> findByStatusStartingWithIgnoreCaseAndDelayNotNull(@Param("status")String status);
	
    List<Alarms> findAllByGeneratedBetween(ZonedDateTime fromDate, ZonedDateTime toDate);
    Page<Alarms> findAllByGeneratedBetween(ZonedDateTime fromDate, ZonedDateTime toDate, Pageable pageable);

	List<Alarms> findByStatusStartingWithIgnoreCase(@Param("status")String status);
	List<Alarms> findAllByDchannelInAndStatusStartingWithIgnoreCase(@Param("dchannel") Set<Dchannel> ch,@Param("status")String status);
	
	List<Alarms>  deleteAllByCreatedDateBefore(ZonedDateTime beforeDate);
	
	List<Alarms>  deleteAllByStatusAndGeneratedBefore(String status,ZonedDateTime beforeDate);

	@Query("select count(1) from Alarms alarms where (alarms.severity='alarm' or alarms.severity='-') and (alarms.status='active' or alarms.status='active acknowledged')")
	Long getAlarmCount();
	
	@Query("select count(1) from Alarms alarms where alarms.severity='warning' and (alarms.status='active' or alarms.status='active acknowledged')")
	Long getWarningCount();
	
	//Long countByStatusActiveAndSeverityAlarm();
	//Long countByStatusActiveAndSeverityWarning();
}
