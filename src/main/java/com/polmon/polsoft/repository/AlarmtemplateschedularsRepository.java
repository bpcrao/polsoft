package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Alarmtemplateschedulars;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Alarmtemplateschedulars entity.
 */
@SuppressWarnings("unused")
public interface AlarmtemplateschedularsRepository extends JpaRepository<Alarmtemplateschedulars,Long> {

}
