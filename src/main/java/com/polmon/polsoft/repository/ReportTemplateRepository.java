package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.ReportTemplate;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ReportTemplate entity.
 */
@SuppressWarnings("unused")
public interface ReportTemplateRepository extends JpaRepository<ReportTemplate,Long> {

}
