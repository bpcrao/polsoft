package com.polmon.polsoft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.domain.UserPermission;

/**
 * Spring Data JPA repository for the UserPermission entity.
 */
@SuppressWarnings("unused")
public interface UserPermissionRepository extends JpaRepository<UserPermission,Long> {

    @Query("select userPermission from UserPermission userPermission where userPermission.user.login = ?#{principal.username}")
    List<UserPermission> findByUserIsCurrentUser();
    
    Page<UserPermission> findAllByUser(User user, Pageable pageable);  
    
    List<UserPermission> findAllByUser(User user);  

    

}
