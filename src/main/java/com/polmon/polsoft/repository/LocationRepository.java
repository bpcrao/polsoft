package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Location;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data JPA repository for the Location entity.
 */
public interface LocationRepository extends JpaRepository<Location,Long> {
	
	Location findOneByParent(Location loc);
	
	@Transactional
	@Modifying
	@Query("update Location u set u.hasData = true where u.id in :ids")
	void setLocationById(@Param("ids") List<Long> ids);
	
	Optional<Location> findOneByName(String name);
	
	List<Location> findByIdIn(List<Long> locationIds);

}
