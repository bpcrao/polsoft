package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Devicedata;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Devicedata entity.
 */
@SuppressWarnings("unused")
public interface DevicedataRepository extends JpaRepository<Devicedata,Long> {

}
