package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Threshold;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Threshold entity.
 */
@SuppressWarnings("unused")
public interface ThresholdRepository extends JpaRepository<Threshold,Long> {

}
