package com.polmon.polsoft.repository;

import com.polmon.polsoft.domain.Authority;
import com.polmon.polsoft.domain.Device;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Device entity.
 */
@SuppressWarnings("unused")
public interface DeviceRepository extends JpaRepository<Device,Long> {

	Device findOneBySerial(String name);

}
