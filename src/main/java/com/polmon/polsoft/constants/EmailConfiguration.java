package com.polmon.polsoft.constants;

public interface EmailConfiguration {
	
	static final String EMAIL_HOST= "Email Host";
	
	static final String EMAIL_PORT= "Email Port";
	
	static final String EMAIL_USER_NAME= "Email UserName";
	
	static final String EMAIL_PASSWORD="Email Password";
	
	static final String CLEAN_PERIOD="Clear Period";
	
	static final String REPORT_DOWNLOAD_FOLDER="Report Download Folder";
	
	static final String COMPANY_LOGO="Company Logo Path";
	
	static final String BACKUP_FOLDER_PATH="Back up folder location";
}
