package com.polmon.polsoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.service.AlarmtemplatelimitService;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.service.dto.AlarmtemplatelimitDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Alarmtemplatelimit.
 */
@RestController
@RequestMapping("/api")
public class AlarmtemplatelimitResource {

    private final Logger log = LoggerFactory.getLogger(AlarmtemplatelimitResource.class);
        
    @Inject
    private AlarmtemplatelimitService alarmtemplatelimitService;

    /**
     * POST  /alarmtemplatelimits : Create a new alarmtemplatelimit.
     *
     * @param alarmtemplatelimitDTO the alarmtemplatelimitDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alarmtemplatelimitDTO, or with status 400 (Bad Request) if the alarmtemplatelimit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarmtemplatelimits",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplatelimitDTO> createAlarmtemplatelimit(@RequestBody AlarmtemplatelimitDTO alarmtemplatelimitDTO) throws URISyntaxException {
        log.debug("REST request to save Alarmtemplatelimit : {}", alarmtemplatelimitDTO);
        if (alarmtemplatelimitDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("alarmtemplatelimit", "idexists", "A new alarmtemplatelimit cannot already have an ID")).body(null);
        }
        AlarmtemplatelimitDTO result = alarmtemplatelimitService.save(alarmtemplatelimitDTO);
        return ResponseEntity.created(new URI("/api/alarmtemplatelimits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("alarmtemplatelimit", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /alarmtemplatelimits : Updates an existing alarmtemplatelimit.
     *
     * @param alarmtemplatelimitDTO the alarmtemplatelimitDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alarmtemplatelimitDTO,
     * or with status 400 (Bad Request) if the alarmtemplatelimitDTO is not valid,
     * or with status 500 (Internal Server Error) if the alarmtemplatelimitDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarmtemplatelimits",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplatelimitDTO> updateAlarmtemplatelimit(@RequestBody AlarmtemplatelimitDTO alarmtemplatelimitDTO) throws URISyntaxException {
        log.debug("REST request to update Alarmtemplatelimit : {}", alarmtemplatelimitDTO);
        if (alarmtemplatelimitDTO.getId() == null) {
            return createAlarmtemplatelimit(alarmtemplatelimitDTO);
        }
        AlarmtemplatelimitDTO result = alarmtemplatelimitService.save(alarmtemplatelimitDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("alarmtemplatelimit", alarmtemplatelimitDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /alarmtemplatelimits : get all the alarmtemplatelimits.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alarmtemplatelimits in body
     */
    @RequestMapping(value = "/alarmtemplatelimits",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplatelimitDTO> getAllAlarmtemplatelimits() {
        log.debug("REST request to get all Alarmtemplatelimits");
        return alarmtemplatelimitService.findAll();
    }

    /**
     * GET  /alarmtemplatelimits/:id : get the "id" alarmtemplatelimit.
     *
     * @param id the id of the alarmtemplatelimitDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alarmtemplatelimitDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/alarmtemplatelimits/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplatelimitDTO> getAlarmtemplatelimit(@PathVariable Long id) {
        log.debug("REST request to get Alarmtemplatelimit : {}", id);
        AlarmtemplatelimitDTO alarmtemplatelimitDTO = alarmtemplatelimitService.findOne(id);
        return Optional.ofNullable(alarmtemplatelimitDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /alarmtemplatelimits/:id : delete the "id" alarmtemplatelimit.
     *
     * @param id the id of the alarmtemplatelimitDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/alarmtemplatelimits/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAlarmtemplatelimit(@PathVariable Long id) {
        log.debug("REST request to delete Alarmtemplatelimit : {}", id);
        alarmtemplatelimitService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("alarmtemplatelimit", id.toString())).build();
    }

    /**
     * SEARCH  /_search/alarmtemplatelimits?query=:query : search for the alarmtemplatelimit corresponding
     * to the query.
     *
     * @param query the query of the alarmtemplatelimit search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/alarmtemplatelimits",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplatelimitDTO> searchAlarmtemplatelimits(@RequestParam String query) {
        log.debug("REST request to search Alarmtemplatelimits for query {}", query);
        return alarmtemplatelimitService.search(query);
    }


}
