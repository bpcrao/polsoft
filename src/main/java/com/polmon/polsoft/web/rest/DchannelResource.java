package com.polmon.polsoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.repository.DeviceRepository;
import com.polmon.polsoft.service.DchannelService;
import com.polmon.polsoft.service.dto.DchannelDTO;
import com.polmon.polsoft.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Dchannel.
 */
@RestController
@RequestMapping("/api")
public class DchannelResource {

    private final Logger log = LoggerFactory.getLogger(DchannelResource.class);
        
    @Inject
    private DchannelService dchannelService;
    
    @Inject
    private DeviceRepository deviceRepository;

    /**
     * POST  /dchannels : Create a new dchannel.
     *
     * @param dchannelDTO the dchannelDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dchannelDTO, or with status 400 (Bad Request) if the dchannel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dchannels",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DchannelDTO> createDchannel(@RequestBody DchannelDTO dchannelDTO) throws URISyntaxException {
        log.debug("REST request to save Dchannel : {}", dchannelDTO);
        if (dchannelDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("dchannel", "idexists", "A new dchannel cannot already have an ID")).body(null);
        }
        DchannelDTO result = dchannelService.save(dchannelDTO);
        return ResponseEntity.created(new URI("/api/dchannels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("dchannel", result.getId().toString()))
            .body(result);
    }
    
    

    /**
     * PUT  /dchannels : Updates an existing dchannel.
     *
     * @param dchannelDTO the dchannelDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dchannelDTO,
     * or with status 400 (Bad Request) if the dchannelDTO is not valid,
     * or with status 500 (Internal Server Error) if the dchannelDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dchannels",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DchannelDTO> updateDchannel(@RequestBody DchannelDTO dchannelDTO) throws URISyntaxException {
        log.debug("REST request to update Dchannel : {}", dchannelDTO);
        if (dchannelDTO.getId() == null) {
            return createDchannel(dchannelDTO);
        }
        DchannelDTO result = dchannelService.save(dchannelDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("dchannel", dchannelDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dchannels : get all the dchannels.
     *
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of dchannels in body
     */
    @RequestMapping(value = "/dchannels/device/{deviceId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
	public List<DchannelDTO> getAllDchannelsOfDevice(@PathVariable Long deviceId,@RequestParam (value="detailedData", required = false) boolean detailedData) {        
        log.debug("REST request to get all Dchannels of device "+deviceId);
        Device device = deviceRepository.findOne(deviceId);
        return dchannelService.findAllByDevice(device, detailedData);
    }
    
    /**
     * GET  /dchannels : get all the dchannels.
     *
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of dchannels in body
     */
    @RequestMapping(value = "/dchannels",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DchannelDTO> getAllDchannels(@RequestParam(required = false) String filter,@RequestParam (required = false) Long deviceId, @RequestParam (required = false) Boolean detailedData) {
        if ("location-is-null".equals(filter)) {
            log.debug("REST request to get all Dchannels where location is null");
            return dchannelService.findAllWhereLocationIsNull();
        }
        if(deviceId!=null){
        	return getAllDchannelsOfDevice(deviceId, detailedData);
        }
        log.debug("REST request to get all Dchannels");
        return dchannelService.findAll();
    }
    
    
    /**
     * GET  /dchannels/:id : get the "id" dchannel.
     *
     * @param id the id of the dchannelDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dchannelDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dchannels/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DchannelDTO> getDchannel(@PathVariable Long id) {
        log.debug("REST request to get Dchannel : {}", id);
        DchannelDTO dchannelDTO = dchannelService.findOne(id);
        return Optional.ofNullable(dchannelDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dchannels/:id : delete the "id" dchannel.
     *
     * @param id the id of the dchannelDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/dchannels/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDchannel(@PathVariable Long id) {
        log.debug("REST request to delete Dchannel : {}", id);
        dchannelService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("dchannel", id.toString())).build();
    }

    /**
     * SEARCH  /_search/dchannels?query=:query : search for the dchannel corresponding
     * to the query.
     *
     * @param query the query of the dchannel search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/dchannels",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DchannelDTO> searchDchannels(@RequestParam String query) {
        log.debug("REST request to search Dchannels for query {}", query);
        return dchannelService.search(query);
    }


}
