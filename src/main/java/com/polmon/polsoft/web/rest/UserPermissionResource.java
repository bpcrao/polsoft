package com.polmon.polsoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.domain.Authority;
import com.polmon.polsoft.domain.Resource;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.service.UserPermissionService;
import com.polmon.polsoft.service.UserService;
import com.polmon.polsoft.service.dto.ResourceDTO;
import com.polmon.polsoft.service.dto.UserPermissionDTO;
import com.polmon.polsoft.service.dto.UserPermissionsDTO;
import com.polmon.polsoft.service.mapper.ResourceMapper;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.web.rest.util.PaginationUtil;

/**
 * REST controller for managing UserPermission.
 */
@RestController
@RequestMapping("/api")
public class UserPermissionResource {

    private final Logger log = LoggerFactory.getLogger(UserPermissionResource.class);
        
    @Inject
    private UserPermissionService userPermissionService;
    
    @Inject
    private UserService userService;
    
    @Inject
    private ResourceMapper resourceMapper;

    /**
     * POST  /user-permissions : Create a new userPermission.
     *
     * @param userPermissionDTO the userPermissionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userPermissionDTO, or with status 400 (Bad Request) if the userPermission has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-permissions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserPermissionDTO> createUserPermission(@RequestBody UserPermissionDTO userPermissionDTO) throws URISyntaxException {
        log.debug("REST request to save UserPermission : {}", userPermissionDTO);
        if (userPermissionDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userPermission", "idexists", "A new userPermission cannot already have an ID")).body(null);
        }
        UserPermissionDTO result = userPermissionService.save(userPermissionDTO);
        return ResponseEntity.created(new URI("/api/user-permissions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userPermission", result.getId().toString()))
            .body(result);
    }
    
    /**
     * POST  /user-permissions : Create a new userPermission.
     *
     * @param userPermissionDTO the userPermissionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userPermissionDTO, or with status 400 (Bad Request) if the userPermission has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/bulkuser-permissions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserPermissionsDTO> createUserPermissions(@RequestBody UserPermissionsDTO userPermissionsDTO) throws URISyntaxException {
        log.debug("REST request to save UserPermissions : {}", userPermissionsDTO);
        UserPermissionsDTO result= new UserPermissionsDTO();
        result.setUserPermissions(new ArrayList<UserPermissionDTO>());
		userPermissionsDTO.getUserPermissions().stream()
				.forEach(userPermissionDTO -> result.addUserPermission(userPermissionService.save(userPermissionDTO)));
		
        return ResponseEntity.created(new URI("/api/bulkuser-permissions"))
            .headers(HeaderUtil.createEntityCreationAlert("UserPermission"))
            .body(result);
    }

    /**
     * PUT  /user-permissions : Updates an existing userPermission.
     *
     * @param userPermissionDTO the userPermissionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userPermissionDTO,
     * or with status 400 (Bad Request) if the userPermissionDTO is not valid,
     * or with status 500 (Internal Server Error) if the userPermissionDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-permissions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserPermissionDTO> updateUserPermission(@RequestBody UserPermissionDTO userPermissionDTO) throws URISyntaxException {
        log.debug("REST request to update UserPermission : {}", userPermissionDTO);
        if (userPermissionDTO.getId() == null) {
            return createUserPermission(userPermissionDTO);
        }
        UserPermissionDTO result = userPermissionService.save(userPermissionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userPermission", userPermissionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-permissions : get all the userPermissions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userPermissions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/user-permissions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UserPermissionDTO>> getAllUserPermissions(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of UserPermissions");
        Page<UserPermissionDTO> page = userPermissionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-permissions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-permissions/:id : get the "id" userPermission.
     *
     * @param id the id of the userPermissionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userPermissionDTO, or with status 404 (Not Found)
     */
//    @RequestMapping(value = "/user-permissions/{id}",
//        method = RequestMethod.GET,
//        produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<UserPermissionDTO> getUserPermission(@PathVariable Long id) {
//        log.debug("REST request to get UserPermission : {}", id);
//        UserPermissionDTO userPermissionDTO = userPermissionService.findOne(id);
//        return Optional.ofNullable(userPermissionDTO)
//            .map(result -> new ResponseEntity<>(
//                result,
//                HttpStatus.OK))
//            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
//    }
//    
    @RequestMapping(value = "/user-permissions/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UserPermissionDTO>> getUserPermission(@PathVariable String name,Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get UserPermission : {}", name);
        List<User> user = userService.getUserWithAuthoritiesByLogin(name).map(Collections::singletonList).
        		orElse(Collections.emptyList());        		
        Page<UserPermissionDTO> page = userPermissionService.findAllByUser(user.get(0), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-permissions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @RequestMapping(value = "/effective-permissions/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ResourceDTO>> getUserPermission1(@PathVariable String name,Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get UserPermission : {}", name);
        List<User> user = userService.getUserWithAuthoritiesByLogin(name).map(Collections::singletonList).
        		orElse(Collections.emptyList());      
        Stream<Resource> ls = user.get(0).getAuthorities().stream().map(Authority::getResources).flatMap(Collection::stream);
        List<ResourceDTO> result = ls.map(resourceMapper::resourceToResourceDTO).collect(Collectors.toList());
        return new ResponseEntity<>(result, HttpStatus.OK);
	}
    
    
//    public void getEffectivePermissions(String loginName){
//    	List<User> user = userService.getUserWithAuthoritiesByLogin(loginName).map(Collections::singletonList).
//        		orElse(Collections.emptyList());      
//        Stream<Resource> ls = user.get(0).getAuthorities().stream().map(Authority::getResources).flatMap(Collection::stream);
//        List<ResourceDTO> authoritiesPermissions = ls.map(resourceMapper::resourceToResourceDTO).collect(Collectors.toList());
//        //List<UserPermissionDTO> userPermissions = userPermissionService.findAllByUser(user.get(0));
//    }

    /**
     * DELETE  /user-permissions/:id : delete the "id" userPermission.
     *
     * @param id the id of the userPermissionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/user-permissions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserPermission(@PathVariable Long id) {
        log.debug("REST request to delete UserPermission : {}", id);
        userPermissionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userPermission", id.toString())).build();
    }

    /**
     * SEARCH  /_search/user-permissions?query=:query : search for the userPermission corresponding
     * to the query.
     *
     * @param query the query of the userPermission search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/user-permissions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UserPermissionDTO>> searchUserPermissions(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of UserPermissions for query {}", query);
        Page<UserPermissionDTO> page = userPermissionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/user-permissions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
