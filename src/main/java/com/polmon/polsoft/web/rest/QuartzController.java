package com.polmon.polsoft.web.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.polmon.polsoft.domain.ReportTemplate;
import com.polmon.polsoft.service.ReportTemplateService;
import com.polmon.polsoft.service.dto.JobData;

@RestController
@RequestMapping("/api/scheduler")
@Profile({"dev","prod"})
public class QuartzController {

	@Autowired
	private SchedulerFactoryBean schedFactory;
	
	@Autowired
	ReportTemplateService reportTemplateService;
	
	@SuppressWarnings("serial")
	Map<String,String> jobclassMap = new HashMap<String, String>() {{
	    put("Reports", "com.polmon.polsoft.web.rest.jobs.ReportsGenerator");
	    put("Backup", "com.polmon.polsoft.web.rest.jobs.BackupJob");	 
	}};
	
	@RequestMapping(value = "/getjobclasses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Set<String> getListOfClassesToschedule(){
		return jobclassMap.keySet();
	}
	@RequestMapping(value = "/getReportTemplates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<Long,String> getReportTempplates(){
		List<ReportTemplate> reportsTemplates = reportTemplateService.findAllReports();
		Map<Long,String> allTemplateIds = reportsTemplates.stream().collect(Collectors.toMap(template -> template.getId(), template -> template.getName()));
		return allTemplateIds;
	}

	@RequestMapping(value = "/schedulejob", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public JobData schedule(@RequestBody JobData jobData) {
		try {

			JobKey jkey = new JobKey(jobData.getJobName(), jobData.getJobGroup());
			String strclassName = jobclassMap.get(jobData.getClassName());
			Class<? extends Job> clazz= (Class<? extends Job>) this.getClass().getClassLoader().loadClass(strclassName);
			JobDetail job = JobBuilder.newJob(clazz).withDescription(jobData.getJobDesciption()).withIdentity(jkey)
					.build();
			String templateId =jobData.getTemplateid();
			if(!org.apache.commons.lang3.StringUtils.isEmpty(templateId)){
				JobDataMap jobMap = job.getJobDataMap();
				jobMap.put("templateId", templateId);
			}
			
			TriggerKey tkey = new TriggerKey(jkey.getName()+jkey.getGroup());
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(tkey.getName(),jkey.getName())
					.withSchedule(CronScheduleBuilder.cronSchedule(jobData.getCronExpression())).build();
			schedFactory.getScheduler().scheduleJob(job, trigger);
			jobData.setStatus("Scheduled");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			jobData.setStatus("Failed To Scheduled :" + e.getMessage());
			e.printStackTrace();
		}
		
		return jobData;
	}

	@RequestMapping(value = "/unschedulejob", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<JobData> unschedule(@RequestBody JobData jobData) {
		String scheduled = "Unscheduled";		
		try {
			JobKey jkey = new JobKey(jobData.getJobName(), jobData.getJobGroup());
			if (schedFactory.getScheduler().checkExists(jkey)) {
				TriggerKey tkey = new TriggerKey(jkey.getName()+jkey.getGroup(),jobData.getJobName());
				schedFactory.getScheduler().unscheduleJob(tkey);
				schedFactory.getScheduler().deleteJob(jkey);
			}
			
		} catch (SchedulerException e) {
			scheduled = "Error while unscheduling " + e.getMessage();
		}
		jobData.setStatus(scheduled);

		return getScheduledJobs();
	}

	@RequestMapping(value = "/startjob", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public JobData scheduleNow(@RequestBody JobData jobData) {
		String scheduled = "triggered";

		try {
			JobKey jkey = new JobKey(jobData.getJobName(), jobData.getJobGroup());
			if (schedFactory.getScheduler().checkExists(jkey)) {
				schedFactory.getScheduler().triggerJob(jkey);
			} else {
				jobData.setStatus("Job not found");
			}

		} catch (SchedulerException e) {
			scheduled = "Error while triggering job " + e.getMessage();
		}
		jobData.setStatus(scheduled);

		return jobData;
	}
	
	@RequestMapping(value = "/updatejob", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public JobData updateJob(@RequestBody JobData jobData) {
		String scheduled = "job updated ";

		try {
			JobKey jkey = new JobKey(jobData.getJobName(), jobData.getJobGroup());
			if (schedFactory.getScheduler().checkExists(jkey)) {
				Scheduler scheduler = schedFactory.getScheduler();
				TriggerKey tkey = new TriggerKey(jkey.getName()+jkey.getGroup(),jobData.getJobName());
				CronTriggerImpl trigger = (CronTriggerImpl) scheduler.getTrigger(tkey);
			    trigger.setCronExpression(jobData.getCronExpression());
			     scheduler.rescheduleJob(tkey, trigger);
			} else {
				jobData.setStatus("Job not found");
			}

		} catch (SchedulerException e) {
			scheduled = "Error while rescheduling job " + e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jobData.setStatus(scheduled);

		return jobData;
	}

	@RequestMapping(value = "/getAllJobs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<JobData> getScheduledJobs() {
		List<JobData> listOfJobs = new ArrayList<>();
		try {
			Scheduler scheduler = schedFactory.getScheduler();
			List<String> groupNames = scheduler.getJobGroupNames();
			for (String groupName : groupNames) {

				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
					JobData jd = new JobData();

					jd.setJobName(jobKey.getName());
					jd.setJobGroup(jobKey.getGroup());
					JobDetail jobdetail = scheduler.getJobDetail(jobKey);
					String jobType = jobclassMap.entrySet().stream()
					  .filter(e -> e.getValue().equals(jobdetail.getJobClass().getName()))
					  .map(Map.Entry::getKey)
					  .findFirst()
					  .orElse(null);
					jd.setClassName(jobType);
					jd.setJobDesciption(jobdetail.getDescription());
					// get job's trigger
					JobDataMap dataMap = jobdetail.getJobDataMap();
					if(dataMap.containsKey("templateId")){
						jd.setTemplateid(dataMap.getString("templateId"));
					}
					List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
					Trigger trigger = triggers.get(0);
					if (trigger instanceof CronTriggerImpl) {
						String cronexpr = ((CronTriggerImpl) trigger).getCronExpression();
						jd.setCronExpression(cronexpr);
					}

					Date nextFireTime = trigger.getNextFireTime();
					Date prevFireTime = trigger.getPreviousFireTime();
					jd.setNextExecutionTime(nextFireTime);
					jd.setLastExceutionTime(prevFireTime);

					listOfJobs.add(jd);
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listOfJobs;
	}
}