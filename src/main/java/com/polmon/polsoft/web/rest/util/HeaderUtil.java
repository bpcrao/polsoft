package com.polmon.polsoft.web.rest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

/**
 * Utility class for HTTP headers creation.
 */
public class HeaderUtil {

    private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-polsoftApp-alert", message);
		if (param != null) {
			headers.add("X-polsoftApp-params", param);
		}
        return headers;
    }

    public static HttpHeaders createEntityCreationAlert(String entityName, String param) {
        return createAlert("polsoftApp." + entityName + ".created", param);
    }
    public static HttpHeaders createEntityCreationAlert(String entityName) {
        return createAlert("polsoftApp." + entityName + ".created", null);
    }

    public static HttpHeaders createEntityUpdateAlert(String entityName, String param) {
        return createAlert("polsoftApp." + entityName + ".updated", param);
    }

    public static HttpHeaders createEntityDeletionAlert(String entityName, String param) {
        return createAlert("polsoftApp." + entityName + ".deleted", param);
    }

    public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
        log.error("Entity creation failed, {}", defaultMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-polsoftApp-error", "error." + errorKey);
        headers.add("X-polsoftApp-params", entityName);
        return headers;
    }
}
