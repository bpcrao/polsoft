package com.polmon.polsoft.web.rest;

import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.polmon.polsoft.InfluxDBBackupAndRestore;
import com.polmon.polsoft.PostGresBackUpAndImport;
import com.polmon.polsoft.config.BackupRestoreProperties;
import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.constants.EmailConfiguration;
import com.polmon.polsoft.domain.Systempreferences;
import com.polmon.polsoft.repository.SystempreferencesRepository;
import com.polmon.polsoft.security.SecurityUtils;

@RestController
@RequestMapping("/api")
@EnableConfigurationProperties({ BackupRestoreProperties.class })
public class BackupAndRestoreResource {
	@Autowired
	private PostGresBackUpAndImport backUpAndImport;
	
	@Autowired
	private InfluxDBBackupAndRestore influxDBBackupAndRestoreService;
	
	@Autowired
	private BackupRestoreProperties backupRestoreProperties;

	@Autowired
	private SystempreferencesRepository systempreferencesRepository;
	
	@Autowired
	private AuditEventPublisher auditEventPublisher;
	
	@PostConstruct
	public void init()
	{
		Systempreferences systempreferences = systempreferencesRepository.findByKeyproperty(EmailConfiguration.BACKUP_FOLDER_PATH);
		if(systempreferences!=null)
		{
			backupRestoreProperties.setBackupDir(systempreferences.getData());
		}
	}
	
	@RequestMapping("/backupandcheck")
	public void backUpAndRestoreDatabase() throws Exception {
		backUpAndImport.export(getBackupFolder()+"/postgres/backup.sql");
		backUpAndImport.importDB(getBackupFolder());
		backUpAndImport.startnewInstance();
	}
	
	@RequestMapping("/backupInfluxDB")
	public void backupInfluxDB()
	{
		influxDBBackupAndRestoreService.backup( getBackupFolder()+"/influx");
	}
	
	@RequestMapping("/backupData")
	public void backupData() throws IOException, InterruptedException{
		String backupFolderName = getBackupFolder();
		(new File(backupRestoreProperties.getBackupDir()+"/"+backupFolderName+"/influx")).mkdirs();
		(new File(backupRestoreProperties.getBackupDir()+"/"+backupFolderName+"/postgres")).mkdirs();
		File f = new File(backupRestoreProperties.getBackupDir()+"/"+backupFolderName+"/postgres/"+backupRestoreProperties.getPostgresbackupname());
		f.createNewFile();
		influxDBBackupAndRestoreService.backup(backupFolderName+"\\influx");
		backUpAndImport.export(backupRestoreProperties.getBackupDir()+"\\"+backupFolderName+"\\postgres\\"+backupRestoreProperties.getPostgresbackupname());
	}
	
	private String getBackupFolder() {
		String dateFormat = "yyyy-MM-dd-HH-mm-ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		String backupFolderName = simpleDateFormat.format(new Date());
		return backupFolderName;
	}
	
	@RequestMapping("/restore/{folderName}")
	public void restoreInfluxDB(@PathVariable("folderName")String folderName) throws IOException, InterruptedException{
		influxDBBackupAndRestoreService.restore(folderName);
		String backupFolderName = folderName;
		backUpAndImport.restoreDB(backupRestoreProperties.getBackupDir()+"\\"+backupFolderName+"\\postgres\\"+backupRestoreProperties.getPostgresbackupname());
	}
	
	@RequestMapping("/restoreVerify/{folderName}")
	public void restoreVerify(@PathVariable("folderName")String folderName) throws IOException, InterruptedException{
		influxDBBackupAndRestoreService.verify(folderName);
		String backupFolderName = folderName;
		backUpAndImport.verifyImportDB(backupRestoreProperties.getBackupDir()+"\\"+backupFolderName+"\\postgres\\"+backupRestoreProperties.getPostgresbackupname());
		backUpAndImport.startnewInstance();
		auditEventPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "backup "+folderName+" verified",Collections.emptyMap()));
		writeBackupFolderToFile(folderName);
	}
	
	@RequestMapping("/verifiedFolderName")
	public String verifiedFolderName()
	{
		File file = new File(backupRestoreProperties.getBackupDir(), "verify.txt");
		FileReader reader = null;
		String folderName =null;
		try {
			reader = new FileReader(file);
			folderName = IOUtils.toString(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			IOUtils.closeQuietly(reader);
		}
		return "{\"data\":\""+StringUtils.defaultString(folderName, "")+"\"}";
	}
	
	@RequestMapping("/backupFolderList")
	public List<String> getInfluxBackupList()
	{
		
		List<String> backups = new ArrayList<>();
		File backUpFolder = new File(backupRestoreProperties.getBackupDir());
		FileFilter folderFilter = new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory();
			}
		};
		File[] folders = backUpFolder.listFiles(folderFilter);
		if(folders!=null && folders.length>0)
		{
			Arrays.sort(folders, Comparator.comparingLong(File::lastModified).reversed());
			for (File folder : folders) {
				backups.add(folder.getName());
			}
		}
		return backups;
	}
	
	private void writeBackupFolderToFile(String folderName) {
		File file = new File(backupRestoreProperties.getBackupDir(), "verify.txt");
		FileWriter outputWriter = null;
		try {
			outputWriter = new FileWriter(file);
			IOUtils.write(folderName, outputWriter);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
              IOUtils.closeQuietly(outputWriter);
		}
	}
}
