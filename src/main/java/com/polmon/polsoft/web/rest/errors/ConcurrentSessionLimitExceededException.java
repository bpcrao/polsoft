package com.polmon.polsoft.web.rest.errors;

import org.springframework.security.core.AuthenticationException;

public class ConcurrentSessionLimitExceededException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public ConcurrentSessionLimitExceededException(String msg) {
		super(msg);
	}

}
