package com.polmon.polsoft.web.rest.dataloggers;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.config.PolSoftNewInstance;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.repository.LocationRepository;
import com.polmon.polsoft.service.DeviceService;
import com.polmon.polsoft.web.rest.InfluxDbLogger;

@RestController
@RequestMapping("/api")
public class DataLoggerProxy {

	private final Logger log = LoggerFactory.getLogger(DataLoggerProxy.class);

	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;

	@Autowired
	private InfluxDbLogger influxDbLogger;

	@Inject
	private LocationRepository locationRepository;
	
	

	@RequestMapping(value = "/write", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public void getList(HttpEntity<byte[]> requestEntity, HttpServletResponse response) throws Exception {
		try {
			influxDbLogger.asyncLogData( new String(requestEntity.getBody()));
			log.info("finished");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	@RequestMapping(value = "/simulate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public void simulateData() {
		influxDBTemplate.createDatabase();
		Collection<Location> lkocations = locationRepository.findAll().stream()
				.filter(location -> location.getDchannel() != null).collect(Collectors.toCollection(LinkedList::new));
		for (Iterator iterator = lkocations.iterator(); iterator.hasNext();) {
			Location location = (Location) iterator.next();
			if (location.getParent() != null) {
				Instant instant = Instant.now();
				ZoneId zoneId = ZoneId.systemDefault();
				ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, zoneId);
				ZonedDateTime twoMonthsBack = zdt.minusHours(5);
				while (twoMonthsBack.isBefore(zdt.now())) {
					log.warn(twoMonthsBack.toString());
					twoMonthsBack = twoMonthsBack.plusSeconds(60);
					final Point point = Point.measurement("data")
							.time(twoMonthsBack.toInstant().toEpochMilli(), TimeUnit.MILLISECONDS)
							.addField("location", location.getId().toString())
							.tag("location_tag", location.getId().toString())
							.addField("path", location.getPath(false))
							.addField("unit", StringUtils.defaultIfBlank(location.getUnit(), ""))
							.addField("value", new BigDecimal(20 + (Math.random() * (20)))).addField("alarm", "warning").build();
					influxDBTemplate.write(point);
				}
			}
		}
	}

	@RequestMapping(value = "/simNow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)	
	public void simulateDataNow() {
		influxDBTemplate.createDatabase();
		Collection<Location> lkocations = locationRepository.findAll().stream()
				.filter(location -> location.getDchannel() != null).collect(Collectors.toCollection(LinkedList::new));
		Instant instant = Instant.now();
		for (Iterator iterator = lkocations.iterator(); iterator.hasNext();) {
			Location location = (Location) iterator.next();
			if (location.getParent() != null) {				
				ZoneId zoneId = ZoneId.systemDefault();
				ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, zoneId);
				final Point point = Point.measurement("data")
						.time(zdt.toInstant().toEpochMilli(), TimeUnit.MILLISECONDS)
						.tag("location", location.getId().toString()).tag("path", location.getPath(false))
						.tag("unit", location.getDchannel().getUnit())
						.addField("value", new BigDecimal(20 + (Math.random() * (20)))).addField("alarm", "warning").build();
				influxDBTemplate.write(point);
			}
		}
	}
}
