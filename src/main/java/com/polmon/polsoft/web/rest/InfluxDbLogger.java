package com.polmon.polsoft.web.rest;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import com.polmon.polsoft.domain.AlarmSpecialValues;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Device;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.repository.AlarmSpecialValuesRepository;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.repository.DeviceRepository;
import com.polmon.polsoft.repository.LocationRepository;
import com.polmon.polsoft.service.AlarmsService;
import com.polmon.polsoft.service.dto.AlarmsDTO;
import com.polmon.polsoft.service.dto.ChannelsData;
import com.polmon.polsoft.service.eventsender.DeviceEventSender;
import com.polmon.polsoft.web.rest.dataloggers.DataLoggerProxy;

@RestController
public class InfluxDbLogger {

	private final Logger log = LoggerFactory.getLogger(DataLoggerProxy.class);
	
	@Inject
	private DeviceRepository deviceRepository;

	@Inject
	private DchannelRepository dchannelRepository;

	@Inject
	private AlarmsService alarmsService;

	@Inject
	private LocationRepository locationRepository;

	@Inject
	private AlarmSpecialValuesRepository alarmSpecialValuesRepository;

	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;

	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@Inject
	DeviceEventSender deviceEventSender;

	public void asyncLogData(String requestData) throws Exception {
		String textData = URLDecoder.decode(requestData, "UTF-8");
		ChannelsData chData = parseData(textData);
		checkandLogData(chData);
	}

	public String checkandLogData(ChannelsData chData) throws Exception {
		Device device = deviceRepository.findOneBySerial(chData.getDeviceSerial());
		if (device != null) {
			Set<Dchannel> dchannels = dchannelRepository.findAllByDeviceAndLocationNotNull(device);
			List<AlarmsDTO> allExistingActiveAlarms = alarmsService.findAllWithChannelAndStatus(dchannels, "active");
			//device.setLastRefresh(ZonedDateTime.now());
			//deviceRepository.save(device);
			messagingTemplate.convertAndSend("/topic/device", device);
			return writeDataPoint(device, dchannels, chData, allExistingActiveAlarms);
		} else {
			log.warn("no device with this serial linked : " + chData.getDeviceSerial());
			throw new Exception("no device with this serial linked : " + chData.getDeviceSerial());
		}
	}

	public ChannelsData parseData(String textData) {
		ChannelsData channelsData = null;
		String[] data = textData.split(",");
		if (data.length > 0) {
			channelsData = new ChannelsData(data[0], data[1], data[2]);
			for (int index = 3; index < data.length; index++) {
				String[] keyValue = data[index].split("=");
				if (keyValue.length == 2) {
					String key = keyValue[0].trim().toUpperCase();
					String value = keyValue[1].trim();
					value = value.equalsIgnoreCase("-") ? Integer.MAX_VALUE + "" : value;
					try {
						channelsData.channelValues.put(key, new BigDecimal(value));
					} catch (NumberFormatException e) {
						log.error(e.getMessage());

					}
				} else {
					log.warn("Invalid data format " + data[index]);
				}
			}
		} else {
			log.warn("data seems to be empty from the DL" + textData);
		}
		return channelsData;
	}

	Boolean Flag = false;
	boolean sendSMS = false;
	private String writeDataPoint(Device device, Set<Dchannel> dchannels, ChannelsData chData,
			List<AlarmsDTO> activeAlarms) throws Exception {
		log.error("Enter writeDataPoint:" + System.currentTimeMillis());

		influxDBTemplate.createDatabase();
		log.error("createDatabase:" + System.currentTimeMillis());
		List<Point> dataPoints = new ArrayList<Point>();
		List<Long> locations = new ArrayList<Long>();
		List<AlarmsDTO> alarmsTosave = new ArrayList<AlarmsDTO>();
		List<AlarmSpecialValues> alarmSpecialList = alarmSpecialValuesRepository.findAll();
		
		dchannels.stream().forEach(channel -> {
			Location location = channel.getLocation();
			log.warn(location != null ? location.toString() : "NO_LOCATION_LINKED to CHANNEL" + channel.getId());
			if (!chData.channelValues.containsKey(channel.getName().toUpperCase())) {
				log.warn("no data found for channel" + channel.getName());
			}
			if (location != null && location.getParent() != null
					&& chData.channelValues.containsKey((channel.getName().toUpperCase().trim()))) {
				float channelVallue = new BigDecimal(
						chData.channelValues.get(channel.getName().toUpperCase().trim()).toString()).floatValue();
				String day = chData.day;
				int hour = chData.hour;
				int minute = chData.minute;
				ZonedDateTime generated = chData.created;
				String serialNumber = chData.getDeviceSerial();
				ZonedDateTime now = ZonedDateTime.now();
				String severity = "none";
				
				try {
					sendSMS = true;
					List<AlarmsDTO> alarmDTOs = alarmsService.getAlarmsToSave(channel, location, channelVallue,
							generated, day, hour, minute, now, serialNumber, activeAlarms, alarmSpecialList,sendSMS);
					
				//	alarmsTosave.addAll(alarmDTOs);
					if (alarmDTOs.size() > 0) {
						alarmsService.save(alarmDTOs);
						messagingTemplate.convertAndSend("/topic/alarms", alarmDTOs);
					}
					// get the severity created for reports
					List<AlarmsDTO> allExistingActiveAlarms = alarmsService.findAllWithChannelAndStatus(dchannels, "active");
					severity = getDataPointSeverity(severity, allExistingActiveAlarms, location);
					if(severity.equalsIgnoreCase("alarm") || severity.equalsIgnoreCase("warning"))
						Flag = true;
					
				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage(), e);
				}	
			}
		});
		
		dchannels.stream().forEach(channel -> {
			Location location = channel.getLocation();
			log.warn(location != null ? location.toString() : "NO_LOCATION_LINKED to CHANNEL" + channel.getId());
			if (!chData.channelValues.containsKey(channel.getName().toUpperCase())) {
				log.warn("no data found for channel" + channel.getName());
			}
			if (location != null && location.getParent() != null
					&& chData.channelValues.containsKey((channel.getName().toUpperCase().trim()))) {
				float channelVallue = new BigDecimal(
						chData.channelValues.get(channel.getName().toUpperCase().trim()).toString()).floatValue();
				String day = chData.day;
				int hour = chData.hour;
				int minute = chData.minute;
				ZonedDateTime generated = chData.created;
				String serialNumber = chData.getDeviceSerial();
				ZonedDateTime now = ZonedDateTime.now();
				String severity = "none";
				sendSMS = false;
				try {
					List<AlarmsDTO> alarmDTOs = alarmsService.getAlarmsToSave(channel, location, channelVallue,
							generated, day, hour, minute, now, serialNumber, activeAlarms, alarmSpecialList,sendSMS);
					// get the severity created for reports
					List<AlarmsDTO> allExistingActiveAlarms = alarmsService.findAllWithChannelAndStatus(dchannels, "active");
					severity = getDataPointSeverity(severity, allExistingActiveAlarms, location);
				//	alarmsTosave.addAll(alarmDTOs);
				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage(), e);
				}
				final Point point = Point.measurement("data").time(chData.time, TimeUnit.MILLISECONDS)
						.addField("location", location.getId().toString())
						.tag("location_tag", location.getId().toString())
						.addField("unit", StringUtils.defaultString(location.getUnit(), "")).addField("alarm", severity)
						.addField("path", location.getPath(false)).addField("value", channelVallue)
						.addField("isReports", Flag.toString()).build();
				dataPoints.add(point);
				// marking that the location has the data
				markLocationHasData(locations, location);
			}
		});
		Flag = false;
		if (locations.size() > 0) {
			locationRepository.setLocationById(locations);
		}
/*		if (alarmsTosave.size() > 0) {
			alarmsService.save(alarmsTosave);
			messagingTemplate.convertAndSend("/topic/alarms", alarmsTosave);
		}*/
		
		influxDBTemplate.write(dataPoints);
		return "200";
	}

	private void markLocationHasData(List<Long> locations, Location loc) {
		if (loc.getHasData() == null || !loc.getHasData()) {
			loc.setHasData(true);
			locations.add(loc.getId());
		}
	}

	private String getDataPointSeverity(String severity, List<AlarmsDTO> newAlarms, Location location) {
	//	List<AlarmsDTO> alarms = new ArrayList<AlarmsDTO>();
	//	alarms.addAll(newAlarms);

		if (newAlarms.size() > 0) {
			//alarms = newAlarms.stream().filter(alarmDTO -> alarmDTO.getSource().equalsIgnoreCase(location.getName())).collect(Collectors.toList());
			Map<String, String> alarmToTriggerSetMap = newAlarms.stream().filter(alarmDTO ->  
					alarmDTO.getSource().equalsIgnoreCase(location.getName())).collect
					(Collectors.toMap(alarmDTO -> alarmDTO.getSeverity(), alarmDTO -> alarmDTO.getSeverity()));
			if (alarmToTriggerSetMap.containsKey("warning")) {
				severity = "warning";
			}
			if (alarmToTriggerSetMap.containsKey("alarm")) {
				severity = "alarm";
			}
		}
/*		else if (alarmDTOs.size() > 0) {
			Map<String, String> alarmToTriggerSetMap = alarmDTOs.stream().filter(alarmDTO -> alarmDTO.isActive())
					.collect(Collectors.toMap(alarmDTO -> alarmDTO.getSeverity(), alarmDTO -> alarmDTO.getSeverity()));
			if (alarmToTriggerSetMap.containsKey("warning")) {
				severity = "warning";
			}
			if (alarmToTriggerSetMap.containsKey("alarm")) {
				severity = "alarm";
			}
		}*/
		return severity;
	}
	

	private String getDataPointState(String state, List<AlarmsDTO> alarmDTOs) {
		if (alarmDTOs.size() > 0) {
			Map<String, String> alarmToTriggerSetMap = alarmDTOs.stream().filter(alarmDTO -> alarmDTO.isActive())
					.collect(Collectors.toMap(alarmDTO -> alarmDTO.getState(), alarmDTO -> alarmDTO.getState()));
			if (alarmToTriggerSetMap.containsKey("HIGH")) {
				state = "HIGH";
			}
			if (alarmToTriggerSetMap.containsKey("LOW")) {
				state = "LOW";
			}
			if(alarmToTriggerSetMap.containsKey("special")){
				state = "special";
			}
		}
		return state;
	}

}
