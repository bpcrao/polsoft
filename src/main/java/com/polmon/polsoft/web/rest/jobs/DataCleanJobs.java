package com.polmon.polsoft.web.rest.jobs;

import javax.inject.Inject;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.polmon.polsoft.service.AlarmsService;
import com.polmon.polsoft.service.AuditEventService;
import com.polmon.polsoft.service.InfluxDbService;


@Service
public class DataCleanJobs  {

	
	@Inject
	AlarmsService alarmsService;
	
	@Inject
	AuditEventService auditEventService;
	
	@Autowired
	private InfluxDbService influxDbService;
	
	@Scheduled(cron = "0 0 0 1/1 * ?")
	public void execute() throws JobExecutionException {
		alarmsService.cleanup();		
		auditEventService.cleanUp();	
		influxDbService.cleanUp();
	}

}