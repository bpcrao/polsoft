package com.polmon.polsoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.EntityConfigService;
import com.polmon.polsoft.service.dto.EntityConfigDTO;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.web.rest.util.PaginationUtil;
import com.polmon.polsoft.web.rest.util.ResponseUtil;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing EntityConfig.
 */
@RestController
@RequestMapping("/api")
public class EntityConfigResource {

    private final Logger log = LoggerFactory.getLogger(EntityConfigResource.class);

    private static final String ENTITY_NAME = "entityConfig";
    
    private final EntityConfigService entityConfigService;

    public EntityConfigResource(EntityConfigService entityConfigService) {
        this.entityConfigService = entityConfigService;
    }

    /**
     * PUT  /entity-configs : Updates an existing entityConfig.
     *
     * @param entityConfigDTO the entityConfigDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated entityConfigDTO,
     * or with status 400 (Bad Request) if the entityConfigDTO is not valid,
     * or with status 500 (Internal Server Error) if the entityConfigDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/entity-configs")
    @Timed
    public ResponseEntity<EntityConfigDTO> updateEntityConfig(@RequestBody EntityConfigDTO entityConfigDTO) throws URISyntaxException {
        log.debug("REST request to update EntityConfig : {}", entityConfigDTO);
        EntityConfigDTO result = entityConfigService.save(entityConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, entityConfigDTO.getId().toString()))
            .body(result);
        }

    /**
     * GET  /entity-configs : get all the entityConfigs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of entityConfigs in body
     * @throws URISyntaxException 
     */
    @GetMapping("/entity-configs")
    @Timed
    public ResponseEntity<List<EntityConfigDTO>> getAllEntityConfigs(@ApiParam Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get a page of EntityConfigs");
        Page<EntityConfigDTO> page = entityConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/entity-configs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * SEARCH  /_search/entity-configs?query=:query : search for the entityConfig corresponding
     * to the query.
     *
     * @param query the query of the entityConfig search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/entity-configs")
    @Timed
    public ResponseEntity<List<EntityConfigDTO>> searchEntityConfigs(@RequestParam String query, @ApiParam Pageable pageable) throws Exception{
        log.debug("REST request to search for a page of EntityConfigs for query {}", query);
        Page<EntityConfigDTO> page = entityConfigService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/entity-configs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
