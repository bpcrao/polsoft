package com.polmon.polsoft.web.rest.jobs;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;

import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.ReportTemplate;
import com.polmon.polsoft.domain.User;
import com.polmon.polsoft.repository.LocationRepository;
import com.polmon.polsoft.repository.UserRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.MailService;
import com.polmon.polsoft.service.ReportTemplateService;
import com.polmon.polsoft.service.util.PdfGeneratorUtil;

/**
 * Controller for view and managing Log Level at runtime.
 */
public class ReportsGenerator implements Job {

	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;

	@Inject
	private LocationRepository locationRepository;
	
	@Inject
	private ReportTemplateService reportTemplateService;
	

	@Inject
	private AuditEventPublisher auditPublisher;
	
	@Autowired
	private PdfGeneratorUtil pdfGeneratorUtil;
	
	@Autowired
	private MailService mailService;
	
	@Autowired
	private UserRepository userRepository;

	@Value("${welcome.message:test}")
	private String message = "Hello World";

	@RequestMapping("/report")
	public String report(Map<String, Object> model) {
		model.put("message", this.message);
		return "report";
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {		
		influxDBTemplate.createDatabase();	
		try {
			List<JobExecutionContext> jobs = context.getScheduler().getCurrentlyExecutingJobs();
            for (JobExecutionContext job : jobs) {
                if (job.getTrigger().equals(context.getTrigger()) && !job.getJobInstance().equals(this)) {
                    return;
                }

            }
			JobDataMap dataMap = context.getJobDetail().getJobDataMap();
            String templateId = dataMap.getString("templateId");               
            long id = Long.parseLong(templateId);
  	    	ReportTemplate report = reportTemplateService.findOneReportTemplate(id);
  	    	byte[] bytes = pdfGeneratorUtil.forEachReport(report, null, null);
  	    	
			Map data = new HashMap<String, Object>();
	    	data.put("scheduled_reports", report.getName());
	    	auditPublisher.publish(new AuditEvent("System", "System generated report",data));
	    	
  	    	File emailFile = new File("C://polsoft//reports//email//" + report.getName() + ".pdf");
  	    	if(emailFile.getParentFile()!=null)
  	    	   {
  	    		emailFile.getParentFile().mkdirs();
  	    	   }
  	    	FileOutputStream os = new FileOutputStream(emailFile);
  	    	IOUtils.write(bytes, os);
  	    	IOUtils.closeQuietly(os);
  	    	if(report.getSendEmail())
  	    	{
  	    		List<String> userLogins = Arrays.asList(report.getUsers().split(",")).stream().collect(Collectors.toList());
  	  	    	List<User> users = userRepository.findByLoginIn(userLogins);
  	  	    	List<String> userEmailIds =users.stream().map(user-> user.getEmail()).collect(Collectors.toList());
  	  	    	mailService.sendEmail(userEmailIds, "Report", "Report", true, true, emailFile);
  	    	}  	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
