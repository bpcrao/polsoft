package com.polmon.polsoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.repository.UserRepository;
import com.polmon.polsoft.service.LogindataService;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.service.dto.LogindataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Logindata.
 */
@RestController
@RequestMapping("/api")
public class LogindataResource {

    private final Logger log = LoggerFactory.getLogger(LogindataResource.class);
        
    @Inject
    private LogindataService logindataService;
    
    @Inject
    private UserRepository userRepository;

    /**
     * POST  /logindata : Create a new logindata.
     *
     * @param logindataDTO the logindataDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new logindataDTO, or with status 400 (Bad Request) if the logindata has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/logindata",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LogindataDTO> createLogindata(@RequestBody LogindataDTO logindataDTO) throws URISyntaxException {
        log.debug("REST request to save Logindata : {}", logindataDTO);
        if (logindataDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("logindata", "idexists", "A new logindata cannot already have an ID")).body(null);
        }
        LogindataDTO result = logindataService.save(logindataDTO);
        return ResponseEntity.created(new URI("/api/logindata/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("logindata", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /logindata : Updates an existing logindata.
     *
     * @param logindataDTO the logindataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated logindataDTO,
     * or with status 400 (Bad Request) if the logindataDTO is not valid,
     * or with status 500 (Internal Server Error) if the logindataDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/logindata",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LogindataDTO> updateLogindata(@RequestBody LogindataDTO logindataDTO) throws URISyntaxException {
        log.debug("REST request to update Logindata : {}", logindataDTO);
        if (logindataDTO.getId() == null) {
            return createLogindata(logindataDTO);
        }
        LogindataDTO result = logindataService.save(logindataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("logindata", logindataDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /logindata : get all the logindata.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of logindata in body
     */
    @RequestMapping(value = "/logindata",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<LogindataDTO> getAllLogindata() {
        log.debug("REST request to get all Logindata");
        return logindataService.findAll();
    }

    /**
     * GET  /logindata/:id : get the "id" logindata.
     *
     * @param id the id of the logindataDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the logindataDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/logindata/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LogindataDTO> getLogindata(@PathVariable Long id) {
        log.debug("REST request to get Logindata : {}", id);
        LogindataDTO logindataDTO = logindataService.findOne(id);
        return Optional.ofNullable(logindataDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /logindata/:id : delete the "id" logindata.
     *
     * @param id the id of the logindataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/logindata/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteLogindata(@PathVariable Long id) {
        log.debug("REST request to delete Logindata : {}", id);
        logindataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("logindata", id.toString())).build();
    }

    /**
     * SEARCH  /_search/logindata?query=:query : search for the logindata corresponding
     * to the query.
     *
     * @param query the query of the logindata search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/logindata",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<LogindataDTO> searchLogindata(@RequestParam String query) {
        log.debug("REST request to search Logindata for query {}", query);
        return logindataService.search(query);
    }


}
