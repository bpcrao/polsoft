package com.polmon.polsoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.domain.AlarmsCount;
import com.polmon.polsoft.repository.AlarmsRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.AlarmsService;
import com.polmon.polsoft.service.dto.AlarmsDTO;
import com.polmon.polsoft.sms.GSMProxy;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.web.rest.util.PaginationUtil;
/*import com.polmon.polsoft.SMSsender;*/
import com.polmon.polsoft.config.audit.AuditEventPublisher;

/**
 * REST controller for managing Alarms.
 */
@RestController
@RequestMapping("/api")
public class AlarmsResource {

    private final Logger log = LoggerFactory.getLogger(AlarmsResource.class);
        
    @Inject
    private AlarmsService alarmsService;
    
    @Inject
	private GSMProxy gsmConnect;
    
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	/*@Inject
	private SMSsender smssender;*/
	
	@Inject
	private AuditEventPublisher auditPublisher;
    /**
     * POST  /alarms : Create a new alarms.
     *
     * @param alarmsDTO the alarmsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alarmsDTO, or with status 400 (Bad Request) if the alarms has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarms",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmsDTO> createAlarms(@RequestBody AlarmsDTO alarmsDTO) throws URISyntaxException {
        log.debug("REST request to save Alarms : {}", alarmsDTO);
        if (alarmsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("alarms", "idexists", "A new alarms cannot already have an ID")).body(null);
        }
        AlarmsDTO result = alarmsService.save(alarmsDTO);
       // new SMSsender();
        messagingTemplate.convertAndSend("/topic/alarms", result);
        return ResponseEntity.created(new URI("/api/alarms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("alarms", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /alarms : Updates an existing alarms.
     *
     * @param alarmsDTO the alarmsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alarmsDTO,
     * or with status 400 (Bad Request) if the alarmsDTO is not valid,
     * or with status 500 (Internal Server Error) if the alarmsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarms",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmsDTO> updateAlarms(@RequestBody AlarmsDTO alarmsDTO) throws URISyntaxException {
        log.debug("REST request to update Alarms : {}", alarmsDTO);
        if (alarmsDTO.getId() == null) {
            return createAlarms(alarmsDTO);
        }
        SecurityContext securityContext = SecurityContextHolder.getContext();
		String userName = "";
        String status = alarmsDTO.getStatus();
        if(status == null){
        	status = "";
        }
        if(alarmsDTO.getAckStatus().equalsIgnoreCase("acked")){
        	alarmsDTO.setAcknowledged(ZonedDateTime.now());
        	alarmsDTO.setStatus(status+" acknowledged");
        	if (securityContext != null && securityContext.getAuthentication() != null) {
    			userName = securityContext.getAuthentication().getName();
    		}
        	Map data = new HashMap<String, Object>();
        	data.put("user1", userName);
        	data.put("alarmId", alarmsDTO.getId());
        	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(),"Alarm acknowledged",data));
        	String timeStamp = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma").format(ZonedDateTime.now());
        	Map<String,String> smsMessages = new HashMap<>();;
			if (alarmsDTO.getMobileNumbers() != null && StringUtils.isNotBlank(alarmsDTO.getMobileNumbers())) {
				if (smsMessages != null && smsMessages.get(alarmsDTO.getMobileNumbers()) != null)
					smsMessages.put(alarmsDTO.getMobileNumbers(),
							smsMessages.get(alarmsDTO.getMobileNumbers()) + "\n" + timeStamp + "\n"
									+ alarmsDTO.getSource() + " : " + alarmsDTO.getSeverity().substring(0,1).toUpperCase()+alarmsDTO.getSeverity().substring(1) + " Acknowledged.");
				else if(alarmsDTO.getSeverity().equalsIgnoreCase("-") && smsMessages != null && smsMessages.get(alarmsDTO.getMobileNumbers()) != null){
					smsMessages.put(alarmsDTO.getMobileNumbers(),smsMessages.get(alarmsDTO.getMobileNumbers()) + "\n" + timeStamp + "\n"
									+ alarmsDTO.getSource() + " : Alarm Acknowledged.");
				}
				else{
					smsMessages.put(alarmsDTO.getMobileNumbers(), timeStamp + "\n" + alarmsDTO.getSource() + " : "
							+ (alarmsDTO.getSeverity().substring(0,1).toUpperCase()+alarmsDTO.getSeverity().substring(1)) + " Acknowledged");
					if(alarmsDTO.getSeverity().equalsIgnoreCase("-")) {
						smsMessages.put(alarmsDTO.getMobileNumbers(), timeStamp + "\n" + alarmsDTO.getSource() + " : Alarm Acknowledged");
					}
				}
			}
			sendSMSs(smsMessages);	
        }
        if(alarmsDTO.getAckStatus().equalsIgnoreCase("paused")){
        	alarmsDTO.setStatus(status+" paused");
        }
        if(alarmsDTO.getAckStatus().equalsIgnoreCase("unpaused")){
        	alarmsDTO.setStatus(status.replace("paused",""));
        }
        alarmsDTO.setUpdated(ZonedDateTime.now());
        AlarmsDTO result = alarmsService.save(alarmsDTO);
        messagingTemplate.convertAndSend("/topic/alarms", result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("alarms", alarmsDTO.getId().toString()))
            .body(result);
    }
    
    private void sendSMSs(Map<String, String> smsMessages) {
		if(smsMessages.size()>0)
		{
			try {
				gsmConnect.sendMessageMap(smsMessages);
			} catch (Exception e) {
				log.error("error sending sms", e);
			}
		}
	}

    /**
     * GET  /alarms : get all the alarms.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of alarms in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/alarms",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE,params = {"fromDate", "toDate"})
    @Timed
    public ResponseEntity<List<AlarmsDTO>> getAllAlarms(@RequestParam(value = "fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @RequestParam(value = "toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate,Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Alarms");
        Page<AlarmsDTO> page = alarmsService.findAllBetweenDates(fromDate,toDate,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/alarms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /alarms : get all the alarms.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alarms in body
     */
    @RequestMapping(value = "/alarms/all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE,params = {"fromDate", "toDate"})
    @Timed
    public ResponseEntity<List<AlarmsDTO>> getAllAlarms(@RequestParam(value = "fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @RequestParam(value = "toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate)
         {
        log.debug("REST request to get a page of Alarms");
        List<AlarmsDTO> alarms = alarmsService.findAllBetweenDates(fromDate,toDate);
        return new ResponseEntity<>(alarms, HttpStatus.OK);
    }

    /**
     * GET  /alarms/:id : get the "id" alarms.
     *
     * @param id the id of the alarmsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alarmsDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/alarms/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmsDTO> getAlarms(@PathVariable Long id) {
        log.debug("REST request to get Alarms : {}", id);
        AlarmsDTO alarmsDTO = alarmsService.findOne(id);
        return Optional.ofNullable(alarmsDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /alarms/:id : delete the "id" alarms.
     *
     * @param id the id of the alarmsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/alarms/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAlarms(@PathVariable Long id) {
        log.debug("REST request to delete Alarms : {}", id);
        alarmsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("alarms", id.toString())).build();
    }

    /**
     * SEARCH  /_search/alarms?query=:query : search for the alarms corresponding
     * to the query.
     *
     * @param query the query of the alarms search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/alarms",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<AlarmsDTO>> searchAlarms(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Alarms for query {}", query);
        Page<AlarmsDTO> page = alarmsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/alarms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * 
     */
    @RequestMapping(value = "/alarms/count",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmsCount> countAlarms() throws URISyntaxException {
        log.debug("REST request to count alarms and warnings ");
        List<AlarmsCount> count = alarmsService.countAlarms();
        return count;
    }
}