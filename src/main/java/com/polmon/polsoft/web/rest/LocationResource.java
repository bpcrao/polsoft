package com.polmon.polsoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.enums.Unit;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.repository.LocationRepository;
import com.polmon.polsoft.service.DchannelService;
import com.polmon.polsoft.service.DeviceService;
import com.polmon.polsoft.service.LocationService;
import com.polmon.polsoft.service.dto.DchannelDTO;
import com.polmon.polsoft.service.dto.DeviceDTO;
import com.polmon.polsoft.service.dto.LocationDTO;
import com.polmon.polsoft.service.eventsender.DeviceEventSender;
import com.polmon.polsoft.service.mapper.LocationMapper;
import com.polmon.polsoft.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Location.
 */
@RestController
@RequestMapping("/api")
public class LocationResource {

    private final Logger log = LoggerFactory.getLogger(LocationResource.class);
        
    @Inject
    private LocationService locationService;

    @Inject 
    private DchannelService dchannelService;
    
    @Inject 
    private DeviceService deviceService;
    
    @Inject    
    private LocationMapper locationMapper;
	
    @Inject
	DchannelRepository dchannelRepository;
    
    @Inject
    private LocationRepository locationRepository;
    
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Inject
	private DeviceEventSender deviceEventSender;
    
    /**
     * POST  /locations : Create a new location.
     *
     * @param locationDTO the locationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new locationDTO, or with status 400 (Bad Request) if the location has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/locations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LocationDTO> createLocation(@RequestBody LocationDTO locationDTO) throws URISyntaxException {
        log.debug("REST request to save Location : {}", locationDTO);
        if (locationDTO.getId() != null) {
            return ResponseEntity.badRequest()
            		.headers(HeaderUtil.createFailureAlert("location", "idexists", "A new location cannot already have an ID"))
            		.body(null);
        }
        else if(locationRepository.findOneByName(locationDTO.getName()).isPresent()){
        	return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("location", "nameexists", "Name already in use Please choose another name"))
					.body(null);
		}
        LocationDTO result = locationService.save(locationDTO);
        Map<String,Object> headers = new HashMap<>();
        headers.put("operation", "CREATE");
        messagingTemplate.convertAndSend("/topic/locations", result,headers);
        return ResponseEntity.created(new URI("/api/locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("location", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /locations : Updates an existing location.
     *
     * @param locationDTO the locationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated locationDTO,
     * or with status 400 (Bad Request) if the locationDTO is not valid,
     * or with status 500 (Internal Server Error) if the locationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/locations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LocationDTO> updateLocation(@RequestBody LocationDTO locationDTO) throws URISyntaxException {
        log.debug("REST request to update Location : {}", locationDTO);
        if (locationDTO.getId() == null) {
            return createLocation(locationDTO);
        }
        LocationDTO result = locationService.save(locationDTO);
    
        if (result.getDeviceid() != null && result.isLinkUnlinkLocation()) {
			deviceEventSender.sendEvents(result.getDeviceid());
		}
        else if (result.getOldLocationDeviceID() != null && result.isLinkUnlinkLocation()){
        	deviceEventSender.sendEvents(result.getOldLocationDeviceID());
        }
        Map<String,Object> headers = new HashMap<>();
        if (locationDTO.getId() == null) {
        	 headers.put("operation", "CREATE");
        }
        else {
        	headers.put("operation", "UPDATE");
        }
        messagingTemplate.convertAndSend("/topic/locations", result,headers);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("location", locationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /locations : get all the locations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of locations in body
     */
    @RequestMapping(value = "/locations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<LocationDTO> getAllLocations() {
        log.debug("REST request to get all Locations");
        List<Location> locations = locationService.findAll();
        List<LocationDTO> result  = locations.stream()
                .map(locationMapper::locationTLocationDTO)
                .collect(Collectors.toCollection(LinkedList::new));
        
        return result;
    }
    
    @RequestMapping(value = "/loctree",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        public LocationDTO getLocationsTree() {
            log.debug("REST request to get all Locations");
            return locationService.findRootNode(this.findPermitedTree());
        }
    
    public Set<Location> findPermitedTree(){
		List<Location> locationsAccessible = locationService.findAll();
		Set<Location> parents = new HashSet<Location>();
 		recursivegetParents(locationsAccessible,parents);
 		parents.addAll(locationsAccessible);
		return parents;		
	}
    
	private void recursivegetParents(List<Location> locationsAccessible, Set<Location> parents) {
		for (Location location : locationsAccessible) {
			Location temp = location;
			while (temp.getParent() != null) {
				temp = temp.getParent();
				parents.add(temp);
			}
		}
	}


    /**
     * GET  /locations/:id : get the "id" location.
     *
     * @param id the id of the locationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the locationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/locations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LocationDTO> getLocation(@PathVariable Long id) {
        log.debug("REST request to get Location : {}", id);
        LocationDTO locationDTO = locationService.findOne(id);
        if(locationDTO!=null && locationDTO.getDchannelId()!=null){
        DchannelDTO dchannelDTO = dchannelService.findOne(locationDTO.getDchannelId());
        DeviceDTO deviceDTO = deviceService.findOne(dchannelDTO.getDeviceId());
        locationDTO.setDevicename(deviceDTO.getName());
        locationDTO.setDeviceid(deviceDTO.getId());
        }else{
        	log.debug("not mapped to a channel and device:{}", id);
        }
        
        return Optional.ofNullable(locationDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /locations/:id : delete the "id" location.
     *
     * @param id the id of the locationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/locations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteLocation(@PathVariable Long id) {
        log.debug("REST request to delete Location : {}", id);
        try {
			locationService.delete(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.badRequest()
            		.headers(HeaderUtil.createFailureAlert("location", "has data", "A new location cannot be deleted with data"))
            		.body(null);
		}
        Map<String,Object> headers = new HashMap<>();
        headers.put("operation", "DELETE");
        messagingTemplate.convertAndSend("/topic/locations", id,headers);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("location", id.toString())).build();
    }

    /**
     * SEARCH  /_search/locations?query=:query : search for the location corresponding
     * to the query.
     *
     * @param query the query of the location search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/locations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<LocationDTO> searchLocations(@RequestParam String query) {
        log.debug("REST request to search Locations for query {}", query);
        return locationService.search(query);
    }
    
    @RequestMapping(value = "/locations/units",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Map<Integer,String> getUnits()
    {
    	Map<Integer,String> units = new HashMap<>();
    	Arrays.asList(Unit.values()).forEach(unit ->{
    		units.put(unit.getValue(), unit.getName());
    	});
    	return units;
    }

    @RequestMapping(value = "/locations/{locationId}/unit",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        public ResponseEntity<LocationDTO> updateLocationUnit(@PathVariable Long locationId,@RequestBody String unit) throws URISyntaxException {
            log.debug("REST request to update Location : {}", locationId,unit);
            LocationDTO locationDTO = locationService.findOne(locationId);
            locationDTO.setUnit(unit);
            LocationDTO result = locationService.save(locationDTO);
            Map<String,Object> headers = new HashMap<>();
            headers.put("operation", "CREATE");
            messagingTemplate.convertAndSend("/topic/locations", result,headers);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("location", locationDTO.getId().toString()))
                .body(result);
        }
}
