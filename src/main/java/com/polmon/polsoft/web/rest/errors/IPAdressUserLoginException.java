package com.polmon.polsoft.web.rest.errors;

import org.springframework.security.core.AuthenticationException;

public class IPAdressUserLoginException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public IPAdressUserLoginException(String msg) {
		super(msg);
	}

}
