package com.polmon.polsoft.web.rest.jobs;

import java.io.IOException;
import java.time.ZonedDateTime;

import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.polmon.polsoft.service.AlarmsService;
import com.polmon.polsoft.service.dto.AlarmsDTO;
import com.polmon.polsoft.web.rest.BackupAndRestoreResource;

public class BackupJob implements Job {

	@Autowired
	BackupAndRestoreResource backupAndRestore;
	
	@Autowired	
	AlarmsService alarmsService;
	
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			backupAndRestore.backupData();
		} catch (IOException e) {
			 AlarmsDTO alarmsDTO = new AlarmsDTO();
			 alarmsDTO.setAlarmdata(e.getMessage());
			 alarmsDTO.setComments("Check if the Backup folder is accessible");
				alarmsDTO.setStatus("active");
			ZonedDateTime createdDate = ZonedDateTime.now();
			alarmsDTO.setCreatedDate(createdDate);
			alarmsService.save(alarmsDTO);
			messagingTemplate.convertAndSend("/topic/alarms", alarmsDTO);
			e.printStackTrace();
		} catch (InterruptedException e) {
			AlarmsDTO alarmsDTO = new AlarmsDTO();
			 alarmsDTO.setAlarmdata(e.getMessage());
			 alarmsDTO.setComments("Backup interrupted");
				alarmsDTO.setStatus("active");
			ZonedDateTime createdDate = ZonedDateTime.now();
			alarmsDTO.setCreatedDate(createdDate);
			alarmsService.save(alarmsDTO);
			messagingTemplate.convertAndSend("/topic/alarms", alarmsDTO);
			e.printStackTrace();
		}
	}

}