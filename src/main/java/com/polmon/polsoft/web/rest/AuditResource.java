package com.polmon.polsoft.web.rest;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.config.audit.EntityAuditAction;
import com.polmon.polsoft.domain.PersistentAuditEvent;
import com.polmon.polsoft.repository.PersistenceAuditEventRepository;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.AuditEventService;
import com.polmon.polsoft.service.dto.CustomAuditEvent;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.web.rest.util.PaginationUtil;

/**
 * REST controller for getting the audit events.
 */
@RestController
@RequestMapping(value = "/management/jhipster/audits", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuditResource {

    private AuditEventService auditEventService;
    
    @Autowired
	private SimpMessagingTemplate messagingTemplate;
    
    @Autowired
	private PersistenceAuditEventRepository persistenceAuditEventRepository;
    
    @Autowired
	private Environment environment;

    @Inject
    public AuditResource(AuditEventService auditEventService) {
        this.auditEventService = auditEventService;
    }

    /**
     * GET  /audits : get a page of AuditEvents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of AuditEvents in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<CustomAuditEvent>> getAll(Pageable pageable) throws URISyntaxException {
        Page<CustomAuditEvent> page = auditEventService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    
    @RequestMapping(value = "/{auditId}", method = RequestMethod.PUT)
    public ResponseEntity<CustomAuditEvent> updateReviewed( @PathVariable Long auditId) {
    	CustomAuditEvent auditData = auditEventService.find(auditId).get();
    	auditData.setReviewedBy(SecurityUtils.getCurrentUserLogin());
    	auditData =  auditEventService.save(auditData);
    	return  new ResponseEntity<>(auditData, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<List<CustomAuditEvent>> updateReviewedAudits(@RequestBody List<Long> auditIds ) {
    	List<CustomAuditEvent> auditDataList = auditEventService.findAll(auditIds);
    	auditDataList.forEach(auditData->{
    		auditData.setReviewedBy(SecurityUtils.getCurrentUserLogin());
        	auditData =  auditEventService.save(auditData);
    	});
    	if(!SecurityUtils.isRestoredApplication(environment))
    	{
    		PersistentAuditEvent auditEvent = new PersistentAuditEvent();
        	auditEvent.setAuditEventDate(LocalDateTime.now());
        	auditEvent.setAuditEventType("Review success");
        	auditEvent.setPrincipal(SecurityContextHolder.getContext().getAuthentication().getName());
        	auditEvent.setAuditEventSource(EntityAuditAction.UPDATE.toString());
        	persistenceAuditEventRepository.save(auditEvent);
        	messagingTemplate.convertAndSend("/topic/audits", auditEvent);
    	}    	
    	HttpHeaders httpHeaders = HeaderUtil.createAlert("audits.created",SecurityUtils.getCurrentUserLogin());
		return ResponseEntity.ok().headers(httpHeaders).body(auditDataList);
    }
    
    
    /**
     * GET  /audits : get a page of AuditEvents between the fromDate and toDate.
     *
     * @param fromDate the start of the time period of AuditEvents to get
     * @param toDate the end of the time period of AuditEvents to get
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of AuditEvents in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */

    @RequestMapping(method = RequestMethod.GET,
        params = {"fromDate", "toDate"})
    public ResponseEntity<List<CustomAuditEvent>> getByDates(
        @RequestParam(value = "fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
        @RequestParam(value = "toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate,
        @RequestParam(value = "eventTypeFilter", required=false)  String eventTypeFilter,
        Pageable pageable) throws URISyntaxException {
    	Page<CustomAuditEvent> page;
		if (eventTypeFilter != null && !eventTypeFilter.equals("ALL")) {
			page = auditEventService.findByDatesAndEventSource(fromDate.atTime(0, 0), toDate.atTime(23, 59),
					eventTypeFilter, pageable);
		} else {
			page = auditEventService.findByDates(fromDate.atTime(0, 0), toDate.atTime(23, 59), pageable);
		}

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET,value="/getAll",
            params = {"fromDate", "toDate"})
        public ResponseEntity<List<CustomAuditEvent>> getByDates(
            @RequestParam(value = "fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @RequestParam(value = "toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate,
            @RequestParam(value = "eventTypeFilter", required=false)  String eventTypeFilter) throws URISyntaxException {
        	List<CustomAuditEvent> page;
    		if (eventTypeFilter != null && !eventTypeFilter.equals("ALL")) {
    			page = auditEventService.findByDatesAndEventSource(fromDate.atTime(0, 0), toDate.atTime(23, 59),
    					eventTypeFilter);
    		} else {
    			page = auditEventService.findByDates(fromDate.atTime(0, 0), toDate.atTime(23, 59));
    		}
            return new ResponseEntity<>(page, HttpStatus.OK);
        }

    /**
     * GET  /audits/:id : get an AuditEvent by id.
     *
     * @param id the id of the entity to get
     * @return the ResponseEntity with status 200 (OK) and the AuditEvent in body, or status 404 (Not Found)
     */
    @RequestMapping(value = "/{id:.+}",
        method = RequestMethod.GET)
    public ResponseEntity<CustomAuditEvent> get(@PathVariable Long id) {
        return auditEventService.find(id)
                .map((entity) -> new ResponseEntity<>(entity, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * fetches all the audited entity types
     *
     * @return
     */
    @RequestMapping(value = "/types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<String> getAuditedEntities() {
        return auditEventService.findAllEntityTypes();
    }
    
    /**
     * fetches a previous version for for an entity class and id
     *
     * @return
     */
    @RequestMapping(value = "/changes/version/previous",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomAuditEvent> getPrevVersion(@RequestParam(value = "qualifiedName") String qualifiedName,
                                                           @RequestParam(value = "entityId") Long entityId,
                                                           @RequestParam(value = "commitVersion") Integer commitVersion)
        throws URISyntaxException {
    	CustomAuditEvent prev = auditEventService.findOneByAuditEventTypeAndEntityIdAndCommitVersion(qualifiedName, entityId, commitVersion-1);
        return new ResponseEntity<>(prev, HttpStatus.OK);

    }
    

}
