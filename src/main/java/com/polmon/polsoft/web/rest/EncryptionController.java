package com.polmon.polsoft.web.rest;

import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSON;
import com.polmon.polsoft.security.RSAUtils;

@RestController
@RequestMapping("/api")
public class EncryptionController {


  @RequestMapping(value = "/encryption-parameters", method = RequestMethod.GET)
  public ResponseEntity<?> getEncryptionPublicKey(HttpServletRequest request) {
    KeyPair keyPair = RSAUtils.generateKeyPair();
    PrivateKey privateKey = keyPair.getPrivate();
    request.getSession().setAttribute("_private_key", privateKey);

    Map<String, Object> publicKeyMap = new HashMap<>();
    publicKeyMap.put("publicKey", Base64.encodeBase64String(keyPair.getPublic().getEncoded()));
    return ResponseEntity.ok(publicKeyMap);
  }

  @RequestMapping(value = "/encryption-data", method = RequestMethod.POST)
  public ResponseEntity<?> decrypt(HttpServletRequest request, @RequestBody String encryptedData)
      throws IOException {
    encryptedData = JSON.parseObject(encryptedData).getString("encryptedData");
    PrivateKey privateKey = (PrivateKey) request.getSession().getAttribute("_private_key");
    return new ResponseEntity(HttpStatus.OK);
  }
}
