package com.polmon.polsoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.polmon.polsoft.service.DevicedataService;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.service.dto.DevicedataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Devicedata.
 */
@RestController
@RequestMapping("/api")
public class DevicedataResource {

    private final Logger log = LoggerFactory.getLogger(DevicedataResource.class);
        
    @Inject
    private DevicedataService devicedataService;

    /**
     * POST  /devicedata : Create a new devicedata.
     *
     * @param devicedataDTO the devicedataDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new devicedataDTO, or with status 400 (Bad Request) if the devicedata has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/devicedata",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DevicedataDTO> createDevicedata(@RequestBody DevicedataDTO devicedataDTO) throws URISyntaxException {
        log.debug("REST request to save Devicedata : {}", devicedataDTO);
        if (devicedataDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("devicedata", "idexists", "A new devicedata cannot already have an ID")).body(null);
        }
        DevicedataDTO result = devicedataService.save(devicedataDTO);
        return ResponseEntity.created(new URI("/api/devicedata/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("devicedata", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /devicedata : Updates an existing devicedata.
     *
     * @param devicedataDTO the devicedataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated devicedataDTO,
     * or with status 400 (Bad Request) if the devicedataDTO is not valid,
     * or with status 500 (Internal Server Error) if the devicedataDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/devicedata",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DevicedataDTO> updateDevicedata(@RequestBody DevicedataDTO devicedataDTO) throws URISyntaxException {
        log.debug("REST request to update Devicedata : {}", devicedataDTO);
        if (devicedataDTO.getId() == null) {
            return createDevicedata(devicedataDTO);
        }
        DevicedataDTO result = devicedataService.save(devicedataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("devicedata", devicedataDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /devicedata : get all the devicedata.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of devicedata in body
     */
    @RequestMapping(value = "/devicedata",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DevicedataDTO> getAllDevicedata() {
        log.debug("REST request to get all Devicedata");
        return devicedataService.findAll();
    }
    
    /**
     * GET  /devicedata : get all the devicedata.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of devicedata in body
     * @throws JsonProcessingException 
     */
    @RequestMapping(value = "/chartdevicedata",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public String getAllDevicedataForCharts() throws JsonProcessingException {
        log.debug("REST request to get all Devicedata");
        return devicedataService.findAllForCharts();
    }

    /**
     * GET  /devicedata/:id : get the "id" devicedata.
     *
     * @param id the id of the devicedataDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the devicedataDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/devicedata/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DevicedataDTO> getDevicedata(@PathVariable Long id) {
        log.debug("REST request to get Devicedata : {}", id);
        DevicedataDTO devicedataDTO = devicedataService.findOne(id);
        return Optional.ofNullable(devicedataDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /devicedata/:id : delete the "id" devicedata.
     *
     * @param id the id of the devicedataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/devicedata/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDevicedata(@PathVariable Long id) {
        log.debug("REST request to delete Devicedata : {}", id);
        devicedataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("devicedata", id.toString())).build();
    }

    /**
     * SEARCH  /_search/devicedata?query=:query : search for the devicedata corresponding
     * to the query.
     *
     * @param query the query of the devicedata search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/devicedata",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DevicedataDTO> searchDevicedata(@RequestParam String query) {
        log.debug("REST request to search Devicedata for query {}", query);
        return devicedataService.search(query);
    }


}
