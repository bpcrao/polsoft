package com.polmon.polsoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.domain.Dchannel;
import com.polmon.polsoft.repository.DchannelRepository;
import com.polmon.polsoft.service.AlarmtemplateService;
import com.polmon.polsoft.service.dto.AlarmtemplateDTO;
import com.polmon.polsoft.service.eventsender.DeviceEventSender;
import com.polmon.polsoft.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Alarmtemplate.
 */
@RestController
@RequestMapping("/api")
public class AlarmtemplateResource {

    private final Logger log = LoggerFactory.getLogger(AlarmtemplateResource.class);
        
    @Inject
    private AlarmtemplateService alarmtemplateService;
    
    @Inject
    private DchannelRepository dChannelRepository;
    
    @Inject
    private DeviceEventSender deviceEventSender;

    /**
     * POST  /alarmtemplates : Create a new alarmtemplate.
     *
     * @param alarmtemplateDTO the alarmtemplateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alarmtemplateDTO, or with status 400 (Bad Request) if the alarmtemplate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarmtemplates",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplateDTO> createAlarmtemplate(@RequestBody AlarmtemplateDTO alarmtemplateDTO) throws URISyntaxException {
        log.debug("REST request to save Alarmtemplate : {}", alarmtemplateDTO);
        if (alarmtemplateDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("alarmtemplate", "idexists", "A new alarmtemplate cannot already have an ID")).body(null);
        }
        AlarmtemplateDTO result = alarmtemplateService.save(alarmtemplateDTO);
    	sendAlarmUpdateNotification(result);
        
        return ResponseEntity.created(new URI("/api/alarmtemplates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("alarmtemplate", result.getId().toString()))
            .body(result);
    }

	private void sendAlarmUpdateNotification(AlarmtemplateDTO result) {
		if (result.getDchannelId() != null && result.getDchannelId()>0l) {			
			Dchannel dChannel = dChannelRepository.findOne(result.getDchannelId());
			deviceEventSender.sendAlarmMinMaxEvent(dChannel.getDevice().getId());
		}
	}

    /**
     * PUT  /alarmtemplates : Updates an existing alarmtemplate.
     *
     * @param alarmtemplateDTO the alarmtemplateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alarmtemplateDTO,
     * or with status 400 (Bad Request) if the alarmtemplateDTO is not valid,
     * or with status 500 (Internal Server Error) if the alarmtemplateDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarmtemplates",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplateDTO> updateAlarmtemplate(@RequestBody AlarmtemplateDTO alarmtemplateDTO) throws URISyntaxException {
        log.debug("REST request to update Alarmtemplate : {}", alarmtemplateDTO);
        if (alarmtemplateDTO.getId() == null) {
        	log.debug(" it has id " + alarmtemplateDTO.getId());
            return createAlarmtemplate(alarmtemplateDTO);
        }
        AlarmtemplateDTO result = alarmtemplateService.save(alarmtemplateDTO);
        sendAlarmUpdateNotification(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("alarmtemplate", alarmtemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /alarmtemplates : get all the alarmtemplates.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alarmtemplates in body
     */
    @RequestMapping(value = "/alarmtemplates",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplateDTO> getAllAlarmtemplates() {
        log.debug("REST request to get all Alarmtemplates");
        return alarmtemplateService.findAll();
    }

    /**
     * GET  /alarmtemplates/:id : get the "id" alarmtemplate.
     *
     * @param id the id of the alarmtemplateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alarmtemplateDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/alarmtemplates/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplateDTO> getAlarmtemplate(@PathVariable Long id) {
        log.debug("REST request to get Alarmtemplate : {}", id);
        AlarmtemplateDTO alarmtemplateDTO = alarmtemplateService.findOne(id);
        return Optional.ofNullable(alarmtemplateDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /alarmtemplates/noChannel : get all the alarmtemplates.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alarmtemplates in body
     */
    @RequestMapping(value = "/alarmtemplates/noChannel",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplateDTO> getAlarmtemplatesWithoutChannelId() {
        log.debug("REST request to get all Alarmtemplates");
        return alarmtemplateService.findAllWithOutChannelId();
    }
    
    /**
     * GET  /alarmtemplates/withChannel : get all the alarmtemplates.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alarmtemplates in body
     */
    @RequestMapping(value = "/alarmtemplates/withChannel/{dchannelId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplateDTO> getAlarmtemplatesWithChannelId(@PathVariable Long dchannelId) {
        log.debug("REST request to get all Alarmtemplates with channel "+dchannelId);
        return alarmtemplateService.findAllWithChannelId(dchannelId);
    }
    
    /**
     * DELETE  /alarmtemplates/:id : delete the "id" alarmtemplate.
     *
     * @param id the id of the alarmtemplateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/alarmtemplates/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAlarmtemplate(@PathVariable Long id) {
        log.debug("REST request to delete Alarmtemplate : {}", id);
        AlarmtemplateDTO alarmtemplateDTO = alarmtemplateService.findOne(id);
        alarmtemplateService.delete(id);
        sendAlarmUpdateNotification(alarmtemplateDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("alarmtemplate", id.toString())).build();
    }

    /**
     * SEARCH  /_search/alarmtemplates?query=:query : search for the alarmtemplate corresponding
     * to the query.
     *
     * @param query the query of the alarmtemplate search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/alarmtemplates",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplateDTO> searchAlarmtemplates(@RequestParam String query) {
        log.debug("REST request to search Alarmtemplates for query {}", query);
        return alarmtemplateService.search(query);
    }


}
