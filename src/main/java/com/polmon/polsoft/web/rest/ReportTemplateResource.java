package com.polmon.polsoft.web.rest;

import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.SECOND;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.config.audit.AuditEventPublisher;
import com.polmon.polsoft.domain.ReportTemplate;
import com.polmon.polsoft.security.SecurityUtils;
import com.polmon.polsoft.service.ReportTemplateService;
import com.polmon.polsoft.service.dto.ReportTemplateDTO;
import com.polmon.polsoft.service.util.PdfGeneratorUtil;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.web.rest.util.PaginationUtil;

/**
 * REST controller for managing ReportTemplate.
 */
@RestController
@RequestMapping("/api")
public class ReportTemplateResource {

    private final Logger log = LoggerFactory.getLogger(ReportTemplateResource.class);
        
    @Inject
    private ReportTemplateService reportTemplateService;
    
    @Autowired
    private PdfGeneratorUtil pdfGeneratorUtil;
    
    @Inject
	private AuditEventPublisher auditPublisher;
    /**
     * POST  /report-templates : Create a new reportTemplate.
     *
     * @param reportTemplateDTO the reportTemplateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reportTemplateDTO, or with status 400 (Bad Request) if the reportTemplate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/report-templates",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ReportTemplateDTO> createReportTemplate(@RequestBody ReportTemplateDTO reportTemplateDTO) throws URISyntaxException {
        log.debug("REST request to save ReportTemplate : {}", reportTemplateDTO);
        if (reportTemplateDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("reportTemplate", "idexists", "A new reportTemplate cannot already have an ID")).body(null);
        }
        ReportTemplateDTO result = reportTemplateService.save(reportTemplateDTO);
        return ResponseEntity.created(new URI("/api/report-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("reportTemplate", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /report-templates : Updates an existing reportTemplate.
     *
     * @param reportTemplateDTO the reportTemplateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reportTemplateDTO,
     * or with status 400 (Bad Request) if the reportTemplateDTO is not valid,
     * or with status 500 (Internal Server Error) if the reportTemplateDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/report-templates",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ReportTemplateDTO> updateReportTemplate(@RequestBody ReportTemplateDTO reportTemplateDTO) throws URISyntaxException {
        log.debug("REST request to update ReportTemplate : {}", reportTemplateDTO);
        if (reportTemplateDTO.getId() == null) {
            return createReportTemplate(reportTemplateDTO);
        }
        ReportTemplateDTO result = reportTemplateService.save(reportTemplateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("reportTemplate", reportTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /report-templates : get all the reportTemplates.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of reportTemplates in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/report-templates",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ReportTemplateDTO>> getAllReportTemplates(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ReportTemplates");
        Page<ReportTemplateDTO> page = reportTemplateService.findAll(pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/report-templates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /report-templates/:id : get the "id" reportTemplate.
     *
     * @param id the id of the reportTemplateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reportTemplateDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/report-templates/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ReportTemplateDTO> getReportTemplate(@PathVariable Long id) {
        log.debug("REST request to get ReportTemplate : {}", id);
        ReportTemplateDTO reportTemplateDTO = reportTemplateService.findOne(id);
        return Optional.ofNullable(reportTemplateDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /report-templates/:id : delete the "id" reportTemplate.
     *
     * @param id the id of the reportTemplateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/report-templates/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteReportTemplate(@PathVariable Long id) {
        log.debug("REST request to delete ReportTemplate : {}", id);
        reportTemplateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("reportTemplate", id.toString())).build();
    }

    
	@RequestMapping(value = "/report-templates/{id}/download", method = RequestMethod.GET)
	@Timed
	public void downloadReportTemplate(@PathVariable Long id,HttpServletResponse response,
										@RequestParam(value = "fromDate") String fromDate,
										@RequestParam(value = "toDate") String toDate) 
									throws Exception {
		log.debug("REST request to download ReportTemplate : {}", id);
		fromDate=  fromDate.replaceAll("^\"|\"$", "");
		toDate =  toDate.replaceAll("^\"|\"$", "");
		ReportTemplate reportTemplate= reportTemplateService.findOneReportTemplate(id);
		byte[] bytes;
		try {
			bytes = pdfGeneratorUtil.forEachReport(reportTemplate,fromDate,toDate);
		} catch (Exception e) {
			bytes = new byte[1024];
		}
		Date From_time = null, To_time = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		From_time = trim(dateFormat.parse(fromDate));
		To_time = trim(dateFormat.parse(toDate));
		String fDate = df.format(From_time).toString();
		String tDate = df.format(To_time).toString();
		String x = reportTemplate.getName()+""+fDate.replaceAll("[^a-zA-Z0-9\\._]+", "")+"_"+tDate.replaceAll("[^a-zA-Z0-9\\._]+", "")+".pdf";
		
		response.setContentLength(bytes.length);
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition","attachment; filename="+x);
		response.getOutputStream().write(bytes);
		response.getOutputStream().flush();
		
		Map data = new HashMap<String, Object>();
    	data.put("name", reportTemplate.getName());
    	auditPublisher.publish(new AuditEvent(SecurityUtils.getCurrentUserLogin(), "Report_download",data));
		return;
	}
    
	 public static Date trim(Date date) {
	        Calendar cal = Calendar.getInstance();
	        TimeZone tz = TimeZone.getTimeZone("GMT");
	        cal.setTime(date);
	        cal.setTimeZone(tz);
	       // cal.set(HOUR_OF_DAY, 0);
	       // cal.set(MINUTE, 0);
	        cal.set(SECOND, 0);
	        cal.set(MILLISECOND, 0);
	        return cal.getTime();
	   }
    /**
     * SEARCH  /_search/report-templates?query=:query : search for the reportTemplate corresponding
     * to the query.
     *
     * @param query the query of the reportTemplate search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/report-templates",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ReportTemplateDTO>> searchReportTemplates(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ReportTemplates for query {}", query);
        Page<ReportTemplateDTO> page = reportTemplateService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/report-templates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}