/**
 * View Models used by Spring MVC REST controllers.
 */
package com.polmon.polsoft.web.rest.vm;
