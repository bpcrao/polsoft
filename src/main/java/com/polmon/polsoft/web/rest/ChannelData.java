package com.polmon.polsoft.web.rest;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Map;

public class ChannelData {
	private String deviceSerial;
	private Map<String, BigDecimal> channelValues;
	private long time;
	private String day;
	private int hour;
	private ZonedDateTime created;

	public ChannelData() {
	}

	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}

	public Map<String, BigDecimal> getChannelValues() {
		return channelValues;
	}

	public void setChannelValues(Map<String, BigDecimal> channelValues) {
		this.channelValues = channelValues;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}
}