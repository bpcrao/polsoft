package com.polmon.polsoft.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.service.ChannelAlarmService;
import com.polmon.polsoft.service.dto.ChannelAlarmDTO;
import com.polmon.polsoft.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ChannelAlarm.
 */
@RestController
@RequestMapping("/api")
public class ChannelAlarmResource {

    private final Logger log = LoggerFactory.getLogger(ChannelAlarmResource.class);
        
    @Inject
    private ChannelAlarmService channelAlarmService;

    /**
     * POST  /channel-alarms : Create a new channelAlarm.
     *
     * @param channelAlarmDTO the channelAlarmDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new channelAlarmDTO, or with status 400 (Bad Request) if the channelAlarm has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/channel-alarms",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ChannelAlarmDTO> createChannelAlarm(@RequestBody ChannelAlarmDTO channelAlarmDTO) throws URISyntaxException {
        log.debug("REST request to save ChannelAlarm : {}", channelAlarmDTO);
        if (channelAlarmDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("channelAlarm", "idexists", "A new channelAlarm cannot already have an ID")).body(null);
        }
        ChannelAlarmDTO result = channelAlarmService.save(channelAlarmDTO);
        return ResponseEntity.created(new URI("/api/channel-alarms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("channelAlarm", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /channel-alarms : Updates an existing channelAlarm.
     *
     * @param channelAlarmDTO the channelAlarmDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated channelAlarmDTO,
     * or with status 400 (Bad Request) if the channelAlarmDTO is not valid,
     * or with status 500 (Internal Server Error) if the channelAlarmDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/channel-alarms",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ChannelAlarmDTO> updateChannelAlarm(@RequestBody ChannelAlarmDTO channelAlarmDTO) throws URISyntaxException {
        log.debug("REST request to update ChannelAlarm : {}", channelAlarmDTO);
        if (channelAlarmDTO.getId() == null) {
            return createChannelAlarm(channelAlarmDTO);
        }
        ChannelAlarmDTO result = channelAlarmService.save(channelAlarmDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("channelAlarm", channelAlarmDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /channel-alarms : get all the channelAlarms.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of channelAlarms in body
     */
    @RequestMapping(value = "/channel-alarms",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ChannelAlarmDTO> getAllChannelAlarms() {
        log.debug("REST request to get all ChannelAlarms");
        return channelAlarmService.findAll();
    }

    /**
     * GET  /channel-alarms/:id : get the "id" channelAlarm.
     *
     * @param id the id of the channelAlarmDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the channelAlarmDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/channel-alarms/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ChannelAlarmDTO> getChannelAlarm(@PathVariable Long id) {
        log.debug("REST request to get ChannelAlarm : {}", id);
        ChannelAlarmDTO channelAlarmDTO = channelAlarmService.findOne(id);
        return Optional.ofNullable(channelAlarmDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /channel-alarms/:id : delete the "id" channelAlarm.
     *
     * @param id the id of the channelAlarmDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/channel-alarms/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteChannelAlarm(@PathVariable Long id) {
        log.debug("REST request to delete ChannelAlarm : {}", id);
        channelAlarmService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("channelAlarm", id.toString())).build();
    }

    /**
     * SEARCH  /_search/channel-alarms?query=:query : search for the channelAlarm corresponding
     * to the query.
     *
     * @param query the query of the channelAlarm search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/channel-alarms",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ChannelAlarmDTO> searchChannelAlarms(@RequestParam String query) {
        log.debug("REST request to search ChannelAlarms for query {}", query);
        return channelAlarmService.search(query);
    }


}
