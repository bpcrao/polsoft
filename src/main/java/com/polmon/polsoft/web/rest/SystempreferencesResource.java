package com.polmon.polsoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.service.SystempreferencesService;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.service.dto.SystempreferencesDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Systempreferences.
 */
@RestController
@RequestMapping("/api")
public class SystempreferencesResource {

    private final Logger log = LoggerFactory.getLogger(SystempreferencesResource.class);
        
    @Inject
    private SystempreferencesService systempreferencesService;

    /**
     * POST  /systempreferences : Create a new systempreferences.
     *
     * @param systempreferencesDTO the systempreferencesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new systempreferencesDTO, or with status 400 (Bad Request) if the systempreferences has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
//    @RequestMapping(value = "/systempreferences",
//        method = RequestMethod.POST,
//        produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<SystempreferencesDTO> createSystempreferences(@RequestBody SystempreferencesDTO systempreferencesDTO) throws URISyntaxException {
//        log.debug("REST request to save Systempreferences : {}", systempreferencesDTO);
//        if (systempreferencesDTO.getId() != null) {
//            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("systempreferences", "idexists", "A new systempreferences cannot already have an ID")).body(null);
//        }
//        SystempreferencesDTO result = systempreferencesService.save(systempreferencesDTO);
//        return ResponseEntity.created(new URI("/api/systempreferences/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert("systempreferences", result.getId().toString()))
//            .body(result);
//    }

    @RequestMapping(value = "/systempreferences",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        public ResponseEntity<List<SystempreferencesDTO>> createSystempreferences(@RequestBody List<SystempreferencesDTO> systempreferencesDTO) throws URISyntaxException {
            log.debug("REST request to save Systempreferences : {}", systempreferencesDTO);
            List<SystempreferencesDTO> result = systempreferencesService.save(systempreferencesDTO);
            return ResponseEntity.created(new URI("/api/systempreferences/"))            
                .body(result);
        }
    /**
     * GET  /systempreferences : get all the systempreferences.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of systempreferences in body
     */
    @RequestMapping(value = "/systempreferences",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SystempreferencesDTO> getAllSystempreferences() {
        log.debug("REST request to get all Systempreferences");
        return systempreferencesService.findAll();
    }

    /**
     * GET  /systempreferences/:id : get the "id" systempreferences.
     *
     * @param id the id of the systempreferencesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the systempreferencesDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/systempreferences/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SystempreferencesDTO> getSystempreferences(@PathVariable Long id) {
        log.debug("REST request to get Systempreferences : {}", id);
        SystempreferencesDTO systempreferencesDTO = systempreferencesService.findOne(id);
        return Optional.ofNullable(systempreferencesDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /systempreferences/:id : delete the "id" systempreferences.
     *
     * @param id the id of the systempreferencesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/systempreferences/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSystempreferences(@PathVariable Long id) {
        log.debug("REST request to delete Systempreferences : {}", id);
        systempreferencesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("systempreferences", id.toString())).build();
    }

    /**
     * SEARCH  /_search/systempreferences?query=:query : search for the systempreferences corresponding
     * to the query.
     *
     * @param query the query of the systempreferences search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/systempreferences",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SystempreferencesDTO> searchSystempreferences(@RequestParam String query) {
        log.debug("REST request to search Systempreferences for query {}", query);
        return systempreferencesService.search(query);
    }
}
