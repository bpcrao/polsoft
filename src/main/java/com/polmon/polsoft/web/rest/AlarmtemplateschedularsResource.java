package com.polmon.polsoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.service.AlarmtemplateschedularsService;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.service.dto.AlarmtemplateschedularsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Alarmtemplateschedulars.
 */
@RestController
@RequestMapping("/api")
public class AlarmtemplateschedularsResource {

    private final Logger log = LoggerFactory.getLogger(AlarmtemplateschedularsResource.class);
        
    @Inject
    private AlarmtemplateschedularsService alarmtemplateschedularsService;

    /**
     * POST  /alarmtemplateschedulars : Create a new alarmtemplateschedulars.
     *
     * @param alarmtemplateschedularsDTO the alarmtemplateschedularsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alarmtemplateschedularsDTO, or with status 400 (Bad Request) if the alarmtemplateschedulars has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarmtemplateschedulars",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplateschedularsDTO> createAlarmtemplateschedulars(@RequestBody AlarmtemplateschedularsDTO alarmtemplateschedularsDTO) throws URISyntaxException {
        log.debug("REST request to save Alarmtemplateschedulars : {}", alarmtemplateschedularsDTO);
        if (alarmtemplateschedularsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("alarmtemplateschedulars", "idexists", "A new alarmtemplateschedulars cannot already have an ID")).body(null);
        }
        AlarmtemplateschedularsDTO result = alarmtemplateschedularsService.save(alarmtemplateschedularsDTO);
        return ResponseEntity.created(new URI("/api/alarmtemplateschedulars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("alarmtemplateschedulars", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /alarmtemplateschedulars : Updates an existing alarmtemplateschedulars.
     *
     * @param alarmtemplateschedularsDTO the alarmtemplateschedularsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alarmtemplateschedularsDTO,
     * or with status 400 (Bad Request) if the alarmtemplateschedularsDTO is not valid,
     * or with status 500 (Internal Server Error) if the alarmtemplateschedularsDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/alarmtemplateschedulars",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplateschedularsDTO> updateAlarmtemplateschedulars(@RequestBody AlarmtemplateschedularsDTO alarmtemplateschedularsDTO) throws URISyntaxException {
        log.debug("REST request to update Alarmtemplateschedulars : {}", alarmtemplateschedularsDTO);
        if (alarmtemplateschedularsDTO.getId() == null) {
            return createAlarmtemplateschedulars(alarmtemplateschedularsDTO);
        }
        AlarmtemplateschedularsDTO result = alarmtemplateschedularsService.save(alarmtemplateschedularsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("alarmtemplateschedulars", alarmtemplateschedularsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /alarmtemplateschedulars : get all the alarmtemplateschedulars.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alarmtemplateschedulars in body
     */
    @RequestMapping(value = "/alarmtemplateschedulars",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplateschedularsDTO> getAllAlarmtemplateschedulars() {
        log.debug("REST request to get all Alarmtemplateschedulars");
        return alarmtemplateschedularsService.findAll();
    }

    /**
     * GET  /alarmtemplateschedulars/:id : get the "id" alarmtemplateschedulars.
     *
     * @param id the id of the alarmtemplateschedularsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alarmtemplateschedularsDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/alarmtemplateschedulars/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AlarmtemplateschedularsDTO> getAlarmtemplateschedulars(@PathVariable Long id) {
        log.debug("REST request to get Alarmtemplateschedulars : {}", id);
        AlarmtemplateschedularsDTO alarmtemplateschedularsDTO = alarmtemplateschedularsService.findOne(id);
        return Optional.ofNullable(alarmtemplateschedularsDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /alarmtemplateschedulars/:id : delete the "id" alarmtemplateschedulars.
     *
     * @param id the id of the alarmtemplateschedularsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/alarmtemplateschedulars/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAlarmtemplateschedulars(@PathVariable Long id) {
        log.debug("REST request to delete Alarmtemplateschedulars : {}", id);
        alarmtemplateschedularsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("alarmtemplateschedulars", id.toString())).build();
    }

    /**
     * SEARCH  /_search/alarmtemplateschedulars?query=:query : search for the alarmtemplateschedulars corresponding
     * to the query.
     *
     * @param query the query of the alarmtemplateschedulars search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/alarmtemplateschedulars",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AlarmtemplateschedularsDTO> searchAlarmtemplateschedulars(@RequestParam String query) {
        log.debug("REST request to search Alarmtemplateschedulars for query {}", query);
        return alarmtemplateschedularsService.search(query);
    }


}
