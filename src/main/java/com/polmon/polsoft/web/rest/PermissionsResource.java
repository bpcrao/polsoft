package com.polmon.polsoft.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.BooleanUtils;
import org.json.JSONException;
import org.springframework.http.MediaType;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.permissions.PolsoftPermission;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class PermissionsResource {

	@Inject
	private JdbcMutableAclService aclService;

	@RequestMapping(value = "/permissions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public Map getResourcePermission(@RequestParam("resource") String resource,
			@RequestParam("resourceId") String recourceId) throws URISyntaxException {
		ObjectIdentity oi = new ObjectIdentityImpl(Location.class, new Long(recourceId));
		Acl acl = aclService.readAclById(oi);
		List<AccessControlEntry> aclEntries = acl.getEntries();
		
		
		Collections.sort(aclEntries,new ACLComparator());  

		Map jsonAcl = new HashMap();
		jsonAcl.put("resource", acl.getObjectIdentity());

		Map<String,List<Map>> acEntries = new HashMap<String,List<Map>>();
		for (AccessControlEntry ace : aclEntries) {
			Map aclEntry = new HashMap();
			aclEntry.put("user", ((PrincipalSid) ace.getSid()).getPrincipal());
			aclEntry.put("permission", ace.getPermission().getMask());
			aclEntry.put("granted", ace.isGranting());
			
			if(acEntries.containsKey(aclEntry.get("user"))){
				acEntries.get(aclEntry.get("user").toString()).add(aclEntry);
			}else{
				 List aList = new ArrayList();
				 aList.add(aclEntry);
				acEntries.put(aclEntry.get("user").toString(),aList);
			}
		}	
		
		jsonAcl.put("permissions", acEntries);
		return jsonAcl;
	}

	@RequestMapping(value = "/permissions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	@ApiParam(name = "permissions", example = "{		\"permissions\":[{	 \"permission\": 1,\"granting\": true}, { \"permission\": 8,\"granting\": true },{\"permission\": 32,\"granting\": true}]}")
	public void createResourcePermission(@RequestParam("resource") String resource,
			@RequestParam("resourceId") String recourceId, @RequestParam("principal") String principal,
			@ApiParam(name = "permissions", example = "{		\"permissions\":[{	 \"permission\": 1,\"granting\": true}, { \"permission\": 8,\"granting\": true },{\"permission\": 32,\"granting\": true}]}") @RequestBody Map permissions)
			throws URISyntaxException, JSONException {

		new ObjectIdentityImpl(Location.class, new Long(recourceId));
		MutableAcl acl = (MutableAcl) aclService
				.readAclById(new ObjectIdentityImpl(Location.class, new Long(recourceId)));

		List<AccessControlEntry> accessControlEnties = acl.getEntries().stream()
				.filter(ace -> ((PrincipalSid) ace.getSid()).getPrincipal().equals(principal))
				.collect(Collectors.toList());
		Map<Integer, AccessControlEntry> permMap = accessControlEnties.stream()
				.collect(Collectors.toMap(ace -> Integer.valueOf(ace.getPermission().getMask()), ace -> ace));

		ArrayList permissionList = (ArrayList) permissions.get("permissions");

		for (int index = 0, size = permissionList.size(); index < size; index++) {
			HashMap permissionData = (HashMap) permissionList.get(index);
			Boolean isGranting = BooleanUtils.toBoolean((Boolean) permissionData.get("granting"));
			Integer permissionID = Integer.valueOf(permissionData.get("permission").toString());
			if (permMap.containsKey(permissionID)) {
				AccessControlEntry ace = permMap.get(permissionID);
				if (isGranting != ace.isGranting()) {
					acl.deleteAce(acl.getEntries().indexOf(ace));
					acl.insertAce(acl.getEntries().size(), (Permission) new PolsoftPermission(permissionID),
							new PrincipalSid(principal), isGranting);
				}
			} else {
				acl.insertAce(acl.getEntries().size(), (Permission) new PolsoftPermission(permissionID),
						new PrincipalSid(principal), isGranting);
			}
		}
		aclService.updateAcl(acl);
	}
	@RequestMapping(value = "/permissions", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	@ApiParam(name = "permissions", example = "{		\"permissions\":[{	 \"permission\": 1,\"granting\": true}, { \"permission\": 8,\"granting\": true },{\"permission\": 32,\"granting\": true}]}")
	public void deleteResourcePermission(@RequestParam("resource") String resource,
			@RequestParam("resourceId") String recourceId, @RequestParam("principal") String principal)
			throws URISyntaxException, JSONException {
		ObjectIdentityImpl identity = new ObjectIdentityImpl(Location.class, new Long(recourceId));
		MutableAcl acl = (MutableAcl) aclService.readAclById(identity);
		List<AccessControlEntry> principalControlEnties = acl.getEntries().stream()
				.filter(ace -> ((PrincipalSid) ace.getSid()).getPrincipal().equals(principal))
				.collect(Collectors.toList());
		principalControlEnties.forEach(accessEntry ->{
			acl.deleteAce(acl.getEntries().indexOf(accessEntry));
		});
		aclService.updateAcl(acl);
	}
	
	public class ACLComparator implements Comparator<AccessControlEntry>{

		@Override
		public int compare(AccessControlEntry ace1, AccessControlEntry ace2) {
			if(ace1.getPermission().getMask() < ace2.getPermission().getMask()){
				return 1;
			}else if(ace1.getPermission().getMask() > ace2.getPermission().getMask()) {
				return -1;
			}
			
			return 0;
		}
		
		
	}
}
