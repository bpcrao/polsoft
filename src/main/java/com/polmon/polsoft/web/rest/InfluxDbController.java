package com.polmon.polsoft.web.rest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.influxdb.dto.QueryResult.Series;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.polmon.polsoft.domain.Location;
import com.polmon.polsoft.service.LocationService;
import com.polmon.polsoft.service.dto.LocationOverviewDTO;

@RestController
@RequestMapping("/api")
public class InfluxDbController {
	
	@Autowired
	private InfluxDBTemplate<Point> influxdbTemplate;
	
	@Autowired
	private LocationService locationService;
	
	@RequestMapping(method=RequestMethod.GET,value="/influx")
	public QueryResult getInfluxResponse(@RequestParam String queryString)
	{
		Query query= new Query(queryString,influxdbTemplate.getDatabase());
		QueryResult queryResult = influxdbTemplate.query(query);
		//List<Result> results = queryResult.getResults();
		return queryResult;
	}
	@RequestMapping(method=RequestMethod.GET,value="/overview")
	public List<LocationOverviewDTO> getOverviewResponse() throws Exception
	{
		List<Long> locationsLinked = new ArrayList<>();
		DateFormat influxDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		List<LocationOverviewDTO> locationValues = new ArrayList<>();
		LocalDateTime toDate = LocalDateTime.now();
		String queryLoc = "";
		LocalDateTime fromDate = toDate.minusMinutes(1000);
		String fromDateString = influxDateFormat.format(dateFormat.parse(fromDate.minusMinutes(330).toString()));
		String toDateString = influxDateFormat.format(dateFormat.parse(toDate.minusMinutes(330).toString()));
		List<Location> locations = locationService.findAll();
		int index = 0;
		for (Location locationObj : locations) {
			locationsLinked.add(locationObj.getId());
		}
		for (Long locationId : locationsLinked) {
			queryLoc = queryLoc + (index == 0 ? "location = '" : "OR location = '") + locationId +"'";
			index++;	
		}

		Query queryCurrentValue = new Query ("SELECT * FROM data where " + queryLoc + "order by time desc limit " +queryLoc.length(),
				influxdbTemplate.getDatabase());
		
		Query query= new Query("SELECT MAX(value),MIN(value),STDDEV(value),MEAN(value) FROM data where " + queryLoc + " and time >= '" + fromDateString
				+ "'and time<='" + toDateString + "' group by location_tag order by time asc",influxdbTemplate.getDatabase());
		//Query query= new Query("SELECT MAX(value),MIN(value),STDDEV(value),MEAN(value) FROM data where location =~ " + queryLoc + "/  group by location_tag order by time asc",influxdbTemplate.getDatabase());
		
		QueryResult queryResultCurrentValue = influxdbTemplate.query(queryCurrentValue);
		List<Result> resultsCurrentValue = queryResultCurrentValue.getResults();
		
		QueryResult queryResult = influxdbTemplate.query(query);
		List<Result> results = queryResult.getResults();
		
		Result resultCurrentValue = resultsCurrentValue.get(0);
		List<Series> seriesesCurrentValue = resultCurrentValue.getSeries();
		List<List<Object>> currentValue = seriesesCurrentValue.get(0).getValues();
		if(results!=null && results.size()>0)
		{
			Result result = results.get(0);
			List<Series> serieses = result.getSeries();
			if(serieses!=null && serieses.size()>0)
			{
				for (Series series : serieses) {
					int index1 = 0;
					LocationOverviewDTO locationvalue = new LocationOverviewDTO();
					locationvalue.setSamples(1000L);
					List<List<Object>> values = series.getValues();
					if(values!=null && values.size()>0)
					{
						List<Object> value = values.get(0);
						locationvalue.setTime(value.get(0).toString());
						
						BigDecimal bdMax = new BigDecimal(value.get(1).toString());
						bdMax = bdMax.setScale(1, RoundingMode.HALF_UP);
						locationvalue.setMaxValue(bdMax.doubleValue());
						
						BigDecimal bdMin = new BigDecimal(value.get(2).toString());
						bdMin = bdMin.setScale(1, RoundingMode.HALF_UP);
						locationvalue.setMinValue(bdMin.doubleValue());
						
						BigDecimal bdStdDev = new BigDecimal(value.get(3).toString());
						bdStdDev = bdStdDev.setScale(1, RoundingMode.HALF_UP);
						locationvalue.setStdDevValue(bdStdDev.doubleValue());
						
						BigDecimal bdMean = new BigDecimal(value.get(4).toString());
						bdMean = bdMean.setScale(1, RoundingMode.HALF_UP);
						locationvalue.setMeanValue(bdMean.doubleValue());
						
						BigDecimal bdCurrValue = new BigDecimal(currentValue.get(index1).get(7).toString());
						bdCurrValue = bdCurrValue.setScale(1, RoundingMode.HALF_UP);
						locationvalue.setCurrValue(bdCurrValue.doubleValue());
					}
					Map<String, String> tags = series.getTags();
					if(tags!=null && tags.size()>0)
					{
						locationvalue.setLocationId(new Long(tags.get("location_tag")));
					}
					locationValues.add(locationvalue);
					index1++;
				}
			}
		}
		return locationValues;
	}
}
