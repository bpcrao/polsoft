package com.polmon.polsoft.web.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.domain.Channeldata;
import com.polmon.polsoft.repository.ChannelDataRepository;

@RestController
@RequestMapping("/api")
public class ChannelDataResource {

	private final Logger log = LoggerFactory.getLogger(ChannelDataResource.class);

	@Inject
	private ChannelDataRepository channelDataRepository;

	@RequestMapping(value = "/channeldata", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Channeldata> getList(@RequestParam("startDateTime") String startDateTime,
			@RequestParam("endDateTime") String endDateTime,
			@RequestParam("selectedInterval") String selectedInterval) {
		log.warn(startDateTime);
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss").parse(startDateTime);
			endDate = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss").parse(endDateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (selectedInterval.contentEquals("Hours")) {
			return channelDataRepository
					.findAllByLogdateGreaterThanAndLogdateLessThanAndHourflagOrderByLogdateAsc(startDate, endDate, 1);
		} else if (selectedInterval.contentEquals("Days")) {
			return channelDataRepository.findAllByLogdateGreaterThanAndLogdateLessThanOrderByLogdateAsc(startDate,
					endDate);
		} else {
			return channelDataRepository.findAllByLogdateGreaterThanAndLogdateLessThanOrderByLogdateAsc(startDate,
					endDate);
		}

	}

}
