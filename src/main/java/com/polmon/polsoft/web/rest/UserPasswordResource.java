package com.polmon.polsoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.service.UserPasswordService;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.service.dto.UserPasswordDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserPassword.
 */
@RestController
@RequestMapping("/api")
public class UserPasswordResource {

    private final Logger log = LoggerFactory.getLogger(UserPasswordResource.class);
        
    @Inject
    private UserPasswordService userPasswordService;

    /**
     * POST  /user-passwords : Create a new userPassword.
     *
     * @param userPasswordDTO the userPasswordDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userPasswordDTO, or with status 400 (Bad Request) if the userPassword has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-passwords",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserPasswordDTO> createUserPassword(@Valid @RequestBody UserPasswordDTO userPasswordDTO) throws URISyntaxException {
        log.debug("REST request to save UserPassword : {}", userPasswordDTO);
        if (userPasswordDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userPassword", "idexists", "A new userPassword cannot already have an ID")).body(null);
        }
        UserPasswordDTO result = userPasswordService.save(userPasswordDTO);
        return ResponseEntity.created(new URI("/api/user-passwords/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userPassword", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-passwords : Updates an existing userPassword.
     *
     * @param userPasswordDTO the userPasswordDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userPasswordDTO,
     * or with status 400 (Bad Request) if the userPasswordDTO is not valid,
     * or with status 500 (Internal Server Error) if the userPasswordDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-passwords",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserPasswordDTO> updateUserPassword(@Valid @RequestBody UserPasswordDTO userPasswordDTO) throws URISyntaxException {
        log.debug("REST request to update UserPassword : {}", userPasswordDTO);
        if (userPasswordDTO.getId() == null) {
            return createUserPassword(userPasswordDTO);
        }
        UserPasswordDTO result = userPasswordService.save(userPasswordDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userPassword", userPasswordDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-passwords : get all the userPasswords.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userPasswords in body
     */
    @RequestMapping(value = "/user-passwords",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserPasswordDTO> getAllUserPasswords() {
        log.debug("REST request to get all UserPasswords");
        return userPasswordService.findAll();
    }

    /**
     * GET  /user-passwords/:id : get the "id" userPassword.
     *
     * @param id the id of the userPasswordDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userPasswordDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/user-passwords/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserPasswordDTO> getUserPassword(@PathVariable Long id) {
        log.debug("REST request to get UserPassword : {}", id);
        UserPasswordDTO userPasswordDTO = userPasswordService.findOne(id);
        return Optional.ofNullable(userPasswordDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /user-passwords/:id : delete the "id" userPassword.
     *
     * @param id the id of the userPasswordDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/user-passwords/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserPassword(@PathVariable Long id) {
        log.debug("REST request to delete UserPassword : {}", id);
        userPasswordService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userPassword", id.toString())).build();
    }

    /**
     * SEARCH  /_search/user-passwords?query=:query : search for the userPassword corresponding
     * to the query.
     *
     * @param query the query of the userPassword search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/user-passwords",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserPasswordDTO> searchUserPasswords(@RequestParam String query) {
        log.debug("REST request to search UserPasswords for query {}", query);
        return userPasswordService.search(query);
    }


}
