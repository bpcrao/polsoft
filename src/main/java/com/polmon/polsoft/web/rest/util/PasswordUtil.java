package com.polmon.polsoft.web.rest.util;

import java.security.PrivateKey;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.polmon.polsoft.security.RSAUtils;

public class PasswordUtil {
	public static String getDecodedPassword(String rawPassword)
	{
		HttpServletRequest request =
		        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		    PrivateKey privateKey = (PrivateKey) request.getSession().getAttribute("_private_key");
		    String password = RSAUtils.decrypt(rawPassword.toString(), privateKey);
		    return password;
	}
}
