package com.polmon.polsoft.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.polmon.polsoft.service.ThresholdService;
import com.polmon.polsoft.web.rest.util.HeaderUtil;
import com.polmon.polsoft.service.dto.ThresholdDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Threshold.
 */
@RestController
@RequestMapping("/api")
public class ThresholdResource {

    private final Logger log = LoggerFactory.getLogger(ThresholdResource.class);
        
    @Inject
    private ThresholdService thresholdService;

    /**
     * POST  /thresholds : Create a new threshold.
     *
     * @param thresholdDTO the thresholdDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new thresholdDTO, or with status 400 (Bad Request) if the threshold has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/thresholds",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ThresholdDTO> createThreshold(@Valid @RequestBody ThresholdDTO thresholdDTO) throws URISyntaxException {
        log.debug("REST request to save Threshold : {}", thresholdDTO);
        if (thresholdDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("threshold", "idexists", "A new threshold cannot already have an ID")).body(null);
        }
        ThresholdDTO result = thresholdService.save(thresholdDTO);
        return ResponseEntity.created(new URI("/api/thresholds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("threshold", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /thresholds : Updates an existing threshold.
     *
     * @param thresholdDTO the thresholdDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated thresholdDTO,
     * or with status 400 (Bad Request) if the thresholdDTO is not valid,
     * or with status 500 (Internal Server Error) if the thresholdDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/thresholds",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ThresholdDTO> updateThreshold(@Valid @RequestBody ThresholdDTO thresholdDTO) throws URISyntaxException {
        log.debug("REST request to update Threshold : {}", thresholdDTO);
        if (thresholdDTO.getId() == null) {
            return createThreshold(thresholdDTO);
        }
        ThresholdDTO result = thresholdService.save(thresholdDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("threshold", thresholdDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /thresholds : get all the thresholds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of thresholds in body
     */
    @RequestMapping(value = "/thresholds",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ThresholdDTO> getAllThresholds() {
        log.debug("REST request to get all Thresholds");
        return thresholdService.findAll();
    }

    /**
     * GET  /thresholds/:id : get the "id" threshold.
     *
     * @param id the id of the thresholdDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the thresholdDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/thresholds/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ThresholdDTO> getThreshold(@PathVariable Long id) {
        log.debug("REST request to get Threshold : {}", id);
        ThresholdDTO thresholdDTO = thresholdService.findOne(id);
        return Optional.ofNullable(thresholdDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /thresholds/:id : delete the "id" threshold.
     *
     * @param id the id of the thresholdDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/thresholds/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteThreshold(@PathVariable Long id) {
        log.debug("REST request to delete Threshold : {}", id);
        thresholdService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("threshold", id.toString())).build();
    }

    /**
     * SEARCH  /_search/thresholds?query=:query : search for the threshold corresponding
     * to the query.
     *
     * @param query the query of the threshold search 
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/thresholds",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ThresholdDTO> searchThresholds(@RequestParam String query) {
        log.debug("REST request to search Thresholds for query {}", query);
        return thresholdService.search(query);
    }


}
