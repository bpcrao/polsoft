package com.polmon.polsoft.web.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

@RestController
@RequestMapping("/api")
public class HeartBeatResource {

	@RequestMapping(value = "/heartbeat", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	@Timed
	public String heartbeat() {
		return "OK";
	}

}
