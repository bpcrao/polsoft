package com.polmon.polsoft.web.rest.dataloggers;

import java.math.BigDecimal;
import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.polmon.polsoft.service.dto.ChannelsData;
import com.polmon.polsoft.web.rest.InfluxDbLogger;

import javagrinko.spring.tcp.Connection;
import javagrinko.spring.tcp.TcpController;

@TcpController
public class TcpLoggerProxy {

	private final Logger log = LoggerFactory.getLogger(DataLoggerProxy.class);

	@Autowired
	private InfluxDbLogger influxDbLogger;
	
	public void receiveData(Connection connection, byte[] data) {
		log.error("Enter receiveData"+System.currentTimeMillis());
		byte[] deviceName = Arrays.copyOfRange(data, 0, 18);
		byte[] channelsData = Arrays.copyOfRange(data, 24, 312);
		StringBuilder builder = new StringBuilder();

		
		builder.append(getNormalized(data[18])).append("/").append(getNormalized(data[19])).append("/")
				.append(getNormalized(data[20])).append(" ").append(getNormalized(data[21])).append(":")
				.append(getNormalized(data[22])).append(":").append(getNormalized(data[23]));

		String dateTimeString = builder.toString();
		ChannelsData channelsDataIObject = new ChannelsData(new String(deviceName), dateTimeString);
		 BigDecimal divisor = new BigDecimal(10);
		for (int channelIndex = 0; channelIndex < channelsData.length; channelIndex = channelIndex + 2) {
			if (((channelsData[channelIndex]) & 0x80) == 0x80) {
				int x = (Integer.parseInt(((channelsData[channelIndex]) & 0xFF) + "", 10)) * 256
						+ Integer.parseInt((channelsData[channelIndex + 1] & 0xFF) + "", 10);
				x = (x - 65535)-1;
				if(x == -8999 || x == -9000 || x == -9003){
					channelsDataIObject.channelValues.put("CH" + (channelIndex + 2) / 2, (new BigDecimal(x)));
				}
				else{
					channelsDataIObject.channelValues.put("CH" + (channelIndex + 2) / 2, (new BigDecimal(x).divide(divisor)));
				}
			} else {
				int x = (Integer.parseInt(((channelsData[channelIndex]) & 0xFF) + "", 10)) * 256
						+ Integer.parseInt((channelsData[channelIndex + 1] & 0xFF) + "", 10);
				//double divisor = 10.0 double should not be used it gives innaccurate values;				
				channelsDataIObject.channelValues.put("CH" + (channelIndex + 2) / 2, (new BigDecimal(x).divide(divisor)));
			}
		}

		try {
			String result = influxDbLogger.checkandLogData(channelsDataIObject);
			connection.send(ArrayUtils.addAll(Arrays.copyOfRange(data, 0, 24), (" "+result).getBytes()));
		} catch (Exception e) {		
			connection.send(ArrayUtils.addAll(Arrays.copyOfRange(data, 0, 24), " 500".getBytes()));
		}		
		log.error("Exit receiveData"+System.currentTimeMillis());
	}

	private String getNormalized(byte b) {
		return b < 10 ? "0" + b : b + "";
	}

	public void connect(Connection connection) {
		System.out.println("New connection " + connection.getAddress().getCanonicalHostName());
	}

	public void disconnect(Connection connection) {
		System.out.println("Disconnect " + connection.getAddress().getCanonicalHostName());
	}

}
