/**
 * Data Access Objects used by WebSocket services.
 */
package com.polmon.polsoft.web.websocket.dto;
