package com.polmon.polsoft.sms;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public interface GSMProxy {

	public void sendMessageMap(Map<String, String> smsMessages);
	
}
