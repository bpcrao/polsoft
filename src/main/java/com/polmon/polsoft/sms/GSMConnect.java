package com.polmon.polsoft.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.TooManyListenersException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.google.common.base.Predicates;

import gnu.io.CommPortIdentifier;
import gnu.io.CommPortOwnershipListener;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

@Component
public class GSMConnect implements SerialPortEventListener, CommPortOwnershipListener, GSMProxy {

	final Logger log = LoggerFactory.getLogger(GSMConnect.class);
	static boolean isFinal = false;
	
	ExecutorService executor = Executors.newFixedThreadPool(1);
	ExecutorService executor1 = Executors.newFixedThreadPool(1);
	public static class GSMConnector implements Callable<Boolean> {
		private GSMConnect gsmConnect;

		public GSMConnector(GSMConnect gsmConnect) {
			this.gsmConnect = gsmConnect;
		}

		public Boolean call() throws GSMConnectException, InterruptedException {
			this.gsmConnect.log.warn("Establishing Connection");
			if (gsmConnect.init()) {
				System.out.println("Initialization Success");
				try {

					Boolean connectStatus = gsmConnect.connect();
					Thread.sleep(5000); // why do we need this?
					gsmConnect.checkStatus();
					Thread.sleep(5000);
					return connectStatus;
				} catch (GSMConnectException e) {
					gsmConnect.log.error("Exception connecting", e);
					throw new GSMConnectException("GSMConnect");
				}

			} else {
				System.out.println("Can't init this card");
				throw new GSMConnectException("Can't init this card");
			}
		}
	}

	private static String comPort = "COM3"; // This COM Port must be connect
											// with GSM Modem or your mobile
											// phone
	private CommPortIdentifier portId = null;
	private Enumeration<CommPortIdentifier> portList;
	private static InputStream inputStream = null;
	private static OutputStream outputStream = null;
	private SerialPort serialPort;
	String readBufferTrial = "";

	public boolean init() {
		portList = CommPortIdentifier.getPortIdentifiers();
		while (portList.hasMoreElements()) {
			portId = portList.nextElement();
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (portId.getName().equals(comPort)) {
					System.out.println("Got PortName");
					return true;
				}
			}
		}
		return false;
	}

	public void checkStatus() {
		send("AT+CREG?\r\n");
	}

	public static void send(String cmd) {
		try {
			// System.out.println("\nCMD : " + cmd);
			outputStream.write(cmd.getBytes());
			// outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static boolean sendMessage(String phoneNumber, String message)
	 * throws InterruptedException { isFinal = false; char quotes = '"';
	 * send("AT+CMGS=" + quotes + phoneNumber + quotes + "\r\n");
	 * Thread.sleep(2000); send(message + '\032'); while (!isFinal){ if
	 * (Thread.interrupted()) return false; }
	 * System.out.println("Message Sent"); return true; }
	 */

	public void hangup() {
		send("ATH\r\n");
	}

	public Boolean connect() throws GSMConnectException {
		if (portId != null) {
			try {

				if (serialPort == null) {
					Future<SerialPort> future = executor.submit(() -> {
						portId.addPortOwnershipListener(this);
						serialPort = (SerialPort) portId.open("COM3", 2000);
						serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
								SerialPort.PARITY_NONE);
						serialPort.disableReceiveTimeout();
						serialPort.enableReceiveThreshold(1);
						return serialPort;
					});
					// time-out
					serialPort = future.get(5, TimeUnit.SECONDS);

					inputStream = serialPort.getInputStream();
					outputStream = serialPort.getOutputStream();

					/** These are the events we want to know about */
					serialPort.addEventListener(this);
					serialPort.notifyOnDataAvailable(true);
					serialPort.notifyOnRingIndicator(true);
				}

			} catch (InterruptedException e) {
				log.error("InterruptedException creating sertial port ", e);
				e.printStackTrace();
				throw new GSMConnectException(e.getMessage() + e.getCause());
			} catch (ExecutionException e) {
				log.error("ExecutionException creating serial port, portin use or unsupported comm operation", e);
				e.printStackTrace();
				throw new GSMConnectException(e.getMessage() + e.getCause());
			} catch (TimeoutException e) {
				log.error("Timeout creating a serial port in 5 seconds", e);
				e.printStackTrace();
				throw new GSMConnectException(e.getMessage() + e.getCause());
			} catch (IOException e) {
				log.error("SMS Start", e);
				throw new GSMConnectException(e.getMessage() + e.getCause());
			} catch (TooManyListenersException e) {
				e.printStackTrace();
				throw new GSMConnectException(e.getMessage() + e.getCause());
			}

			// Register to home network of sim card
			send("ATZ\r\n");

		} else {
			throw new GSMConnectException("COM Port not found!!");
		}
		return null;
	}

	public void serialEvent(SerialPortEvent serialPortEvent) {
		switch (serialPortEvent.getEventType()) {
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
		case SerialPortEvent.DATA_AVAILABLE:

			String line = null;

			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
				while (br.ready() && (line = br.readLine()) != null) { // should
																		// this
																		// be if
																		// or
																		// while?
					System.out.println(line);
					isFinal = line.equalsIgnoreCase("OK");
					/*
					 * if(line == "ERROR") isFinal =
					 * line.equalsIgnoreCase("ERROR");
					 */
					/*
					 * switch (line.charAt(0)) { case '+': if
					 * (line.equalsIgnoreCase("+CME ERROR:")) { isFinal = true;
					 * } if (line.equalsIgnoreCase("+CMS ERROR:")) { isFinal =
					 * true; } isFinal = false; case 'B': if
					 * (line.equalsIgnoreCase("BUSY\r\n")) { isFinal = true; }
					 * isFinal = false;
					 * 
					 * case 'E': if (line.equalsIgnoreCase("ERROR\r\n")) {
					 * isFinal = true; } isFinal = false; case 'N': if
					 * (line.equalsIgnoreCase("NO ANSWER\r\n")) { isFinal =
					 * true; } if (line.equalsIgnoreCase("NO CARRIER\r\n")) {
					 * isFinal = true; } if
					 * (line.equalsIgnoreCase("NO DIALTONE\r\n")) { isFinal =
					 * true; } isFinal = false; case 'O': if
					 * (line.equalsIgnoreCase("OK\r\n")) { isFinal = true; } no
					 * break default: isFinal = false; }
					 */

				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			break;
		}
	}

	/*
	 * static boolean is_final_result(String line) { switch (line.charAt(0)) {
	 * case '+': if (line.equalsIgnoreCase("+CME ERROR:")) { return true; } if
	 * (line.equalsIgnoreCase("+CMS ERROR:")) { return true; } return false;
	 * case 'B': if (line.equalsIgnoreCase("BUSY\r\n")) { return true; } return
	 * false;
	 * 
	 * case 'E': if (line.equalsIgnoreCase("ERROR\r\n")) { return true; } return
	 * false; case 'N': if (line.equalsIgnoreCase("NO ANSWER\r\n")) { return
	 * true; } if (line.equalsIgnoreCase("NO CARRIER\r\n")) { return true; } if
	 * (line.equalsIgnoreCase("NO DIALTONE\r\n")) { return true; } return false;
	 * case 'O': if (line.equalsIgnoreCase("OK\r\n")) { return true; }
	 * 
	 * default: return false; }
	 * 
	 * }
	 */
	public void outCommand() {
		System.out.print(readBufferTrial);
	}

	public void ownershipChange(int type) {
		switch (type) {
		case CommPortOwnershipListener.PORT_UNOWNED:
			System.out.println(portId != null ? portId.getName() : "" + ": PORT_UNOWNED");
			break;
		case CommPortOwnershipListener.PORT_OWNED:
			System.out.println(portId != null ? portId.getName() : "" + ": PORT_OWNED");
			break;
		case CommPortOwnershipListener.PORT_OWNERSHIP_REQUESTED:
			System.out.println(portId != null ? portId.getName() : "" + ": PORT_INUSED");
			break;
		}

	}

	@PreDestroy
	public void closePort() {
		// close the port before Application Exit
		hangup();
		portId = null;
		serialPort.close();
		outCommand();
	}

	@PostConstruct
	public void postConstruct() {
		try {
			initializeConnection();
		} catch (GSMConnectException e) {
			log.warn("Error initializing gsm device with application", e);
		}
	}

	private void initializeConnection() throws GSMConnectException {
		if (serialPort == null) {
			Retryer<Boolean> retry = RetryerBuilder.<Boolean>newBuilder()
					.retryIfExceptionOfType(GSMConnectException.class).retryIfRuntimeException()
					.withStopStrategy(StopStrategies.stopAfterAttempt(5)).build();

			GSMConnector connector = new GSMConnector(this);
			try {
				retry.call(connector);
			} catch (ExecutionException | RetryException e) {
				e.printStackTrace();
				log.warn("Error Connecting to device", e);
			}
		}

	}

	public void sendMessageMap(Map<String, String> smsMessages) {
		log.info("SMS Start");

		executor1.submit(() -> {

			try {
				initializeConnection();
			} catch (GSMConnectException e) {
				log.warn("Error during reconnect while sending sms", e);
			}
			smsMessages.forEach((mobileNumber, text) -> {
				String[] mobileNumbers = mobileNumber.split(",");
				for (String s : mobileNumbers) {
					Future<?> future = executor.submit(() -> {
							try {
								isFinal = false;
								char quotes = '"';
								send("AT+CMGS=" + quotes + s + quotes + "\r\n");
								Thread.sleep(3000);
								send(text + '\032');
								while (!isFinal) {
									if (Thread.currentThread().isInterrupted()) {
										return 0;
									}
								}
								System.out.println("Message Sent");
								return 1;
								

							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							return 1;
						});
						try {
							future.get(20, TimeUnit.SECONDS);
						} catch (TimeoutException | InterruptedException | ExecutionException e) {
							System.out.println(e);
							future.cancel(true);
							try {
								Thread.sleep(3000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					
				}
			});

			log.info("SMS END");
		});
	}
}