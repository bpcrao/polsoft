package com.polmon.polsoft;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.influxdb.InfluxDBProperties;
import org.springframework.stereotype.Component;

import com.polmon.polsoft.config.BackupRestoreProperties;
import com.polmon.polsoft.config.PolSoftNewInstance;

@Component
@EnableConfigurationProperties({ PolSoftNewInstance.class, InfluxDBProperties.class, BackupRestoreProperties.class  })
public class InfluxDBBackupAndRestore {

	@Autowired
	private InfluxDBProperties influxDBProperties;

	@Autowired
	private PolSoftNewInstance polSoftNewInstance;
	
	@Autowired
	private BackupRestoreProperties backupRestoreProperties;


	public void backup(String backupFolderName) {
		String backUpDir = backupRestoreProperties.getBackupDir();
		ProcessBuilder pbMeta = new ProcessBuilder(polSoftNewInstance.getInflux().getHome()+"influxd", "backup", backUpDir+"\\"+backupFolderName);
		ProcessBuilder pbData = new ProcessBuilder(polSoftNewInstance.getInflux().getHome()+"influxd", "backup", "-database", influxDBProperties.getDatabase(),
				backUpDir+"\\"+backupFolderName);
		startAndWaitForProcess(pbMeta);
		startAndWaitForProcess(pbData);
	}

	public void restore(String backupFolderName) {
		String backUpDir = backupRestoreProperties.getBackupDir();
		ProcessBuilder processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getPath(),"stop");
		startAndWaitForProcess(processBuilder);		
		deleteMetaAndData(polSoftNewInstance.getInflux().getHome());
		processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getHome()+"influxd", "restore", "-metadir",polSoftNewInstance.getInflux().getHome()+"\\meta",backUpDir+"\\"+backupFolderName+"\\influx");
		startAndWaitForProcess(processBuilder);
		processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getHome()+"influxd", "restore", "-database",influxDBProperties.getDatabase(),"-datadir",polSoftNewInstance.getInflux().getHome()+"\\data",backUpDir+"\\"+backupFolderName+"\\influx");
		startAndWaitForProcess(processBuilder);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getPath(),"start");
		startAndWaitForProcess(processBuilder);
	}

	private void deleteMetaAndData(String path) {
		File meta = new File(path+"\\meta");
		File data = new File(path+"\\data");
		try {
			FileUtils.forceDelete(meta);
			FileUtils.forceDelete(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public void verify(String backupFolderName) {
		String backUpDir = backupRestoreProperties.getBackupDir();
		ProcessBuilder processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getVerifyPath(),"stop");
		startAndWaitForProcess(processBuilder);
		deleteMetaAndData(polSoftNewInstance.getInflux().getVerifyHome());
		processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getVerifyHome()+"influxd", "restore", "-metadir",polSoftNewInstance.getInflux().getVerifyHome()+"\\meta",backUpDir+"\\"+backupFolderName+"\\influx\\");
		startAndWaitForProcess(processBuilder);
		processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getVerifyHome()+"influxd", "restore", "-database",influxDBProperties.getDatabase(),"-datadir",polSoftNewInstance.getInflux().getVerifyHome()+"\\data",backUpDir+"\\"+backupFolderName+"\\influx\\");
		startAndWaitForProcess(processBuilder);
		processBuilder = new ProcessBuilder(polSoftNewInstance.getInflux().getVerifyPath(),"start");
		startAndWaitForProcess(processBuilder);
	}
	
	private void startAndWaitForProcess(ProcessBuilder processBuilder){
		try{
	        final Map<String, String> env = processBuilder.environment();
	        env.put("PATH",env.get("PATH")+";"+polSoftNewInstance.getInflux().getHome());
	        Process process = processBuilder.start();
	        final BufferedReader r = new BufferedReader(
	                new InputStreamReader(process.getErrorStream()));
	        String line = r.readLine();
	        while (line != null) {
	            System.err.println(line);
	            line = r.readLine();
	        }
	        r.close();
	        process.waitFor(5,TimeUnit.SECONDS);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void verifyRestore(String folderName) {
		this.restore(folderName);
	}
	

}