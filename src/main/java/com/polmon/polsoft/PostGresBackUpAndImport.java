package com.polmon.polsoft;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import com.polmon.polsoft.config.PolSoftNewInstance;
import com.polmon.polsoft.config.PostGresBackUpAndRestoreProperties;
@Component
@EnableConfigurationProperties({PostGresBackUpAndRestoreProperties.class,PolSoftNewInstance.class})
public class PostGresBackUpAndImport {
	
	@Autowired
	private PostGresBackUpAndRestoreProperties backUpAndRestoreProperties;
	
	@Autowired
	private PolSoftNewInstance polSoftNewInstance;
	
	@Autowired
	private StandardPBEStringEncryptor stringEncryptor;
	
	public  void export(String fileName) throws IOException, InterruptedException {
	    Process p;
	    ProcessBuilder pb;
	    pb = new ProcessBuilder(
	    		backUpAndRestoreProperties.getDump().getExepath(),
	            "--host", backUpAndRestoreProperties.getDump().getHost(),
	            "--port", backUpAndRestoreProperties.getDump().getPort(),
	            "--username", backUpAndRestoreProperties.getDump().getUsername(),
	            "--no-password",
	            "--format", "custom",
	            "--blobs",
	            "--verbose", "--file", fileName, backUpAndRestoreProperties.getDump().getDatabase());
	    try {
	        final Map<String, String> env = pb.environment();
	        env.put("PGPASSWORD", stringEncryptor.decrypt(backUpAndRestoreProperties.getDump().getPassword()));	        
	        p = pb.start();
	        final BufferedReader r = new BufferedReader(
	                new InputStreamReader(p.getErrorStream()));
	        String line = r.readLine();
	        while (line != null) {
	            System.err.println(line);
	            line = r.readLine();
	        }
	        r.close();
	        p.waitFor();
	        System.out.println(p.exitValue());

	    } catch (IOException | InterruptedException e) {
	        System.out.println(e.getMessage());
	    	throw e;
	    }
	}
	
	public void importDB(String folderName) throws IOException, InterruptedException {
	    Process p;
	    ProcessBuilder pb;
	    pb = new ProcessBuilder(
	    		backUpAndRestoreProperties.getRestore().getExepath(),
	            "--host", backUpAndRestoreProperties.getRestore().getHost(),
	            "--port", backUpAndRestoreProperties.getRestore().getPort(),
	            "--username", backUpAndRestoreProperties.getRestore().getUsername(),
	            "--no-password",
	            "--clean",
	            "--dbname",backUpAndRestoreProperties.getRestore().getDatabase(),folderName);
	    try {
	        final Map<String, String> env = pb.environment();
	        env.put("PGPASSWORD", stringEncryptor.decrypt(backUpAndRestoreProperties.getRestore().getPassword()));
	        p = pb.start();
	        final BufferedReader r = new BufferedReader(
	                new InputStreamReader(p.getErrorStream()));
	        String line = r.readLine();
	        while (line != null) {
	            System.err.println(line);
	            line = r.readLine();
	        }
	        r.close();
	        p.waitFor();
	        System.out.println(p.exitValue());

	    } catch (IOException | InterruptedException e) {
	        System.out.println(e.getMessage());
	    	throw e;
	    }
	}
	
	public void restoreDB(String folderName) throws IOException, InterruptedException {
	    Process p;
	    ProcessBuilder pb;
	    pb = new ProcessBuilder(
	    		backUpAndRestoreProperties.getRestore().getExepath(),
	            "--host", backUpAndRestoreProperties.getRestore().getHost(),
	            "--port", backUpAndRestoreProperties.getRestore().getPort(),
	            "--username", backUpAndRestoreProperties.getRestore().getUsername(),
	            "--no-password",
	            "--clean",
	            "--dbname",backUpAndRestoreProperties.getDump().getDatabase(),folderName);
	    try {
	        final Map<String, String> env = pb.environment();
	        env.put("PGPASSWORD", stringEncryptor.decrypt(backUpAndRestoreProperties.getRestore().getPassword()));
	        p = pb.start();
	        final BufferedReader r = new BufferedReader(
	                new InputStreamReader(p.getErrorStream()));
	        String line = r.readLine();
	        while (line != null) {
	            System.err.println(line);
	            line = r.readLine();
	        }
	        r.close();
	        p.waitFor();
	        System.out.println(p.exitValue());

	    } catch (IOException | InterruptedException e) {
	        System.out.println(e.getMessage());
	    	throw e;
	    }
	}
	public void startnewInstance() throws IOException, InterruptedException {
	    Process p;
	    ProcessBuilder pb;
	    pb = new ProcessBuilder(polSoftNewInstance.getService().getPath(),"restart");
	    try {
	        p = pb.start();
	        final BufferedReader r = new BufferedReader(
	                new InputStreamReader(p.getErrorStream()));
	        String line = r.readLine();
	        while (line != null) {
	            System.err.println(line);
	            line = r.readLine();
	        }
	        r.close();
	        p.waitFor();
	        System.out.println(p.exitValue());

	    } catch (IOException  e) {
	        System.out.println(e.getMessage());
	        throw e;
	    }
	}

	public void verifyImportDB(String string) throws IOException, InterruptedException {
		this.importDB(string);
		
	}
	
	
}
