# Polsoft

## Development

#Prerequisites

1. need to have postgres installed with 'polsoftdb' Database & user 'postgress' with password 'root'
2. need to have Java 8 installed and JAVA_HOME set.
3. need to install JCE8 extension for Java (http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html)
refer this [link](Link http://suhothayan.blogspot.in/2012/05/how-to-install-java-cryptography.html) so the files need to be copied to a location like this
C:\Program Files\Java\jdk1.8.0_111\jre\lib\security


#  Softwares used 
1. Angular 1.5.8
2. Spring 4
3. Postgress 9.6.1-1 [More on replication and backp](./postgress_replication_backup.md)
4. Influx DB 1.2  [More about this ](./readme_locationdata.md)
5. PG Admin for postgress db
6. Gulp, NPM and Maven
7. HTML, CSS


Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools (like
[Bower][] and [BrowserSync][]). You will only need to run this command when dependencies change in package.json.

    npm install

We use [Gulp][] as our build system. Install the Gulp command-line tool globally with:

    npm install -g gulp

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    gulp

Bower is used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in `bower.json`. You can also run `bower install` to manage dependencies.


## Building for production

To optimize the Polsoft client for production, run:

    ./mvnw -Pprod clean package

This will concatenate and minify CSS and JavaScript files. It will also modify `index.html` so it references
these new files.

To ensure everything worked, run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

## Testing

Unit tests are run by [Karma][] and written with [Jasmine][]. They're located in `src/test/javascript/` and can be run with:

    gulp test



## Continuous Integration

To setup this project in Jenkins, use the following configuration:

* Project name: `Polsoft`
* Source Code Management
    * Git Repository: `git@github.com:xxxx/Polsoft.git`
    * Branches to build: `*/master`
    * Additional Behaviours: `Wipe out repository & force clone`
* Build Triggers
    * Poll SCM / Schedule: `H/5 * * * *`
* Build
    * Invoke Maven / Tasks: `-Pprod clean package`
* Post-build Actions
    * Publish JUnit test result report / Test Report XMLs: `build/test-results/*.xml`

[JHipster]: https://jhipster.github.io/
[Node.js]: https://nodejs.org/
[Bower]: http://bower.io/
[Gulp]: http://gulpjs.com/
[BrowserSync]: http://www.browsersync.io/
[Karma]: http://karma-runner.github.io/
[Jasmine]: http://jasmine.github.io/2.0/introduction.html
[Protractor]: https://angular.github.io/protractor/



##REST API's 
**
Permission Mapping 
* 32: MANAGE ALARM     
* 64: APPLY_THRESHOLD
* 128: SHOW_LOCATION (This must be given if the Location has to be shown to the user and for add user this must be given) 

* Get Permissions on A Resource

http://127.0.0.1:8080/api/permissions?resource=Location&resourceId=1313


```
#!js

{
  "resource": {
    "type": "com.polmon.polsoft.domain.Location",
    "identifier": 1313
  },
  "permissions": {
    "admin": [
      {
        "permission": 32,
        "user": "admin",
        "granted": true
      },
      {
        "permission": 64,
        "user": "admin",
        "granted": true
      },
      {
        "permission": 128,
        "user": "admin",
        "granted": true
      },
      {
        "permission": 16,
        "user": "admin",
        "granted": true
      }
    ]
  }
}
```


* Add permissions on a resource
http://127.0.0.1:8080/api/permissions?resource=Location&resourceId=1315&principal=user
permissions in request body 

```
#!js

{ "permissions": [ { "permission": 32,"granting": true}, { "permission": 64,"granting": true },{"permission": 128,"granting": true} ] }

```

* Update User Permeations
use the previous API for this too , either you update granting to true or granting to false.


* Get channel linked for a location 
GET  http://127.0.0.1:8080/api/locations/1313


```
#!js

{
  "id": 1313,
  "name": "RootLocation",
  "leaf": true,
  "parentId": null,
  "label": "RootLocation",
  "dchannelId": 1008
}
dchannelID is the llinked Channel
```

* Link a channel to the Location 
URL:http://127.0.0.1:8080/api/locations?cacheBuster=1492620787201
Method:PUT
LocationDTO  as body parameter

```
#!js

{
  "children": [
    {}
  ],
  "dchannelId": 0,
  "id": 0,
  "label": "string",
  "leaf": true,
  "name": "string",
  "parentId": 0
}
```

*Update an alarm
Request URL:http://127.0.0.1:8080/api/channel-alarms
Request Method:PUT

```
channelAlarmDTO as request body
#!js
{id: 1152, low: 100, high: 100, lowLow: 0, highHigh: 567, dchannelId: 1011}
```

Get Alarms on A channel


```
#!js

Request URL:http://127.0.0.1:8080/api/dchannels/1008
Request Method:GET

Response
{
  "id" : 1008,
  "name" : "ch19",
  "unit" : null,
  "channeldata" : "{\"unit\":\"Pressure\",\"instrument\":{\"mulfactor\":\"345\"},\"maLow\":\"3453\",\"alarmLow\":\"34534\",\"mvLow\":\"53453\"}",
  "channelAlarms" : [ {
    "id" : 1153,
    "low" : 100.00,
    "high" : 100.00,
    "lowLow" : 0.00,
    "highHigh" : 100.00,
    "dchannelId" : 1008
  } ],
  "deviceId" : 1007
}
```

##To get all devices


```
#!js
Request URL:http://127.0.0.1:8080/api/devices?cacheBuster=1492885770750
Request Method:GET

{
  "id" : 1008,
  "name" : "ch19",
  "unit" : null,
  "channeldata" : "{\"unit\":\"Pressure\",\"instrument\":{\"mulfactor\":\"345\"},\"maLow\":\"3453\",\"alarmLow\":\"34534\",\"mvLow\":\"53453\"}",
  "channelAlarms" : [ {
    "id" : 1153,
    "low" : 100.00,
    "high" : 100.00,
    "lowLow" : 0.00,
    "highHigh" : 100.00,
    "dchannelId" : 1008
  } ],
  "deviceId" : 1007
}

```

## To get channels on a Device configured 
GET 
'http://127.0.0.1:8080/api/dchannels/device/{deviceId}'


```
#!js

[
  {
    "id": 1008,
    "name": "ch19",
    "unit": null,
    "channeldata": "{\"unit\":\"Pressure\",\"instrument\":{\"mulfactor\":\"345\"},\"maLow\":\"3453\",\"alarmLow\":\"34534\",\"mvLow\":\"53453\"}",
    "channelAlarms": [
      {
        "id": 1153,
        "low": 100,
        "high": 100,
        "lowLow": 0,
        "highHigh": 100,
        "dchannelId": 1008
      }
    ],
    "deviceId": 1007
  },
  {
    "id": 1009,
    "name": "ch88",
    "unit": null,
    "channeldata": "{\"unit\":\"Pressure\",\"instrument\":{\"mulfactor\":\"345\"},\"maLow\":\"3453\",\"alarmLow\":\"34534\",\"mvLow\":\"53453\"}",
    "channelAlarms": [
      {
        "id": 1176,
        "low": 100,
        "high": 100,
        "lowLow": 0,
        "highHigh": 100,
        "dchannelId": 1009
      }
    ],
    "deviceId": 1007
  }
]
```