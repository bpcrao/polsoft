##SSL Certificate for HTTPS
created and checked in the certificate in  src/main/resources/piscada.p12, so we can use and install it directly but if we want to regenerate we need to do the below.



C:\Program Files (x86)\GnuWin32\bin>keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore piscada1.p12 -validity 3650 -ext SAN=dns:localhost,ip:127.0.0.1
Enter keystore password:
Re-enter new password:
What is your first and last name?
  [Unknown]:  polmon
What is the name of your organizational unit?
  [Unknown]:  polmon
What is the name of your organization?
  [Unknown]:  polmon
What is the name of your City or Locality?
  [Unknown]:  HYd
What is the name of your State or Province?
  [Unknown]:  telangana
What is the two-letter country code for this unit?
  [Unknown]:  IN
Is CN=polmon, OU=polmon, O=polmon, L=HYd, ST=telangana, C=IN correct?
  [no]:  yes

###the above step generates a piscada1.p12  the same is configured in application.properties


server:
  compression:
    enabled: true
    mime-types: text/html,text/css,application/javascript,application/json
  port: 8443
  tomcat:
    remote-ip-header: x-forwarded-for
    protocol-header: x-forwarded-proto
  ssl:
    key-store: classpath:piscada.p12
    key-store-password: 123456
    key-store-type: PKCS12
    key-alias: tomcat
    enabled: true
 ### 
 
 Import the ssl certificate in chrome and the  os 
 ####Chrome its in setings, search ssl
 ####Windows right click and install( Make sure we select Trusted Security) 


##InfluxDB properties

influxdb:
        url: http://localhost:8086
        username: admin
        password: LnG5/Cp58wcX96ss7/dhFA==
        database: polsoftinflux
        retention-policy: autogen
		
https://github.com/saschat/angular-influxdb

## Creating Admin user in InfluxDB
	Create admin user using command: CREATE USER "admin" WITH PASSWORD 'root' WITH ALL PRIVILEGES

## Enabling Security in InfluxDB Configuration file
	change auth-enabled = true in http section

##Eanbling Admin port in InfluxDB
uncomment the below lines admin section
	 
	 enabled = true	 
	 bind-address = ":8083"

Measurements (single measurement)
 Data

Tag 
- Location  : locationId
- Path      : parentID/FirstChildId/
- Unit      : Temperature  Humidity Pressure

##To Query a perticular location by ID 

"Http GET using 
http://localhost:8086/query?db=polsoftinflux&q=select+*+from+/Data/%20where%20location=%271293%27

Query = select  * from data 0where location=71293 

##To Query a all sub locations by ID (when clicked on Parent)

http://localhost:8086/query?db=polsoftinflux&q=select+*+from+data/%20where%20path=~ /129*/

Query = select  * from data where path=~ /1291.*/        "1291 is parent path location" 


## To Query mulltiple locations 
http://localhost:8086/query?db=polsoftinflux&q=select *from data where (location='1376' or location='1378')
and time > '2017-05-27T13:55:08.672Z' and time < '2017-05-27T14:55:08.672Z'

##TO Query by date

http://localhost:8086/query?db=polsoftinflux&q=select+*+from+data%20where%20path=~%20/129*/%20AND%20time%20%3E=%20%272017-04-30T02:30:00Z%27%20AND%20time%20%3C=%20now()

select * from data where path=~ /129*/ AND time>=2017-04-30T02:30:00Z AND time<=now()


## To simulate a DL trigger invoke the CURL below  
- the device should be mapped with DL122M-X-YYMM-XXXX
- Date=19/12/16,Time=15:40:00 ( for Data and time)
- CH will map the ch of device.

C:\code\polsoft\polsoft>curl -i -XPOST http://127.0.0.1:8080/api/write --data-binary DL122M-X-YYMM-XXXX,Date=19/12/16,Time=15:40:00,CH1=+41.56,CH2=+41.56,CH3=ERR1,CH4=ERR2,CH5=+41.56,CH6=+41.56,CH7=+41.56,CH8=+41.56,CH9=+41.56,CH10=+41.56,CH11=+41.56,CH12=+41.56,CH13=+41.56,CH14=+41.56,CH15=+41.56,CH16=+41.56,CH17=+41.56,CH18=+41.56,CH19=+41.56,CH20=+41.56,CH21=+41.56,CH22=+41.56,CH23=+41.56,CH24=+41.56,CH25=+41.56,CH26=+41.56,CH27=+41.56,CH28=+41.56,CH29=+41.56,CH30=+41.56,CH31=+41.56,CH32=+41.56,CH33=+41.56,CH34=+41.56,CH35=+41.56,CH36=+41.56,CH37=+41.56,CH38=+41.56,CH39=+41.56,CH40=+41.56,CH41=+41.56,CH42=+41.56,CH43=+41.56,CH44=+41.56,CH45=+41.56,CH46=+41.56,CH47=+41.56,CH48=+41.56,CH49=+41.56,CH50=+41.56,CH51=+41.56,CH52=+41.56,CH53=+41.56,CH54=+41.56,CH55=+41.56,CH56=+41.56,CH57=+41.56,CH58=+41.56,CH59=+41.56,CH60=+41.56,CH61=+41.56,CH62=+41.56,CH63=+41.56,CH64=+41.56,CH65=+41.56,CH66=+41.56,CH67=+41.56,CH68=+41.56,CH69=+41.56,CH70=+41.56,CH71=+41.56,CH72=+41.56,CH73=+41.56,CH74=+41.56,CH75=+41.56,CH76=+41.56,CH77=+41.56,CH78=+41.56,CH79=+41.56,CH80=+41.56,CH81=+41.56,CH82=+41.56,CH83=+41.56,CH84=+41.56,CH85=+41.56,CH86=+41.56,CH87=+41.56,CH88=+41.56,CH89=+41.56,CH90=+41.56,CH91=+41.56,CH92=+41.56,CH93=+41.56,CH94=+41.56,CH95=+41.56,CH96=+41.56,CH97=+41.56,CH98=+41.56,CH99=+41.56,CH100=+41.56,CH101=+41.56,CH102=+41.56,CH103=+41.56,CH104=+41.56,CH105=+41.56,CH106=+41.56,CH107=+41.56,CH108=+41.56,CH109=+41.56,CH110=+41.56,CH111=+41.56,CH112=+41.56,CH113=+41.56,CH114=+41.56,CH115=+41.56,CH116=+41.56,CH117=+41.56,CH118=+41.56,CH119=+41.56,CH120=+41.56,CH121=+41.56,CH122=+41.56,CH123=+41.56,CH124=+41.56,CH125=+41.56,CH126=+41.56,CH127=+41.56,CH128=+41.56,CH129=+41.56,CH130=+41.56,CH131=+41.56,CH132=+41.56,CH133=+41.56,CH134=+41.56,CH135=+41.56,CH136=+41.56,CH137=+41.56,CH138=+41.56,CH139=+41.56,CH140=+41.56,CH141=+41.56,CH142=+41.56,CH143=+41.56,CH144=+41.56,MSG1=254,MSG2=254,MSG3=254,MSG4=254,MSG5=254,MSG6=254,MSG7=254,MSG8=254,MSG9=254